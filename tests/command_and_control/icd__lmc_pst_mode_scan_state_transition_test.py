# pylint:disable=missing-module-docstring,missing-function-docstring,missing-class-docstring,too-many-arguments


import allure
import pytest
import tango
from pytest_check.check_functions import check_func
from ska_control_model import ObsMode, ObsState, PstProcessingMode

from ska_low_csp_test.cbf.helpers import (
    cbf_station_beam_delay_poly_uri,
    cbf_timing_beam_delay_poly_uri,
    cbf_timing_beam_jones_uri,
)
from ska_low_csp_test.domain.channels import coarse_channel_range_center_frequency, coarse_channel_range_total_bandwidth
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.lmc.devices import LowCspCapabilityPst
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest, SystemUnderTest
from ska_low_csp_test.synchronisation import wait_for_condition

PST_BEAM_ID = 1


@pytest.fixture(name="subarray_id")
def fxt_subarray_id() -> int:
    return 3


@pytest.fixture(name="assign_resources_json")
def fxt_assign_resources_json(subarray_id: int) -> LowCspAssignResourcesSchema:
    """
    Fixture that creates a default JSON payload for the AssignResources command.
    """
    return {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.2",
        "common": {
            "subarray_id": subarray_id,
        },
        "lowcbf": {},
        "pst": {
            "beams_id": [PST_BEAM_ID],
        },
    }


@pytest.fixture(name="configure_json")
def fxt_configure_json(
    subarray_id: int,
    config_id: str,
    pst_firmware_version: str,
    eb_id: str,
) -> LowCspConfigureSchema:
    """
    Fixture that creates a default JSON payload for the Configure command including PST beam configuration.
    """
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "science period 23",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": subarray_id,
            "eb_id": eb_id,
        },
        "lowcbf": {
            "stations": {
                "stns": [(405, 1), (393, 1), (315, 1), (261, 1), (255, 1), (243, 1)],
                "stn_beams": [
                    {
                        "beam_id": 1,
                        "freq_ids": [397, 398, 399, 400, 401, 402, 403, 404],
                        "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, station_beam_id=1),
                    },
                ],
            },
            "timing_beams": {
                "fsp": {"firmware": pst_firmware_version, "fsp_ids": [1]},
                "beams": [
                    {
                        "pst_beam_id": PST_BEAM_ID,
                        "stn_beam_id": 1,
                        "stn_weights": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                        "delay_poly": cbf_timing_beam_delay_poly_uri(subarray_id, station_beam_id=1, pst_beam_id=PST_BEAM_ID),
                        "jones": cbf_timing_beam_jones_uri(),
                    },
                ],
            },
        },
        "pst": {
            "beams": [
                {
                    "beam_id": PST_BEAM_ID,
                    "scan": {
                        "activation_time": "2022-01-19T23:07:45Z",
                        "bits_per_sample": 32,
                        "num_of_polarizations": 2,
                        "udp_nsamp": 32,
                        "wt_nsamp": 32,
                        "udp_nchan": 24,
                        "num_frequency_channels": 8 * 216,
                        "centre_frequency": coarse_channel_range_center_frequency(397, 404),
                        "total_bandwidth": coarse_channel_range_total_bandwidth(397, 404),
                        "observation_mode": "VOLTAGE_RECORDER",
                        "observer_id": "jdoe",
                        "project_id": "project1",
                        "pointing_id": "pointing1",
                        "source": "J1921+2153",
                        "itrf": [5109360.133, 2006852.586, -3238948.127],
                        "receiver_id": "receiver3",
                        "feed_polarization": "LIN",
                        "feed_handedness": 1,
                        "feed_angle": 1.234,
                        "feed_tracking_mode": "FA",
                        "feed_position_angle": 10.0,
                        "oversampling_ratio": [4, 3],
                        "coordinates": {"equinox": 2000.0, "ra": "19:21:44.815", "dec": "21:53:02.400"},
                        "max_scan_length": 30.0,
                        "subint_duration": 30.0,
                        "receptors": ["receptor1", "receptor2"],
                        "receptor_weights": [0.4, 0.6],
                        "num_channelization_stages": 1,
                        "channelization_stages": [],
                    },
                },
            ],
        },
    }


@pytest.fixture(autouse=True, scope="module")
def revert_system_under_test_state(system_under_test: SystemUnderTest):
    yield
    system_under_test.low_csp_devices.controller().on()


@check_func
@allure.step("Verify that the PST beam {beam_id} obsState is {expected}")
def verify_pst_capability_obs_state(pst_capability: LowCspCapabilityPst, beam_id: int, expected: ObsState):
    """Verify that the ``obsState`` of the PST beam matches the expected observing state."""
    assert pst_capability.get_pst_beam_obs_state(beam_id) == expected


@check_func
@allure.step("Verify that the PST beam {beam_id} processing mode is {expected}")
def verify_pst_capability_processing_mode(pst_capability: LowCspCapabilityPst, beam_id: int, expected: PstProcessingMode):
    """Verify that the ``processingMode`` of the PST beam matches the expected processing mode."""
    try:
        wait_for_condition(lambda: pst_capability.get_pst_processing_mode(beam_id) == expected, 10)
        assert pst_capability.get_pst_processing_mode(beam_id) == expected
    except TimeoutError:
        assert False, f"Pst is in {pst_capability.get_pst_processing_mode(beam_id)} instead of {expected}"


@check_func
@allure.step("Verify that the PST beam {beam_id} state is {expected}")
def verify_pst_capability_state(pst_capability: LowCspCapabilityPst, beam_id: int, expected: tango.DevState):
    """Verify that the ``state`` of the PST beam matches the expected state."""
    try:
        wait_for_condition(lambda: pst_capability.get_pst_beam_state(beam_id) == expected.name, 10)
        assert pst_capability.get_pst_beam_state(beam_id) == expected.name
    except TimeoutError:
        assert False, f"Pst is in {pst_capability.get_pst_beam_state(beam_id)} instead of {expected}"


@test_case(
    title="ICD LMC to PST scan state transition",
    suite=TestSuite.COMMAND_AND_CONTROL,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1126978?projectId=335",
    xray_id="CLT-23",
)
def test_lmc_pst_mode_scan_state_transition(
    subarray_under_test: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
    scan_json: LowCspScanSchema,
    pst_capability: LowCspCapabilityPst,
    system_under_test: SystemUnderTest,
):
    subarray_under_test.verify_obs_state(ObsState.EMPTY)
    subarray_under_test.verify_obs_mode(ObsMode.IDLE)
    verify_pst_capability_state(pst_capability, PST_BEAM_ID, tango.DevState.ON)

    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.verify_obs_state(ObsState.IDLE)
    subarray_under_test.verify_obs_state_transitions(ObsState.RESOURCING, ObsState.IDLE)
    subarray_under_test.verify_obs_mode(ObsMode.IDLE)
    verify_pst_capability_obs_state(pst_capability, PST_BEAM_ID, ObsState.IDLE)

    subarray_under_test.configure(configure_json)
    subarray_under_test.verify_obs_state(ObsState.READY)
    subarray_under_test.verify_obs_state_transitions(ObsState.CONFIGURING, ObsState.READY)
    subarray_under_test.verify_obs_mode(ObsMode.PULSAR_TIMING)
    verify_pst_capability_obs_state(pst_capability, PST_BEAM_ID, ObsState.READY)
    verify_pst_capability_processing_mode(pst_capability, PST_BEAM_ID, PstProcessingMode.VOLTAGE_RECORDER)

    subarray_under_test.scan(scan_json)
    subarray_under_test.verify_obs_state(ObsState.SCANNING)
    subarray_under_test.verify_obs_state_transitions(ObsState.SCANNING)
    subarray_under_test.verify_obs_mode(ObsMode.PULSAR_TIMING)
    verify_pst_capability_obs_state(pst_capability, PST_BEAM_ID, ObsState.SCANNING)

    subarray_under_test.end_scan()
    subarray_under_test.verify_obs_state(ObsState.READY)
    subarray_under_test.verify_obs_state_transitions(ObsState.READY)
    subarray_under_test.verify_obs_mode(ObsMode.PULSAR_TIMING)
    verify_pst_capability_obs_state(pst_capability, PST_BEAM_ID, ObsState.READY)
    verify_pst_capability_processing_mode(pst_capability, PST_BEAM_ID, PstProcessingMode.VOLTAGE_RECORDER)

    subarray_under_test.end()
    subarray_under_test.verify_obs_state(ObsState.IDLE)
    subarray_under_test.verify_obs_state_transitions(ObsState.IDLE)
    subarray_under_test.verify_obs_mode(ObsMode.IDLE)
    verify_pst_capability_obs_state(pst_capability, PST_BEAM_ID, ObsState.IDLE)
    verify_pst_capability_processing_mode(pst_capability, PST_BEAM_ID, PstProcessingMode.IDLE)

    system_under_test.low_csp_devices.controller().off()
    verify_pst_capability_state(pst_capability, PST_BEAM_ID, tango.DevState.OFF)
