# pylint:disable=missing-module-docstring,missing-function-docstring,missing-class-docstring,too-many-arguments

import pytest
from ska_control_model import ObsMode, ObsState

from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest

SUBARRAY_ID = 3


@pytest.fixture()
def subarray_id() -> int:
    return SUBARRAY_ID


@test_case(
    title="ICD LMC scan state transition",
    suite=TestSuite.COMMAND_AND_CONTROL,
    xray_id="CLT-21",
    jama_url="https://skaoffice.jamacloud.com/perspective.req?docId=1122107&projectId=335",
)
def test_lmc_scan_state_transition(
    subarray_under_test: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
    scan_json: LowCspScanSchema,
):
    subarray_under_test.verify_obs_state(ObsState.EMPTY)
    subarray_under_test.verify_obs_mode(ObsMode.IDLE)

    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.verify_obs_state(ObsState.IDLE)
    subarray_under_test.verify_obs_state_transitions(ObsState.RESOURCING, ObsState.IDLE)
    subarray_under_test.verify_obs_mode(ObsMode.IDLE)

    subarray_under_test.configure(configure_json)
    subarray_under_test.verify_obs_state(ObsState.READY)
    subarray_under_test.verify_obs_state_transitions(ObsState.CONFIGURING, ObsState.READY)
    subarray_under_test.verify_obs_mode(ObsMode.IMAGING)

    subarray_under_test.scan(scan_json)
    subarray_under_test.verify_obs_state(ObsState.SCANNING)
    subarray_under_test.verify_obs_state_transitions(ObsState.SCANNING)
    subarray_under_test.verify_obs_mode(ObsMode.IMAGING)

    subarray_under_test.end_scan()
    subarray_under_test.verify_obs_state(ObsState.READY)
    subarray_under_test.verify_obs_state_transitions(ObsState.READY)
    subarray_under_test.verify_obs_mode(ObsMode.IMAGING)

    subarray_under_test.end()
    subarray_under_test.verify_obs_state(ObsState.IDLE)
    subarray_under_test.verify_obs_state_transitions(ObsState.IDLE)
    subarray_under_test.verify_obs_mode(ObsMode.IDLE)

    subarray_under_test.release_all_resources()
    subarray_under_test.verify_obs_state(ObsState.EMPTY)
    subarray_under_test.verify_obs_state_transitions(ObsState.RESOURCING, ObsState.EMPTY)
    subarray_under_test.verify_obs_mode(ObsMode.IDLE)
