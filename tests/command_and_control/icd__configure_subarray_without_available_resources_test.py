# pylint:disable=missing-module-docstring
# pylint:disable=missing-function-docstring

import allure
import pytest
from ska_control_model import AdminMode, ObsState

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest


@pytest.fixture(name="subarray_id")
def fxt_subarray_id():
    return 1


@pytest.fixture(autouse=True)
def move_processors_to_engineering_mode(low_cbf_devices: LowCbfDevices):
    processors = low_cbf_devices.all_processors()
    admin_mode_cache = {}

    for processor in processors:
        admin_mode_cache[processor.fqdn] = processor.admin_mode
        with allure.step(f"Set adminMode of {processor.fqdn} to ENGINEERING"):
            processor.admin_mode = AdminMode.ENGINEERING

    yield

    for processor in processors:
        admin_mode = admin_mode_cache[processor.fqdn]
        with allure.step(f"Revert adminMode of {processor.fqdn} to {admin_mode.name}"):
            processor.admin_mode = admin_mode


@test_case(
    title="Configure subarray without available resources",
    suite=TestSuite.COMMAND_AND_CONTROL,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1137469?projectId=335",
    xray_id="CLT-87",
)
def test_configure_subarray_without_available_resources(
    subarray_under_test: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
):
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.verify_obs_state(ObsState.IDLE)

    with pytest.raises(RuntimeError, match="Long-running command 'Configure' finished with result FAILED"):
        subarray_under_test.configure(configure_json)

    subarray_under_test.verify_obs_state(ObsState.FAULT)
