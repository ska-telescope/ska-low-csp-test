# pylint:disable=missing-module-docstring,missing-function-docstring,missing-class-docstring

from typing import Protocol

import allure
import pytest

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest, SystemUnderTest

STATION_BEAM_ID = 1


class GenerateAssignResourcesJson(Protocol):
    def __call__(self, subarray_id: int) -> LowCspAssignResourcesSchema:
        ...


class GenerateConfigureJson(Protocol):
    def __call__(
        self,
        subarray_id: int,
        station_ids: list[tuple[int, int]],
        fsp_ids: list[int],
        vis_firmware: str,
    ) -> LowCspConfigureSchema:
        ...


@pytest.fixture(name="subarray_1")
def fxt_subarray_1(system_under_test: SystemUnderTest):
    with system_under_test.subarray(1) as subarray:
        subarray.reset()
        yield subarray
        subarray.reset()


@pytest.fixture(name="subarray_2")
def fxt_subarray_2(system_under_test: SystemUnderTest):
    with system_under_test.subarray(2) as subarray:
        subarray.reset()
        yield subarray
        subarray.reset()


@pytest.fixture(name="generate_assign_resources_json")
def fxt_generate_assign_resources_json() -> GenerateAssignResourcesJson:
    def generate(subarray_id: int) -> LowCspAssignResourcesSchema:
        return {
            "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.2",
            "common": {
                "subarray_id": subarray_id,
            },
            "lowcbf": {},
        }

    return generate


@pytest.fixture(name="generate_configure_json")
def fxt_generate_configure_json() -> GenerateConfigureJson:
    def generate(
        subarray_id: int,
        station_ids: list[tuple[int, int]],
        fsp_ids: list[int],
        vis_firmware: str,
    ) -> LowCspConfigureSchema:
        return {
            "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
            "subarray": {
                "subarray_name": "ITC.L.AA0.5.NB Configure without available resources",
            },
            "common": {
                "config_id": "",
                "subarray_id": subarray_id,
                "eb_id": "eb-x321-20240111-10012",
            },
            "lowcbf": {
                "stations": {
                    "stns": station_ids,
                    "stn_beams": [
                        {
                            "beam_id": STATION_BEAM_ID,
                            "freq_ids": list(range(141, 149)),
                            "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, STATION_BEAM_ID),
                        },
                    ],
                },
                "vis": {
                    "fsp": {
                        "firmware": vis_firmware,
                        "fsp_ids": fsp_ids,
                    },
                    "stn_beams": [
                        {
                            "stn_beam_id": 1,
                            "host": [(0, "192.168.2.2")],
                            "mac": [(0, "0c-42-a1-9c-a2-1b")],
                            "port": [(0, 20000, 1)],
                            "integration_ms": 849,
                        },
                    ],
                },
            },
        }

    return generate


@test_case(
    title="Configure subarray without available resources",
    suite=TestSuite.COMMAND_AND_CONTROL,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1137469?projectId=335",
    xray_id="CLT-87",
)
def test_configure_subarray_without_available_resources(  # pylint: disable=too-many-arguments
    subarray_1: SubarrayUnderTest,
    subarray_2: SubarrayUnderTest,
    generate_assign_resources_json: GenerateAssignResourcesJson,
    generate_configure_json: GenerateConfigureJson,
    vis_firmware_version: str,
    vis_alt_firmware_version: str,
):
    with allure.step("Assign resources and configure on subarray 1"):
        subarray_1.assign_resources(generate_assign_resources_json(subarray_id=1))
        subarray_1.configure(
            generate_configure_json(
                subarray_id=1,
                station_ids=[(345, 1), (350, 1)],
                fsp_ids=[1],
                vis_firmware=vis_firmware_version,
            )
        )

    with allure.step("Assign resources and configure on subarray 2"):
        subarray_2.assign_resources(generate_assign_resources_json(subarray_id=2))
        with pytest.raises(RuntimeError, match="Long-running command 'Configure' finished with result FAILED"):
            subarray_2.configure(
                generate_configure_json(
                    subarray_id=2,
                    station_ids=[(352, 1), (431, 1)],
                    fsp_ids=[1],
                    vis_firmware=vis_alt_firmware_version,
                )
            )
