# pylint:disable=missing-module-docstring,missing-function-docstring

from datetime import datetime

import allure
import numpy
import pandas as pd
import pytest
import xarray as xr
from pytest_check import check

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cbf.visibilities import (
    get_visibility_data,
    get_visibility_metadata,
    time_averaged_visibilities,
    unpack_pcap_file,
)
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema
from ska_low_csp_test.domain.channels import coarse_channel_frequency_range
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.domain.spead import LowCbfVisibilitySpeadItem
from ska_low_csp_test.reporting import RecordTestEvidence, TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator, VisibilityOutputCapture

FREQUENCY_IDS = list(range(208, 304))
FSP_IDS = [1]
SCAN_ID = 6789
STATIONS = [(345, 1), (350, 1), (352, 1), (431, 1)]
STATION_BEAM_ID = 1
SUBARRAY_ID = 2


@pytest.fixture(name="subarray_id")
def override_subarray_id() -> int:
    return SUBARRAY_ID


@pytest.fixture(name="configure_json")
def override_config_json(
    subarray_id: int,
    config_id: str,
    vis_firmware_version: str,
    visibility_output_capture: VisibilityOutputCapture,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "ITC.L.AA0.5.ICD CBF to SDP SPEAD ouput inspection",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": subarray_id,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, STATION_BEAM_ID),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": FSP_IDS,
                },
                "stn_beams": [
                    {
                        "stn_beam_id": STATION_BEAM_ID,
                        "host": [(0, visibility_output_capture.ip_address)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@pytest.fixture(name="scan_id")
def override_scan_id() -> int:
    return SCAN_ID


@pytest.fixture(name="cnic_vd_config")
def fxt_cnic_vd_config() -> CnicConfigureVirtualDigitiserSchema:
    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,  # Disregarded
                "beam": STATION_BEAM_ID,
                "frequency": frequency_id,
                "station": station_id,
                "substation": substation_id,
                "subarray": SUBARRAY_ID,
                "sources": {
                    "x": [{"tone": False, "seed": station_id, "scale": 4000}],
                    "y": [{"tone": False, "seed": station_id, "scale": 4000}],
                },
            }
            for station_id, substation_id in STATIONS
            for frequency_id in FREQUENCY_IDS
        ],
    }


@pytest.fixture(name="output")
@pytest.mark.usefixtures("check_cbf_sdp_arp")
def fxt_output(  # pylint:disable=too-many-arguments
    assign_resources_json: LowCspAssignResourcesSchema,
    cnic_vd_config: CnicConfigureVirtualDigitiserSchema,
    configure_json: LowCspConfigureSchema,
    observation_time: datetime,
    scan_json: LowCspScanSchema,
    station_signal_generator: StationSignalGenerator,
    subarray_under_test: SubarrayUnderTest,
    visibility_output_capture: VisibilityOutputCapture,
) -> tuple[xr.DataArray, pd.DataFrame]:
    capture_file_name = f"{observation_time}_cbf_to_spead_output_inspection.pcap"

    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)

    with visibility_output_capture.capture(destination_file=capture_file_name) as session:
        output_file_path = session.destination_file_path

        with station_signal_generator.generate(vd_config=cnic_vd_config):
            subarray_under_test.scan(scan_json)
            session.monitor(duration_s=2)
            subarray_under_test.end_scan()

    subarray_under_test.end()
    subarray_under_test.release_all_resources()

    output = unpack_pcap_file(output_file_path)
    data = get_visibility_data(output, subarray_id=SUBARRAY_ID, beam_id=STATION_BEAM_ID, scan_id=SCAN_ID)
    metadata = get_visibility_metadata(output, subarray_id=SUBARRAY_ID, beam_id=STATION_BEAM_ID, scan_id=SCAN_ID)
    return data, metadata


def check_item_exists(metadata: pd.DataFrame, item: LowCbfVisibilitySpeadItem):
    with check:
        with allure.step(f"Verify that the SPEAD headers contain item `{item.value}`"):
            assert item in metadata.columns


def check_visibility_channel_id(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.VISIBILITY_CHANNEL_ID)

    expected = 13824
    with check:
        with allure.step(f"Verify that the SPEAD headers contain {expected} unique channels"):
            assert metadata.nunique()[LowCbfVisibilitySpeadItem.VISIBILITY_CHANNEL_ID] == expected


def check_number_of_baselines(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.NUMBER_OF_BASELINES)

    expected = 10
    with check:
        with allure.step(f"Verify that the baseline count is equal to {expected}"):
            assert metadata[LowCbfVisibilitySpeadItem.NUMBER_OF_BASELINES].unique().tolist() == [expected]


def check_scan_id(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.SCAN_ID)

    expected = 6789
    with check:
        with allure.step(f"Verify that the scan id is equal to {expected}"):
            assert metadata[LowCbfVisibilitySpeadItem.SCAN_ID].unique().tolist() == [expected]


def check_source_id(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.CBF_SOURCE_ID)

    expected = b"L"
    with check:
        with allure.step(f"Verify that the source id is equal to {expected}"):
            assert metadata[LowCbfVisibilitySpeadItem.CBF_SOURCE_ID].unique().tolist() == [expected]


def check_hardware_id(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.CBF_HARDWARE_ID)


def check_beam_id(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.STATION_BEAM_ID)

    expected = 1
    with check:
        with allure.step(f"Verify that the beam id is equal to {expected}"):
            assert metadata[LowCbfVisibilitySpeadItem.STATION_BEAM_ID].unique().tolist() == [expected]


def check_subarray_id(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.SUBARRAY_ID)

    expected = 2
    with check:
        with allure.step(f"Verify that the subarray id is equal to {expected}"):
            assert metadata[LowCbfVisibilitySpeadItem.SUBARRAY_ID].unique().tolist() == [expected]


def check_visibility_integration_period(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.VISIBILITY_INTEGRATION_PERIOD)

    expected = 0.849
    with check:
        with allure.step(f"Verify that the visibility integration period is equal to {expected} seconds"):
            assert metadata[LowCbfVisibilitySpeadItem.VISIBILITY_INTEGRATION_PERIOD].median() == pytest.approx(
                expected, abs=1e-3
            )


def check_visibility_frequency_resolution(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.VISIBILITY_FREQUENCY_RESOLUTION)

    expected = 5425.347
    with check:
        with allure.step(f"Verify that the visibility frequency resolution is equal to {expected} Hz"):
            assert metadata[LowCbfVisibilitySpeadItem.VISIBILITY_FREQUENCY_RESOLUTION].median() == pytest.approx(expected)


def check_visibility_output_resolution(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.VISIBILITY_OUTPUT_RESOLUTION)


def check_visibility_center_frequency(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.VISIBILITY_CENTRE_FREQUENCY)

    output_frequency_resolution = metadata[LowCbfVisibilitySpeadItem.VISIBILITY_FREQUENCY_RESOLUTION].median()

    input_frequency_range_start = coarse_channel_frequency_range(208)[0]
    output_frequency_range_start = (
        metadata[LowCbfVisibilitySpeadItem.VISIBILITY_CENTRE_FREQUENCY].min() - output_frequency_resolution / 2
    )
    with check:
        with allure.step(f"Verify that the visibility center frequency starts at {input_frequency_range_start} Hz"):
            assert output_frequency_range_start == pytest.approx(
                input_frequency_range_start, abs=output_frequency_resolution / 2
            )

    input_frequency_range_end = coarse_channel_frequency_range(303)[1]
    output_frequency_range_end = (
        metadata[LowCbfVisibilitySpeadItem.VISIBILITY_CENTRE_FREQUENCY].max() + output_frequency_resolution / 2
    )
    with check:
        with allure.step(f"Verify that the visibility center frequency stops at {input_frequency_range_end} Hz"):
            assert output_frequency_range_end == pytest.approx(input_frequency_range_end, abs=output_frequency_resolution / 2)


def check_zoom_window_id(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.ZOOM_WINDOW_ID)


def check_firmware_id(metadata: pd.DataFrame):
    check_item_exists(metadata, LowCbfVisibilitySpeadItem.CBF_FIRMWARE_VERSION)


def check_visibility_data(data: xr.DataArray):
    data = time_averaged_visibilities(data)

    expected_dtype = numpy.dtype(numpy.complex64)
    with check:
        with allure.step(f"Verify that the visibility data has dtype {expected_dtype}"):
            assert data.dtype == expected_dtype

    expected_shape = (13824, 10, 4)
    with check:
        with allure.step(f"Verify that the visibility data has shape {expected_shape}"):
            assert data.shape == expected_shape


@test_case(
    title="ICD CBF->SDP SPEAD output inspection",
    suite=TestSuite.CORRELATOR_BEAMFORMER,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1119059?projectId=335",
    xray_id="CLT-26",
)
def test_cbf_to_sdp_spead_output_inspection(
    output: tuple[xr.DataArray, pd.DataFrame],
    record_test_evidence: RecordTestEvidence,
):
    data, metadata = output
    record_test_evidence(
        file_name="SPEAD headers.csv",
        content_type="text/csv",
        content=metadata.to_csv(),
    )

    check_visibility_channel_id(metadata)
    check_number_of_baselines(metadata)
    check_scan_id(metadata)
    check_source_id(metadata)
    check_hardware_id(metadata)
    check_beam_id(metadata)
    check_subarray_id(metadata)
    check_visibility_integration_period(metadata)
    check_visibility_frequency_resolution(metadata)
    check_visibility_output_resolution(metadata)
    check_visibility_center_frequency(metadata)
    check_zoom_window_id(metadata)
    check_firmware_id(metadata)
    check_visibility_data(data)
