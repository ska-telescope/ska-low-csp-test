# pylint:disable=missing-module-docstring,missing-function-docstring,too-many-locals

import itertools
from datetime import datetime

import allure
import numpy as np
import pytest
import xarray as xr
from pytest_check import check

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cbf.visibilities import (
    Polarization,
    baselines_amplitude,
    get_visibility_data,
    time_averaged_visibilities,
    unpack_pcap_file,
)
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema, CnicVirtualDigitiserConfigSourceItemSchema
from ska_low_csp_test.domain.channels import coarse_channel_id
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.domain.spead import LowCbfSdpSpeadStationBaselineMapping
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator, VisibilityOutputCapture

FREQUENCY_IDS = list(range(208, 304))
FSP_IDS = [1]
SCAN_DURATION_S = 15.0
STATIONS = [(352, 1), (345, 1), (431, 1), (350, 1)]
STATION_BEAM_ID = 1
STATION_CW_Hz = {345: 163e6, 350: 200e6, 431: 237e6}
SUBARRAY_ID = 1

STATION_CW_CHANNEL_ID = {key: coarse_channel_id(val) for key, val in STATION_CW_Hz.items()}

station_baseline_mapping = LowCbfSdpSpeadStationBaselineMapping(STATIONS)


@pytest.fixture(name="subarray_id")
def override_subarray_id() -> int:
    return SUBARRAY_ID


@pytest.fixture(name="configure_json")
def override_config_json(
    config_id: str,
    vis_firmware_version: str,
    visibility_output_capture: VisibilityOutputCapture,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "ITC.L.AA0.5.ICD.1",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": SUBARRAY_ID,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": FSP_IDS,
                },
                "stn_beams": [
                    {
                        "stn_beam_id": STATION_BEAM_ID,
                        "host": [(0, visibility_output_capture.ip_address)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@pytest.fixture(name="cnic_vd_json")
def fxt_cnic_vd_json() -> CnicConfigureVirtualDigitiserSchema:
    counter = itertools.count()

    def noise() -> CnicVirtualDigitiserConfigSourceItemSchema:
        return {"tone": False, "scale": 1000, "seed": next(counter)}

    def tone() -> CnicVirtualDigitiserConfigSourceItemSchema:
        return {"tone": True, "scale": 4}

    def tone_or_noise(station_id: int, channel_id: int) -> list[CnicVirtualDigitiserConfigSourceItemSchema]:
        return (
            [tone()] if station_id in STATION_CW_CHANNEL_ID and channel_id == STATION_CW_CHANNEL_ID[station_id] else [noise()]
        )

    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,  # Disregarded
                "beam": STATION_BEAM_ID,
                "frequency": channel_id,
                "station": station_id,
                "substation": substation_id,
                "subarray": SUBARRAY_ID,
                "sources": {
                    "x": tone_or_noise(station_id, channel_id),
                    "y": tone_or_noise(station_id, channel_id),
                },
            }
            for station_id, substation_id in STATIONS
            for channel_id in FREQUENCY_IDS
        ],
    }


def check_baseline_has_spike_in_channels(visibilities: xr.DataArray, baseline_idx: int, channel_ids: list[int]):
    for polarization in [Polarization.XX, Polarization.YY]:
        amplitude = baselines_amplitude(
            visibilities,
            baseline_idx=baseline_idx,
            polarization=polarization,
        )
        cw_indices = np.unique(np.argwhere(amplitude.data > (np.mean(amplitude.data) + 7 * np.std(amplitude.data))).squeeze())
        cw_channels = cw_indices + amplitude.coords["channel_id"].data[0]
        with check:
            with allure.step(
                f"Verify that autocorrelation amplitudes contain a spike in channels {channel_ids} "
                f"for baseline index {baseline_idx} and polarization {polarization}"
            ):
                assert cw_channels.tolist() == channel_ids


@test_case(
    title="ICD Station input and baseline ordering",
    suite=TestSuite.CORRELATOR_BEAMFORMER,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1141258?projectId=335",
    xray_id="CLT-86",
)
@pytest.mark.usefixtures("check_cbf_sdp_arp")
def test_station_input_and_baseline_ordering(  # pylint: disable=too-many-arguments
    assign_resources_json: LowCspAssignResourcesSchema,
    cnic_vd_json: CnicConfigureVirtualDigitiserSchema,
    configure_json: LowCspConfigureSchema,
    observation_time: datetime,
    scan_json: LowCspScanSchema,
    station_signal_generator: StationSignalGenerator,
    subarray_under_test: SubarrayUnderTest,
    visibility_output_capture: VisibilityOutputCapture,
) -> None:
    capture_file_name = f"{observation_time}_station_input_and_baseline_ordering.pcap"

    with allure.step(f"Perform scan on subarray {subarray_under_test.id}"):
        subarray_under_test.assign_resources(assign_resources_json)
        subarray_under_test.configure(configure_json)

        with visibility_output_capture.capture(destination_file=capture_file_name) as session:
            output_file_path = session.destination_file_path

            with station_signal_generator.generate(vd_config=cnic_vd_json):
                subarray_under_test.scan(scan_json)
                session.monitor(duration_s=SCAN_DURATION_S)
                subarray_under_test.end_scan()

        subarray_under_test.end()
        subarray_under_test.release_all_resources()

    scan_id = scan_json["lowcbf"]["scan_id"]
    output = unpack_pcap_file(output_file_path)
    data = get_visibility_data(
        output,
        subarray_id=subarray_under_test.id,
        beam_id=STATION_BEAM_ID,
        scan_id=scan_id,
        station_baseline_mapping=station_baseline_mapping,
    )
    visibilities = time_averaged_visibilities(data)

    check_baseline_has_spike_in_channels(visibilities, baseline_idx=0, channel_ids=[216])
    check_baseline_has_spike_in_channels(visibilities, baseline_idx=2, channel_ids=[6984])
    check_baseline_has_spike_in_channels(visibilities, baseline_idx=5, channel_ids=[])
    check_baseline_has_spike_in_channels(visibilities, baseline_idx=9, channel_ids=[13752])
