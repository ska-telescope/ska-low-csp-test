# pylint:disable=missing-module-docstring,missing-function-docstring,missing-class-docstring,too-many-arguments

from dataclasses import dataclass
from datetime import datetime
from typing import Callable

import allure
import pandas as pd
import pytest
import xarray as xr
from astropy import units as u
from astropy.coordinates import AltAz

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cbf.visibilities import Polarization, get_visibility_data, unpack_pcap_file
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema
from ska_low_csp_test.delaypoly.devices import DelaypolyDevice, DelaypolyRaDecSchema
from ska_low_csp_test.delaypoly.helpers import sky_coord_to_ra_dec
from ska_low_csp_test.domain import plotting, sky_coordinates_from_altaz
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.domain.spead import LowCbfSdpSpeadStationBaselineMapping
from ska_low_csp_test.reporting import RecordTestEvidence, TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator, VisibilityOutputCapture

FREQUENCY_IDS = list(range(254, 262))
FSP_IDS = [1]
SCAN_1_BEAM_DIRECTION = AltAz(alt=50 * u.deg, az=10 * u.deg)
SCAN_2_BEAM_DIRECTION = AltAz(alt=55 * u.deg, az=45 * u.deg)
STATIONS = [(345, 1), (350, 1), (352, 1), (431, 1)]
STATION_BEAM_ID = 1
SUBARRAY_ID = 2

station_baseline_mapping = LowCbfSdpSpeadStationBaselineMapping(STATIONS)


@pytest.fixture(name="subarray_id")
def override_subarray_id():
    return SUBARRAY_ID


@pytest.fixture(name="configure_json")
def override_config_json(
    config_id: str,
    vis_firmware_version: str,
    visibility_output_capture: VisibilityOutputCapture,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "ITC.L.AA0.5.CORR.3",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": SUBARRAY_ID,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": FSP_IDS,
                },
                "stn_beams": [
                    {
                        "stn_beam_id": STATION_BEAM_ID,
                        "host": [(0, visibility_output_capture.ip_address)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@dataclass(frozen=True)
class ScanConfig:
    beam_direction: DelaypolyRaDecSchema
    scan_id: int
    scan_duration_s: float
    trim_first_s: int

    def json(self) -> LowCspScanSchema:
        return {
            "interface": "https://schema.skao.int/ska-low-csp-scan/3.2",
            "common": {
                "subarray_id": SUBARRAY_ID,
            },
            "lowcbf": {
                "scan_id": self.scan_id,
            },
        }


@pytest.fixture(name="scan_configs")
def fxt_scan_configs(
    config_id: str,
    generate_scan_id: Callable[[str], int],
    observation_time: datetime,
    shorten_scan_duration: bool,
) -> list[ScanConfig]:
    _, scan_1_direction, scan_2_direction = sky_coordinates_from_altaz(
        SCAN_1_BEAM_DIRECTION,
        SCAN_2_BEAM_DIRECTION,
        time=observation_time,
    )

    return [
        ScanConfig(
            scan_id=generate_scan_id(config_id),
            scan_duration_s=10 if shorten_scan_duration else 5 * 60,
            beam_direction=sky_coord_to_ra_dec(scan_1_direction),
            trim_first_s=5,
        ),
        ScanConfig(
            scan_id=generate_scan_id(config_id),
            scan_duration_s=20 if shorten_scan_duration else 12 * 60,
            beam_direction=sky_coord_to_ra_dec(scan_2_direction),
            trim_first_s=6 if shorten_scan_duration else 60,
        ),
    ]


@pytest.fixture(name="cnic_vd_config")
def fxt_cnic_vd_config() -> CnicConfigureVirtualDigitiserSchema:
    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,  # Disregarded
                "beam": STATION_BEAM_ID,
                "frequency": frequency_id,
                "station": station_id,
                "substation": substation_id,
                "subarray": SUBARRAY_ID,
                "sources": {
                    "x": [{"tone": False, "seed": 1981, "scale": 4138}],
                    "y": [{"tone": False, "seed": 1981, "scale": 4138}],
                },
            }
            for station_id, substation_id in STATIONS
            for frequency_id in FREQUENCY_IDS
        ],
    }


@test_case(
    title="CORR Two consecutive scans with delay tracking",
    suite=TestSuite.CORRELATOR_BEAMFORMER,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1122136?projectId=335",
    xray_id="CLT-19",
)
@pytest.mark.usefixtures("check_cbf_sdp_arp")
def test_two_consecutive_scans_with_delay_tracking(  # pylint: disable=too-many-locals
    assign_resources_json: LowCspAssignResourcesSchema,
    cnic_vd_config: CnicConfigureVirtualDigitiserSchema,
    configure_json: LowCspConfigureSchema,
    delaypoly_device: DelaypolyDevice,
    observation_time: datetime,
    record_test_evidence: RecordTestEvidence,
    scan_configs: list[ScanConfig],
    station_signal_generator: StationSignalGenerator,
    subarray_under_test: SubarrayUnderTest,
    visibility_output_capture: VisibilityOutputCapture,
):
    capture_file_name = f"{observation_time}_two_consecutive_scans_with_delay_tracking.pcap"

    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)

    with visibility_output_capture.capture(destination_file=capture_file_name) as session:
        output_file_path = session.destination_file_path

        for scan_config in scan_configs:
            with allure.step(f"Perform scan {scan_config.scan_id}"):
                delaypoly_device.beam_ra_dec(
                    {
                        "subarray_id": SUBARRAY_ID,
                        "beam_id": STATION_BEAM_ID,
                        "direction": scan_config.beam_direction,
                    }
                )
                delaypoly_device.source_ra_dec(
                    {
                        "subarray_id": SUBARRAY_ID,
                        "beam_id": STATION_BEAM_ID,
                        "direction": [scan_config.beam_direction] * 4,
                    }
                )

                with station_signal_generator.generate(vd_config=cnic_vd_config):
                    subarray_under_test.scan(scan_config.json())
                    session.monitor(duration_s=scan_config.scan_duration_s)
                    subarray_under_test.end_scan()

    subarray_under_test.end()
    subarray_under_test.release_all_resources()

    output = unpack_pcap_file(output_file_path)

    all_scan_data = []
    baseline_idx = 8  # 431 -- 352 baseline

    for scan_config in scan_configs:
        scan_data = get_visibility_data(
            output,
            subarray_id=SUBARRAY_ID,
            beam_id=STATION_BEAM_ID,
            scan_id=scan_config.scan_id,
            station_baseline_mapping=station_baseline_mapping,
        )
        all_scan_data.append(scan_data)

        record_test_evidence(
            file_name=f"Scan {scan_config.scan_id} XY time averaged phase vs channel.png",
            content_type="image/png",
            content=plotting.plot_time_averaged_phase_vs_channel(
                scan_data,
                baseline_idx,
                Polarization.XY,
                trim_first_s=scan_config.trim_first_s,
            ),
        )

    record_test_evidence(
        file_name="XY channel averaged phase vs time.png",
        content_type="image/png",
        content=plotting.plot_channel_averaged_phase_vs_time(
            xr.concat(
                all_scan_data,
                dim=pd.Index([sc.scan_id for sc in scan_configs], name="scan_id"),
            ),
            baseline_idx,
            Polarization.XY,
        ),
    )
