# pylint:disable=missing-module-docstring,missing-function-docstring,too-many-locals

import itertools
from datetime import datetime

import allure
import numpy
import pytest
import xarray as xr
from pytest_check import check

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cbf.visibilities import Polarization, get_visibility_data, time_averaged_visibilities, unpack_pcap_file
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema
from ska_low_csp_test.domain import plotting
from ska_low_csp_test.domain.channels import coarse_channel_id
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.domain.spead import LowCbfSdpSpeadStationBaselineMapping
from ska_low_csp_test.reporting import RecordTestEvidence, TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator, VisibilityOutputCapture

FREQUENCY_IDS = list(range(252, 260))
FSP_IDS = [1]
SCAN_DURATION_S = 10.0
STATIONS = [(350, 1)]
STATION_BEAM_ID = 1
SUBARRAY_ID = 2

station_baseline_mapping = LowCbfSdpSpeadStationBaselineMapping(STATIONS)


@pytest.fixture(name="subarray_id")
def override_subarray_id() -> int:
    return SUBARRAY_ID


@pytest.fixture(name="configure_json")
def override_config_json(
    config_id: str,
    vis_firmware_version: str,
    visibility_output_capture: VisibilityOutputCapture,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "ITC.L.AA0.5.CORR.1",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": SUBARRAY_ID,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": FSP_IDS,
                },
                "stn_beams": [
                    {
                        "stn_beam_id": STATION_BEAM_ID,
                        "host": [(0, visibility_output_capture.ip_address)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@pytest.fixture(name="cnic_vd_json")
def fxt_cnic_vd_json() -> CnicConfigureVirtualDigitiserSchema:
    counter = itertools.count()
    cw_channels = list(map(coarse_channel_id, [200e6]))
    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,  # Disregarded
                "beam": STATION_BEAM_ID,
                "frequency": frequency_id,
                "station": station_id,
                "substation": substation_id,
                "subarray": SUBARRAY_ID,
                "sources": {
                    "x": [
                        {"tone": False, "scale": 1000, "seed": next(counter)},
                        {"tone": frequency_id in cw_channels, "scale": 4 if frequency_id in cw_channels else 0},
                    ],
                    "y": [{"tone": False, "scale": 1000, "seed": next(counter)}],
                },
            }
            for station_id, substation_id in STATIONS
            for frequency_id in FREQUENCY_IDS
        ],
    }


def check_visibility_data(data: xr.DataArray):
    try:
        data = time_averaged_visibilities(data)
    except IndexError:
        pytest.fail("No visibility data found in the unpacked output.")

    expected_dtype = numpy.dtype(numpy.complex64)
    with check:
        with allure.step(f"Verify that the visibility data has dtype {expected_dtype}"):
            assert data.dtype == expected_dtype

    expected_shape = (1152, 1, 4)
    with check:
        with allure.step(f"Verify that the visibility data has shape {expected_shape}"):
            assert data.shape == expected_shape


@test_case(
    title="CORR Single scan with one station",
    suite=TestSuite.CORRELATOR_BEAMFORMER,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1145561?projectId=335",
    xray_id="CLT-96",
)
@pytest.mark.usefixtures("check_cbf_sdp_arp")
def test_single_scan_with_one_station(  # pylint: disable=too-many-arguments
    assign_resources_json: LowCspAssignResourcesSchema,
    cnic_vd_json: CnicConfigureVirtualDigitiserSchema,
    configure_json: LowCspConfigureSchema,
    observation_time: datetime,
    record_test_evidence: RecordTestEvidence,
    scan_json: LowCspScanSchema,
    station_signal_generator: StationSignalGenerator,
    subarray_under_test: SubarrayUnderTest,
    visibility_output_capture: VisibilityOutputCapture,
) -> None:
    capture_file_name = f"{observation_time}_single_scan_with_one_station.pcap"

    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)

    with visibility_output_capture.capture(destination_file=capture_file_name) as session:
        output_file_path = session.destination_file_path

        with station_signal_generator.generate(vd_config=cnic_vd_json):
            subarray_under_test.scan(scan_json)
            session.monitor(duration_s=SCAN_DURATION_S)
            subarray_under_test.end_scan()

    subarray_under_test.end()
    subarray_under_test.release_all_resources()

    scan_id = scan_json["lowcbf"]["scan_id"]
    output = unpack_pcap_file(output_file_path)
    data = get_visibility_data(
        output,
        subarray_id=subarray_under_test.id,
        beam_id=STATION_BEAM_ID,
        scan_id=scan_id,
        station_baseline_mapping=station_baseline_mapping,
    )

    check_visibility_data(data)

    for pol in [Polarization.XX, Polarization.YY]:
        record_test_evidence(
            file_name=f"{pol} time averaged amplitude vs channel.png",
            content_type="image/png",
            content=plotting.plot_time_averaged_amplitude_vs_channel(
                data,
                station_baseline_mapping.auto_indices,
                pol,
            ),
        )
