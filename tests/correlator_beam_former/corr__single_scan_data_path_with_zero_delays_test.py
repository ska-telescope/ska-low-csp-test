# pylint:disable=missing-module-docstring,missing-function-docstring,too-many-locals

import itertools
from datetime import datetime

import pytest

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cbf.visibilities import Polarization, get_visibility_data, unpack_pcap_file
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema, CnicVirtualDigitiserConfigSourceItemSchema
from ska_low_csp_test.delaypoly.devices import DelaypolyDevice
from ska_low_csp_test.delaypoly.helpers import NO_DELAY
from ska_low_csp_test.domain import plotting
from ska_low_csp_test.domain.channels import coarse_channel_id
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.domain.spead import LowCbfSdpSpeadStationBaselineMapping
from ska_low_csp_test.reporting import RecordTestEvidence, TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator, VisibilityOutputCapture

CW_FREQUENCY_IDS = list(map(coarse_channel_id, [163e6, 200e6, 237e6]))
FREQUENCY_IDS = list(range(208, 304))
FSP_IDS = [1]
STATIONS = [(345, 1), (350, 1), (352, 1), (431, 1)]
STATION_BEAM_ID = 1
SUBARRAY_ID = 1

station_baseline_mapping = LowCbfSdpSpeadStationBaselineMapping(STATIONS)


@pytest.fixture(name="configure_json")
def override_config_json(
    subarray_id: int,
    config_id: str,
    vis_firmware_version: str,
    visibility_output_capture: VisibilityOutputCapture,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "ITC.L.AA0.5.CORR.1",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": subarray_id,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, STATION_BEAM_ID),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": FSP_IDS,
                },
                "stn_beams": [
                    {
                        "stn_beam_id": STATION_BEAM_ID,
                        "host": [(0, visibility_output_capture.ip_address)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@pytest.fixture(name="cnic_vd_json")
def fxt_generate_cnic_vd_config() -> CnicConfigureVirtualDigitiserSchema:
    counter = itertools.count()

    def noise() -> CnicVirtualDigitiserConfigSourceItemSchema:
        return {"tone": False, "scale": 1000, "seed": next(counter)}

    def tone() -> CnicVirtualDigitiserConfigSourceItemSchema:
        return {"tone": True, "scale": 4}

    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,  # Disregarded
                "beam": STATION_BEAM_ID,
                "frequency": frequency_id,
                "station": station_id,
                "substation": substation_id,
                "subarray": SUBARRAY_ID,
                "sources": {
                    "x": [noise(), tone()] if frequency_id in CW_FREQUENCY_IDS else [noise()],
                    "y": [noise()],
                },
            }
            for station_id, substation_id in STATIONS
            for frequency_id in FREQUENCY_IDS
        ],
    }


@pytest.fixture(name="scan_duration_s")
def fxt_scan_duration(shorten_scan_duration: bool) -> float:
    return 10 if shorten_scan_duration else 90  # seconds


@test_case(
    title="CORR Single scan data path with zero delays",
    suite=TestSuite.CORRELATOR_BEAMFORMER,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1136473?projectId=335",
    xray_id="CLT-82",
)
@pytest.mark.usefixtures("check_cbf_sdp_arp")
def test_single_scan_data_path_with_zero_delays(  # pylint: disable=too-many-arguments
    assign_resources_json: LowCspAssignResourcesSchema,
    cnic_vd_json: CnicConfigureVirtualDigitiserSchema,
    configure_json: LowCspConfigureSchema,
    delaypoly_device: DelaypolyDevice,
    observation_time: datetime,
    record_test_evidence: RecordTestEvidence,
    scan_duration_s: float,
    scan_id: int,
    scan_json: LowCspScanSchema,
    station_signal_generator: StationSignalGenerator,
    subarray_under_test: SubarrayUnderTest,
    visibility_output_capture: VisibilityOutputCapture,
) -> None:
    capture_file_name = f"{observation_time}_single_scan_data_path_with_zero_delays.pcap"
    with visibility_output_capture.capture(destination_file=capture_file_name) as session:
        output_file_path = session.destination_file_path

        subarray_under_test.assign_resources(assign_resources_json)
        subarray_under_test.configure(configure_json)

        delaypoly_device.beam_delay({"subarray_id": SUBARRAY_ID, "beam_id": STATION_BEAM_ID, "delay": [NO_DELAY]})
        delaypoly_device.source_delay({"subarray_id": SUBARRAY_ID, "beam_id": STATION_BEAM_ID, "delay": [[NO_DELAY]] * 4})

        with station_signal_generator.generate(vd_config=cnic_vd_json):
            subarray_under_test.scan(scan_json)
            session.monitor(duration_s=scan_duration_s)
            subarray_under_test.end_scan()

        subarray_under_test.end()
        subarray_under_test.release_all_resources()

    output = unpack_pcap_file(output_file_path)
    data = get_visibility_data(
        output,
        subarray_id=subarray_under_test.id,
        beam_id=STATION_BEAM_ID,
        scan_id=scan_id,
        station_baseline_mapping=station_baseline_mapping,
    )

    record_test_evidence(
        file_name="Epoch offset increment.png",
        content_type="image/png",
        content=plotting.plot_epoch_offset_increment(data),
    )

    for pol in [Polarization.XX, Polarization.YY]:
        record_test_evidence(
            file_name=f"{pol} time averaged amplitude vs channel.png",
            content_type="image/png",
            content=plotting.plot_time_averaged_amplitude_vs_channel(
                data,
                station_baseline_mapping.auto_indices,
                pol,
            ),
        )
    for pol in [Polarization.XY, Polarization.YX]:
        record_test_evidence(
            file_name=f"{pol} time averaged phase vs channel.png",
            content_type="image/png",
            content=plotting.plot_time_averaged_phase_vs_channel(
                data,
                station_baseline_mapping.cross_sample(),
                pol,
            ),
        )
