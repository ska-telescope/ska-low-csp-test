# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring

import allure
import pytest
from ska_control_model import AdminMode, ObsState
from tango import DevFailed

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest, SystemUnderTest

STATION_BEAM_ID = 1
SUBARRAY_ID = 3


@pytest.fixture(name="subarray_id")
def override_subarray_id() -> int:
    return SUBARRAY_ID


@pytest.fixture(name="beam_id")
def fxt_beam_id() -> int:
    return STATION_BEAM_ID


@pytest.fixture(name="configure_json")
def override_configure_json(
    beam_id: int,
    config_id: str,
    subarray_id: int,
    vis_firmware_version: str,
    default_sdp_ip: str,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "ITC.L.AA0.5.CORR.2",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": subarray_id,
        },
        "lowcbf": {
            "stations": {
                "stns": [(345, 1), (431, 1)],
                "stn_beams": [
                    {
                        "beam_id": beam_id,
                        "freq_ids": list(range(141, 149)),
                        "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, beam_id),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": [1],
                },
                "stn_beams": [
                    {
                        "stn_beam_id": beam_id,
                        "host": [(0, default_sdp_ip)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@pytest.fixture(autouse=True)
def revert_processors_to_online_mode(low_cbf_devices: LowCbfDevices):
    admin_mode_cache, processors = {}, low_cbf_devices.all_processors()

    for processor in processors:
        admin_mode_cache[processor.fqdn] = processor.admin_mode

    yield

    for processor in processors:
        processor.admin_mode = admin_mode_cache[processor.fqdn]


@test_case(
    title="Low CBF processor OFFLINE during scan",
    suite=TestSuite.CORRELATOR_BEAMFORMER,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1137224?projectId=335",
    xray_id="CLT-88",
)
def test_low_cbf_processor_offline_during_scan(
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
    scan_json: LowCspScanSchema,
    subarray_under_test: SubarrayUnderTest,
    system_under_test: SystemUnderTest,
):
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)
    subarray_under_test.scan(scan_json)

    [processor_serial, *_] = system_under_test.low_cbf_devices.subarray_by_id(SUBARRAY_ID).assigned_processors
    processor_device_fqdns = system_under_test.low_cbf_devices.allocator().processor_device_fqdns
    processor_fqdn = processor_device_fqdns[processor_serial]
    correlator = system_under_test.low_cbf_devices.processor_by_id(processor_fqdn)

    with allure.step("Set correlator AdminMode to OFFLINE"):
        with pytest.raises(DevFailed) as er:
            correlator.admin_mode = AdminMode.OFFLINE
    expected_error_msg = "ValueError: Can't change adminMode while FPGA is in use\n"
    actual_error_msg = er.value.args[0].desc
    assert expected_error_msg == actual_error_msg
    subarray_under_test.verify_obs_state(ObsState.SCANNING)
