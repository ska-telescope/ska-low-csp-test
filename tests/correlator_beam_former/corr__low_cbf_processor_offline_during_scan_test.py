# pylint:disable=missing-module-docstring,missing-function-docstring,too-many-arguments

import allure
import pytest
from ska_control_model import AdminMode, ObsState
from tango import DevFailed

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri, get_processors_for_fsp_ids
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest, SystemUnderTest


@pytest.fixture(name="subarray_id")
def override_subarray_id() -> int:
    return 3


@pytest.fixture(name="beam_id")
def fxt_beam_id() -> int:
    return 1


@pytest.fixture(name="configure_json")
def override_configure_json(
    beam_id: int,
    config_id: str,
    subarray_id: int,
    vis_firmware_version: str,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "ITC.L.AA0.5.CORR.2",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": subarray_id,
        },
        "lowcbf": {
            "stations": {
                "stns": [(345, 1), (431, 1)],
                "stn_beams": [
                    {
                        "beam_id": beam_id,
                        "freq_ids": list(range(141, 149)),
                        "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, beam_id),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": [1],
                },
                "stn_beams": [
                    {
                        "stn_beam_id": beam_id,
                        "host": [(0, "192.168.2.2")],
                        "mac": [(0, "0c-42-a1-9c-a2-1b")],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@test_case(
    title="Low CBF processor OFFLINE during scan",
    suite=TestSuite.CORRELATOR_BEAMFORMER,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1137224?projectId=335",
    xray_id="CLT-88",
)
def test_low_cbf_processor_offline_during_scan(
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
    scan_json: LowCspScanSchema,
    subarray_under_test: SubarrayUnderTest,
    system_under_test: SystemUnderTest,
):
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)
    subarray_under_test.scan(scan_json)

    correlator_fsp_id = configure_json["lowcbf"]["vis"]["fsp"]["fsp_ids"][0]
    correlator = next(get_processors_for_fsp_ids(system_under_test.low_cbf_devices, [correlator_fsp_id]))

    with allure.step("Set correlator AdminMode to OFFLINE"):
        with pytest.raises(DevFailed) as er:
            correlator.admin_mode = AdminMode.OFFLINE
    expected_error_msg = "ValueError: Can't change adminMode while FPGA is in use\n"
    actual_error_msg = er.value.args[0].desc
    assert expected_error_msg == actual_error_msg
    subarray_under_test.verify_obs_state(ObsState.SCANNING)
