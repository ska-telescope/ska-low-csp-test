# pylint:disable=missing-module-docstring,missing-function-docstring,too-many-arguments

import os
from datetime import datetime

import allure
import pytest

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator, VisibilityOutputCapture

FSP_IDS = [1, 2]
STATION_BEAM_ID = 1
FREQUENCY_IDS = list(range(208, 304))
STATIONS = [
    (345, 1),
    (346, 1),
    (347, 1),
    (348, 1),
    (349, 1),
    (350, 1),
    (351, 1),
    (352, 1),
    (353, 1),
    (354, 1),
    (355, 1),
    (356, 1),
    (429, 1),
    (430, 1),
    (431, 1),
    (432, 1),
    (433, 1),
    (434, 1),
]
SCAN_DURATION_S = 1
SUBARRAY_ID = 2


@pytest.fixture(name="subarray_id")
def override_subarray_id():
    return SUBARRAY_ID


@pytest.fixture(name="configure_json")
def override_config_json(
    config_id: str,
    vis_firmware_version: str,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": f"Subarray_{SUBARRAY_ID}",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": SUBARRAY_ID,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": FSP_IDS,
                },
                "stn_beams": [
                    {
                        "stn_beam_id": STATION_BEAM_ID,
                        "host": [(0, "192.168.4.1")],
                        "mac": [(0, "0c-42-a1-9c-a2-1b")],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@pytest.fixture(name="cnic_vd_config")
def fxt_cnic_vd_config() -> CnicConfigureVirtualDigitiserSchema:
    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,  # Disregarded
                "beam": STATION_BEAM_ID,
                "frequency": frequency_id,
                "station": station_id,
                "substation": substation_id,
                "subarray": SUBARRAY_ID,
                "sources": {
                    "x": [{"tone": False, "seed": 1981, "scale": 4138}],
                    "y": [{"tone": False, "seed": 1981, "scale": 4138}],
                },
            }
            for station_id, substation_id in STATIONS
            for frequency_id in FREQUENCY_IDS
        ],
    }


@test_case(
    title="CORR Single scan data path with 2 processors",
    suite=TestSuite.CORRELATOR_BEAMFORMER,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1187745?projectId=335",
    xray_id="CLT-336",
)
def test_single_scan_data_path_with_two_processors(
    assign_resources_json: LowCspAssignResourcesSchema,
    cnic_vd_config: CnicConfigureVirtualDigitiserSchema,
    configure_json: LowCspConfigureSchema,
    observation_time: datetime,
    scan_json: LowCspScanSchema,
    station_signal_generator: StationSignalGenerator,
    subarray_under_test: SubarrayUnderTest,
    visibility_output_capture: VisibilityOutputCapture,
) -> None:
    capture_file_name = f"{observation_time}_single_scan_data_path_with_two_processors.pcap"
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)

    with visibility_output_capture.capture(destination_file=capture_file_name) as session:
        output_file_path = session.destination_file_path

        with station_signal_generator.generate(vd_config=cnic_vd_config):
            subarray_under_test.scan(scan_json)
            session.monitor(duration_s=SCAN_DURATION_S)
            subarray_under_test.end_scan()

    subarray_under_test.end()
    subarray_under_test.release_all_resources()

    with allure.step(f"Verify that {output_file_path} exists"):
        assert os.path.exists(output_file_path), f"{output_file_path} does not exist"
