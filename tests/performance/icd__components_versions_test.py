# pylint:disable=missing-module-docstring,missing-function-docstring,missing-class-docstring,too-many-arguments,subprocess-run-check
import logging
import os
import subprocess
from dataclasses import dataclass

import allure
import pytest
import yaml
from pytest_check.check_functions import check_func

from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SystemUnderTest
from ska_low_csp_test.tango.ska_devices import SKABaseDevice

logger = logging.getLogger(__name__)


@dataclass
class CspComponentsVersions:
    lmc_ver: str | None
    cbf_ver: str | None
    cbf_proc_ver: str | None
    cbf_connector_ver: str | None
    pst_ver: str | None


def extract_version(name, parsed_yaml):
    for dependency in parsed_yaml.get("dependencies", []):
        if dependency.get("name") == name:
            return dependency.get("version")
    return None


@pytest.fixture(name="components_versions")
def fxt_get_components_versions(tmpdir) -> CspComponentsVersions:
    repo_url = "https://gitlab.com/ska-telescope/ska-low-csp.git"
    chart_file_path = tmpdir.join("charts/ska-low-csp/Chart.yaml")
    commit_sha = os.getenv("COMPONENTS_VERSION_COMMIT", "main")
    logger.info("Commit/branch to use: %s", commit_sha)

    subprocess.run(["git", "clone", repo_url, tmpdir.strpath])
    subprocess.run(["git", "-C", tmpdir.strpath, "checkout", commit_sha])

    with open(chart_file_path, "r", encoding="utf-8") as file:
        parsed_yaml = yaml.safe_load(file.read())

    return CspComponentsVersions(
        lmc_ver=extract_version("ska-csp-lmc-low", parsed_yaml),
        cbf_ver=extract_version("ska-low-cbf", parsed_yaml),
        cbf_proc_ver=extract_version("ska-low-cbf-proc", parsed_yaml),
        cbf_connector_ver=extract_version("ska-low-cbf-conn", parsed_yaml),
        pst_ver=extract_version("ska-pst", parsed_yaml),
    )


@check_func
@allure.step("Verify that the {component} version is {expected}")
def verify_version(component: SKABaseDevice, expected: str | None):
    assert component.version_id == expected


@test_case(
    title="ICD Check LOW CSP components versions",
    suite=TestSuite.PERFORMANCE,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1154643?projectId=335",
    xray_id="CLT-163",
)
def test_check_components_version(system_under_test: SystemUnderTest, components_versions: CspComponentsVersions):
    verify_version(system_under_test.low_csp_devices.controller(), components_versions.lmc_ver)
    verify_version(system_under_test.low_csp_devices.all_subarrays()[0], components_versions.lmc_ver)

    verify_version(system_under_test.low_cbf_devices.controller(), components_versions.cbf_ver)
    verify_version(system_under_test.low_cbf_devices.all_subarrays()[0], components_versions.cbf_ver)
    verify_version(system_under_test.low_cbf_devices.allocator(), components_versions.cbf_ver)
    verify_version(system_under_test.low_cbf_devices.all_processors()[0], components_versions.cbf_proc_ver)
    verify_version(system_under_test.low_cbf_devices.connector(), components_versions.cbf_connector_ver)

    verify_version(system_under_test.pst_devices.all_beams()[0], components_versions.pst_ver)
