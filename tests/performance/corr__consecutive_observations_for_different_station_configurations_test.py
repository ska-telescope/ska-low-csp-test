# pylint:disable=missing-module-docstring,missing-function-docstring,too-many-arguments

import os
from datetime import datetime
from typing import Callable

import allure
import pytest

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cbf.visibilities import unpack_pcap_file
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator, VisibilityOutputCapture

EMPTY_PCAP_SIZE = 24

FREQUENCY_IDS_a = list(range(208, 303))
FREQUENCY_IDS_b = list(range(254, 261))
FSP_IDS = [1]
STATIONS_a = [(345, 1), (431, 1)]
STATIONS_b = [(345, 1), (350, 1), (352, 1), (431, 1)]
STATION_BEAM_ID = 1
SUBARRAY_ID = 1


@pytest.fixture(name="subarray_id")
def override_subarray_id() -> int:
    return SUBARRAY_ID


@pytest.fixture(name="beam_id")
def fxt_beam_id() -> int:
    return STATION_BEAM_ID


@pytest.fixture(name="generate_configure_json")
def fxt_generate_configure_json(
    beam_id: int,
    generate_config_id: Callable[[], str],
    subarray_id: int,
    vis_firmware_version: str,
    visibility_output_capture: VisibilityOutputCapture,
) -> Callable:
    def _generate(stations: list[tuple[int]], channels: list[int]):
        return {
            "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
            "subarray": {
                "subarray_name": "ITC.L.AA1.CORR.2",
            },
            "common": {
                "config_id": generate_config_id(),
                "subarray_id": subarray_id,
            },
            "lowcbf": {
                "stations": {
                    "stns": stations,
                    "stn_beams": [
                        {
                            "beam_id": beam_id,
                            "freq_ids": channels,
                            "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, beam_id),
                        },
                    ],
                },
                "vis": {
                    "fsp": {
                        "firmware": vis_firmware_version,
                        "fsp_ids": FSP_IDS,
                    },
                    "stn_beams": [
                        {
                            "stn_beam_id": beam_id,
                            "host": [(0, visibility_output_capture.ip_address)],
                            "port": [(0, 20000, 1)],
                            "integration_ms": 849,
                        },
                    ],
                },
            },
        }

    return _generate


@pytest.fixture(name="generate_scan_json")
def fxt_generate_scan_json() -> Callable:
    def _generate(scan_id: int):
        return {
            "interface": "https://schema.skao.int/ska-low-csp-scan/3.2",
            "common": {
                "subarray_id": SUBARRAY_ID,
            },
            "lowcbf": {
                "scan_id": scan_id,
            },
        }

    return _generate


@pytest.fixture(name="configure_json_a")
def fxt_configure_json_a(generate_configure_json) -> LowCspConfigureSchema:
    return generate_configure_json(stations=STATIONS_a, channels=FREQUENCY_IDS_a)


@pytest.fixture(name="configure_json_b")
def fxt_configure_json_b(generate_configure_json) -> LowCspConfigureSchema:
    return generate_configure_json(stations=STATIONS_b, channels=FREQUENCY_IDS_b)


@pytest.fixture(name="generate_cnic_vd_config")
def fxt_generate_cnic_vd_config() -> Callable:
    def _generate(stations: list[tuple[int, int]], frequency_ids: list[int]) -> CnicConfigureVirtualDigitiserSchema:
        return {
            "sps_packet_version": 3,
            "stream_configs": [
                {
                    "scan": 0,  # Disregarded
                    "beam": STATION_BEAM_ID,
                    "frequency": frequency_id,
                    "station": station_id,
                    "substation": substation_id,
                    "subarray": SUBARRAY_ID,
                    "sources": {
                        "x": [{"tone": False, "seed": 1981, "scale": 4000}],
                        "y": [{"tone": False, "seed": 1981, "scale": 4000}],
                    },
                }
                for station_id, substation_id in stations
                for frequency_id in frequency_ids
            ],
        }

    return _generate


@test_case(
    title="CORR Consecutive observations for different station configurations",
    suite=TestSuite.PERFORMANCE,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1143560?projectId=335",
    xray_id="CLT-137",
)
@pytest.mark.usefixtures("check_cbf_sdp_arp")
@pytest.mark.skipif(os.environ.get("TEST_SCAN_DURATION_MODE", "").lower() in ["short", ""], reason="Skipping for short runs")
def test_consecutive_observations_for_different_station_configurations(
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json_a: LowCspConfigureSchema,
    configure_json_b: LowCspConfigureSchema,
    generate_cnic_vd_config: Callable[[list[tuple[int, int]], list[int]], CnicConfigureVirtualDigitiserSchema],
    generate_scan_json: Callable[[int], LowCspScanSchema],
    observation_time: datetime,
    station_signal_generator: StationSignalGenerator,
    subarray_under_test: SubarrayUnderTest,
    visibility_output_capture: VisibilityOutputCapture,
):
    subarray_under_test.assign_resources(assign_resources_json)

    for iteration in range(10):
        with allure.step(f"Iteration {iteration}"):
            with allure.step("Scan with configuration A"):
                capture_file_name = (
                    f"{observation_time}_consecutive_observations_for_different_station_configurations"
                    f".iteration-{iteration}.config-A.pcap"
                )
                with visibility_output_capture.capture(destination_file=capture_file_name) as session:
                    output_file_path = session.destination_file_path

                    subarray_under_test.configure(configure_json_a)
                    with station_signal_generator.generate(vd_config=generate_cnic_vd_config(STATIONS_a, FREQUENCY_IDS_a)):
                        subarray_under_test.scan(generate_scan_json(iteration * 3 + 1))
                        session.monitor(duration_s=20)
                        subarray_under_test.end_scan()
                    subarray_under_test.end()
                try:
                    _ = unpack_pcap_file(output_file_path)
                except KeyError:
                    pytest.fail("Capture failure detected: no correlator output seen.")

            with allure.step("Scans with configuration B"):
                capture_file_name = (
                    f"{observation_time}_consecutive_observations_for_different_station_configurations"
                    f".iteration-{iteration}.config-B.pcap"
                )

                with visibility_output_capture.capture(destination_file=capture_file_name) as session:
                    output_file_path = session.destination_file_path

                    subarray_under_test.configure(configure_json_b)
                    with station_signal_generator.generate(vd_config=generate_cnic_vd_config(STATIONS_b, FREQUENCY_IDS_b)):
                        subarray_under_test.scan(generate_scan_json(iteration * 3 + 2))
                        session.monitor(duration_s=15)
                        subarray_under_test.end_scan()

                        subarray_under_test.scan(generate_scan_json(iteration * 3 + 3))
                        session.monitor(duration_s=20)
                        subarray_under_test.end_scan()
                    subarray_under_test.end()
                assert os.stat(output_file_path).st_size > EMPTY_PCAP_SIZE, pytest.fail(
                    "Capture failure detected: no correlator output seen."
                )
                try:
                    _ = unpack_pcap_file(output_file_path)
                except KeyError:
                    pytest.fail("Capture failure detected: failed to unpack PCAP.")

    subarray_under_test.release_all_resources()
