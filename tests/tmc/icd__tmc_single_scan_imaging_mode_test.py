# pylint:disable=missing-module-docstring
# pylint:disable=missing-function-docstring
# pylint:disable=missing-class-docstring

import queue
from typing import Any

import allure
import pytest
from pytest_check.check_functions import check_func

from ska_low_csp_test.cbf.devices import LowCbfDevices, LowCbfSubarrayDevice
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.synchronisation import wait_for_condition
from ska_low_csp_test.tmc.devices import LowTmcCentralNodeDevice, LowTmcDevices, LowTmcSubarrayLeafNodeDevice, TmcLeafNodeType
from ska_low_csp_test.tmc.sut import TmcSubarrayUnderTest
from ska_low_csp_test.tmc.testware import TmcStationSignalGenerator

STATIONS = [
    (345, 1),
    (346, 1),
    (347, 1),
    (348, 1),
    (349, 1),
    (350, 1),
    (351, 1),
    (352, 1),
    (353, 1),
    (354, 1),
    (355, 1),
    (356, 1),
    (429, 1),
    (430, 1),
    (431, 1),
    (432, 1),
    (433, 1),
    (434, 1),
]  # AA1
FREQUENCY_IDS = list(range(100, 108))

SUBARRAY_ID = 1
FULL_SCAN_DURATION_S = 60.0
SHORT_SCAN_DURATION_S = 15.0
STATION_BEAM_ID = 1
FSP_IDS = [1]
SKY_COORDINATES = {
    "c1": 180.0,
    "c2": 50.0,
}


CNIC_VD_JSON: CnicConfigureVirtualDigitiserSchema = {
    "sps_packet_version": 3,
    "stream_configs": [
        {
            "scan": 0,  # Disregarded
            "beam": STATION_BEAM_ID,
            "frequency": frequency_id,
            "station": station_id,
            "substation": substation_id,
            "subarray": SUBARRAY_ID,
            "sources": {
                "x": [{"tone": False, "seed": 1000, "scale": 4000}],
                "y": [{"tone": False, "seed": 1000, "scale": 4000}],
            },
        }
        for station_id, substation_id in STATIONS
        for frequency_id in FREQUENCY_IDS
    ],
}

TMC_RELEASE_RESOURCES_JSON = {
    "interface": "https://schema.skao.int/ska-tmc-releaseresources/3.0",
    "subarray_id": SUBARRAY_ID,
    "release_all": True,
}


@pytest.fixture(name="scan_duration_s")
def fxt_scan_duration_s(shorten_scan_duration: bool) -> float:
    return SHORT_SCAN_DURATION_S if shorten_scan_duration else FULL_SCAN_DURATION_S


@pytest.fixture(name="tmc_assign_resources_json")
def fxt_tmc_assign_resources_json(eb_id: str) -> dict[str, Any]:
    return {
        "interface": "https://schema.skao.int/ska-low-tmc-assignresources/4.0",
        "subarray_id": SUBARRAY_ID,
        "csp": {},
        "mccs": {  # dummy MCCS block
            "interface": "https://schema.skao.int/ska-low-mccs-controller-allocate/3.0",
            "subarray_beams": [],
        },
        "sdp": {  # dummy SDP block
            "resources": {"receive_nodes": 1},
            "execution_block": {
                "eb_id": eb_id,
                "max_length": 0.0,
                "beams": [],
                "channels": [],
                "context": {},
                "fields": [],
                "polarisations": [],
                "scan_types": [],
            },
        },
    }


@pytest.fixture(name="tmc_configure_json")
def fxt_tmc_configure_json(
    eb_id: str,
    config_id: str,
    vis_firmware_version: str,
    scan_duration_s: float,
) -> dict[str, Any]:
    return {
        "interface": "https://schema.skao.int/ska-low-tmc-configure/4.0",
        "csp": {
            "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
            "common": {
                "config_id": config_id,
                "eb_id": eb_id,
            },
            "lowcbf": {
                "stations": {
                    "stns": [[station_id, substation_id] for station_id, substation_id in STATIONS],
                    "stn_beams": [
                        {
                            "beam_id": STATION_BEAM_ID,
                            "freq_ids": FREQUENCY_IDS,
                        }
                    ],
                },
                "vis": {
                    "fsp": {"firmware": vis_firmware_version, "fsp_ids": FSP_IDS},
                    "stn_beams": [
                        {
                            "stn_beam_id": STATION_BEAM_ID,
                            "integration_ms": 849,
                        }
                    ],
                },
            },
        },
        "mccs": {
            "subarray_beams": [
                {
                    "subarray_beam_id": STATION_BEAM_ID,
                    "update_rate": 0.0,
                    "logical_bands": [],
                    "apertures": [],
                    "sky_coordinates": {
                        "reference_frame": "ICRS",
                        **SKY_COORDINATES,
                    },
                }
            ],
        },
        "sdp": {
            "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
            "scan_type": "target:a",
        },
        "tmc": {
            "scan_duration": scan_duration_s,
        },
    }


@pytest.fixture(name="tmc_scan_json")
def fxt_tmc_scan_json(scan_id: int):
    return {
        "interface": "https://schema.skao.int/ska-tmc-scan/4.0",
        "scan_id": scan_id,
    }


@pytest.fixture(name="tmc_csp_subarray_leaf_node")
def fxt_tmc_csp_subarray_leaf_node(tmc_devices: LowTmcDevices) -> LowTmcSubarrayLeafNodeDevice:
    """Fixture to retrieve the central node from the LOW-TMC devices."""
    return tmc_devices.subarray_leaf_node(TmcLeafNodeType.CSP, SUBARRAY_ID)


@pytest.fixture(name="cbf_subarray")
def fxt_cbf_subarray(low_cbf_devices: LowCbfDevices) -> LowCbfSubarrayDevice:
    return low_cbf_devices.subarray_by_id(SUBARRAY_ID)


@check_func
@allure.step("Validate delays on CBF subarray")
def check_cbf_delays_valid(cbf_subarray: LowCbfSubarrayDevice, timeout_s: float):
    wait_for_condition(
        lambda: cbf_subarray.delays_valid != LowCbfSubarrayDevice.DelaysValidity.UNUSED,
        timeout_s=timeout_s,
        message_on_timeout=lambda: "Delays were never used",
    )
    assert cbf_subarray.delays_valid == LowCbfSubarrayDevice.DelaysValidity.VALID


@check_func
@allure.step("Validate the number of station beam delays")
def check_station_beam_delays(csp_subarray_leaf_node: LowTmcSubarrayLeafNodeDevice):
    station_beam_delays = csp_subarray_leaf_node.delay_model["station_beam_delays"]
    assert len(station_beam_delays) == len(STATIONS)


@check_func
@allure.step("Verify that the delay model pushed at least one change event during scan")
def check_delay_model_change_events(delay_model_change_events: queue.SimpleQueue):
    assert not delay_model_change_events.empty(), "Delay model did not push any change events"


@test_case(
    title="TMC single scan in IMAGING mode",
    suite=TestSuite.TELESCOPE_MONITORING_AND_CONTROL,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1166325?projectId=335",
    xray_id="CLT-344",
)
def test_tmc_single_scan_imaging_mode(  # pylint: disable=too-many-arguments
    cbf_subarray: LowCbfSubarrayDevice,
    scan_duration_s: float,
    station_signal_generator: TmcStationSignalGenerator,
    tmc_assign_resources_json: dict[str, Any],
    tmc_central_node: LowTmcCentralNodeDevice,
    tmc_configure_json: dict[str, Any],
    tmc_csp_subarray_leaf_node: LowTmcSubarrayLeafNodeDevice,
    tmc_scan_json: dict[str, Any],
    tmc_subarray_under_test: TmcSubarrayUnderTest,
):
    tmc_central_node.telescope_on()
    tmc_central_node.assign_resources(tmc_assign_resources_json)
    tmc_subarray_under_test.configure(tmc_configure_json)

    with station_signal_generator.generate(vd_config=CNIC_VD_JSON):
        with tmc_csp_subarray_leaf_node.collect_delay_model_change_events() as delay_model_change_events:
            tmc_subarray_under_test.scan(tmc_scan_json)
            check_cbf_delays_valid(cbf_subarray, scan_duration_s)
            check_station_beam_delays(tmc_csp_subarray_leaf_node)
            tmc_subarray_under_test.wait_for_scan_end(scan_duration_s + 2)

    check_delay_model_change_events(delay_model_change_events)

    tmc_subarray_under_test.end()
    tmc_central_node.release_resources(TMC_RELEASE_RESOURCES_JSON)
