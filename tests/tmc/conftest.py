"""Test configuration shared by all TMC test packages."""

import ipaddress
from typing import Callable, Generator

import pytest

from ska_low_csp_test.cnic.devices import CnicDevice
from ska_low_csp_test.tango.tango import TangoContext
from ska_low_csp_test.tmc.devices import LowTmcCentralNodeDevice, LowTmcDevices
from ska_low_csp_test.tmc.sut import TmcSubarrayUnderTest
from ska_low_csp_test.tmc.testware import TmcStationSignalGenerator


@pytest.fixture(name="tmc_devices", scope="session")
def fxt_tmc_devices(tango_context: TangoContext) -> LowTmcDevices:
    """Fixture to create a container to reference LOW-TMC TANGO devices."""
    devices = LowTmcDevices(tango_context)
    return devices


@pytest.fixture(name="tmc_subarray_under_test")
def fxt_tmc_subarray(tmc_devices: LowTmcDevices, subarray_id: int) -> Generator[TmcSubarrayUnderTest, None, None]:
    """Fixture to retrieve the subarray under test from the LOW-TMC devices."""
    with TmcSubarrayUnderTest(subarray_device=tmc_devices.subarray_by_id(subarray_id)) as sut:
        sut.reset()
        yield sut
        sut.reset()


@pytest.fixture(name="tmc_central_node")
def fxt_tmc_central_node(tmc_devices: LowTmcDevices) -> LowTmcCentralNodeDevice:
    """Fixture to retrieve the central node from the LOW-TMC devices."""
    return tmc_devices.central_node()


@pytest.fixture(name="scan_id")
def fxt_scan_id(generate_scan_id: Callable[[str], int], config_id: str):
    """Fixture that creates a scan id."""
    return generate_scan_id(config_id)


@pytest.fixture(name="station_signal_generator")
def fxt_station_signal_generator(cnic_device: CnicDevice):
    """Fixture to create a station signal generator for TMC."""
    return TmcStationSignalGenerator(cnic=cnic_device)


@pytest.fixture(autouse=True, scope="session")
def workaround_skb_463_skb_732(default_sdp_ip: str, tmc_devices: LowTmcDevices):
    """
    SKB-463: The SDP mocks created by TMC have hard-coded receive addresses,
    which incorrectly contain host names instead of IP addresses.

    SKB-732: The SDP mocks created by TMC don't have the stride value in the
    port definition.

    While addressing SKB-463, the TMC Team introduced SKB-732 as solution to a
    MID problem.

    Either SKB above causes an issue in LOW-CBF, so we manually overwrite them
    in the mock subarrays.
    """
    for tmc_sdp_subarray_mock in tmc_devices.all_sdp_subarrays():
        receive_addresses = tmc_sdp_subarray_mock.receive_addresses
        mutate = False
        for key, val in receive_addresses.items():
            for each in val:
                if not isinstance(val[each], dict):  # nested dict vs nested list
                    continue

                # SKB-463 workaround
                for idx, (num, host) in enumerate(receive_addresses[key][each]["host"]):
                    try:
                        ipaddress.ip_address(host)
                    except ValueError:
                        mutate = True
                        receive_addresses[key][each]["host"][idx] = [num, default_sdp_ip]

                # SKB-732 workaround
                for idx, port_def in enumerate(receive_addresses[key][each]["port"]):
                    if len(port_def) == 2:
                        mutate = True
                        port_def.append(1)

        if mutate:
            tmc_sdp_subarray_mock.set_direct_receive_addresses(receive_addresses)
