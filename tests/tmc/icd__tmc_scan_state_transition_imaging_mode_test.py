# pylint:disable=missing-module-docstring,missing-function-docstring,missing-class-docstring

from typing import Any

import pytest
import tango
from ska_control_model import ObsMode, ObsState

from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.tmc.devices import LowTmcCentralNodeDevice
from ska_low_csp_test.tmc.sut import TmcSubarrayUnderTest

STATION_IDS = [345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 429, 430, 431, 432, 433, 434]  # AA1
CHANNEL_IDS = list(range(100, 108))

FULL_SCAN_DURATION_S = 60.0
SHORT_SCAN_DURATION_S = 15.0
STATION_BEAM_ID = 1
SUBSTATION_ID = 1
FSP_IDS = [1]


@pytest.fixture(name="scan_duration_s")
def fxt_scan_duration_s(shorten_scan_duration: bool) -> float:
    return SHORT_SCAN_DURATION_S if shorten_scan_duration else FULL_SCAN_DURATION_S


@pytest.fixture(name="tmc_assign_resources_json")
def fxt_tmc_assign_resources_json(eb_id: str, subarray_id: int) -> dict[str, Any]:
    return {
        "interface": "https://schema.skao.int/ska-low-tmc-assignresources/4.0",
        "subarray_id": subarray_id,
        "csp": {},
        "mccs": {  # dummy MCCS block
            "interface": "https://schema.skao.int/ska-low-mccs-controller-allocate/3.0",
            "subarray_beams": [],
        },
        "sdp": {  # dummy SDP block
            "resources": {"receive_nodes": 1},
            "execution_block": {
                "eb_id": eb_id,
                "max_length": 0.0,
                "beams": [],
                "channels": [],
                "context": {},
                "fields": [],
                "polarisations": [],
                "scan_types": [],
            },
        },
    }


@pytest.fixture(name="tmc_configure_json")
def fxt_tmc_configure_json(
    eb_id: str,
    config_id: str,
    vis_firmware_version: str,
    scan_duration_s: float,
) -> dict[str, Any]:
    return {
        "interface": "https://schema.skao.int/ska-low-tmc-configure/4.0",
        "csp": {
            "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
            "common": {
                "config_id": config_id,
                "eb_id": eb_id,
            },
            "lowcbf": {
                "stations": {
                    "stns": [[station, SUBSTATION_ID] for station in STATION_IDS],
                    "stn_beams": [
                        {
                            "beam_id": STATION_BEAM_ID,
                            "freq_ids": CHANNEL_IDS,
                        }
                    ],
                },
                "vis": {
                    "fsp": {"firmware": vis_firmware_version, "fsp_ids": FSP_IDS},
                    "stn_beams": [
                        {
                            "stn_beam_id": STATION_BEAM_ID,
                            "integration_ms": 849,
                        }
                    ],
                },
            },
        },
        "mccs": {
            "subarray_beams": [
                {
                    "subarray_beam_id": STATION_BEAM_ID,
                    "update_rate": 0.0,
                    "logical_bands": [],
                    "apertures": [],
                    "sky_coordinates": {
                        "reference_frame": "ICRS",
                        "c1": 180.0,
                        "c2": 50.0,
                    },
                }
            ],
        },
        "sdp": {
            "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
            "scan_type": "target:a",
        },
        "tmc": {
            "scan_duration": scan_duration_s,
        },
    }


@pytest.fixture(name="tmc_scan_json")
def fxt_tmc_scan_json(scan_id: int):
    return {
        "interface": "https://schema.skao.int/ska-tmc-scan/4.0",
        "scan_id": scan_id,
    }


@pytest.fixture(name="tmc_release_resources_json")
def fxt_tmc_release_resources_json(subarray_id: int):
    return {
        "interface": "https://schema.skao.int/ska-tmc-releaseresources/3.0",
        "subarray_id": subarray_id,
        "release_all": True,
    }


@test_case(
    title="TMC scan state transition in IMAGING mode",
    suite=TestSuite.TELESCOPE_MONITORING_AND_CONTROL,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1161505?projectId=335",
    xray_id="CLT-343",
    known_defects=["SKB-678"],
)
def test_tmc_scan_state_transition_imaging_mode(  # pylint: disable=too-many-arguments
    scan_duration_s: float,
    tmc_subarray_under_test: TmcSubarrayUnderTest,
    tmc_central_node: LowTmcCentralNodeDevice,
    tmc_assign_resources_json: dict[str, Any],
    tmc_configure_json: dict[str, Any],
    tmc_scan_json: dict[str, Any],
    tmc_release_resources_json: dict[str, Any],
):
    tmc_central_node.telescope_on()
    assert tmc_central_node.state == tango.DevState.ON
    tmc_subarray_under_test.verify_state(tango.DevState.ON)

    tmc_central_node.assign_resources(tmc_assign_resources_json)
    tmc_subarray_under_test.verify_obs_state(ObsState.IDLE)

    tmc_subarray_under_test.configure(tmc_configure_json)
    tmc_subarray_under_test.verify_obs_state(ObsState.READY)
    tmc_subarray_under_test.verify_obs_mode(ObsMode.IMAGING)

    tmc_subarray_under_test.scan(tmc_scan_json)
    tmc_subarray_under_test.verify_obs_state(ObsState.SCANNING)
    tmc_subarray_under_test.verify_obs_mode(ObsMode.IMAGING)

    tmc_subarray_under_test.wait_for_scan_end(scan_duration_s + 2)

    tmc_subarray_under_test.verify_obs_state(ObsState.READY)
    tmc_subarray_under_test.verify_obs_mode(ObsMode.IMAGING)

    tmc_subarray_under_test.end()
    tmc_subarray_under_test.verify_obs_state(ObsState.IDLE)
    tmc_subarray_under_test.verify_obs_mode(ObsMode.IDLE)

    tmc_central_node.release_resources(tmc_release_resources_json)
    tmc_subarray_under_test.verify_obs_state(ObsState.EMPTY)
    tmc_subarray_under_test.verify_obs_mode(ObsMode.IDLE)

    tmc_central_node.telescope_off()
    assert tmc_central_node.state == tango.DevState.ON
