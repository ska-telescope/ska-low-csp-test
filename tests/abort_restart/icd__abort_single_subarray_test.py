# pylint:disable=missing-module-docstring
# pylint:disable=missing-function-docstring
# pylint:disable=missing-class-docstring

from typing import Protocol

import pytest
from ska_control_model import ObsState

from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest, SystemUnderTest

STATION_BEAM_ID = 1

SUBARRAY_CONFIGS = {
    2: {
        "stations": [(345, 1), (346, 1)],
        "frequency_ids": list(range(141, 149)),
        "fsp_ids": [1],
    },
    4: {
        "stations": [(433, 1), (434, 1)],
        "frequency_ids": list(range(150, 158)),
        "fsp_ids": [2],
    },
}


class GenerateAssignResourcesJson(Protocol):
    def __call__(self, subarray_id: int) -> LowCspAssignResourcesSchema:
        ...


class GenerateConfigureJson(Protocol):
    def __call__(
        self,
        subarray_id: int,
        stations: list[tuple[int, int]],
        frequency_ids: list[int],
        fsp_ids: list[int],
    ) -> LowCspConfigureSchema:
        ...


class GenerateScanJson(Protocol):
    def __call__(self, subarray_id: int) -> LowCspScanSchema:
        ...


@pytest.fixture(name="generate_assign_resources_json")
def fxt_generate_assign_resources_json() -> GenerateAssignResourcesJson:
    def generate(subarray_id: int) -> LowCspAssignResourcesSchema:
        return {
            "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.2",
            "common": {
                "subarray_id": subarray_id,
            },
            "lowcbf": {},
        }

    return generate


@pytest.fixture(name="generate_configure_json")
def fxt_generate_configure_json(
    vis_firmware_version: str,
    default_sdp_ip: str,
) -> GenerateConfigureJson:
    def generate(
        subarray_id: int,
        stations: list[tuple[int, int]],
        frequency_ids: list[int],
        fsp_ids: list[int],
    ) -> LowCspConfigureSchema:
        return {
            "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
            "subarray": {
                "subarray_name": "ITC.L.AA0.5.NB Configure without available resources",
            },
            "common": {
                "config_id": "",
                "subarray_id": subarray_id,
                "eb_id": "eb-x321-20240111-10012",
            },
            "lowcbf": {
                "stations": {
                    "stns": stations,
                    "stn_beams": [
                        {
                            "beam_id": STATION_BEAM_ID,
                            "freq_ids": frequency_ids,
                            "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, STATION_BEAM_ID),
                        },
                    ],
                },
                "vis": {
                    "fsp": {
                        "firmware": vis_firmware_version,
                        "fsp_ids": fsp_ids,
                    },
                    "stn_beams": [
                        {
                            "stn_beam_id": 1,
                            "host": [(0, default_sdp_ip)],
                            "port": [(0, 20000, 1)],
                            "integration_ms": 849,
                        },
                    ],
                },
            },
        }

    return generate


@pytest.fixture(name="generate_scan_json")
def fxt_generate_scan_json(scan_id: int) -> GenerateScanJson:
    """
    Fixture that creates a default JSON payload for the Scan command.
    """

    def generate(subarray_id: int) -> LowCspScanSchema:
        return {
            "interface": "https://schema.skao.int/ska-low-csp-scan/3.2",
            "common": {
                "subarray_id": subarray_id,
            },
            "lowcbf": {
                "scan_id": scan_id,
            },
        }

    return generate


@pytest.fixture(name="subarray_2")
def fxt_subarray_2(system_under_test: SystemUnderTest):
    with system_under_test.subarray(2) as subarray:
        subarray.reset()
        yield subarray
        subarray.reset()


@pytest.fixture(name="subarray_4")
def fxt_subarray_4(system_under_test: SystemUnderTest):
    with system_under_test.subarray(4) as subarray:
        subarray.reset()
        yield subarray
        subarray.reset()


@pytest.fixture(name="scanning_subarrays")
def fxt_test_setup(
    subarray_2: SubarrayUnderTest,
    subarray_4: SubarrayUnderTest,
    generate_assign_resources_json: GenerateAssignResourcesJson,
    generate_configure_json: GenerateConfigureJson,
    generate_scan_json: GenerateScanJson,
):
    for subarray in [subarray_2, subarray_4]:
        subarray_config = SUBARRAY_CONFIGS[subarray.id]
        assign_resources_json = generate_assign_resources_json(subarray.id)
        configure_json = generate_configure_json(
            subarray_id=subarray.id,
            stations=subarray_config["stations"],
            frequency_ids=subarray_config["frequency_ids"],
            fsp_ids=subarray_config["fsp_ids"],
        )
        scan_json = generate_scan_json(subarray.id)
        subarray.assign_resources(assign_resources_json)
        subarray.configure(configure_json)
        subarray.scan(scan_json)

        # This check is technically not needed, but required in order to flush the
        # queue of captured ObsState transitions so that it starts out empty in the test function.
        subarray.verify_obs_state_transitions(
            ObsState.RESOURCING,
            ObsState.IDLE,
            ObsState.CONFIGURING,
            ObsState.READY,
            ObsState.SCANNING,
        )

    return subarray_2, subarray_4


@test_case(
    title="Abort single subarray while multiple subarrays are configured",
    suite=TestSuite.ABORT_RESTART,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1190201?projectId=335",
    xray_id=None,
)
def test_abort_single_subarray_while_multiple_are_configured(scanning_subarrays: tuple[SubarrayUnderTest, SubarrayUnderTest]):
    subarray_2, subarray_4 = scanning_subarrays

    subarray_4.abort()
    subarray_4.verify_obs_state(ObsState.ABORTED)
    subarray_4.verify_obs_state_transitions(ObsState.ABORTING, ObsState.ABORTED)
    subarray_2.verify_obs_state(ObsState.SCANNING)

    subarray_4.restart()
    subarray_4.verify_obs_state(ObsState.EMPTY)
    subarray_4.verify_obs_state_transitions(ObsState.RESTARTING, ObsState.EMPTY)
    subarray_2.verify_obs_state(ObsState.SCANNING)
