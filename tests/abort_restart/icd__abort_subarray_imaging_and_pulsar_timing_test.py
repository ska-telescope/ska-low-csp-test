# pylint:disable=missing-module-docstring,missing-function-docstring

import pytest
from ska_control_model import ObsMode, ObsState

from ska_low_csp_test.cbf.helpers import (
    cbf_station_beam_delay_poly_uri,
    cbf_timing_beam_delay_poly_uri,
    cbf_timing_beam_jones_uri,
)
from ska_low_csp_test.domain.channels import coarse_channel_range_center_frequency, coarse_channel_range_total_bandwidth
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest

VIS_FSP_IDS = [1]
PST_FSP_IDS = [2]

SUBARRAY_ID = 3
STATIONS = [(345, 1), (346, 1), (347, 1), (348, 1), (349, 1), (350, 1), (351, 1), (352, 1)]
STATION_BEAM_ID = 1
FREQUENCY_IDS = list(range(292, 308))
PST_BEAM_ID = 1


@pytest.fixture(name="subarray_id")
def fxt_subarray_id() -> int:
    """Fixture controlling the identifier for the subarray under test."""
    return SUBARRAY_ID


@pytest.fixture(name="assign_resources_json")
def fxt_assign_resources_json() -> LowCspAssignResourcesSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.2",
        "common": {
            "subarray_id": SUBARRAY_ID,
        },
        "lowcbf": {},
        "pst": {
            "beams_id": [PST_BEAM_ID],
        },
    }


@pytest.fixture(name="configure_json")
def fxt_configure_json(
    config_id: str,
    eb_id: str,
    vis_firmware_version: str,
    pst_firmware_version: str,
    default_sdp_ip: str,
) -> LowCspConfigureSchema:
    """
    Fixture that creates a JSON payload for the Configure command including both IMAGING and PST beam configuration.
    """
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "science period 23",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": SUBARRAY_ID,
            "eb_id": eb_id,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID),
                    }
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": VIS_FSP_IDS,
                },
                "stn_beams": [
                    {
                        "stn_beam_id": STATION_BEAM_ID,
                        "host": [(0, default_sdp_ip)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    }
                ],
            },
            "timing_beams": {
                "fsp": {
                    "firmware": pst_firmware_version,
                    "fsp_ids": PST_FSP_IDS,
                },
                "beams": [
                    {
                        "pst_beam_id": PST_BEAM_ID,
                        "stn_beam_id": STATION_BEAM_ID,
                        "stn_weights": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                        "delay_poly": cbf_timing_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID, PST_BEAM_ID),
                        "jones": cbf_timing_beam_jones_uri(),
                    },
                ],
            },
        },
        "pst": {
            "beams": [
                {
                    "beam_id": PST_BEAM_ID,
                    "scan": {
                        "activation_time": "2022-01-19T23:07:45Z",
                        "bits_per_sample": 32,
                        "num_of_polarizations": 2,
                        "udp_nsamp": 32,
                        "wt_nsamp": 32,
                        "udp_nchan": 24,
                        "num_frequency_channels": len(FREQUENCY_IDS) * 216,
                        "centre_frequency": coarse_channel_range_center_frequency(FREQUENCY_IDS[0], FREQUENCY_IDS[-1]),
                        "total_bandwidth": coarse_channel_range_total_bandwidth(FREQUENCY_IDS[0], FREQUENCY_IDS[-1]),
                        "observation_mode": "VOLTAGE_RECORDER",
                        "observer_id": "jdoe",
                        "project_id": "project1",
                        "pointing_id": "pointing1",
                        "source": "J1921+2153",
                        "itrf": [5109360.133, 2006852.586, -3238948.127],
                        "receiver_id": "receiver3",
                        "feed_polarization": "LIN",
                        "feed_handedness": 1,
                        "feed_angle": 1.234,
                        "feed_tracking_mode": "FA",
                        "feed_position_angle": 10.0,
                        "oversampling_ratio": [4, 3],
                        "coordinates": {
                            "equinox": 2000.0,
                            "ra": "10h00m01.00000000s",
                            "dec": "12h00m02.00000000s",
                        },
                        "max_scan_length": 30.0,
                        "subint_duration": 30.0,
                        "receptors": ["receptor1", "receptor2"],
                        "receptor_weights": [0.4, 0.6],
                        "num_channelization_stages": 1,
                        "channelization_stages": [],
                    },
                },
            ],
        },
    }


@pytest.fixture(name="configured_subarray")
def fxt_test_setup(
    subarray_under_test: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
) -> SubarrayUnderTest:
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)
    subarray_under_test.verify_obs_state(ObsState.READY)
    subarray_under_test.verify_obs_mode(ObsMode.IMAGING, ObsMode.PULSAR_TIMING)

    # This check is technically not needed, but required in order to flush the
    # queue of captured ObsState transitions so that it starts out empty in the test function.
    subarray_under_test.verify_obs_state_transitions(
        ObsState.RESOURCING,
        ObsState.IDLE,
        ObsState.CONFIGURING,
        ObsState.READY,
    )
    return subarray_under_test


@test_case(
    title="Abort subarray with both IMAGING and PULSAR_TIMING modes configured",
    suite=TestSuite.ABORT_RESTART,
    jama_url="https://skaoffice.jamacloud.com/perspective.req?docId=1190211&projectId=335",
    xray_id=None,
)
def test_abort_subarray_with_both_imaging_and_pulsar_timing_modes_configured(
    configured_subarray: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
):
    configured_subarray.abort()
    configured_subarray.verify_obs_state(ObsState.ABORTED)
    configured_subarray.verify_obs_state_transitions(ObsState.ABORTING, ObsState.ABORTED)

    configured_subarray.restart()
    configured_subarray.verify_obs_state(ObsState.EMPTY)
    configured_subarray.verify_obs_state_transitions(ObsState.RESTARTING, ObsState.EMPTY)

    configured_subarray.assign_resources(assign_resources_json)
    configured_subarray.verify_obs_state(ObsState.IDLE)

    configured_subarray.configure(configure_json)
    configured_subarray.verify_obs_state(ObsState.READY)
    configured_subarray.verify_obs_mode(ObsMode.IMAGING, ObsMode.PULSAR_TIMING)
