# pylint:disable=missing-module-docstring
# pylint:disable=missing-function-docstring

import pytest
from ska_control_model import ObsState

from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest


@pytest.fixture(name="subarray_id")
def fxt_subarray_id():
    return 2


@pytest.fixture(name="configuring_subarray")
def fxt_configuring_subarray(
    subarray_under_test: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
):
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(
        configure_json,
        wait_for_completion=False,
        wait_for_obs_state=ObsState.CONFIGURING,
    )

    subarray_under_test.verify_obs_state(ObsState.CONFIGURING)

    # This check is technically not needed, but required in order to flush the
    # queue of captured ObsState transitions so that it starts out empty in the test function.
    subarray_under_test.verify_obs_state_transitions(
        ObsState.RESOURCING,
        ObsState.IDLE,
        ObsState.CONFIGURING,
    )

    return subarray_under_test


@test_case(
    title="Abort subarray from CONFIGURING state",
    suite=TestSuite.ABORT_RESTART,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1192371?projectId=335",
    xray_id=None,
)
def test_abort_subarray_while_configuring(
    configuring_subarray: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
):
    configuring_subarray.abort()
    configuring_subarray.verify_obs_state(ObsState.ABORTED)
    configuring_subarray.verify_obs_state_transitions(ObsState.ABORTING, ObsState.ABORTED)

    configuring_subarray.restart()
    configuring_subarray.verify_obs_state(ObsState.EMPTY)
    configuring_subarray.verify_obs_state_transitions(ObsState.RESTARTING, ObsState.EMPTY)

    configuring_subarray.assign_resources(assign_resources_json)
    configuring_subarray.verify_obs_state(ObsState.IDLE)
    configuring_subarray.verify_obs_state_transitions(ObsState.RESOURCING, ObsState.IDLE)
