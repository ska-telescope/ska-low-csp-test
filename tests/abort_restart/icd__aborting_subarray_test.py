# pylint:disable=missing-module-docstring,missing-function-docstring

import pytest
import tango
from ska_control_model import ObsState

from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest


@test_case(
    title="ICD Abort subarray after configuration",
    suite=TestSuite.ABORT_RESTART,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1117824?projectId=335",
    xray_id="CLT-22",
)
def test_abort_subarray_after_configuration(
    subarray_under_test: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
):
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.verify_obs_state(ObsState.IDLE)
    subarray_under_test.verify_obs_state_transitions(ObsState.RESOURCING, ObsState.IDLE)

    subarray_under_test.configure(configure_json)
    subarray_under_test.verify_obs_state(ObsState.READY)
    subarray_under_test.verify_obs_state_transitions(ObsState.CONFIGURING, ObsState.READY)

    subarray_under_test.abort()
    subarray_under_test.verify_obs_state(ObsState.ABORTED)
    subarray_under_test.verify_obs_state_transitions(ObsState.ABORTING, ObsState.ABORTED)

    subarray_under_test.restart()
    subarray_under_test.verify_obs_state(ObsState.EMPTY)
    subarray_under_test.verify_obs_state_transitions(ObsState.RESTARTING, ObsState.EMPTY)


@test_case(
    title="ICD Abort subarray during scan",
    suite=TestSuite.ABORT_RESTART,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1141257?projectId=335",
    xray_id="CLT-83",
)
def test_abort_subarray_during_scan(
    subarray_under_test: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
    scan_json: LowCspScanSchema,
):
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.verify_obs_state(ObsState.IDLE)
    subarray_under_test.verify_obs_state_transitions(ObsState.RESOURCING, ObsState.IDLE)

    subarray_under_test.configure(configure_json)
    subarray_under_test.verify_obs_state(ObsState.READY)
    subarray_under_test.verify_obs_state_transitions(ObsState.CONFIGURING, ObsState.READY)

    subarray_under_test.scan(scan_json)
    subarray_under_test.verify_obs_state(ObsState.SCANNING)
    subarray_under_test.verify_obs_state_transitions(ObsState.SCANNING)

    subarray_under_test.abort()
    subarray_under_test.verify_obs_state(ObsState.ABORTED)
    subarray_under_test.verify_obs_state_transitions(ObsState.ABORTING, ObsState.ABORTED)

    subarray_under_test.restart()
    subarray_under_test.verify_obs_state(ObsState.EMPTY)
    subarray_under_test.verify_obs_state_transitions(ObsState.RESTARTING, ObsState.EMPTY)

    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.verify_obs_state(ObsState.IDLE)
    subarray_under_test.verify_obs_state_transitions(ObsState.RESOURCING, ObsState.IDLE)

    subarray_under_test.configure(configure_json)
    subarray_under_test.verify_obs_state(ObsState.READY)
    subarray_under_test.verify_obs_state_transitions(ObsState.CONFIGURING, ObsState.READY)

    subarray_under_test.scan(scan_json)
    subarray_under_test.verify_obs_state(ObsState.SCANNING)
    subarray_under_test.verify_obs_state_transitions(ObsState.SCANNING)


@test_case(
    title="Abort subarray from EMPTY state",
    suite=TestSuite.ABORT_RESTART,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1137419?projectId=335",
    xray_id="CLT-89",
)
def test_abort_subarray_from_empty_state(subarray_under_test: SubarrayUnderTest):
    with pytest.raises(tango.DevFailed) as er:
        subarray_under_test.abort()
    expected_error_msg = "ska_tango_base.faults.StateModelError: Abort command not permitted in observation state EMPTY\n"
    actual_error_msg = er.value.args[0].desc
    assert expected_error_msg == actual_error_msg
    subarray_under_test.verify_obs_state(ObsState.EMPTY)
