# pylint:disable=missing-module-docstring
# pylint:disable=missing-function-docstring

import contextlib

import allure
import pytest
from ska_control_model import AdminMode, ObsState

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest


@pytest.fixture(name="subarray_id")
def fxt_subarray_id():
    return 3


@pytest.fixture(autouse=True)
def move_processors_to_engineering_mode(low_cbf_devices: LowCbfDevices):
    processors = low_cbf_devices.all_processors()
    admin_mode_cache = {}

    for processor in processors:
        admin_mode_cache[processor.fqdn] = processor.admin_mode
        with allure.step(f"Set adminMode of {processor.fqdn} to ENGINEERING"):
            processor.admin_mode = AdminMode.ENGINEERING

    yield

    for processor in processors:
        admin_mode = admin_mode_cache[processor.fqdn]
        with allure.step(f"Revert adminMode of {processor.fqdn} to {admin_mode.name}"):
            processor.admin_mode = admin_mode


@pytest.fixture(name="faulty_subarray")
def fxt_faulty_subarray(
    subarray_under_test: SubarrayUnderTest,
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
):
    subarray_under_test.assign_resources(assign_resources_json)

    with contextlib.suppress(RuntimeError):
        subarray_under_test.configure(configure_json)

    subarray_under_test.verify_obs_state(ObsState.FAULT)

    # This check is technically not needed, but required in order to flush the
    # queue of captured ObsState transitions so that it starts out empty in the test function.
    subarray_under_test.verify_obs_state_transitions(
        ObsState.RESOURCING,
        ObsState.READY,
        ObsState.CONFIGURING,
        ObsState.FAULT,
    )


@test_case(
    title="Restart subarray from FAULT state",
    suite=TestSuite.ABORT_RESTART,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1192481?projectId=335",
    xray_id=None,
)
def test_restart_subarray_from_fault_state(
    faulty_subarray: SubarrayUnderTest,
):
    faulty_subarray.restart()
    faulty_subarray.verify_obs_state(ObsState.EMPTY)
    faulty_subarray.verify_obs_state_transitions(ObsState.RESTARTING, ObsState.EMPTY)
