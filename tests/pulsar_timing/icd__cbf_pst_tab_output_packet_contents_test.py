# pylint:disable=missing-module-docstring,missing-function-docstring,missing-class-docstring

from dataclasses import dataclass
from datetime import datetime

import allure
import pytest
from astropy import units as u
from astropy.coordinates import AltAz
from pytest_check import check

from ska_low_csp_test.cbf.helpers import (
    cbf_station_beam_delay_poly_uri,
    cbf_timing_beam_delay_poly_uri,
    cbf_timing_beam_jones_uri,
)
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema, CnicPulseConfigSchema
from ska_low_csp_test.delaypoly.devices import DelaypolyDevice, DelaypolyRaDecSchema
from ska_low_csp_test.delaypoly.helpers import sky_coord_to_ra_dec
from ska_low_csp_test.domain import sky_coordinates_from_altaz
from ska_low_csp_test.domain.channels import coarse_channel_range_center_frequency, coarse_channel_range_total_bandwidth
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.psr import PSR_MAGIC, PsrDataReader, PsrDataVisitor, PsrPacket, PsrPacketDestination
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import PsrOutputCapture, StationSignalGenerator

BEAM_DIRECTION = AltAz(alt=50 * u.deg, az=10 * u.deg)
FREQUENCY_IDS = [397, 398, 399, 400, 401, 402, 403, 404]
FSP_IDS = [1]
PST_BEAM_ID = 1
SCAN_DURATION_S = 10.0
SCAN_ID = 5453
STATION_BEAM_ID = 1
STATIONS = [(345, 1), (434, 1)]
STATION_WEIGHTS = [1.0] * len(STATIONS)
SUBARRAY_ID = 1

EXPECTED_SCAN_ID = SCAN_ID
EXPECTED_BEAM_NUMBER = PST_BEAM_ID
EXPECTED_PERIOD_NUMERATOR = 5184
EXPECTED_PERIOD_DENOMINATOR = 25
EXPECTED_PACKET_DESTINATION = PsrPacketDestination.LOW_PST
EXPECTED_BIT_SIZES = [8, 16]
EXPECTED_MAGIC_NUMBER = PSR_MAGIC

MIN_CHANNEL_SEPERATION = 3.6e6 - (3.6e6 / 2)  # 3.6kHz lower boundary
MAX_CHANNEL_SEPERATION = 3.6e6 + (3.6e6 / 2)  # 3.6kHz upper boundary
MAX_CHANNEL_NUMBER = 82943
MAX_CHANNELS_PER_PACKET = 24
MAX_NOF_TIME_SAMPLES = 32
MAX_NOF_TIME_SAMPLES_PER_RELATIVE_WEIGHT = 32


@pytest.fixture(name="subarray_id")
def fxt_subarray_id():
    return SUBARRAY_ID


@pytest.fixture(name="scan_id")
def fxt_scan_id():
    return SCAN_ID


@pytest.fixture(name="assign_resources_json")
def fxt_assign_resources_json() -> LowCspAssignResourcesSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.2",
        "common": {
            "subarray_id": SUBARRAY_ID,
        },
        "lowcbf": {},
        "pst": {
            "beams_id": [PST_BEAM_ID],
        },
    }


@pytest.fixture(name="beam_direction")
def fxt_beam_direction_ra_dec(observation_time: datetime) -> DelaypolyRaDecSchema:
    _, direction = sky_coordinates_from_altaz(BEAM_DIRECTION, time=observation_time)
    return sky_coord_to_ra_dec(direction)


@pytest.fixture(name="configure_json")
def fxt_configure_json(
    config_id: str,
    pst_firmware_version: str,
    eb_id: str,
    beam_direction: DelaypolyRaDecSchema,
) -> LowCspConfigureSchema:
    pst_beam_channel_count = len(FREQUENCY_IDS) * 216
    pst_beam_centre_frequency = coarse_channel_range_center_frequency(FREQUENCY_IDS[0], FREQUENCY_IDS[-1])
    pst_beam_total_bandwidth = coarse_channel_range_total_bandwidth(FREQUENCY_IDS[0], FREQUENCY_IDS[-1])

    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "science period 23",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": SUBARRAY_ID,
            "eb_id": eb_id,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID),
                    },
                ],
            },
            "timing_beams": {
                "fsp": {"firmware": pst_firmware_version, "fsp_ids": FSP_IDS},
                "beams": [
                    {
                        "pst_beam_id": PST_BEAM_ID,
                        "stn_beam_id": STATION_BEAM_ID,
                        "stn_weights": STATION_WEIGHTS,
                        "delay_poly": cbf_timing_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID, PST_BEAM_ID),
                        "jones": cbf_timing_beam_jones_uri(),
                    },
                ],
            },
        },
        "pst": {
            "beams": [
                {
                    "beam_id": PST_BEAM_ID,
                    "scan": {
                        "activation_time": "2022-01-19T23:07:45Z",
                        "bits_per_sample": 32,
                        "num_of_polarizations": 2,
                        "udp_nsamp": 32,
                        "wt_nsamp": 32,
                        "udp_nchan": 24,
                        "num_frequency_channels": pst_beam_channel_count,
                        "centre_frequency": pst_beam_centre_frequency,
                        "total_bandwidth": pst_beam_total_bandwidth,
                        "observation_mode": "VOLTAGE_RECORDER",
                        "observer_id": "jdoe",
                        "project_id": "project1",
                        "pointing_id": "pointing1",
                        "source": "J1921+2153",
                        "itrf": [5109360.133, 2006852.586, -3238948.127],
                        "receiver_id": "receiver3",
                        "feed_polarization": "LIN",
                        "feed_handedness": 1,
                        "feed_angle": 1.234,
                        "feed_tracking_mode": "FA",
                        "feed_position_angle": 10.0,
                        "oversampling_ratio": [4, 3],
                        "coordinates": {
                            "equinox": 2000.0,
                            **beam_direction,
                        },
                        "max_scan_length": max(SCAN_DURATION_S, 30.0),  # The schema specifies a range with a minimum of 30
                        "subint_duration": 30.0,
                        "receptors": ["receptor1", "receptor2"],
                        "receptor_weights": [0.4, 0.6],
                        "num_channelization_stages": 1,
                        "channelization_stages": [],
                    },
                },
            ],
        },
    }


@pytest.fixture(name="cnic_vd_config")
def fxt_cnic_vd_config() -> CnicConfigureVirtualDigitiserSchema:
    tone_channel_id = 400

    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": SCAN_ID,  # disregarded
                "subarray": SUBARRAY_ID,
                "station": station_id,
                "substation": substation_id,
                "frequency": channel_id,
                "beam": STATION_BEAM_ID,
                "sources": {
                    "x": [
                        {"tone": False, "seed": station_id, "scale": 4000},
                        {
                            "tone": channel_id == tone_channel_id,
                            "scale": 4 if channel_id == tone_channel_id else 0,
                        },
                    ],
                    "y": [
                        {"tone": False, "seed": station_id, "scale": 4000},
                        {
                            "tone": channel_id == tone_channel_id,
                            "scale": 4 if channel_id == tone_channel_id else 0,
                        },
                    ],
                },
            }
            for station_id, substation_id in STATIONS
            for channel_id in FREQUENCY_IDS
        ],
    }


@pytest.fixture(name="cnic_pulse_config")
def fxt_cnic_pulse_config() -> CnicPulseConfigSchema:
    return {
        "enable": True,
        "sample_count": [2048, 9216, 9216],
    }


@pytest.fixture(name="cbf_pst_tab_output_path")
def fxt_cbf_pst_tab_output(  # pylint: disable=too-many-arguments
    assign_resources_json: LowCspAssignResourcesSchema,
    beam_direction: DelaypolyRaDecSchema,
    cnic_pulse_config: CnicPulseConfigSchema,
    cnic_vd_config: CnicConfigureVirtualDigitiserSchema,
    configure_json: LowCspConfigureSchema,
    delaypoly_device: DelaypolyDevice,
    observation_time: datetime,
    psr_output_capture: PsrOutputCapture,
    scan_json: LowCspScanSchema,
    station_signal_generator: StationSignalGenerator,
    subarray_under_test: SubarrayUnderTest,
):
    capture_file_name = f"{observation_time}_cbf_pst_tab_output_packet_contents.pcap"
    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)

    delaypoly_device.beam_ra_dec(
        {
            "subarray_id": SUBARRAY_ID,
            "beam_id": STATION_BEAM_ID,
            "direction": beam_direction,
        }
    )
    delaypoly_device.source_ra_dec(
        {
            "subarray_id": SUBARRAY_ID,
            "beam_id": STATION_BEAM_ID,
            "direction": [beam_direction] * 4,
        }
    )
    delaypoly_device.pst_ra_dec(
        {
            "subarray_id": SUBARRAY_ID,
            "beam_id": STATION_BEAM_ID,
            "direction": [beam_direction],
        }
    )

    with psr_output_capture.capture(beam_ids=[PST_BEAM_ID], destination_file=capture_file_name) as capture_session:
        capture_file_path = capture_session.destination_file_path

        with station_signal_generator.generate(vd_config=cnic_vd_config, pulse_config=cnic_pulse_config):
            subarray_under_test.scan(scan_json)
            capture_session.monitor(duration_s=SCAN_DURATION_S)
            subarray_under_test.end_scan()

    subarray_under_test.end()
    subarray_under_test.release_all_resources()

    return capture_file_path


class CheckPacketContents(PsrDataVisitor):
    @dataclass
    class Assertion:
        allure_step: str
        outcome: bool
        failure_message: str

    _assertions: list[Assertion] = []
    previous_samples_since_ska_epoch = 0
    previous_packet_sequence_number = 0

    def _assert(self, allure_step: str, outcome: bool, failure_message: str):
        try:
            assertion = next(each for each in self._assertions if each.allure_step == allure_step)
            assertion.outcome &= outcome
        except StopIteration:
            assertion = self.Assertion(allure_step, outcome, failure_message)
            self._assertions.append(assertion)

    def visit_packet(self, index: int, packet: PsrPacket):
        self._assert(
            "Verify that for each packet the sequence number linearly increases",
            packet.packet_sequence_number
            in [
                self.previous_packet_sequence_number,
                self.previous_packet_sequence_number + 1,
            ],
            "The packet sequence number should linearly increase",
        )
        self.previous_packet_sequence_number = packet.packet_sequence_number

        self._assert(
            "Verify that for each packet the SKA epoch increases over time",
            self.previous_samples_since_ska_epoch <= packet.samples_since_ska_epoch,
            "The sample since SKA epoch should increase over time",
        )
        self.previous_samples_since_ska_epoch = packet.samples_since_ska_epoch

        self._assert(
            f"Verify that for each packet the period numerator is equal to {EXPECTED_PERIOD_NUMERATOR}",
            packet.period_numerator == EXPECTED_PERIOD_NUMERATOR,
            f"The period numerator should be {EXPECTED_PERIOD_NUMERATOR} but was {packet.period_numerator}",
        )
        self._assert(
            f"Verify that for each packet the period denominator is equal to {EXPECTED_PERIOD_DENOMINATOR}",
            packet.period_denominator == EXPECTED_PERIOD_DENOMINATOR,
            f"The period denominator should be {EXPECTED_PERIOD_DENOMINATOR} but was {packet.period_denominator}",
        )
        self._assert(
            f"Verify that for each packet the channel seperation is bewteen"
            f" {MIN_CHANNEL_SEPERATION} and {MAX_CHANNEL_SEPERATION}",
            MIN_CHANNEL_SEPERATION < packet.channel_separation_mHz < MAX_CHANNEL_SEPERATION,
            f"The channel seperat should be between {MIN_CHANNEL_SEPERATION} and {MAX_CHANNEL_NUMBER}"
            f" but was {packet.channel_separation_mHz}",
        )
        self._assert(
            "Verify that each packet contains a first channel frequency",
            packet.first_channel_frequency_mHz != 0,
            f"The first channel frequency should be present and not be 0 but was {packet.first_channel_frequency_mHz}",
        )
        self._assert(
            f"Verify that for each packet the data precision is one of {EXPECTED_BIT_SIZES}",
            packet.data_precision in EXPECTED_BIT_SIZES,
            f"The data precision should be one of {EXPECTED_BIT_SIZES} but was {packet.data_precision}",
        )
        self._assert(
            "Verify that for each packet the scales 2,3 and 4 are all 0",
            packet.scale_2 == packet.scale_3 == packet.scale_4 == 0,
            f"Scales 2, 3 and 4 should be 0 but were {packet.scale_2}, {packet.scale_3} and {packet.scale_4} respectively",
        )
        self._assert(
            "Verify that for each packet the offsets are all 0",
            packet.offset_1 == packet.offset_2 == packet.offset_3 == packet.offset_4 == 0,
            f"The offsets should be 0 but were"
            f" {packet.offset_1}, {packet.offset_2}, {packet.offset_3} and {packet.offset_4} respectively",
        )
        self._assert(
            f"Verify that for each packet the first channel number is between 0 and {MAX_CHANNEL_NUMBER}",
            0 < packet.first_channel_number <= MAX_CHANNEL_NUMBER,
            f"The first channel number should be between 0 and {MAX_CHANNEL_NUMBER} but was {packet.first_channel_number}",
        )
        self._assert(
            f"Verify that for each packet the channels per packet is between 0 and {MAX_CHANNELS_PER_PACKET}",
            0 < packet.channels_per_packet <= MAX_CHANNELS_PER_PACKET,
            f"Channels per packet should be between 0 and {MAX_CHANNELS_PER_PACKET} but was {packet.channels_per_packet}",
        )
        self._assert(
            "Verify that for each packet the valid channels per packet between 0 and channels per packet",
            0 <= packet.valid_channels_per_packet <= packet.channels_per_packet,
            f"Valid channels per packet should be between 0 and {packet.channels_per_packet}"
            f" but was {packet.valid_channels_per_packet}",
        )
        self._assert(
            f"Verify that for each packet the numer of time samples is between 0 and {MAX_NOF_TIME_SAMPLES}",
            0 < packet.nof_time_samples <= MAX_NOF_TIME_SAMPLES,
            f"The number of time samples should be between 0 and {MAX_NOF_TIME_SAMPLES} but was {packet.nof_time_samples}",
        )
        self._assert(
            f"Verify that for each packet the beam number is equal to {EXPECTED_BEAM_NUMBER}",
            packet.beam_number == EXPECTED_BEAM_NUMBER,
            f"The beam number should be {EXPECTED_BEAM_NUMBER} but was {packet.beam_number}",
        )
        self._assert(
            f"Verify that for each packet the destination is equal to {EXPECTED_PACKET_DESTINATION}",
            packet.packet_destination == EXPECTED_PACKET_DESTINATION,
            f"The packet destination should be {EXPECTED_PACKET_DESTINATION} but was {packet.packet_destination}",
        )
        self._assert(
            "Verify that each packet contains the number of power samples averaged",
            packet.nof_power_samples_averaged is not None,
            f"The number of power samples averaged should be present in the packet but was {None}",
        )
        self._assert(
            "Verify that for each packet the reserved value is equal to 0",
            packet.reserved == 0,
            f"The reserved value should be 0 but was {packet.reserved}",
        )
        self._assert(
            "Verify that for each packet the beamformer version is not equal to 0",
            packet.beamformer_version != 0,
            f"The beamformer version should have a non zero value but was {packet.beamformer_version}",
        )
        self._assert(
            f"Verify that for each packet the magic number is equal to {EXPECTED_MAGIC_NUMBER}",
            packet.magic_number == EXPECTED_MAGIC_NUMBER,
            f"The magic number should be {EXPECTED_MAGIC_NUMBER} but was {packet.magic_number}",
        )
        self._assert(
            "Verify that for each packet the number of time samples per relative weight"
            f" is between 0 and {MAX_NOF_TIME_SAMPLES_PER_RELATIVE_WEIGHT}",
            0 < packet.nof_time_samples_per_relative_weight <= MAX_NOF_TIME_SAMPLES_PER_RELATIVE_WEIGHT,
            f"The number of time samples per relative weight should be between"
            f" 0 and {MAX_NOF_TIME_SAMPLES_PER_RELATIVE_WEIGHT} but was {packet.nof_time_samples_per_relative_weight}",
        )
        self._assert(
            "Verify that for each packet the the reserved bits are all 0",
            packet.validity_reserved_bit_5 == packet.validity_reserved_bit_6 == packet.validity_reserved_bit_7 == 0,
            f"The reserved bits 5, 6 and 7 should all be 0 but were {packet.validity_reserved_bit_5},"
            f" {packet.validity_reserved_bit_6} and {packet.validity_reserved_bit_7} respectively",
        )
        self._assert(
            f"Verify that for each packet the scan ID is equal to {EXPECTED_SCAN_ID}",
            packet.scan_id == EXPECTED_SCAN_ID,
            f"The scan ID should be {EXPECTED_SCAN_ID} but was {packet.scan_id}",
        )
        self._assert(
            "Verify that for each packet the data is fully unpacked",
            packet.data == b"",
            f"All data should be unpacked from the packet but it still contained {packet.data}",
        )

    def on_finish(self) -> None:
        for assertion in self._assertions:
            with check, allure.step(assertion.allure_step):
                assert assertion.outcome, assertion.failure_message


@test_case(
    title="CBF PST TAB output packet contents",
    suite=TestSuite.PULSAR_TIMING,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1128428?projectId=335",
    xray_id="CLT-33",
)
def test_cbf_pst_tab_output_packet_contents(
    cbf_pst_tab_output_path: str,
):
    reader = PsrDataReader(CheckPacketContents())
    reader.read_pcap_file(cbf_pst_tab_output_path)
