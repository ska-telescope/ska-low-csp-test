# pylint:disable=missing-module-docstring
# pylint:disable=missing-function-docstring
# pylint:disable=too-many-arguments

import allure
import pytest
from pytest_check import check

from ska_low_csp_test.cbf.helpers import cbf_timing_beam_delay_poly_uri, cbf_timing_beam_jones_uri
from ska_low_csp_test.cbf.workarounds import Skb769Workaround
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema, CnicPulseConfigSchema
from ska_low_csp_test.delaypoly.devices import DelaypolyDevice, DelaypolyRaDecSchema
from ska_low_csp_test.domain import channels
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.pst.output import PstBeamMonitor, PstOutput, PstVoltageRecorderDataProduct
from ska_low_csp_test.reporting import TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator

BEAM_DIRECTION: DelaypolyRaDecSchema = {"ra": "00h18m29.06s", "dec": "-47d15m09.1s"}
SCAN_DURATION_S = 30.0
STATION_BEAM_ID = 1
STATION_CHANNEL_IDS = list(range(225, 321))
STATIONS = [(345, 1), (350, 1), (355, 1), (434, 1)]
STATION_WEIGHTS = [1.0] * len(STATIONS)
SUBARRAY_ID = 1
PST_BEAM_ID = 1


@pytest.fixture(name="subarray_id")
def fxt_subarray_id():
    return SUBARRAY_ID


@pytest.fixture(name="assign_resources_json")
def fxt_assign_resources_json() -> LowCspAssignResourcesSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.2",
        "common": {
            "subarray_id": SUBARRAY_ID,
        },
        "lowcbf": {},
        "pst": {
            "beams_id": [PST_BEAM_ID],
        },
    }


@pytest.fixture(name="configure_json")
def fxt_configure_json(
    config_id: str,
    pst_firmware_version: str,
    eb_id: str,
) -> LowCspConfigureSchema:
    pst_beam_channel_count = len(STATION_CHANNEL_IDS) * 216
    pst_beam_total_bandwidth = channels.coarse_channel_range_total_bandwidth(STATION_CHANNEL_IDS[0], STATION_CHANNEL_IDS[-1])
    pst_beam_centre_frequency = channels.coarse_channel_range_center_frequency(STATION_CHANNEL_IDS[0], STATION_CHANNEL_IDS[-1])

    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "science period 23",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": SUBARRAY_ID,
            "eb_id": eb_id,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": STATION_CHANNEL_IDS,
                        "delay_poly": f"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b{STATION_BEAM_ID:02}",
                    },
                ],
            },
            "timing_beams": {
                "fsp": {
                    "firmware": pst_firmware_version,
                    "fsp_ids": [1],
                },
                "beams": [
                    {
                        "pst_beam_id": PST_BEAM_ID,
                        "stn_beam_id": STATION_BEAM_ID,
                        "stn_weights": STATION_WEIGHTS,
                        "delay_poly": cbf_timing_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID, PST_BEAM_ID),
                        "jones": cbf_timing_beam_jones_uri(),
                    },
                ],
            },
        },
        "pst": {
            "beams": [
                {
                    "beam_id": PST_BEAM_ID,
                    "scan": {
                        "activation_time": "2022-01-19T23:07:45Z",
                        "bits_per_sample": 32,
                        "num_of_polarizations": 2,
                        "udp_nsamp": 32,
                        "wt_nsamp": 32,
                        "udp_nchan": 24,
                        "num_frequency_channels": pst_beam_channel_count,
                        "centre_frequency": pst_beam_centre_frequency,
                        "total_bandwidth": pst_beam_total_bandwidth,
                        "observation_mode": "VOLTAGE_RECORDER",
                        "observer_id": "jdoe",
                        "project_id": "project1",
                        "pointing_id": "pointing1",
                        "source": "J1921+2153",
                        "itrf": [5109360.133, 2006852.586, -3238948.127],
                        "receiver_id": "receiver3",
                        "feed_polarization": "LIN",
                        "feed_handedness": 1,
                        "feed_angle": 1.234,
                        "feed_tracking_mode": "FA",
                        "feed_position_angle": 10.0,
                        "oversampling_ratio": [4, 3],
                        "coordinates": {
                            "equinox": 2000.0,
                            **BEAM_DIRECTION,
                        },
                        "max_scan_length": max(30.0, SCAN_DURATION_S),
                        "subint_duration": 30.0,
                        "receptors": ["receptor1", "receptor2"],
                        "receptor_weights": [0.4, 0.6],
                        "num_channelization_stages": 2,
                        "channelization_stages": [
                            {
                                "num_filter_taps": 1,
                                "filter_coefficients": [1.0],
                                "num_frequency_channels": 1024,
                                "oversampling_ratio": [32, 27],
                            },
                            {
                                "num_filter_taps": 1,
                                "filter_coefficients": [1.0],
                                "num_frequency_channels": 256,
                                "oversampling_ratio": [4, 3],
                            },
                        ],
                    },
                },
            ],
        },
    }


@pytest.fixture(name="cnic_vd_config")
def fxt_cnic_vd_config() -> CnicConfigureVirtualDigitiserSchema:
    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,  # disregarded
                "subarray": SUBARRAY_ID,
                "station": station_id,
                "substation": substation_id,
                "frequency": channel_id,
                "beam": STATION_BEAM_ID,
                "sources": {
                    "x": [{"tone": False, "seed": station_id, "scale": 4000}],
                    "y": [{"tone": False, "seed": station_id, "scale": 4000}],
                },
            }
            for station_id, substation_id in STATIONS
            for channel_id in STATION_CHANNEL_IDS
        ],
    }


@pytest.fixture(name="cnic_pulse_config")
def fxt_cnic_pulse_config() -> CnicPulseConfigSchema:
    return {
        "enable": True,
        "sample_count": [2048, 10420, 10420],
    }


@allure.step("Verify structure of PST voltage recorder data product")
def check_data_product(
    data_product: PstVoltageRecorderDataProduct,
    expected_config_id: str,
):
    with allure.step("Inspect ska-data-product.yaml"):
        ska_data_product = data_product.read_ska_data_product_file()

        for field, expected in [
            ("dataproduct_type", "timeseries"),
            ("dataproduct_subtype", "voltages"),
            ("s_ra", pytest.approx(4.6210833333)),
            ("s_dec", pytest.approx(-47.2525277778)),
            ("t_resolution", 0.00020736),
            ("obs_id", str(data_product.scan_id)),
        ]:
            with check:
                with allure.step(f"Verify that {field} is equal to {expected}"):
                    assert ska_data_product["obscore"][field] == expected

    with allure.step("Inspect scan_configuration.json"):
        scan_config = data_product.read_scan_config_file()

        with check:
            with allure.step("Verify that 'config_id' matches the subarray configuration"):
                assert scan_config["common"]["config_id"] == expected_config_id

    with allure.step("Inspect data folder contents"):
        data = data_product.get_data_files()

        with check:
            with allure.step("Verify that the folder contains at least one file"):
                assert len(data) > 0

    with allure.step("Inspect weights folder contents"):
        weights = data_product.get_weights_files()
        with check:
            with allure.step("Verify that the folder contains the same number of files as the data folder"):
                assert len(weights) == len(data)

    with allure.step("Inspect stat folder contents"):
        stats = data_product.get_stat_files()
        with check:
            with allure.step("Verify that the folder contains at least one file"):
                assert len(stats) > 0


@test_case(
    title="PST beam voltage data path",
    suite=TestSuite.PULSAR_TIMING,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/testCases/1129936?projectId=335",
    xray_id="CLT-35",
    known_defects=["SKB-769"],
)
def test_pst_beam_voltage_data_path(
    assign_resources_json: LowCspAssignResourcesSchema,
    configure_json: LowCspConfigureSchema,
    scan_json: LowCspScanSchema,
    subarray_under_test: SubarrayUnderTest,
    station_signal_generator: StationSignalGenerator,
    cnic_vd_config: CnicConfigureVirtualDigitiserSchema,
    cnic_pulse_config: CnicPulseConfigSchema,
    delaypoly_device: DelaypolyDevice,
    pst_beam_monitor: PstBeamMonitor,
    pst_output: PstOutput,
    eb_id: str,
    scan_id: int,
    config_id: str,
    skb_769_workaround: Skb769Workaround,
):
    delaypoly_device.beam_ra_dec(
        {
            "subarray_id": SUBARRAY_ID,
            "beam_id": STATION_BEAM_ID,
            "direction": BEAM_DIRECTION,
        }
    )
    delaypoly_device.source_ra_dec(
        {
            "subarray_id": SUBARRAY_ID,
            "beam_id": STATION_BEAM_ID,
            "direction": [BEAM_DIRECTION] * 4,
        }
    )
    delaypoly_device.pst_ra_dec(
        {
            "subarray_id": SUBARRAY_ID,
            "beam_id": STATION_BEAM_ID,
            "direction": [BEAM_DIRECTION],
        }
    )

    subarray_under_test.assign_resources(assign_resources_json)
    subarray_under_test.configure(configure_json)

    with skb_769_workaround.route_timing_beams([PST_BEAM_ID]):
        with station_signal_generator.generate(vd_config=cnic_vd_config, pulse_config=cnic_pulse_config):
            subarray_under_test.scan(scan_json)
            pst_beam_monitor.monitor_voltage_recorder(
                beam_id=PST_BEAM_ID,
                duration_s=SCAN_DURATION_S,
            )
            subarray_under_test.end_scan()

    subarray_under_test.end()
    subarray_under_test.release_all_resources()

    data_product = pst_output.get_voltage_recorder_data_product(eb_id=eb_id, scan_id=scan_id)
    check_data_product(
        data_product,
        expected_config_id=config_id,
    )
