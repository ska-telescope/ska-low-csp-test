# pylint:disable=too-many-arguments
# pylint:disable=missing-function-docstring

"""Test configuration shared by all test packages."""

import logging
import os
import platform
from datetime import datetime, timezone
from typing import Callable, Generator

import allure
import pytest
import tango
from ska_control_model import AdminMode

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cbf.workarounds import Skb769Workaround
from ska_low_csp_test.cnic.devices import CnicDevice, CnicDevices
from ska_low_csp_test.cnic.helpers import initialise_cnic
from ska_low_csp_test.delaypoly.devices import DelaypolyDevice, DelaypolyDevices
from ska_low_csp_test.domain import TAI_Y2000
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.generators import ConfigIdGenerator, ExecutionBlockIdGenerator, ScanIdGenerator
from ska_low_csp_test.lmc.devices import LowCspCapabilityPst, LowCspDevices
from ska_low_csp_test.monitors import monitor_p4_routing_tables_during_scans
from ska_low_csp_test.pst.devices import PstDevices
from ska_low_csp_test.pst.output import PstBeamMonitor, PstOutput
from ska_low_csp_test.reporting import RecordEnvironmentProperty
from ska_low_csp_test.sut import SubarrayUnderTest, SystemUnderTest
from ska_low_csp_test.sut_config import SystemUnderTestConfiguration
from ska_low_csp_test.tango.ska_devices import SKABaseDevice
from ska_low_csp_test.tango.tango import TangoContext
from ska_low_csp_test.testware import PsrOutputCapture, StationSignalGenerator, TestwareDevices, VisibilityOutputCapture
from ska_low_csp_test.workarounds import Skb797Workaround

DEFAULT_CNIC_FW_SOURCE = "nexus"
DEFAULT_CNIC_FW_VERSION = "0.1.14"
DEFAULT_SUBARRAY_ID = 1
DEFAULT_PST_FW_VERSION = "pst:1.0.2"
DEFAULT_VIS_FW_VERSION = "vis:0.1.2"


def get_device_version(device: SKABaseDevice):
    """
    Attempt to retrieve the version id from the given device.

    :param device: Instance of :py:class:`SKABaseDevice`
    :return: The contents of the device's ``versionId`` TANGO attribute,
    or ``"unknown"`` if reading the attribute failed.
    """
    try:
        return device.version_id
    except tango.DevFailed:
        return "unknown"


@pytest.fixture(name="sut_config", scope="session", autouse=True)
def fxt_sut_config() -> SystemUnderTestConfiguration:
    """Fixture to get SUT configuration."""
    return SystemUnderTestConfiguration()


@pytest.fixture(scope="session", autouse=True)
def autocollect_environment_info(record_environment_property: RecordEnvironmentProperty) -> None:
    """Fixture that automatically records environment information."""
    record_environment_property("python_version", platform.python_version())
    record_environment_property("pytango_version", tango.__version__)


@pytest.fixture(name="generate_config_id", scope="session")
def fxt_generate_config_id() -> Callable[[], str]:
    """Fixture to generate a unique scan configuration id."""
    return ConfigIdGenerator()


@pytest.fixture(name="generate_eb_id", scope="session")
def fxt_generate_eb_id() -> Callable[[], str]:
    """Fixture to generate a unique scan execution block identifier."""
    return ExecutionBlockIdGenerator()


@pytest.fixture(name="generate_scan_id", scope="session")
def fxt_generate_scan_id() -> Callable[[str], int]:
    """Fixture to generate a unique scan id for a scan configuration."""
    return ScanIdGenerator()


@pytest.fixture(name="tango_context", scope="session")
def fxt_tango_context() -> TangoContext:
    """Fixture to access the TANGO context."""
    return TangoContext()


@pytest.fixture(name="low_csp_devices", scope="session")
def fxt_low_csp_devices(
    tango_context: TangoContext,
    record_environment_property: RecordEnvironmentProperty,
) -> LowCspDevices:
    """Fixture to create a container to reference LOW-CSP TANGO devices."""
    devices = LowCspDevices(tango_context)
    record_environment_property("low_csp_version", get_device_version(devices.controller()))
    return devices


@pytest.fixture(name="low_cbf_devices", scope="session")
def fxt_low_cbf_devices(
    tango_context: TangoContext,
    record_environment_property: RecordEnvironmentProperty,
) -> LowCbfDevices:
    """Fixture to create a container to reference LOW-CBF TANGO devices."""
    devices = LowCbfDevices(tango_context)
    record_environment_property("low_cbf_version", get_device_version(devices.controller()))
    record_environment_property("low_cbf_processor_version", get_device_version(devices.all_processors()[0]))
    record_environment_property("low_cbf_connector_version", get_device_version(devices.connector()))
    return devices


@pytest.fixture(name="pst_devices", scope="session")
def fxt_pst_devices(
    tango_context: TangoContext,
    record_environment_property: RecordEnvironmentProperty,
) -> PstDevices:
    """Fixture to create a container to reference PST TANGO devices."""
    devices = PstDevices(tango_context)
    record_environment_property("pst_version", get_device_version(devices.all_beams()[0]))
    return devices


@pytest.fixture(name="delaypoly_devices", scope="session")
def fxt_delaypoly_devices(tango_context: TangoContext) -> DelaypolyDevices:
    """Fixture to create a container to reference Delaypoly TANGO devices."""
    return DelaypolyDevices(tango_context)


@pytest.fixture(name="cnic_devices", scope="session")
def fxt_cnic_devices(tango_context: TangoContext) -> CnicDevices:
    """Fixture to create a container to reference CNIC TANGO devices."""
    return CnicDevices(tango_context)


@pytest.fixture(name="testware_devices", scope="session")
def fxt_testware_devices(tango_context: TangoContext):
    """Fixture to create a container to reference testware TANGO devices."""
    return TestwareDevices(tango_context)


@pytest.fixture(name="system_under_test", scope="session")
def fxt_system_under_test(
    low_csp_devices: LowCspDevices,
    low_cbf_devices: LowCbfDevices,
    pst_devices: PstDevices,
    skb_797_workaround: Skb797Workaround,
) -> SystemUnderTest:
    """Fixture to create a new system under test."""
    sut = SystemUnderTest(low_csp_devices, low_cbf_devices, pst_devices)
    skb_797_workaround.skb_797_workaround_for_all_subarrays()
    sut.reset()
    return sut


@pytest.fixture(name="subarray_id")
def fxt_subarray_id() -> int:
    """Fixture controlling the identifier for the subarray under test."""
    return DEFAULT_SUBARRAY_ID


@pytest.fixture(name="subarray_under_test")
def fxt_subarray_under_test(
    subarray_id: int, system_under_test: SystemUnderTest, skb_797_workaround: Skb797Workaround
) -> Generator[SubarrayUnderTest, None, None]:
    """Fixture to retrieve a subarray under test."""
    with system_under_test.subarray(subarray_id) as subarray:
        skb_797_workaround.skb_797_workaround_for_subarray_under_test(subarray)
        subarray.reset()
        yield subarray
        subarray.reset()
        skb_797_workaround.skb_797_workaround_for_subarray_under_test(subarray)


@pytest.fixture(name="pst_capability")
def fxt_pst_capability(low_csp_devices: LowCspDevices) -> LowCspCapabilityPst:
    """Fixture to retrieve the PST capability from the LOW-CSP devices."""
    return low_csp_devices.capability_pst()


@pytest.fixture(name="pst_firmware_version")
def fxt_pst_firmware_version(record_environment_property: RecordEnvironmentProperty):
    """Fixture to get PST firmware version."""
    record_environment_property("low_cbf_pst_firmware_version", DEFAULT_PST_FW_VERSION)
    return DEFAULT_PST_FW_VERSION


@pytest.fixture(name="vis_firmware_version")
def fxt_vis_firmware_version(record_environment_property: RecordEnvironmentProperty):
    """Fixture to get vis firmware version."""
    record_environment_property("low_cbf_correlator_firmware_version", DEFAULT_VIS_FW_VERSION)
    return DEFAULT_VIS_FW_VERSION


@pytest.fixture(name="cnic_firmware_version")
def fxt_cnic_firmware_version(record_environment_property: RecordEnvironmentProperty) -> str:
    """Fixture that controls the firmware version used by the CNICs."""
    record_environment_property("cnic_firmware_version", DEFAULT_CNIC_FW_VERSION)
    return DEFAULT_CNIC_FW_VERSION


@pytest.fixture(name="cnic_firmware_source")
def fxt_cnic_firmware_source() -> str | None:
    """Fixture that controls the firmware source used for the CNICs.

    Note: ``None`` makes the CNIC select its default source.
    """
    return DEFAULT_CNIC_FW_SOURCE


@pytest.fixture(name="cnic_device")
def fxt_cnic_device(cnic_devices: CnicDevices, cnic_firmware_version: str, cnic_firmware_source: str | None) -> CnicDevice:
    """Fixture to retrieve the CNIC TANGO device used in duplex mode."""
    cnic = cnic_devices.rx()
    initialise_cnic(cnic, firmware_version=cnic_firmware_version, firmware_source=cnic_firmware_source)
    return cnic


@pytest.fixture(name="delaypoly_device")
def fxt_delaypoly_device(delaypoly_devices: DelaypolyDevices):
    """Fixture for access to the delaypoly device."""
    return delaypoly_devices.delaypoly()


@pytest.fixture(autouse=True, scope="session")
def fxt_ptp_config(system_under_test: SystemUnderTest, sut_config: SystemUnderTestConfiguration) -> None:
    """Fixture to set up the PTP routes once per session."""
    system_under_test.configure_ptp(ptp_clock_port=sut_config.p4_ptp_master_port)


@allure.step("Set time offset on delaypoly emulator")
@pytest.fixture(autouse=True, scope="session")
def fxt_epoch_offset(
    delaypoly_devices: DelaypolyDevices,
) -> None:
    """Fixture to set the seconds after epoch once per session."""
    reference_datetime = datetime.now(timezone.utc)
    epoch_offset = reference_datetime - TAI_Y2000
    offset_seconds = int(epoch_offset.total_seconds())
    delaypoly_devices.delaypoly().set_seconds_after_epoch(offset_seconds)


@pytest.fixture(name="shorten_scan_duration", scope="session")
def fix_shorten_scan_duration() -> bool:
    """Fixture that indicates whether tests should shorten their scan duration for faster feedback."""
    scan_duration_mode = os.environ.get("TEST_SCAN_DURATION_MODE", "").lower()
    return scan_duration_mode != "full"


@pytest.fixture(autouse=True, scope="module")
def fxt_log_test_module_name(request):
    """Fixture to log the test module name."""
    logging.debug(" --MARK--  %s", request.node.name)


@pytest.fixture(name="config_id")
def default_config_id(generate_config_id: Callable[[], str]) -> str:
    """
    Fixture that creates a default config id.
    """
    return generate_config_id()


@pytest.fixture(name="eb_id")
def default_eb_id(generate_eb_id: Callable[[], str]) -> str:
    """
    Fixture that creates a default execution block identifier.
    """
    return generate_eb_id()


@pytest.fixture(name="scan_id")
def default_scan_id(generate_scan_id: Callable[[str], int], configure_json: LowCspConfigureSchema) -> int:
    """
    Fixture that creates a default scan id.
    """
    return generate_scan_id(configure_json["common"]["config_id"])


@pytest.fixture(name="assign_resources_json")
def default_assign_resources_json(subarray_id: int) -> LowCspAssignResourcesSchema:
    """
    Fixture that creates a default JSON payload for the AssignResources command.
    """
    return {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.2",
        "common": {
            "subarray_id": subarray_id,
        },
        "lowcbf": {},
    }


@pytest.fixture(name="configure_json")
def default_configure_json(
    subarray_id: int,
    config_id: str,
    vis_firmware_version: str,
    default_sdp_ip: str,
) -> LowCspConfigureSchema:
    """
    Fixture that creates a default JSON payload for the Configure command.
    """
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "science period 23",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": subarray_id,
        },
        "lowcbf": {
            "stations": {
                "stns": [(405, 1), (393, 1), (315, 1), (261, 1), (255, 1), (243, 1)],
                "stn_beams": [
                    {
                        "beam_id": 1,
                        "freq_ids": [397, 398, 399, 400, 401, 402, 403, 404],
                        "delay_poly": cbf_station_beam_delay_poly_uri(subarray_id, 1),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": [1],
                },
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "host": [(0, default_sdp_ip)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@pytest.fixture(name="scan_json")
def default_scan_json(subarray_id: int, scan_id: int) -> LowCspScanSchema:
    """
    Fixture that creates a default JSON payload for the Scan command.
    """
    return {
        "interface": "https://schema.skao.int/ska-low-csp-scan/3.2",
        "common": {
            "subarray_id": subarray_id,
        },
        "lowcbf": {
            "scan_id": scan_id,
        },
    }


@pytest.fixture(autouse=True, scope="session")
def fxt_clear_basic_routing_table(low_cbf_devices: LowCbfDevices) -> None:
    """Fixture to clear basic routing table at start of regression run."""
    low_cbf_devices.connector().clear_basic_routing_table()


@pytest.fixture(name="station_signal_generator")
def fxt_station_signal_generator(
    cnic_device: CnicDevice,
    delaypoly_device: DelaypolyDevice,
):
    """Fixture to create a station signal generator."""
    return StationSignalGenerator(
        cnic=cnic_device,
        delaypoly=delaypoly_device,
    )


@pytest.fixture(name="psr_output_capture")
def fxt_psr_output_capture(
    cnic_device: CnicDevice,
    low_cbf_devices: LowCbfDevices,
    sut_config: SystemUnderTestConfiguration,
):
    """Fixture to create an output capture for PSR output."""
    return PsrOutputCapture(
        cnic=cnic_device,
        allocator=low_cbf_devices.allocator(),
        connector=low_cbf_devices.connector(),
        remote_capture_dir=os.path.join(sut_config.test_data_location_cnic, sut_config.pcap_output_file_location),
        local_capture_dir=os.path.join(sut_config.test_data_location_ci, sut_config.pcap_output_file_location),
    )


@pytest.fixture(name="visibility_output_capture")
def fxt_visibility_output_capture(
    testware_devices: TestwareDevices,
    sut_config: SystemUnderTestConfiguration,
):
    """Fixture to create an output capture for visibility output."""
    return VisibilityOutputCapture(
        packet_capture_device=testware_devices.all_packet_capture_devices()[0],
        local_capture_dir=os.path.join(sut_config.test_data_location_ci, sut_config.pcap_output_file_location),
    )


@pytest.fixture(name="check_cbf_sdp_arp")
def fxt_check_cbf_sdp_arp(
    low_cbf_devices: LowCbfDevices,
):
    """Check whether LOW-CBF should be able to resolve SDP IP addresses using ARP."""
    arp_port = "33/3"
    ports_status = low_cbf_devices.connector().health_status["ports"]
    if not ports_status[arp_port]["Up"]:
        pytest.fail(f"LOW-CBF P4 port {arp_port} is down, SDP IP addresses cannot be resolved using ARP. Refer to SKB-752.")


@pytest.fixture(name="observation_time")
def fxt_observation_time():
    """Fixture to supply the observation time."""

    class DecoratedDatetime(datetime):
        """Format datetime with a squeezed ISO format."""

        def __str__(self) -> str:
            return f"{super().strftime('%Y%m%d_%H%M%S')}"

    return DecoratedDatetime.now(timezone.utc)


@pytest.fixture(name="pst_output")
def fxt_pst_output(sut_config: SystemUnderTestConfiguration):
    return PstOutput(root_dir=sut_config.sdp_data_location)


@pytest.fixture(name="pst_beam_monitor")
def fxt_pst_beam_monitor(pst_devices: PstDevices):
    return PstBeamMonitor(pst_devices)


@pytest.fixture(name="skb_769_workaround")
def fxt_skb_769_workaround(low_cbf_devices: LowCbfDevices, pst_devices: PstDevices):
    return Skb769Workaround(low_cbf_devices, pst_devices)


@pytest.fixture(name="skb_797_workaround", scope="session")
def fxt_skb_797_workaround(low_csp_devices: LowCspDevices) -> Skb797Workaround:
    return Skb797Workaround(low_csp_devices)


@pytest.fixture(scope="session", autouse=True)
def monitor_p4_routing_tables(system_under_test: SystemUnderTest):
    with monitor_p4_routing_tables_during_scans(system_under_test):
        yield


@pytest.fixture(name="default_sdp_ip", scope="session")
def fxt_default_sdp_ip(sut_config: SystemUnderTestConfiguration):
    return sut_config.sdp_default_ip


@pytest.fixture(autouse=True, scope="session")
def fxt_ensure_processors_online(low_cbf_devices: LowCbfDevices) -> None:
    """Fixture to ensure processors are online at start of regression run."""
    for processor in low_cbf_devices.all_processors():
        processor.admin_mode = AdminMode.ONLINE
