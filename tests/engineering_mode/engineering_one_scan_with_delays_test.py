# pylint:disable=missing-module-docstring,missing-function-docstring,too-many-arguments,missing-class-docstring

from dataclasses import dataclass
from datetime import datetime
from typing import Generator

import allure
import pytest
from astropy import units as u
from astropy.coordinates import AltAz
from ska_control_model import AdminMode

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.cbf.helpers import cbf_station_beam_delay_poly_uri
from ska_low_csp_test.cbf.visibilities import Polarization, get_visibility_data, unpack_pcap_file
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema
from ska_low_csp_test.delaypoly.devices import DelaypolyDevice, DelaypolyRaDecSchema
from ska_low_csp_test.delaypoly.helpers import sky_coord_to_ra_dec
from ska_low_csp_test.domain import plotting, sky_coordinates_from_altaz
from ska_low_csp_test.domain.model import LowCspAssignResourcesSchema, LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.domain.spead import LowCbfSdpSpeadStationBaselineMapping
from ska_low_csp_test.reporting import RecordTestEvidence, TestSuite, test_case
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.testware import StationSignalGenerator, VisibilityOutputCapture

BEAM_DIRECTION = AltAz(alt=50 * u.deg, az=10 * u.deg)
FREQUENCY_IDS = list(range(338, 346))
STATIONS = [(341, 1), (431, 1)]
STATION_BEAM_ID = 1
SUBARRAY_ID = 2

station_baseline_mapping = LowCbfSdpSpeadStationBaselineMapping(STATIONS)


@pytest.fixture(name="subarray_id")
def override_subarray_id() -> int:
    return SUBARRAY_ID


@pytest.fixture(name="configure_json")
def fxt_configure_json(
    config_id: str,
    vis_firmware_version: str,
    visibility_output_capture: VisibilityOutputCapture,
) -> LowCspConfigureSchema:
    return {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "subarray": {
            "subarray_name": "ENGINEERING subarray",
        },
        "common": {
            "config_id": config_id,
            "subarray_id": SUBARRAY_ID,
        },
        "lowcbf": {
            "stations": {
                "stns": STATIONS,
                "stn_beams": [
                    {
                        "beam_id": STATION_BEAM_ID,
                        "freq_ids": FREQUENCY_IDS,
                        "delay_poly": cbf_station_beam_delay_poly_uri(SUBARRAY_ID, STATION_BEAM_ID),
                    },
                ],
            },
            "vis": {
                "fsp": {
                    "firmware": vis_firmware_version,
                    "fsp_ids": [1],
                },
                "stn_beams": [
                    {
                        "stn_beam_id": STATION_BEAM_ID,
                        "host": [(0, visibility_output_capture.ip_address)],
                        "port": [(0, 20000, 1)],
                        "integration_ms": 849,
                    },
                ],
            },
        },
    }


@pytest.fixture(name="cnic_vd_json")
def fxt_cnic_vd_json() -> CnicConfigureVirtualDigitiserSchema:
    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,  # Disregarded
                "beam": STATION_BEAM_ID,
                "frequency": frequency_id,
                "station": station_id,
                "substation": substation_id,
                "subarray": SUBARRAY_ID,
                "sources": {
                    "x": [{"tone": False, "seed": 1981, "scale": 4138}],
                    "y": [{"tone": False, "seed": 1981, "scale": 4138}],
                },
            }
            for station_id, substation_id in STATIONS
            for frequency_id in FREQUENCY_IDS
        ],
    }


@dataclass(frozen=True)
class ScanConfig:
    beam_direction: DelaypolyRaDecSchema
    scan_duration_s: float
    trim_first_s: int


@pytest.fixture(name="scan_config")
def fxt_scan_config(
    observation_time: datetime,
    shorten_scan_duration: bool,
):
    _, coordinates = sky_coordinates_from_altaz(
        BEAM_DIRECTION,
        time=observation_time,
    )

    return ScanConfig(
        beam_direction=sky_coord_to_ra_dec(coordinates),
        scan_duration_s=15 if shorten_scan_duration else 300,
        trim_first_s=5 if shorten_scan_duration else 60,
    )


@pytest.fixture(name="engineering_subarray")
def fxt_engineering_subarray(subarray_under_test: SubarrayUnderTest) -> Generator[SubarrayUnderTest, None, None]:
    with allure.step(f"Set adminMode of subarray {SUBARRAY_ID} to ENGINEERING"):
        subarray_under_test.admin_mode = AdminMode.ENGINEERING
    yield subarray_under_test
    subarray_under_test.reset()
    subarray_under_test.admin_mode = AdminMode.ONLINE


@pytest.fixture(autouse=True)
def move_processors_to_engineering_mode(low_cbf_devices: LowCbfDevices):
    processors = low_cbf_devices.all_processors()
    admin_mode_cache = {}
    # TODO change to only one processor in ENGINEERING after cbf upgrade
    for processor in processors:
        admin_mode_cache[processor.fqdn] = processor.admin_mode
        with allure.step(f"Set adminMode of {processor.fqdn} to ENGINEERING"):
            processor.admin_mode = AdminMode.ENGINEERING

    yield
    for processor in processors:
        admin_mode = admin_mode_cache[processor.fqdn]
        with allure.step(f"Revert adminMode of {processor.fqdn} to {admin_mode.name}"):
            processor.admin_mode = admin_mode


@test_case(
    title="ENGINEERING one scan with delays",
    suite=TestSuite.ENGINEERING_MODE,
    jama_url="https://skaoffice.jamacloud.com/perspective.req#/items/1167290?projectId=335",
    xray_id="CLT-334",
)
@pytest.mark.usefixtures("check_cbf_sdp_arp")
def test_engineering_one_scan_with_delays(  # pylint: disable=too-many-locals
    assign_resources_json: LowCspAssignResourcesSchema,
    cnic_vd_json: CnicConfigureVirtualDigitiserSchema,
    configure_json: LowCspConfigureSchema,
    delaypoly_device: DelaypolyDevice,
    observation_time: datetime,
    record_test_evidence: RecordTestEvidence,
    scan_config: ScanConfig,
    scan_json: LowCspScanSchema,
    station_signal_generator: StationSignalGenerator,
    engineering_subarray: SubarrayUnderTest,
    visibility_output_capture: VisibilityOutputCapture,
):
    capture_file_name = f"{observation_time}_engineering_one_scan_with_delays.pcap"

    engineering_subarray.assign_resources(assign_resources_json)
    engineering_subarray.configure(configure_json)

    with visibility_output_capture.capture(destination_file=capture_file_name) as session:
        output_file_path = session.destination_file_path

        delaypoly_device.beam_ra_dec(
            {
                "subarray_id": SUBARRAY_ID,
                "beam_id": STATION_BEAM_ID,
                "direction": scan_config.beam_direction,
            }
        )
        delaypoly_device.source_ra_dec(
            {
                "subarray_id": SUBARRAY_ID,
                "beam_id": STATION_BEAM_ID,
                "direction": [scan_config.beam_direction] * 4,
            }
        )

        with station_signal_generator.generate(vd_config=cnic_vd_json):
            engineering_subarray.scan(scan_json)
            session.monitor(duration_s=scan_config.scan_duration_s)
            engineering_subarray.end_scan()

    engineering_subarray.end()
    engineering_subarray.release_all_resources()

    output = unpack_pcap_file(output_file_path)
    data = get_visibility_data(
        output,
        subarray_id=SUBARRAY_ID,
        beam_id=STATION_BEAM_ID,
        scan_id=scan_json["lowcbf"]["scan_id"],
        station_baseline_mapping=station_baseline_mapping,
    )

    record_test_evidence(
        file_name="XY time averaged phase vs channel.png",
        content_type="image/png",
        content=plotting.plot_time_averaged_phase_vs_channel(
            data,
            station_baseline_mapping.cross_sample(),
            Polarization.XY,
            trim_first_s=scan_config.trim_first_s,
        ),
    )

    record_test_evidence(
        file_name="XY channel averaged phase vs time.png",
        content_type="image/png",
        content=plotting.plot_channel_averaged_phase_vs_time(
            data,
            station_baseline_mapping.cross_sample()[0],
            Polarization.XY,
        ),
    )
