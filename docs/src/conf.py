# This file is not processed by Pylint when invoked through `make python-lint`,
# but Visual Studio Code does not know that.
# To get rid of the multitude of linting errors in this file, we therefore disable Pylint completely.
# pylint: disable=all

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "ska-low-csp-test"
copyright = "2023–2025 TOPIC Team"
author = "TOPIC Team"
release = "0.1.0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
]

source_suffix = [".rst"]
exclude_patterns = []

pygments_style = "sphinx"

intersphinx_mapping = {
    "astropy": ("https://docs.astropy.org/en/stable", None),
    "numpy": ("https://numpy.org/doc/stable", None),
    "pandas": ("https://pandas.pydata.org/pandas-docs/stable", None),
    "ska-dev-portal": ("https://developer.skao.int/en/latest", None),
    "ska-tango-base": ("https://developer.skao.int/projects/ska-tango-base/en/0.20.2", None),
    "ska-telmodel": ("https://developer.skao.int/projects/ska-telmodel/en/latest/", None),
    "sphinx": ("https://www.sphinx-doc.org/", None),
    "xarray": ("https://docs.xarray.dev/en/stable/", None),
}

extlinks = {
    "jira": ("https://jira.skatelescope.org/browse/%s", "%s"),
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"

html_context = {
    "theme_logo_only": True,
    "display_gitlab": True,
    "gitlab_user": "ska-telescope",
    "gitlab_repo": project,
    "gitlab_version": "main",
    "conf_py_path": "/docs/src/",  # Path in the checkout to the docs root
    "theme_vcs_pageview_mode": "edit",
    "suffix": ".rst",
}

# -- sphinx-autoapi configuration ----------------------------------------------
# https://github.com/readthedocs/sphinx-autoapi

extensions.append("autoapi.extension")
autoapi_dirs = ["../../src/ska_low_csp_test"]
autoapi_ignore = ["*_test.py"]
autoapi_root = "api"
