********************************************************************************
Analysing PCAP files containing (simulated) SPS station data output
********************************************************************************

The ``sps-data-analyser`` tool can be used to extract metadata from PCAP files
containing (simulated) SPS station data output.

.. tip::

    The command-line tools are installed automatically when using Poetry, see
    :ref:`how-to-use-poetry`.


Usage
=====

::

    $ sps-data-analyser --help
    usage: sps-data-analyser [-h] [--png-output-file PNG_OUTPUT_FILE] file

    Extracts and prints information from a PCAP file containing a SPS station data
    SPEAD stream.

    positional arguments:
      file                  Path to the PCAP file

    options:
      -h, --help            show this help message and exit
      --png-output-file PNG_OUTPUT_FILE, -o PNG_OUTPUT_FILE
                            When provided, creates .png graph, based on
                            channelized data and save it to the provided path

Sample output
=============

::

    $ sps-data-analyser ../pcap/LFAA_1sa_6stations_3ch_cof_10s.pcap
    Data file ../pcap/LFAA_1sa_6stations_3ch_cof_10s.pcap
        has 73440 packets of size 8306 bytes
        simulating data from 6 stations
        for 3 channels with frequencies [311.71875, 312.5, 313.28125] MHz
        approximately 12 seconds representing 13.30 correlator dumps
