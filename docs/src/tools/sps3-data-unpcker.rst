********************************************************************************
Unpacking PCAP files containing SPS v3 station data
********************************************************************************

The ``sps-data-unpacker`` tool can be used to unpack data from PCAP files
containing SPS station data.  It currently only supports a single mode of operation:
dump the SPEAD data stream, contained in the PCAP file, in a
human-readable format to ``STDOUT``.

.. tip::

    The command-line tools are installed automatically when using Poetry, see
    :ref:`how-to-use-poetry`.


Usage
=====

::

    $ sps3-data-unpacker --help
    usage: sps3-data-unpacker [-h] file

    Unpacks a PCAP file containing a SPS station data SPEAD stream.

    positional arguments:
    file        Path to the PCAP file

    options:
    -h, --help  show this help message and exit


Sample output
=============

::

    $ sps-data-unpacker sps-data.pcap
    SpsV3Packet(
      magic=83,  # 0x53
      version=4,
      pointer_width=2,
      header_width=6,
      header_reserved=0,
      number_of_items=6,
      heap_counter_addr_mode=1,  # SpeadAddressMode.IMMEDIATE
      heap_counter_ident=1,  # <SpeadItemId.HEAP_COUNTER, 0x0001>
      heap_counter_reserved=0,
      packet_count=0,
      payload_length_addr_mode=1,  # SpeadAddressMode.IMMEDIATE
      payload_length_ident=4,  # <SpeadItemId.PACKET_LEN, 0x0004>
      packet_payload_length=8192,
      scan_id_addr_mode=1,  # SpeadAddressMode.IMMEDIATE
      scan_id_ident=12304,  # <SpeadItemId.SCAN_ID, 0x3010>
      scan_id=0,
      channel_info_addr_mode=1,  # SpeadAddressMode.IMMEDIATE
      channel_info_ident=12288,  # <SpeadItemId.CHANNEL_INFO, 0x3000>
      channel_info_reserved=0,
      beam_id=1,
      frequency_id=100,
      antenna_info_addr_mode=1,  # SpeadAddressMode.IMMEDIATE
      antenna_info_ident=12289,  # <SpeadItemId.ANTENNA_INFO, 0x3001>
      substation_id=1,
      subarray_id=1,
      station_id=1,
      antenna_info_reserved=0,
      payload_offset_addr_mode=0,  # SpeadAddressMode.ADDRESS
      payload_offset_ident=13056,  # <SpeadItemId.SAMPLE_INFO, 0x3300>
      payload_offset=0,
      samples=array([[[ -13,   99],
            [ -13,   99]],

           [[ -15,   99],
            [ -15,   99]],

           [[ -17,   99],
            [ -17,   99]],

           ...,

           [[-100,   -7],
            [-100,   -7]],

           [[-100,   -9],
            [-100,   -9]],

           [[-100,  -11],
            [ -99,  -11]]], dtype=int8),
    )  # SpsV3Packet
    SpsV3Packet(
    ...
    ...
    ...
    )  # SpsV3Packet
    -eod-

The output above shows the data values for the first packet packet from a
Version 3 packet stream. Each packet contains 2048 samples of data and each
packet contains the data from a single station. The station, among other
things, is identified by the AntennaInfo section.
