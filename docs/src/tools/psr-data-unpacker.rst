********************************************************************************
Unpacking PCAP files containing PSR beamformer data
********************************************************************************

The ``psr-data-unpacker`` tool can be used to unpack data from PCAP files
containing PSR beamformer data.  It supports two modes of operation:
the first is to pretty-print the PSR packets, contained in the PCAP file, in a
human-readable format to ``STDOUT``; the second mode is to write the PSR
packets as an Xarray structure to a zarr key-value store.

.. tip::

    The command-line tools are installed automatically when using Poetry, see
    :ref:`how-to-use-poetry`.


Usage
=====

::

    $ psr-data-unpacker --help
    usage: psr-data-unpacker [-h] [--output-base path/to/unpacked/psr-data] [--write-zarr] [--verbose] pcap_file_path

    Unpacks a PCAP file containing PSR TAB data.

    positional arguments:
      pcap_file_path        Path to the PCAP file

    options:
      -h, --help            show this help message and exit
      --output-base path/to/unpacked/psr-data, -o path/to/unpacked/psr-data
                            Filename and / or directory for output zarr store; specifying only this implies `-z` too
      --verbose, -v         Increase verbosity level
      --write-zarr, -z      Write PSR packet contents as an Xarray to a zarr key-value store

Sample output
=============

::

    $ psr-data-unpacker psr-data.pcap
    PsrPacket(
      packet_sequence_number=0,
      samples_since_ska_epoch=4014848,
      period_numerator=5184,
      period_denominator=25,
      channel_separation_mHz=3616898,
      first_channel_frequency_mHz=77734375016,
      scale_1=0.875,
      scale_2=0.0,
      scale_3=0.0,
      scale_4=0.0,
      first_channel_number=7776,
      channels_per_packet=24,
      valid_channels_per_packet=24,
      nof_time_samples=32,
      beam_number=1,
      magic_number=3199074029,  # 0xbeadfeed
      packet_destination=2,  # PsrPacketDestination.LOW_PST
      data_precision=16,
      nof_power_samples_averaged=0,
      nof_time_samples_per_relative_weight=32,
      validity_reserved_bit_7=0,
      validity_reserved_bit_6=0,
      validity_reserved_bit_5=0,
      validity_first_sample_has_zero_phase=0,
      validity_jones_polarisation_corrections_have_not_expired=0,
      validity_valid_jones_polarisations_corrections_applied=1,
      validity_valid_pulsar_beam_delay_polynomials_applied=0,
      validity_valid_station_beam_delay_polynomials_applied=1,
      reserved=0,
      beamformer_version=256,  # v1.0
      scan_id=7234,
      offset_1=0,
      offset_2=0,
      offset_3=0,
      offset_4=0,
      weights=array([32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768,
           32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768,
           32768, 32768, 32768, 32768, 32768, 32768], dtype=uint16),
      samples=array([[[[ 1006,   -98],
             [ -266,   413],
             [  357,  -203],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]],

            [[ 1006,   -98],
             [ -266,   413],
             [  357,  -203],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]]],


           [[[  -14,  -399],
             [  873,  -553],
             [  488,   684],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]],
    
            [[  -14,  -399],
             [  873,  -553],
             [  488,   684],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]]],


           [[[-1386, -1036],
             [  810,   537],
             [ -294,   768],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]],

            [[-1386, -1036],
             [  810,   537],
             [ -294,   768],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]]],


           ...,


           [[[ -686,    98],
             [  838,   182],
             [  586,   -42],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]],

            [[ -686,    98],
             [  838,   182],
             [  586,   -42],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]]],


           [[[ -763,  1069],
             [  294,    14],
             [  -56,   824],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]],

            [[ -763,  1069],
             [  294,    14],
             [  -56,   824],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]]],


           [[[ -847,   453],
             [  894,   196],
             [  161,  -434],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]],

            [[ -847,   453],
             [  894,   196],
             [  161,  -434],
             ...,
             [    0,     0],
             [    0,     0],
             [    0,     0]]]], dtype=int16),
    )  # PsrPacket
    PsrPacket(
    ...
    ...
    ...
    )  # PsrPacket
    -eod-

::

    $ psr-data-unpacker -z psr-data.pcap

If the output base filename is not specified, the base file name of the input
data is used as default.

The created output store contains all of the packets from the input file,
unpacked as an Xarray Dataset.  Note that no modification is made to the
unpacked data -- all values are stored as they are interpreted from the input
PCAP file.  The scale, as read from each packet, applies to 24 channels and is
accumulated in the scale attribute of the Xarray Dataset.  The 24 weights per
packet is also accumulated to the weights attribute to the Xarray Dataset.


Example usage
=============

::

    $ psr-data-unpacker -vz ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Start reading PSR data from file: ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Finished reading PSR data from file: ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Start writing PSR data to file: 20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Finished writing PSR data to file: 20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap

    $ psr-data-unpacker -vz ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap -o foo/bar
    Start reading PSR data from file: ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Finished reading PSR data from file: ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Start writing PSR data to file: foo/bar
    Finished writing PSR data to file: foo/bar

    $ psr-data-unpacker -vz ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap -o foo/bar/
    Start reading PSR data from file: ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Finished reading PSR data from file: ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Start writing PSR data to file: foo/bar/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    Finished writing PSR data to file: foo/bar/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap

    $ rm -rf 20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap.zarr
    $ psr-data-unpacker -z ../_caps/20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap
    $ ls 20250107_095701_corr-1_0_2.cnic-0_1_13.tab.pcap.zarr/
    channel_id   component    polarisation scale_1      scale_2      scale_3      scale_4
    stokes_1     stokes_2     stokes_3     stokes_4     time_sample  voltages     weights
