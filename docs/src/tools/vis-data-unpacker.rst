********************************************************************************
Unpacking PCAP files containing LOW-CBF visibility data
********************************************************************************

The ``vis-data-unpacker`` tool can be used to unpack a PCAP file containing LOW-CBF visibility data captured by a CNIC.
It is able to write the unpacked data to a ``.zarr`` archive.
For more information on the structure contained in the archive, refer to :py:func:`ska_low_csp_test.cbf.visibilities.unpack_pcap_file`.

.. tip::

    The command-line tools are installed automatically when using Poetry, see
    :ref:`how-to-use-poetry`.

Usage
=====

::

    $ vis-data-unpacker -h
    usage: vis-data-unpacker [-h] [--write-zarr path/to/output.zarr] [-v] pcap_file_path

    positional arguments:
    pcap_file_path        Path to a PCAP file containing visibility data

    options:
    -h, --help            show this help message and exit
    --write-zarr path/to/output.zarr, -z path/to/output.zarr
                            Write the unpacked data to a .zarr archive at the provided path
    -v, --verbose

    Documentation for this command can be found at https://developer.skao.int/projects/ska-low-csp-test/en/latest/tools/vis-data-unpacker.html
