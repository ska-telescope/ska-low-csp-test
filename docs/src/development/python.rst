********************************************************************************
Python Code
********************************************************************************

Coding guidelines
=================

All Python code in ``ska-low-csp-test`` follows the SKAO Python coding guidelines.
For more information, refer to :doc:`ska-dev-portal:tools/codeguides/python-codeguide` on the SKAO Developer Portal.

Code formatting and linting
===========================

All Python code in ``ska-low-csp-test`` is formatted and linted using the SKAO standards, unless there is a *very* good reason to do otherwise.

The GitLab CI/CD pipeline configuration includes jobs to check the code for formatting and linting errors, and will fail if any errors are found.

It is recommended to run these tools prior to committing your changes to speed up your development process.
Use the following commands to format and then lint all Python code in the repository:

.. code-block:: console

    $ make python-format
    $ make python-lint
