********************************************************************************
Updating Documentation
********************************************************************************

The documentation you are reading now is maintained in the ``docs`` folder of the ``ska-low-csp-test`` repository.
It uses the `reStructuredText`_ format and is built using :doc:`Sphinx <sphinx:index>`. 

It is good practice to keep the documentation of ``ska-low-csp`` up-to-date as you are developing.
Since the documentation is under the same version control, this means you can add documentation and source code in the same Merge Request, which can then be reviewed and merged in one go!


Building the documentation
==========================

To build the documentation from source, run the following ``make`` command from the root of the repository:

.. code-block:: console

    $ make docs-build html

This will build the documentation as HTML pages and write them to ``docs/build/html``.
You can then preview the documentation by opening ``docs/build/html/index.html`` in your web browser.

.. tip::

    Sphinx builds the documentation incrementally, so any pages that weren't updated after the last time you built the documentation are not re-built.

    This may lead to some confusion when navigating around the preview, especially when making changes to the navigation sidebar.

    The easiest way around this is to clean the Sphinx build directory by running:

    .. code-block:: console

        $ make docs-build clean

Previewing the documentation from a GitLab Merge Request
========================================================

The GitLab CI/CD pipeline configuration includes a job to build the documentation and store the result as a pipeline artifact.
You can navigate to this artifact to view the HTML from GitLab:

1. From the GitLab CI/CD pipeline, select the ``docs:build`` job.
2. In the GitLab CI/CD job view, look for the *Job Artifacts* section and click *Browse*.
3. In the artifact browser, navigate to ``docs/build/html`` and select ``index.html``.
4. GitLab will complain about being redirected away from GitLab.
   Ignore the warning and click the link under the warning to view the documentation preview.

.. _reStructuredText: https://docutils.sourceforge.io/rst.html
