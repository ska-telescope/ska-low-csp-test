********************************************************************************
Getting Started
********************************************************************************

Welcome to the LOW-CSP test repository!
This guide should get you started by setting up your development environment.

.. note::

    This guide is a work-in-progress.


WSL
===

If you're working on Windows - like most of us at TOPIC - it is recommended to install the Ubuntu using the Windows Subsystem for Linux (WSL).
Refer to the `WSL installation documentation`_ on how to get started.


Clone the git repository
========================

You can clone the git repository using SSH or HTTPS using one of the commands below.

.. tip::

    If you are not sure which option to choose, clone using HTTPS.

To clone the git repository with SSH, run:

.. code-block:: console

    $ git clone --recurse-submodules git@gitlab.com:ska-telescope/ska-low-csp-test.git

To clone the git repository with HTTPS, run:

.. code-block:: console

    $ git clone --recurse-submodules https://gitlab.com/ska-telescope/ska-low-csp-test.git

.. note::

    The ``--recurse-submodules`` option in the ``git clone`` command is required to also clone the repository's submodules.

    In case you already cloned the repository without this option, you can get the submodules working by running ``git submodule update --init``

.. _how-to-use-poetry:

Using Poetry
============

The ``ska-low-csp-test`` repository uses `Poetry`_ to manage Python dependencies and virtual environments, which is the tooling recommended by SKAO.
You can install  Poetry by following the `Poetry installation documentation`_.

With Poetry installed, run the following command in the root of the ``ska-low-csp-test`` repository to install the project and its dependencies.
Poetry will automatically create a virtual environment by default.

.. code-block:: console

    $ poetry install


Virtual environments in Poetry
------------------------------

By default, Poetry will create a virtual environment in a central directory outside of the repository.
The easiest way to activate this virtual environment on the command line is by using:

.. code-block:: console

    $ poetry shell

This will create a nested shell where you can interact with all Python modules installed from the ``pyproject.toml``.

Alternatively, you can quickly run single commands without having to activate the virtual environment by prefixing your command with ``poetry run``:

.. code-block:: console

    $ poetry run <COMMAND>

If you prefer to have the virtual environment created inside of the repository, you can configure Poetry to do this for you.
The following command configures Poetry to create the virtual environment in the ``.venv`` folder in the root of this repository:

.. code-block:: console
    
    $ poetry config virtualenvs.in-project true
    $ poetry install

.. _WSL installation documentation: https://learn.microsoft.com/en-us/windows/wsl/install
.. _Poetry: https://python-poetry.org/
.. _Poetry installation documentation: https://python-poetry.org/docs/#installation
