********************************************************************************
Using the Makefile
********************************************************************************

The ``ska-low-csp-test`` repository relies on ``make`` to automate most development tasks like building and linting source code, running tests and deployments.

The ``make`` targets are defined in the ``Makefile``. This file includes targets from `ska-cicd-makefile`_, refer to their README for more information on how this works.

Make targets
============

To get an overview of the ``make`` targets supported by our ``Makefile``, run:

.. code-block:: console

    $ make help

Make variables
==============

The ``Makefile`` contains default values for a lot of variables, which control how the ``make`` targets behave.
The ``make help`` output lists the supported variables and their default values.

Overriding from the command line
--------------------------------

To override a variable from the command line, use the following format:

.. code-block:: console

    $ make <target> VARIABLE=value

For example, this command runs the baseline tests, but overrides the logging level to ``DEBUG``:

.. code-block:: console

    $ make test-baseline TEST_LOG_LEVEL=DEBUG

.. _ska-cicd-makefile: https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile
