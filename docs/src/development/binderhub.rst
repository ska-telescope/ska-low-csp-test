********************************************************************************
BinderHub Support
********************************************************************************

.. admonition:: Launch on CLP BinderHub

    .. image:: http://k8s.clp.skao.int/binderhub/badge_logo.svg
        :target: http://k8s.clp.skao.int/binderhub/v2/gl/ska-telescope%2Fska-low-csp-test/HEAD

The ``ska-low-csp-test`` repository can be launched as a binder on BinderHub.
Doing so will automatically install the main dependencies using Poetry.
It will also install the :py:mod:`ska_low_csp_test` module, so it can be imported in notebooks running in the binder environment.

To launch the ``ska-low-csp-test`` repository as a binder, navigate to a BinderHub instance and enter the following in the repository field::

    ska-telescope/ska-low-csp-test


