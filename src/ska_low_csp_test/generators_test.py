"""
Unit tests for the ska_low_csp_test.generators module.
"""

import re

import pytest
from hypothesis import given
from hypothesis.strategies import integers, uuids

from ska_low_csp_test.generators import ExecutionBlockIdGenerator, ScanIdGenerator

eb_id_pattern = re.compile(r"^eb\-[a-z0-9]+\-[0-9]{8}\-[a-z0-9]+$")
"""
Defined in:
https://developer.skao.int/projects/ska-telmodel/en/1.19.7/schemas/csp/low/configure/ska-low-csp-configure-3.2.html#common-configuration-schema-3-2
"""


@pytest.fixture(name="generate_eb_id", scope="session")
def fxt_generate_eb_id():
    """Fixture to retrieve execution block identifier generator"""
    return ExecutionBlockIdGenerator()


@given(uuids(), integers(min_value=1, max_value=100))
def test_scan_ids_are_unique_for_config_id(config_id: str, repeats: int) -> None:
    """
    Test that checks that generating multiple scan_ids for the same config_id yields unique results.
    """
    generate = ScanIdGenerator()
    scan_ids = list(generate(config_id) for _ in range(repeats))

    assert len(scan_ids) == len(set(scan_ids))


@pytest.mark.repeat(100)
def test_eb_ids_validate_against_regex(generate_eb_id: ExecutionBlockIdGenerator) -> None:
    """
    Test that checks that a generated eb_id will match the regex pattern defined in the configuration schema.
    """
    eb_id = generate_eb_id()
    assert eb_id_pattern.match(eb_id) is not None
