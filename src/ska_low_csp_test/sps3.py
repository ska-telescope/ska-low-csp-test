"""SPS packet data format."""

from enum import Enum, IntEnum

import dpkt
import numpy as np


class Component(IntEnum):
    """SPS sample component."""

    REAL = 0
    IMAG = 1


class Polarisation(IntEnum):
    """SPS sample polarisation."""

    V = 0
    H = 1


class SpeadAddressMode(Enum):
    """SPEAD address mode"""

    ADDRESS = 0
    IMMEDIATE = 1


def pprint_address_mode(mode: int) -> str:
    """Pretty-print SpeadAddressMode."""
    return str(SpeadAddressMode(mode))


class SpeadItemId(Enum):
    """SPS v3 SPEAD Item Identifier."""

    HEAP_COUNTER = 0x0001
    PACKET_LEN = 0x0004
    CHANNEL_INFO = 0x3000
    ANTENNA_INFO = 0x3001
    SCAN_ID = 0x3010
    SAMPLE_INFO = 0x3300

    def __repr__(self):
        cls_name = self.__class__.__name__
        return f"<{cls_name}.{self.name}, 0x{self.value:04x}>"


def pprint_item_id(mode: int) -> str:
    """Pretty-print SpeadItemId."""
    return repr(SpeadItemId(mode))


spead_packet_header = [
    ("magic", "B", 0x53),
    ("version", "B", 0x04),
    ("pointer_width", "b", 0x02),
    ("header_width", "b", 0x06),
    ("header_reserved", "H", 0x0000),
    ("number_of_items", "H", 0x0000),
]


class SpsV3Packet(dpkt.Packet):
    """SPS v3 packet format."""

    magic: int
    version: int
    pointer_width: int
    header_width: int
    header_reserved: int
    number_of_items: int

    heap_counter_addr_mode: int
    heap_counter_ident: int
    heap_counter_reserved: int
    packet_count: int

    payload_length_addr_mode: int
    payload_length_ident: int
    packet_payload_length: int

    scan_id_addr_mode: int
    scan_id_ident: int
    scan_id: int

    channel_info_addr_mode: int
    channel_info_ident: int
    channel_info_reserved: int
    beam_id: int
    frequency_id: int

    antenna_info_addr_mode: int
    antenna_info_ident: int
    substation_id: int
    subarray_id: int
    station_id: int
    antenna_info_reserved: int

    payload_offset_addr_mode: int
    payload_offset_ident: int
    payload_offset: int

    samples: np.ndarray

    __hdr_len__: int

    __hdr__ = (
        *spead_packet_header,
        ("_heap_counter_item", "Q", 0),
        ("_payload_length_item", "Q", 0),
        ("_scan_id_item", "Q", 0),
        ("_channel_info_item", "Q", 0),
        ("_antenna_info_item", "Q", 0),
        ("_payload_offset_item", "Q", 0),
    )

    __bit_fields__ = {
        "_heap_counter_item": (
            ("heap_counter_addr_mode", 1),
            ("heap_counter_ident", 15),
            ("heap_counter_reserved", 8),
            ("packet_count", 40),
        ),
        "_payload_length_item": (
            ("payload_length_addr_mode", 1),
            ("payload_length_ident", 15),
            ("packet_payload_length", 48),
        ),
        "_scan_id_item": (
            ("scan_id_addr_mode", 1),
            ("scan_id_ident", 15),
            ("scan_id", 48),
        ),
        "_channel_info_item": (
            ("channel_info_addr_mode", 1),
            ("channel_info_ident", 15),
            ("channel_info_reserved", 16),
            ("beam_id", 16),
            ("frequency_id", 16),
        ),
        "_antenna_info_item": (
            ("antenna_info_addr_mode", 1),
            ("antenna_info_ident", 15),
            ("substation_id", 8),
            ("subarray_id", 8),
            ("station_id", 16),
            ("antenna_info_reserved", 16),
        ),
        "_payload_offset_item": (
            ("payload_offset_addr_mode", 1),
            ("payload_offset_ident", 15),
            ("payload_offset", 48),
        ),
    }

    __pprint_funcs__ = {
        "magic": hex,
        "antenna_info_addr_mode": pprint_address_mode,
        "antenna_info_ident": pprint_item_id,
        "channel_info_addr_mode": pprint_address_mode,
        "channel_info_ident": pprint_item_id,
        "heap_counter_addr_mode": pprint_address_mode,
        "heap_counter_ident": pprint_item_id,
        "payload_length_addr_mode": pprint_address_mode,
        "payload_length_ident": pprint_item_id,
        "payload_offset_addr_mode": pprint_address_mode,
        "payload_offset_ident": pprint_item_id,
        "scan_id_addr_mode": pprint_address_mode,
        "scan_id_ident": pprint_item_id,
    }

    def unpack(self, buf):
        """Unpack SPSv3 SPEAD packet.

        :param buf: buffer to decode

        :raises ValueError: when unexpected magic number, or number of items seen
        :raises dpkt.NeedData: when buffer is too short to unpack
        """

        dpkt.Packet.unpack(self, buf)
        offset = self.__hdr_len__

        if self.magic != ord("S"):
            raise ValueError(f"Not a SPEAD packet, magic ({self.magic:d}) is not {ord('S')}")
        if self.number_of_items != 6:
            raise ValueError(f"Number of SPEAD items ({self.number_of_items}) is not `6'; possibly incorrect SPS version?")

        payload_len = self.packet_payload_length
        buf_len, need_len = len(buf), self.__hdr_len__ + payload_len
        if buf_len < need_len:  # pylint: disable=access-member-before-definition
            raise dpkt.NeedData(f"got {buf_len}, {need_len} needed at least")

        self.samples = np.frombuffer(buf, dtype=np.dtype("b"), count=payload_len, offset=offset).reshape(
            -1, len(Component), len(Polarisation)
        )
        offset += self.samples.nbytes

        self.data = self.data[offset:]
