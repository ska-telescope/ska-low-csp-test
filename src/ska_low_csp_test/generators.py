"""Generators for testing."""

import logging
import random
import string
from collections import defaultdict
from datetime import datetime

__all__ = ["ConfigIdGenerator", "ScanIdGenerator"]


class ConfigIdGenerator:
    """A generator that generates a unique config_id."""

    def __init__(self, logger: logging.Logger | None = None) -> None:
        self._logger = logger or logging.getLogger(__name__)

    def __call__(self) -> str:
        config_id = f"{datetime.now():%Y-%m-%dT%H-%M-%S.%f}"
        self._logger.debug("Generated new config_id: %s", config_id)
        return config_id


class ScanIdGenerator:
    """A generator that generates a unique scan_id for a given config_id."""

    def __init__(self, logger: logging.Logger | None = None) -> None:
        self._logger = logger or logging.getLogger(__name__)
        self._scan_ids: dict[str, int] = defaultdict(lambda: 0)

    def __call__(self, config_id: str) -> int:
        scan_id = self._scan_ids[config_id] + 1
        self._logger.debug("Generated new scan_id: %d for config_id: %s", scan_id, config_id)
        self._scan_ids[config_id] = scan_id
        return scan_id


class ExecutionBlockIdGenerator:
    """
    A generator that generates a unique eb_id according to the pattern defined in:

    https://developer.skao.int/projects/ska-telmodel/en/1.19.7/schemas/csp/low/configure/ska-low-csp-configure-3.2.html#common-configuration-schema-3-2
    """

    def __init__(self, logger: logging.Logger | None = None) -> None:
        self._logger = logger or logging.getLogger(__name__)

    def _random_sequence(self, length: int) -> str:
        return "".join(random.choices(string.ascii_lowercase + string.digits, k=length))

    def __call__(self) -> str:
        eb_id = f"eb-{self._random_sequence(4)}-{datetime.now():%Y%m%d}-{self._random_sequence(5)}"
        self._logger.debug("Generated new eb_id: %s", eb_id)
        return eb_id
