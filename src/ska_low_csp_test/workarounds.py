# pylint: disable=all
"""Module that provides classes for workarounds within SystemUnderTest."""

import logging

import tango
from ska_control_model import AdminMode, ObsState

from ska_low_csp_test.lmc.devices import LowCspDevices
from ska_low_csp_test.sut import SubarrayUnderTest
from ska_low_csp_test.synchronisation import wait_for_condition

__all__ = [
    "Skb797Workaround",
]


class Skb797Workaround:
    """
    Workaround for :jira:`SKB-797` to recover system from ABORTING/RESETTING state.
    """

    def __init__(
        self,
        low_csp_devices: LowCspDevices,
        logger: logging.Logger | None = None,
    ):
        self._csp_subarrays = low_csp_devices.all_subarrays()
        self._logger = logger or logging.getLogger(__name__)

    def _apply_skb_797_workaround(self, subarray):
        """Helper function to apply the SKB-797 workaround to a subarray."""
        if subarray.obs_state == ObsState.ABORTING:
            self._logger.warning("Subarray %s is in ABORTING state. Applying workaround", subarray)
            subarray.init()
            wait_for_condition(lambda: subarray.obs_state == ObsState.FAULT, timeout_s=5)
            subarray.restart()
            wait_for_condition(lambda: subarray.obs_state == ObsState.EMPTY, timeout_s=5)

        if subarray.obs_state == ObsState.RESETTING:
            self._logger.warning("Subarray %s is in RESETTING state. Applying workaround", subarray)
            subarray.init()
            wait_for_condition(lambda: subarray.state == tango.DevState.DISABLE, timeout_s=5)
            subarray.admin_mode = AdminMode.ONLINE

    def skb_797_workaround_for_all_subarrays(self):
        """Function to recover all subarrays."""
        for subarray in self._csp_subarrays:
            self._apply_skb_797_workaround(subarray)

    def skb_797_workaround_for_subarray_under_test(self, subarray_under_test: SubarrayUnderTest):
        """Function to recover a specific subarray."""
        self._apply_skb_797_workaround(subarray_under_test._subarray)
