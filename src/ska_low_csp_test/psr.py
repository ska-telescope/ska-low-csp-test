"""Module to work with PSR data."""

import abc
import logging
from dataclasses import dataclass
from enum import IntEnum

import allure
import dpkt
import numpy as np
import xarray as xr

module_logger = logging.getLogger(__name__)

__all__ = [
    "PsrBeamformerVersion",
    "PsrComponent",
    "PsrDataReader",
    "PsrDataVisitor",
    "PsrPacket",
    "PsrPacketDestination",
    "PsrPolarisation",
    "unpack_pcap_file",
]


@dataclass
class PsrBeamformerVersion:
    """PSR Beamformer Version specifier."""

    major: int
    minor: int

    def __str__(self):
        return f"v{self.major}.{self.minor}"


def pprint_beamformer_version(version: int) -> PsrBeamformerVersion:
    """Pretty-print beamformer version."""
    major = (version >> 8) & 0xFF
    minor = version & 0xFF
    return PsrBeamformerVersion(major, minor)


class PsrComponent(IntEnum):
    """PSR sample component."""

    I = 0  # noqa: E741 (Ambiguous variable name: `I`)
    Q = 1


class PsrPacketDestination(IntEnum):
    """PSR packet destination."""

    LOW_PSS = 0
    MID_PSS = 1
    LOW_PST = 2
    MID_PST = 3


def pprint_packet_destination(destination: int) -> IntEnum:
    """Pretty-print packet destination."""
    return PsrPacketDestination(destination)


class PsrPolarisation(IntEnum):
    """PSR sample polarisation."""

    A = 0
    B = 1


PSR_MAGIC = 0xBEADFEED
"PSR Magic number."


class PsrPacket(dpkt.Packet):
    """PSR Packet format."""

    packet_sequence_number: int
    """packetSequenceNumber"""
    samples_since_ska_epoch: int
    """samplesSinceSkaEpoch"""
    period_denominator: int
    """periodDenominator"""
    period_numerator: int
    """periodNumerator"""

    samples: np.ndarray
    scale_1: float
    scale_2: float
    scale_3: float
    scale_4: float
    weights: np.ndarray
    channel_separation_mHz: int
    """channelSeparation_mHz"""
    first_channel_frequency_mHz: int
    """firstChannelFrequency_mHz"""
    first_channel_number: int
    """firstChannelNumber"""
    channels_per_packet: int
    """channelsPerPacket"""
    valid_channels_per_packet: int
    """validChannelsPerPacket"""
    nof_time_samples: int
    """nofTimeSamples"""
    beam_number: int
    """beamNumber"""
    magic_number: int
    """magicNumber"""
    packet_destination: PsrPacketDestination
    """packetDestination"""
    data_precision: int
    """dataPrecision"""
    nof_power_samples_averaged: int
    """nofPowerSamplesAveraged"""
    nof_time_samples_per_relative_weight: int
    """nofTimeSamplesPerRelativeWeight"""
    validity_reserved_bit_7: bool
    """Bit 7: Reserved for future flags, currently set to zero"""
    validity_reserved_bit_6: bool
    """Bit 6: Reserved for future flags, currently set to zero"""
    validity_reserved_bit_5: bool
    """Bit 5: Reserved for future flags, currently set to zero"""
    validity_first_sample_has_zero_phase: bool
    """Bit 4: if set indicates the first sample of the packet has zero phase"""
    validity_jones_polarisation_corrections_have_not_expired: bool
    """Bit 3: 1=Jones polarisation corrections have not expired (TBD LOW only)"""
    validity_valid_jones_polarisations_corrections_applied: bool
    """Bit 2: 1=valid Jones polarisations corrections have been applied (LOW and MID)"""
    validity_valid_pulsar_beam_delay_polynomials_applied: bool
    """Bit 1: 1=valid Pulsar beam delay polynomials applied (LOW only)"""
    validity_valid_station_beam_delay_polynomials_applied: bool
    """Bit 0: 1=valid Station beam delay polynomials applied (LOW only)"""
    reserved: int
    beamformer_version: int
    """beamformerVersion"""
    scan_id: int
    """scanId"""
    offset_1: int
    offset_2: int
    offset_3: int
    offset_4: int

    stokes_1: np.ndarray | None = None
    stokes_2: np.ndarray | None = None
    stokes_3: np.ndarray | None = None
    stokes_4: np.ndarray | None = None

    __hdr_len__: int

    # ``struct`` hint:
    #   '<': little-endian,
    #   'b': s1, 'B': u1,
    #   'h': s2, 'H': u2,
    #   'L': u4, 'Q': u8,
    #   'f': 32-bit float

    __byte_order__ = "<"

    __hdr__ = (
        ("packet_sequence_number", "Q", 0),
        ("samples_since_ska_epoch", "Q", 0),
        ("period_numerator", "H", 0),
        ("period_denominator", "H", 0),
        ("channel_separation_mHz", "L", 0),
        ("first_channel_frequency_mHz", "Q", 0),
        ("scale_1", "f", 0),
        ("scale_2", "f", 0),
        ("scale_3", "f", 0),
        ("scale_4", "f", 0),
        ("first_channel_number", "L", 0),
        ("channels_per_packet", "H", 0),
        ("valid_channels_per_packet", "H", 0),
        ("nof_time_samples", "H", 0),
        ("beam_number", "H", 0),
        ("magic_number", "L", PSR_MAGIC),
        ("packet_destination", "B", 0),
        ("data_precision", "B", 0),
        ("nof_power_samples_averaged", "B", 0),
        ("nof_time_samples_per_relative_weight", "B", 0),
        ("_validity_flags", "B", 0),
        ("reserved", "B", 0),
        ("beamformer_version", "H", 0),
        ("scan_id", "Q", 0),
        ("offset_1", "L", 0),
        ("offset_2", "L", 0),
        ("offset_3", "L", 0),
        ("offset_4", "L", 0),
    )

    __bit_fields__ = {
        "_validity_flags": (
            ("validity_reserved_bit_7", 1),
            ("validity_reserved_bit_6", 1),
            ("validity_reserved_bit_5", 1),
            ("validity_first_sample_has_zero_phase", 1),
            ("validity_jones_polarisation_corrections_have_not_expired", 1),
            ("validity_valid_jones_polarisations_corrections_applied", 1),
            ("validity_valid_pulsar_beam_delay_polynomials_applied", 1),
            ("validity_valid_station_beam_delay_polynomials_applied", 1),
        )
    }

    __pprint_funcs__ = {
        "beamformer_version": pprint_beamformer_version,
        "magic_number": hex,
        "packet_destination": pprint_packet_destination,
    }

    def unpack(self, buf):
        """Unpack PSR packet.

        Currently this unpacks the samples as unscaled values, to aid verification.

        To scale the LOW PST samples, use the following logic::

            samples = psr_packet.samples / psr_packet.scale_1

        :param buffer: data buffer containing PSR data structure
        :raises ValueError: when the unpacked PSR packet magic number is invalid
        :raises dpkt.NeedData: when the data in the buffer ends prematurely
        """
        dpkt.Packet.unpack(self, buf)
        offset = self.__hdr_len__

        if self.magic_number != PSR_MAGIC:
            raise ValueError(f"Invalid PSR magic number in packet: 0x{self.magic_number:x}")

        n_weights = self.channels_per_packet * (self.nof_time_samples // self.nof_time_samples_per_relative_weight)
        self.weights = np.frombuffer(buf, dtype=np.dtype("H"), count=n_weights, offset=offset)
        offset += self.weights.nbytes

        sample_fmt = "b" if self.data_precision == 8 else "h"
        n_samples = self.channels_per_packet * len(PsrPolarisation) * self.nof_time_samples * len(PsrComponent)
        self.samples = np.frombuffer(buf, dtype=np.dtype(sample_fmt), count=n_samples, offset=offset).reshape(
            self.channels_per_packet, len(PsrPolarisation), self.nof_time_samples, len(PsrComponent)
        )
        offset += self.samples.nbytes

        n_stokes = (
            self.channels_per_packet * self.nof_time_samples
            if self.packet_destination in [PsrPacketDestination.LOW_PSS, PsrPacketDestination.MID_PSS]
            else 0
        )

        if n_stokes:
            self.stokes_1 = np.frombuffer(buf, dtype=np.dtype("b"), count=n_stokes, offset=self.offset_1).reshape(
                self.channels_per_packet,
                self.nof_time_samples,
            )
            offset += self.stokes_1.nbytes

            self.stokes_2 = np.frombuffer(buf, dtype=np.dtype("b"), count=n_stokes, offset=self.offset_2).reshape(
                self.channels_per_packet,
                self.nof_time_samples,
            )
            offset += self.stokes_2.nbytes

            self.stokes_3 = np.frombuffer(buf, dtype=np.dtype("b"), count=n_stokes, offset=self.offset_3).reshape(
                self.channels_per_packet,
                self.nof_time_samples,
            )
            offset += self.stokes_3.nbytes

            self.stokes_4 = np.frombuffer(buf, dtype=np.dtype("b"), count=n_stokes, offset=self.offset_4).reshape(
                self.channels_per_packet,
                self.nof_time_samples,
            )

        if self.__hdr_len__ + len(self.data) < offset:
            raise dpkt.NeedData(
                f"Tried to read {offset} bytes from packet of size " f"{len(self.data)} bytes",
            )
        self.data = self.data[offset:]


class PsrDataVisitor(abc.ABC):
    """Abstract base class to visit PSR data heaps."""

    def visit_packet(self, index: int, packet: PsrPacket) -> None:
        """
        Visit a packet.

        :param index: The packet index
        :param packet: The packet to visit
        """

    def on_finish(self) -> None:
        """
        Handler called after all PSR data is processed.
        """


class PsrDataReader:
    """Utility to read and process PSR data."""

    def __init__(self, *visitors: PsrDataVisitor) -> None:
        self._visitors = visitors

    def read_pcap_file(self, pcap_file_path: str, logger: logging.Logger | None = None) -> None:
        """
        Read PSR packets from the given PCAP file.

        :param pcap_file_path: Path to a PCAP file containing PSR beamformer data.
        :param logger: Python logger.
        """
        logger = logger or module_logger
        logger.info("Start reading PSR data from file: %s", pcap_file_path)

        packet_number = -1
        with open(pcap_file_path, "rb") as pcap_file:
            reader = dpkt.pcap.UniversalReader(pcap_file)
            for packet_number, (_timestamp, packet) in enumerate(reader):
                eth = dpkt.ethernet.Ethernet(packet)
                if eth.type != dpkt.ethernet.ETH_TYPE_IP:
                    continue

                ip = eth.data
                if ip.p != dpkt.ip.IP_PROTO_UDP:  # type: ignore
                    continue

                udp = ip.data
                try:
                    packet = PsrPacket(udp.data)
                except Exception as ex:
                    module_logger.warning("Failed to interpret packet %d as PSR data: %s", packet_number, ex)
                else:
                    for visitor in self._visitors:
                        visitor.visit_packet(packet_number, packet)

        logger.debug("Read %d packets", packet_number + 1)
        logger.info("Finished reading PSR data from file: %s", pcap_file_path)

        for visitor in self._visitors:
            visitor.on_finish()


class _PsrDataExtractor(PsrDataVisitor):
    def __init__(self, logger: logging.Logger | None = None) -> None:
        self._logger = logger or module_logger

        self._sequences = {}
        self._current_sequence = {}
        self._sequence_number = None

    def visit_packet(self, index: int, packet: PsrPacket) -> None:
        if self._sequence_number != packet.packet_sequence_number:
            self._flush_sequence()

        self._sequence_number = packet.packet_sequence_number
        channel_id = packet.first_channel_number + np.array(range(packet.channels_per_packet))
        self._current_sequence[index] = xr.Dataset(
            {
                "voltages": xr.DataArray(
                    packet.samples,
                    dims=["channel_id", "polarisation", "time_sample", "component"],
                    coords={
                        "channel_id": channel_id,
                        "polarisation": [each.name for each in PsrPolarisation],
                        "time_sample": range(packet.nof_time_samples),
                        "component": [each.name for each in PsrComponent],
                    },
                    attrs={
                        "validity_reserved_bit_7": packet.validity_reserved_bit_7,
                        "validity_reserved_bit_6": packet.validity_reserved_bit_6,
                        "validity_reserved_bit_5": packet.validity_reserved_bit_5,
                        "validity_first_sample_has_zero_phase": packet.validity_first_sample_has_zero_phase,
                        # fmt: off
                        "validity_jones_polarisation_corrections_have_not_expired":
                        packet.validity_jones_polarisation_corrections_have_not_expired,
                        "validity_valid_jones_polarisations_corrections_applied":
                        packet.validity_valid_jones_polarisations_corrections_applied,
                        "validity_valid_pulsar_beam_delay_polynomials_applied":
                        packet.validity_valid_pulsar_beam_delay_polynomials_applied,
                        "validity_valid_station_beam_delay_polynomials_applied":
                        packet.validity_valid_station_beam_delay_polynomials_applied,
                        # fmt: on
                    },
                ),
                "weights": xr.DataArray(
                    packet.weights,
                    dims=["channel_id"],
                    coords={
                        "channel_id": channel_id,
                    },
                ),
                "scale_1": packet.scale_1,
                "scale_2": packet.scale_2,
                "scale_3": packet.scale_3,
                "scale_4": packet.scale_4,
                "stokes_1": (
                    xr.DataArray(
                        packet.stokes_1,
                        dims=["channel_id", "power_sample"],
                        coords={
                            "channel_id": channel_id,
                            "power_sample": range(packet.nof_time_samples),
                        },
                    )
                    if packet.stokes_1 is not None
                    else []
                ),
                "stokes_2": (
                    xr.DataArray(
                        packet.stokes_2,
                        dims=["channel_id", "power_sample"],
                        coords={
                            "channel_id": channel_id,
                            "power_sample": range(packet.nof_time_samples),
                        },
                    )
                    if packet.stokes_2 is not None
                    else []
                ),
                "stokes_3": (
                    xr.DataArray(
                        packet.stokes_3,
                        dims=["channel_id", "power_sample"],
                        coords={
                            "channel_id": channel_id,
                            "power_sample": range(packet.nof_time_samples),
                        },
                    )
                    if packet.stokes_3 is not None
                    else []
                ),
                "stokes_4": (
                    xr.DataArray(
                        packet.stokes_4,
                        dims=["channel_id", "power_sample"],
                        coords={
                            "channel_id": channel_id,
                            "power_sample": range(packet.nof_time_samples),
                        },
                    )
                    if packet.stokes_4 is not None
                    else []
                ),
            },
            attrs={
                "beam_id": packet.beam_number,
                "scan_id": packet.scan_id,
            },
        )

    def on_finish(self):
        self._flush_sequence()

    def _flush_sequence(self):
        if self._sequence_number is not None:
            self._sequences[self._sequence_number] = xr.concat(self._current_sequence.values(), dim="channel_id")
        self._current_sequence, self._sequence_number = {}, None

    @property
    def data(self) -> xr.Dataset:
        """
        Property to access the extracted data as an xarray dataset.
        """
        return xr.concat(self._sequences.values(), dim="sequence")


@allure.step("Unpack PCAP file")
def unpack_pcap_file(pcap_file_path: str, logger: logging.Logger | None = None) -> xr.Dataset:
    """Unpack a PCAP file containing PSR beamformer data.

    :param pcap_file_path: Path to the PCAP file containing visibility data.
    :param logger: Python logger.
    :returns: The unpacked data.
    """
    extractor = _PsrDataExtractor(logger=logger)
    reader = PsrDataReader(extractor)
    reader.read_pcap_file(pcap_file_path, logger=logger)

    return extractor.data
