"""
Unit tests for the ska_low_csp_test.domain.spead module.
"""

import random

import pytest

from ska_low_csp_test.domain import spead

low_cbf_sdp_spead_heap_counter_data = [
    (0, {"beam_id": 0, "channel_id": 0, "integration_id": 0, "subarray_id": 0}),
    (1, {"beam_id": 0, "channel_id": 0, "integration_id": 1, "subarray_id": 0}),
    (1 << 15, {"beam_id": 0, "channel_id": 0, "integration_id": 1 << 15, "subarray_id": 0}),
    (1 << 16, {"beam_id": 0, "channel_id": 1, "integration_id": 0, "subarray_id": 0}),
    (1 << (16 + 16), {"beam_id": 0, "channel_id": 1 << 16, "integration_id": 0, "subarray_id": 0}),
    (1 << (17 + 16), {"beam_id": 1, "channel_id": 0, "integration_id": 0, "subarray_id": 0}),
    (1 << (8 + 17 + 16), {"beam_id": 1 << 8, "channel_id": 0, "integration_id": 0, "subarray_id": 0}),
    (1 << (9 + 17 + 16), {"beam_id": 0, "channel_id": 0, "integration_id": 0, "subarray_id": 1}),
    (1 << (4 + 9 + 17 + 16), {"beam_id": 0, "channel_id": 0, "integration_id": 0, "subarray_id": 1 << 4}),
    (1 << (5 + 9 + 17 + 16), {"beam_id": 0, "channel_id": 0, "integration_id": 0, "subarray_id": 0}),
]


@pytest.mark.parametrize("test_input, expected", low_cbf_sdp_spead_heap_counter_data)
def test_low_cbf_sdp_spead_heap_counter(test_input: int, expected: dict[str, int]) -> None:
    """Test LowCbfSdpSpeadHeapCounter.unpack()."""
    a = spead.LowCbfSdpSpeadHeapCounter.unpack(test_input)
    b = spead.LowCbfSdpSpeadHeapCounter(**expected)
    assert a == b


station_list = [(11, 0), (13, 0), (14, 0), (19, 0), (20, 0), (21, 0)]
random.shuffle(station_list)

baseline_data = [
    [],  # no stations
    [(1, 1)],  # single station
    [(1, 4), (1, 2), (1, 3), (1, 1)],  # substation ordering
    [(345, 1), (350, 1), (352, 1), (431, 1)],  # station ordering
    [345, 350, 352, 431],  # station int in stead of station-substation tuple
    [350, 345, 431, 352],  # unordered station int in stead of station-substation tuple
    station_list,  # baseline example from spec
]

baseline_order_data = [
    [],  # no stations
    [((1, 1), (1, 1))],  # single station
    [  # substation ordering
        ((1, 1), (1, 1)),
        ((1, 2), (1, 1)),
        ((1, 2), (1, 2)),
        ((1, 3), (1, 1)),
        ((1, 3), (1, 2)),
        ((1, 3), (1, 3)),
        ((1, 4), (1, 1)),
        ((1, 4), (1, 2)),
        ((1, 4), (1, 3)),
        ((1, 4), (1, 4)),
    ],
    [  # station ordering
        ((345, 1), (345, 1)),
        ((350, 1), (345, 1)),
        ((350, 1), (350, 1)),
        ((352, 1), (345, 1)),
        ((352, 1), (350, 1)),
        ((352, 1), (352, 1)),
        ((431, 1), (345, 1)),
        ((431, 1), (350, 1)),
        ((431, 1), (352, 1)),
        ((431, 1), (431, 1)),
    ],
    [  # station int instead of station-substation tuple
        (345, 345),
        (350, 345),
        (350, 350),
        (352, 345),
        (352, 350),
        (352, 352),
        (431, 345),
        (431, 350),
        (431, 352),
        (431, 431),
    ],
    [  # unsorted station int instead of station-substation tuple
        (345, 345),
        (350, 345),
        (350, 350),
        (352, 345),
        (352, 350),
        (352, 352),
        (431, 345),
        (431, 350),
        (431, 352),
        (431, 431),
    ],
    [  # baseline example from spec
        ((11, 0), (11, 0)),
        ((13, 0), (11, 0)),
        ((13, 0), (13, 0)),
        ((14, 0), (11, 0)),
        ((14, 0), (13, 0)),
        ((14, 0), (14, 0)),
        ((19, 0), (11, 0)),
        ((19, 0), (13, 0)),
        ((19, 0), (14, 0)),
        ((19, 0), (19, 0)),
        ((20, 0), (11, 0)),
        ((20, 0), (13, 0)),
        ((20, 0), (14, 0)),
        ((20, 0), (19, 0)),
        ((20, 0), (20, 0)),
        ((21, 0), (11, 0)),
        ((21, 0), (13, 0)),
        ((21, 0), (14, 0)),
        ((21, 0), (19, 0)),
        ((21, 0), (20, 0)),
        ((21, 0), (21, 0)),
    ],
]

baseline_auto_data = [
    [],  # no stations
    [((1, 1), (1, 1))],  # single station
    [  # substation ordering
        ((1, 1), (1, 1)),
        ((1, 2), (1, 2)),
        ((1, 3), (1, 3)),
        ((1, 4), (1, 4)),
    ],
    [  # station ordering
        ((345, 1), (345, 1)),
        ((350, 1), (350, 1)),
        ((352, 1), (352, 1)),
        ((431, 1), (431, 1)),
    ],
    [  # station int instead of station-substation tuple
        (345, 345),
        (350, 350),
        (352, 352),
        (431, 431),
    ],
    [  # unsorted station int instead of station-substation tuple
        (345, 345),
        (350, 350),
        (352, 352),
        (431, 431),
    ],
    [  # baselines example from spec
        ((11, 0), (11, 0)),
        ((13, 0), (13, 0)),
        ((14, 0), (14, 0)),
        ((19, 0), (19, 0)),
        ((20, 0), (20, 0)),
        ((21, 0), (21, 0)),
    ],
]

baseline_cross_data = [
    [],  # no stations
    [],  # single station
    [  # substation ordering
        ((1, 2), (1, 1)),
        ((1, 3), (1, 1)),
        ((1, 3), (1, 2)),
        ((1, 4), (1, 1)),
        ((1, 4), (1, 2)),
        ((1, 4), (1, 3)),
    ],
    [  # station ordering
        ((350, 1), (345, 1)),
        ((352, 1), (345, 1)),
        ((352, 1), (350, 1)),
        ((431, 1), (345, 1)),
        ((431, 1), (350, 1)),
        ((431, 1), (352, 1)),
    ],
    [  # station int instead of station-substation tuple
        (350, 345),
        (352, 345),
        (352, 350),
        (431, 345),
        (431, 350),
        (431, 352),
    ],
    [  # unsorted station int instead of station-substation tuple
        (350, 345),
        (352, 345),
        (352, 350),
        (431, 345),
        (431, 350),
        (431, 352),
    ],
    [  # baselines example from spec
        ((13, 0), (11, 0)),
        ((14, 0), (11, 0)),
        ((14, 0), (13, 0)),
        ((19, 0), (11, 0)),
        ((19, 0), (13, 0)),
        ((19, 0), (14, 0)),
        ((20, 0), (11, 0)),
        ((20, 0), (13, 0)),
        ((20, 0), (14, 0)),
        ((20, 0), (19, 0)),
        ((21, 0), (11, 0)),
        ((21, 0), (13, 0)),
        ((21, 0), (14, 0)),
        ((21, 0), (19, 0)),
        ((21, 0), (20, 0)),
    ],
]


@pytest.mark.parametrize(
    "test_input, expected, expected_auto, expected_cross ",
    list(zip(baseline_data, baseline_order_data, baseline_auto_data, baseline_cross_data)),
)
def test_baseline_order(test_input, expected, expected_auto, expected_cross) -> None:
    """Tests the ordering of LowCbfSdpSpeadStationBaselineMapping."""
    station_map = spead.LowCbfSdpSpeadStationBaselineMapping(station_substation=test_input)
    assert len(station_map.station_substation) == len(test_input)

    for each in test_input:
        assert each in station_map.station_substation

    N = len(station_map.station_substation)  # pylint: disable=invalid-name
    assert len(station_map.baselines) == N * (N + 1) // 2

    assert len(station_map) == len(expected)
    for idx, each in enumerate(expected):
        assert station_map[idx] == each

    assert [station_map[idx] for idx in station_map.auto_indices] == expected_auto
    assert [station_map[idx] for idx in station_map.cross_indices] == expected_cross


def test_baseline_sampling():
    """Test the sampling of LowCbfSdpSpeadStationBaselineMapping."""
    station_map = spead.LowCbfSdpSpeadStationBaselineMapping(station_substation=station_list)
    for sample_size in range(len(station_list)):
        auto_sample = station_map.auto_sample(sample_size=sample_size)
        assert len(auto_sample) == len(set(auto_sample))
        assert len(auto_sample) == sample_size
        for sample in auto_sample:
            assert sample in station_map.auto_indices

        cross_sample = station_map.cross_sample(sample_size=sample_size)
        assert len(cross_sample) == len(set(cross_sample))
        assert len(cross_sample) == sample_size
        for sample in cross_sample:
            assert sample in station_map.cross_indices
