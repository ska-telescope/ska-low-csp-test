"""Constants and functions related to LOW channelisation."""

TOTAL_BANDWIDTH = 400 * 1e6
"""The total bandwidth of the LOW telescope, in Hz."""

LOWEST_VALID_FREQUENCY = 50.78125e6
"""The lower bound of the nominal band used by the LOW telescope, in Hz."""

HIGHEST_VALID_FREQUENCY = 350e6
"""The upper bound of the nominal band used by the LOW telescope, in Hz."""

COARSE_CHANNEL_WIDTH = 781.25 * 1e3
"""The width of a single coarse channel, in Hz."""

COARSE_CHANNEL_COUNT = int(TOTAL_BANDWIDTH / COARSE_CHANNEL_WIDTH)
"""The number of coarse channels in the LOW telescope."""

LOWEST_VALID_COARSE_CHANNEL = 65
"""The first valid coarse channel in the LOW telescope."""

HIGHEST_VALID_COARSE_CHANNEL = 448
"""The first valid coarse channel in the LOW telescope."""

VALID_COARSE_CHANNELS = set(range(LOWEST_VALID_COARSE_CHANNEL, HIGHEST_VALID_COARSE_CHANNEL + 1))
"""All valid coarse channels in the LOW telescope."""

FINE_CHANNELS_PER_COARSE_CHANNEL_COUNT = 3456
"""The number of fine channels per coarse channel in the LOW telescope."""

FINE_CHANNEL_COUNT = COARSE_CHANNEL_COUNT * FINE_CHANNELS_PER_COARSE_CHANNEL_COUNT
"""The number of fine channels in the LOW telescope."""

FINE_CHANNEL_WIDTH = COARSE_CHANNEL_WIDTH / FINE_CHANNELS_PER_COARSE_CHANNEL_COUNT
"""The width of a single fine channel, in Hz."""

SPECTRAL_MODE_FINE_CHANNELS_PER_COARSE_CHANNEL = 144
"""The number of fine channels per coarse channel output by the correlator in spectral mode."""


def coarse_channel_center_frequency(channel: int) -> float:
    """
    Retrieve the center frequency for the coarse channel with the given identifier.

    :param channel: The coarse channel identifier.
    :return: The center frequency, in Hz.
    """
    return round(float(channel) / float(COARSE_CHANNEL_COUNT) * float(TOTAL_BANDWIDTH)) % TOTAL_BANDWIDTH


def coarse_channel_frequency_range(channel: int) -> tuple[float, float]:
    """
    Retrieve the frequency range for the coarse channel with the given identifier.

    :param channel: The coarse channel identifier.
    :return: A tuple containing the lower and upper bound of the frequency range, in Hz.
    """
    center_frequency = coarse_channel_center_frequency(channel)
    half_width = COARSE_CHANNEL_WIDTH / 2
    return center_frequency - half_width, center_frequency + half_width


def coarse_channel_id(frequency: float) -> int:
    """
    Retrieve the coarse channel identifier that belongs to the given frequency.

    :param frequency: The frequency, in Hz.
    :return: The identifier of the coarse channel.
    """
    return int(round(float(frequency) / float(TOTAL_BANDWIDTH) * COARSE_CHANNEL_COUNT) % COARSE_CHANNEL_COUNT)


def coarse_channel_range_center_frequency(
    lower_boundary_channel_id: int,
    upper_boundary_channel_id: int,
) -> int:
    """
    Retrieve the center frequency of the given coarse channel range.

    :param lower_boundary_channel_id: The coarse channel identifier of the lower boundary in the range of channels.
    :param upper_boundary_channel_id: The coarse channel identifier of the upper boundary in the range of channels.
    :return: The center frequency of the coarse channel range.
    """
    lower_boundary, _ = coarse_channel_frequency_range(lower_boundary_channel_id)
    total_bandwidth = coarse_channel_range_total_bandwidth(lower_boundary_channel_id, upper_boundary_channel_id)
    return lower_boundary + total_bandwidth / 2


def coarse_channel_range_total_bandwidth(
    lower_boundary_channel_id: int,
    upper_boundary_channel_id: int,
) -> int:
    """
    Retrieve the total bandwidth of the given coarse channel range.

    :param lower_boundary_channel_id: The coarse channel identifier of the lower boundary in the range of channels.
    :param upper_boundary_channel_id: The coarse channel identifier of the upper boundary in the range of channels.
    :return: The total bandwidth of the coarse channel range.
    """
    lower_boundary, _ = coarse_channel_frequency_range(lower_boundary_channel_id)
    _, upper_boundary = coarse_channel_frequency_range(upper_boundary_channel_id)
    return upper_boundary - lower_boundary
