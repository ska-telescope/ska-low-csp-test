"""Schemas currently missing from the SKA telescope model.

Schemas in this module are based on https://developer.skao.int/projects/ska-csp-lmc-low/en/0.10.1/templates/templates.html
"""

from typing import Literal

from typing_extensions import NotRequired, TypedDict

__all__ = [
    "LowCspAssignResourcesSchema",
    "LowCspConfigureSchema",
    "LowCspScanSchema",
    "LowCspReleaseResourcesSchema",
    "LowCspDelayModelSchema",
]


class LowCspAssignResourcesCommonSchema(TypedDict):
    """Schema for the ``common`` object in the LOW-CSP AssignResources command parameter."""

    subarray_id: int


class LowCspAssignResourcesLowCbfSchema(TypedDict):
    """Schema for the ``lowcbf`` object in the LOW-CSP AssignResources command parameter."""


class LowCspAssignResourcesPstSchema(TypedDict):
    """Schema for the ``pst`` object in the LOW-CSP AssignResources command parameter."""

    beams_id: list[int]


class LowCspAssignResourcesSchema(TypedDict):
    """Schema for the LOW-CSP AssignResources command parameter."""

    interface: Literal["https://schema.skao.int/ska-low-csp-assignresources/3.2"]
    transaction_id: NotRequired[str]
    common: LowCspAssignResourcesCommonSchema
    lowcbf: LowCspAssignResourcesLowCbfSchema
    pst: NotRequired[LowCspAssignResourcesPstSchema]


class LowCspConfigureCommonSchema(TypedDict):
    """Schema for the ``common`` object in the LOW-CSP Configure command parameter."""

    config_id: str
    subarray_id: int
    eb_id: NotRequired[str]


class LowCspConfigureLowCbfStationsStationBeamSchema(TypedDict):
    """Schema for the ``lowcbf.stations.stn_beams[]`` objects in the LOW-CSP Configure command parameter."""

    beam_id: int
    freq_ids: list[int]
    delay_poly: str


class LowCspConfigureLowCbfStationsSchema(TypedDict):
    """Schema for the ``lowcbf.stations`` object in the LOW-CSP Configure command parameter."""

    stns: list[tuple[int, int]]
    stn_beams: list[LowCspConfigureLowCbfStationsStationBeamSchema]


class LowCspConfigureLowCbfFspSchema(TypedDict):
    """Schema for the ``lowcbf.*.fsp`` object in the LOW-CSP Configure command parameter."""

    firmware: str
    fsp_ids: list[int]


class LowCspConfigureLowCbfVisStationBeamSchema(TypedDict):
    """Schema for the ``lowcbf.vis.stn_beams[]`` objects in the LOW-CSP Configure command parameter."""

    stn_beam_id: int
    host: list[tuple[int, str]]  # [[0, "192.168.0.1"]]
    mac: NotRequired[list[tuple[int, str]]]  # [[0, "0c-42-a1-9c-a2-1b"]]
    port: list[tuple[int, int, int]]  # [[0, 20000, 1]]
    integration_ms: int


class LowCspConfigureLowCbfVisSchema(TypedDict):
    """Schema for the ``lowcbf.vis`` object in the LOW-CSP Configure command parameter."""

    fsp: LowCspConfigureLowCbfFspSchema
    stn_beams: list[LowCspConfigureLowCbfVisStationBeamSchema]


class LowCspConfigureLowCbfTimingBeamsBeamSchema(TypedDict):
    """Schema for the ``lowcbf.timing_beams.beams[]`` objects in the LOW-CSP Configure command parameter."""

    pst_beam_id: int
    stn_beam_id: int
    stn_weights: list[float]
    delay_poly: str
    jones: str


class LowCspConfigureLowCbfTimingBeamsSchema(TypedDict):
    """Schema for the ``lowcbf.timing_beams`` object in the LOW-CSP Configure command parameter."""

    fsp: LowCspConfigureLowCbfFspSchema
    beams: list[LowCspConfigureLowCbfTimingBeamsBeamSchema]


class LowCspConfigureLowCbfSchema(TypedDict):
    """Schema for the ``lowcbf`` object in the LOW-CSP Configure command parameter."""

    stations: LowCspConfigureLowCbfStationsSchema
    vis: NotRequired[LowCspConfigureLowCbfVisSchema]
    timing_beams: NotRequired[LowCspConfigureLowCbfTimingBeamsSchema]


class LowCspConfigurePstBeamScanCoordinatesSchema(TypedDict):
    """Schema for the ``pst.beams[].scan.coordinates`` object in the LOW-CSP Configure command parameter."""

    equinox: float
    ra: str
    dec: str


class LowCspConfigurePstBeamScanChannelizationStageSchema(TypedDict):
    """Schema for the ``pst.beams[].scan.channelization_stages[]`` objects in the LOW-CSP Configure command parameter."""

    num_filter_taps: int
    filter_coefficients: list[float]
    num_frequency_channels: int
    oversampling_ratio: list[int]


class LowCspConfigurePstBeamScanSchema(TypedDict):
    """Schema for the ``pst.beams[].scan`` object in the LOW-CSP Configure command parameter."""

    activation_time: str
    bits_per_sample: int
    num_of_polarizations: int
    udp_nsamp: int
    wt_nsamp: int
    udp_nchan: int
    num_frequency_channels: int
    centre_frequency: float
    total_bandwidth: float
    observation_mode: Literal["VOLTAGE_RECORDER"]
    observer_id: str
    project_id: str
    pointing_id: str
    source: str
    itrf: list[float]
    receiver_id: str
    feed_polarization: str
    feed_handedness: int
    feed_angle: float
    feed_tracking_mode: str
    feed_position_angle: float
    oversampling_ratio: list[int]
    coordinates: LowCspConfigurePstBeamScanCoordinatesSchema
    max_scan_length: float
    subint_duration: float
    receptors: list[str]
    receptor_weights: list[float]
    num_channelization_stages: int
    channelization_stages: list[LowCspConfigurePstBeamScanChannelizationStageSchema]


class LowCspConfigurePstBeamSchema(TypedDict):
    """Schema for the ``pst.beams[]`` objects in the LOW-CSP Configure command parameter."""

    beam_id: int
    scan: LowCspConfigurePstBeamScanSchema


class LowCspConfigurePssSchema(TypedDict):
    """Schema for the ``pss`` object in the LOW-CSP Configure command parameter."""


class LowCspConfigurePstSchema(TypedDict):
    """Schema for the ``pst`` object in the LOW-CSP Configure command parameter."""

    beams: list[LowCspConfigurePstBeamSchema]


class LowCspConfigureSubarraySchema(TypedDict):
    """Schema for the ``subarray`` object in the LOW-CSP configure command parameter."""

    subarray_name: str


class LowCspConfigureSchema(TypedDict):
    """Schema for the LOW-CSP Configure command parameter."""

    interface: Literal["https://schema.skao.int/ska-low-csp-configure/3.2"]
    transaction_id: NotRequired[str]
    common: LowCspConfigureCommonSchema
    lowcbf: LowCspConfigureLowCbfSchema
    pss: NotRequired[LowCspConfigurePssSchema]
    pst: NotRequired[LowCspConfigurePstSchema]
    subarray: LowCspConfigureSubarraySchema


class LowCspScanCommonSchema(TypedDict):
    """Schema for the ``common`` object in the LOW-CSP Scan command parameter."""

    subarray_id: int


class LowCspScanLowCbfSchema(TypedDict):
    """Schema for the ``lowcbf`` object in the LOW-CSP Scan command parameter."""

    scan_id: int


class LowCspScanSchema(TypedDict):
    """Schema for the LOW-CSP Scan command parameter."""

    interface: Literal["https://schema.skao.int/ska-low-csp-scan/3.2"]
    transaction_id: NotRequired[str]
    common: LowCspScanCommonSchema
    lowcbf: LowCspScanLowCbfSchema


class LowCspReleaseResourcesCommonSchema(TypedDict):
    """Schema for the ``common`` object in the LOW-CSP ReleaseResources command parameter."""

    subarray_id: int


class LowCspReleaseResourcesLowCbfSchema(TypedDict):
    """Schema for the ``lowcbf`` object in the LOW-CSP ReleaseResources command parameter."""


class LowCspReleaseResourcesSchema(TypedDict):
    """Schema for the LOW-CSP ReleaseResources command parameter."""

    interface: Literal["https://schema.skao.int/ska-low-csp-releaseresources/3.2"]
    transaction_id: NotRequired[str]
    common: LowCspReleaseResourcesCommonSchema
    lowcbf: LowCspReleaseResourcesLowCbfSchema


class LowCspDelayModelStationBeamDelaySchema(TypedDict):
    """Schema for the ``station_beam_delays`` array in the LOW-CSP delay model."""

    station_id: int
    substation_id: int
    xypol_coeffs_ns: list[float]
    ypol_offset_ns: float


class LowCspDelayModelSchema(TypedDict):
    """Schema for the LOW-CSP delay model."""

    interface: Literal["https://schema.skao.int/ska-low-csp-delaymodel/1.0"]
    start_validity_sec: float
    cadence_sec: float
    validity_period_sec: float
    config_id: str
    station_beam: int
    subarray: int
    station_beam_delays: list[LowCspDelayModelStationBeamDelaySchema]
