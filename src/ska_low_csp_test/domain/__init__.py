"""
This package contains model information about LOW-CSP that is used in testing.
"""

from datetime import datetime, timedelta, timezone

from astropy import units as u
from astropy.coordinates import AltAz, EarthLocation, SkyCoord
from astropy.time import Time
from typing_extensions import Unpack

__all__ = [
    "SKA_LOW_ARRAY_CENTER",
    "TAI_Y2000",
    "sky_coordinates_from_altaz",
    "sky_coordinates_from_elevations",
]

SKA_LOW_ARRAY_CENTER = EarthLocation(
    lat=-26.82472208 * u.deg,
    lon=116.7644482 * u.deg,
    height=355.0 * u.m,
)
"""
SKA1-LOW array center location.
Reference: SKA1 Low Configuration Coordinates - Complete Set (SKA-TEL-SKO-0000422)
"""

TAI_Y2000 = datetime(1999, 12, 31, 23, 59, 28, tzinfo=timezone.utc)


def sky_coordinates_from_elevations(
    *elevation: float,
    ref_location: EarthLocation = SKA_LOW_ARRAY_CENTER,
    time: datetime | None = None,
) -> tuple[timedelta, Unpack[tuple[SkyCoord, ...]]]:
    """
    Calculate sky coordinates for the given elevations from the given reference location.

    :param elevation: One or more elevations
    :param ref_location: The reference location for the observation.
    :param time: The time of the observation. Defaults to the current time when not specified.
    :returns: Tuple containing the time offset relative to TAI Y2000 and the corresponding sky coordinates.
    """
    observing_time = time or datetime.now(timezone.utc)
    right_ascension_z = Time(observing_time, scale="utc", location=ref_location).sidereal_time("mean").to("deg")
    declinations = [ref_location.lat - (90 * u.deg - el * u.deg) for el in elevation]
    coordinates = tuple(SkyCoord(ra=right_ascension_z, dec=declination, frame="icrs") for declination in declinations)
    epoch_offset = observing_time - TAI_Y2000

    return epoch_offset, *coordinates


def sky_coordinates_from_altaz(
    *altaz_coords: AltAz,
    ref_location: EarthLocation = SKA_LOW_ARRAY_CENTER,
    time: datetime | None = None,
) -> tuple[timedelta, Unpack[tuple[SkyCoord, ...]]]:
    """
    Calculate sky coordinates for the given AltAz coordinates from the given reference location.

    :param altaz_coords: One or more AltAz coordinates.
    :param ref_location: The reference location for the observation.
    :param time: The time of the observation. Defaults to the current time when not specified.
    :returns: Tuple containing the time offset relative to TAI Y2000 and the corresponding sky coordinates.
    """
    observing_time = time or datetime.now(timezone.utc)
    observing_time_rel = Time(observing_time, scale="utc", location=ref_location)
    coordinates = tuple(
        SkyCoord(
            alt=altaz.alt,
            az=altaz.az,
            obstime=observing_time_rel,
            frame="altaz",
            location=ref_location,
        )
        for altaz in altaz_coords
    )
    epoch_offset = observing_time - TAI_Y2000

    return epoch_offset, *coordinates
