"""
Model information about SPEAD data in LOW-CSP.
"""

import enum
import itertools
import random
from dataclasses import dataclass
from typing import Any, Generator, Iterable

__all__ = [
    "LowCbfSdpSpeadStationBaselineMapping",
    "LowCbfSdpSpeadHeapCounter",
    "LowCbfSdpSpeadPolarizationOrderingIndex",
    "LowCbfVisibilitySpeadItem",
]


class LowCbfVisibilitySpeadItem(str, enum.Enum):
    """
    SPEAD items used in LOW-CBF visibility data.

    Source: `SKAO_IF-FLD-1611`_

    .. _SKAO_IF-FLD-1611: https://skaoffice.jamacloud.com/perspective.req#/containers/1034892?projectId=314
    """

    CBF_FIRMWARE_VERSION = "Firmw"
    CBF_HARDWARE_ID = "Hardw"
    CBF_SOURCE_ID = "SrcID"
    NUMBER_OF_BASELINES = "Basel"
    SCAN_ID = "ScaID"
    STATION_BEAM_ID = "BeaID"
    SUBARRAY_ID = "Subar"
    VISIBILITY_BASELINE_DATA = "Corre"
    VISIBILITY_CENTRE_FREQUENCY = "FreHz"
    VISIBILITY_CHANNEL_ID = "Chann"
    VISIBILITY_EPOCH_OFFSET = "tOffs"
    VISIBILITY_FREQUENCY_RESOLUTION = "Frequ"
    VISIBILITY_INTEGRATION_PERIOD = "Integ"
    VISIBILITY_OUTPUT_RESOLUTION = "Resol"
    VISIBILITY_SPS_EPOCH = "Epoch"
    ZOOM_WINDOW_ID = "ZoomI"


@dataclass
class LowCbfSdpSpeadHeapCounter:
    """Low-CBF SDP SPEAD heap counter unpacking.

    Source: `SKAO_IF-FLD-1610`_

    .. _SKAO_IF-FLD-1610: https://skaoffice.jamacloud.com/perspective.req#/items/1034860?projectId=314
    """

    subarray_id: int
    beam_id: int
    channel_id: int
    integration_id: int

    @classmethod
    def unpack(cls, cnt: int):
        """Unpack heap counter fields."""
        integration_id, cnt = cnt & 0b1111_1111_1111_1111, cnt >> 16
        channel_id, cnt = cnt & 0b1_1111_1111_1111_1111, cnt >> 17
        beam_id, cnt = cnt & 0b1_1111_1111, cnt >> 9
        subarray_id = cnt & 0b1_1111

        return cls(
            integration_id=integration_id,
            channel_id=channel_id,
            beam_id=beam_id,
            subarray_id=subarray_id,
        )


def _combinations_with_replacement(station_substation: list[int | tuple[int, int]]) -> Generator[Any, Any, Any]:
    # based on https://docs.python.org/3/library/itertools.html#itertools.combinations_with_replacement
    pool = tuple(station_substation)
    n = len(pool)
    for indices in itertools.product(range(n), repeat=2):
        if sorted(indices, reverse=True) == list(indices):
            yield tuple(pool[i] for i in indices)


class LowCbfSdpSpeadStationBaselineMapping:
    """Low-CBF SDP SPEAD Station to Visibility Baseline index mapping.

    ID: SKAO_IF-LOW_CBF_SDP-14 (v5)
    Source: `SKAO_IF-FLD-1608/SKAO_IF-LOW_CBF_SDP-14`_

    .. _SKAO_IF-FLD-1608/SKAO_IF-LOW_CBF_SDP-14: https://skaoffice.jamacloud.com/perspective.req?docId=1034855&projectId=314
    """

    station_substation: list[int | tuple[int, int]]
    baselines: list[tuple[int, int] | tuple[tuple[int, int], tuple[int, int]]]
    auto_indices: list[int]
    cross_indices: list[int]

    def __init__(self, station_substation: list[int | tuple[int, int]] | Iterable):
        self.station_substation = sorted(station_substation)
        self.baselines = list(_combinations_with_replacement(self.station_substation))
        self.auto_indices, self.cross_indices = [], []
        for idx, (a, b) in enumerate(self.baselines):
            lst = self.auto_indices if a == b else self.cross_indices
            lst.append(idx)

    def __len__(self) -> int:
        return len(self.baselines)

    def __getitem__(self, idx) -> tuple[int, int] | tuple[tuple[int, int], tuple[int, int]]:
        return self.baselines[idx]

    def auto_sample(self, sample_size=4):
        """Sample the auto-correlation indices."""
        return random.sample(self.auto_indices, min(len(self.auto_indices), sample_size))

    def cross_sample(self, sample_size=4):
        """Sample the cross-correlation indices."""
        return random.sample(self.cross_indices, min(len(self.cross_indices), sample_size))


class Polarization(enum.Enum):
    """Enum with ordering of Polarisation Products.

    ID: SKAO_IF-LOW_CBF_SDP-32 (v6)
    Source: `SKAO_IF-FLD-1611/SKAO_IF-LOW_CBF_SDP-32`_

    .. _SKAO_IF-FLD-1611/SKAO_IF-LOW_CBF_SDP-32: https://skaoffice.jamacloud.com/perspective.req?docId=1034873&projectId=314
    """

    XX = 0
    """Ant 1 Pol X × Ant 2 Pol X"""
    XY = 1
    """Ant 1 Pol X × Ant 2 Pol Y"""
    YX = 2
    """Ant 1 Pol Y × Ant 2 Pol X"""
    YY = 3
    """Ant 1 Pol Y × Ant 2 Pol Y"""


LowCbfSdpSpeadPolarizationOrderingIndex = Polarization
