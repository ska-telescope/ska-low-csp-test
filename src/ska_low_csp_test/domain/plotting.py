"""Helpers to plot data"""

import logging
from io import BytesIO

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from matplotlib.figure import Figure

from ska_low_csp_test.cbf.visibilities import (
    Polarization,
    baselines_amplitude,
    baselines_phase,
    channel_averaged_visibilities,
    time_averaged_scan_amplitude,
    time_averaged_scan_phase,
)

__all__ = [
    "plot_channel_averaged_amplitude_vs_time",
    "plot_channel_averaged_phase_vs_time",
    "plot_epoch_offset_increment",
    "plot_time_averaged_amplitude_vs_channel",
    "plot_time_averaged_phase_vs_channel",
]

logger = logging.getLogger(__name__)


def _plot(data: np.ndarray, label: str, x_label: str, y_label: str) -> Figure:
    plt.plot(range(data.shape[0]), data, label=label)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend(loc="lower right")
    plt.grid(True)
    return plt.gcf()


def _save_to_buffer(plot: Figure) -> bytes:
    with BytesIO() as buffer:
        plot.savefig(buffer, format="png")
        plt.close()
        return buffer.getvalue()


def plot_channel_averaged_amplitude_vs_time(
    data: xr.DataArray,
    baseline_idx: int | list[int],
    polarization: Polarization,
):
    """Generate plot for time averaged channels.

    :param data: :py:class:`~xarray.DataArray` containing visibilities.
    :param baseline_idx: crosscorrelation baseline index / indexes
    :param polarization: Polarisation values, normally "XY" and "YX"

    :return plotted figure in bytes
    """
    chan_avg = channel_averaged_visibilities(data)
    amplitude = baselines_amplitude(chan_avg, baseline_idx=baseline_idx, polarization=polarization)
    amplitude.plot.line(x="epoch_offset")

    plt.grid(True)
    return _save_to_buffer(plt.gcf())


def plot_channel_averaged_phase_vs_time(
    data: xr.DataArray,
    baseline_idx: int | list[int],
    polarization: Polarization,
):
    """Generate plot for time averaged channels.

    :param data: :py:class:`~xarray.DataArray` containing visibilities.
    :param baseline_idx: crosscorrelation baseline index / indexes
    :param polarization: Polarisation values, normally "XY" and "YX"

    :return plotted figure in bytes
    """
    chan_avg = channel_averaged_visibilities(data)
    phase = baselines_phase(chan_avg, baseline_idx=baseline_idx, polarization=polarization)
    phase.plot.line(x="epoch_offset")

    plt.grid(True)
    return _save_to_buffer(plt.gcf())


def plot_epoch_offset_increment(data: xr.DataArray) -> bytes:
    """Generate plotting figure for epoch offset increment.

    :param data: :py:class:`~xarray.DataArray` containing visibilities.

    :return plotted figure in bytes
    """
    time_sequence_ns = np.repeat(data.coords["epoch_offset"].data, len(data.coords["channel_id"].data))
    plot = _plot(time_sequence_ns / 1e9, "seconds", "packet number", "seconds")
    return _save_to_buffer(plot)


def plot_time_averaged_amplitude_vs_channel(
    data: xr.DataArray,
    baseline_idx: int | list[int],
    polarization: Polarization,
    trim_first_s: int = 5,
) -> bytes:
    """Generate plotting figures for amplitude.

    :param data: :py:class:`~xarray.DataArray` containing visibilities.
    :param baseline_idx: crosscorrelation baselines index / indexes
    :param polarization: Polarisation values, normally "XY" and "YX"
    :param trim_first_s: number of seconds to discard at beginning of scan; defaults to 5 seconds

    :return plotted figure in bytes
    """
    amplitude = time_averaged_scan_amplitude(data, baseline_idx, polarization, trim_first_s=trim_first_s)
    handles: list = amplitude.plot.line(x="channel_id", add_legend=False)
    labels = (
        [f"baseline {each}" for each in amplitude.coords["baseline"].data]
        if isinstance(baseline_idx, list)
        else [f"baseline {amplitude.coords['baseline'].data}"]
    )

    amp = amplitude.data.squeeze()
    detections = np.argwhere(amp > (np.mean(amp) + 5 * np.std(amp)))  # result shape: [[channel_idx, baseline_idx],
    #                                                                                  [channel_idx, baseline_idx]]
    cw_indices = np.unique(detections[:, 0])
    cw_channels = cw_indices + amplitude.coords["channel_id"].data[0]
    logger.info("Spikes: %s for %s", cw_channels, polarization)

    plt.grid(True)

    ax = plt.gca()
    for channel in cw_channels:
        labels.append(f"channel_id {channel}")
        handles.append(ax.axvline(x=channel, color="black", ls="dotted"))

    ax.axhline(y=float(np.nanmean(amplitude)), color="blue", ls="dotted")
    ax.axhline(y=float(np.nanmean(amplitude) + np.std(amplitude)), color="yellow", linestyle="dashed")
    ax.axhline(y=float(np.nanmean(amplitude) - np.std(amplitude)), color="yellow", linestyle="solid")

    plt.legend(handles=handles, labels=labels, loc="lower right")

    return _save_to_buffer(plt.gcf())


def plot_time_averaged_phase_vs_channel(
    data: xr.DataArray,
    baseline_idx: int | list[int],
    polarization: Polarization,
    trim_first_s: int = 5,
) -> bytes:
    """Generate plotting figures for phase.

    :param data: :py:class:`~xarray.DataArray` containing visibilities.
    :param baseline_idx: crosscorrelation baselines index / indexes
    :param polarization: Polarisation values, normally "XY" and "YX"
    :param trim_first_s: number of seconds to discard at beginning of scan; defaults to 5 seconds

    :return plotted figure in bytes
    """
    phase = time_averaged_scan_phase(data, baseline_idx, polarization, trim_first_s=trim_first_s)
    handles: list = phase.plot.line(x="channel_id", add_legend=False)
    labels = (
        [f"baseline {each}" for each in phase.coords["baseline"].data]
        if isinstance(baseline_idx, list)
        else [f"baseline {phase.coords['baseline'].data}"]
    )

    plt.grid(True)

    ax = plt.gca()
    handles.append(ax.axhline(y=float(np.nanmean(phase.data)), color="blue", ls="dotted"))
    handles.append(ax.axhline(y=float(np.nanmean(phase.data) + np.std(phase.data)), color="yellow", ls="dashed"))
    handles.append(ax.axhline(y=float(np.nanmean(phase.data) - np.std(phase.data)), color="yellow", ls="solid"))
    labels.append("mean")
    labels.append("mean + std")
    labels.append("mean - std")
    plt.legend(handles=handles, labels=labels, loc="lower right")

    return _save_to_buffer(plt.gcf())
