"""
Unit tests for the ska_low_csp_test.domain.channels module.
"""

import pytest
from hypothesis import given, target
from hypothesis.strategies import floats, integers

from ska_low_csp_test.domain import channels


@pytest.mark.parametrize("channel,expected", [(400, 312.5e6)])
def test_coarse_channel_center_frequency(channel: int, expected: float):
    """
    Tests whether the center frequency for the given coarse channel matches the expected value.
    """
    center = channels.coarse_channel_center_frequency(channel)
    assert center == pytest.approx(expected)


@given(integers(min_value=channels.LOWEST_VALID_COARSE_CHANNEL, max_value=channels.HIGHEST_VALID_COARSE_CHANNEL))
def test_coarse_channel_center_frequency_valid(channel: int):
    """
    Tests whether the center frequency for a given coarse channel is within the valid frequency range.
    """
    center = channels.coarse_channel_center_frequency(channel)
    target(center)

    assert center >= channels.LOWEST_VALID_FREQUENCY
    assert center <= channels.HIGHEST_VALID_FREQUENCY


@given(integers(min_value=channels.LOWEST_VALID_COARSE_CHANNEL + 1, max_value=channels.HIGHEST_VALID_COARSE_CHANNEL - 1))
def test_coarse_channel_center_frequency_valid_distance_to_neighbours(channel: int):
    """
    Tests whether the center frequency for a given coarse channel has the right distance to the center frequencies of
    the surrounding coarse channels.
    """
    center = channels.coarse_channel_center_frequency(channel)
    target(center)

    assert center - channels.coarse_channel_center_frequency(channel - 1) == pytest.approx(channels.COARSE_CHANNEL_WIDTH)
    assert channels.coarse_channel_center_frequency(channel + 1) - center == pytest.approx(channels.COARSE_CHANNEL_WIDTH)


@given(integers(min_value=channels.LOWEST_VALID_COARSE_CHANNEL + 1, max_value=channels.HIGHEST_VALID_COARSE_CHANNEL - 1))
def test_coarse_channel_frequency_range_valid(channel: int):
    """
    Tests whether the boundaries of the frequency range for a given coarse channel overlap with the boundaries of the
    surrounding coarse channels.
    """
    lower_bound, upper_bound = channels.coarse_channel_frequency_range(channel)
    prev_upper_bound = channels.coarse_channel_frequency_range(channel - 1)[1]
    next_lower_bound = channels.coarse_channel_frequency_range(channel + 1)[0]

    assert lower_bound == pytest.approx(prev_upper_bound)
    assert upper_bound == pytest.approx(next_lower_bound)


@given(floats(min_value=channels.LOWEST_VALID_FREQUENCY, max_value=channels.HIGHEST_VALID_FREQUENCY))
def test_coarse_channel_id_valid(frequency: float):
    """
    Tests whether the coarse channel identifier returned for the given frequency is valid.
    """
    channel = channels.coarse_channel_id(frequency)
    target(channel)

    assert channel >= channels.LOWEST_VALID_COARSE_CHANNEL
    assert channel <= channels.HIGHEST_VALID_COARSE_CHANNEL
