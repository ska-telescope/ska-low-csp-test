"""Module that provides classes to interact with LOW-CSP LMC TANGO devices."""

import json
import logging
import queue
from types import TracebackType

import tango
from ska_control_model import ObsMode, ObsState, PstProcessingMode, ResultCode

from ska_low_csp_test.domain.model import (
    LowCspAssignResourcesSchema,
    LowCspConfigureSchema,
    LowCspReleaseResourcesSchema,
    LowCspScanSchema,
)
from ska_low_csp_test.tango.events import AttributeChangeEventSubscription
from ska_low_csp_test.tango.ska_devices import SKABaseDevice, SKASubarrayDevice
from ska_low_csp_test.tango.tango import TangoContext

__all__ = ["LowCspControllerDevice", "LowCspSubarrayDevice", "LowCspDevices"]

LRC_TIMEOUT_MARGIN_S = 5


class LowCspControllerDevice(SKABaseDevice):
    """TANGO device that controls the LOW-CSP sub-system."""

    def on(
        self,
        timeout_s: float | None = None,
        wait_for_completion=True,
        devices: list[str] | None = None,
    ) -> None:
        self.long_running_command(
            "On",
            devices or [],
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def off(
        self,
        timeout_s: float | None = None,
        wait_for_completion=True,
        devices: list[str] | None = None,
    ) -> None:
        self.long_running_command(
            "Off",
            devices or [],
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def reset(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        return super().reset(
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def standby(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        return super().standby(
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def _handle_long_running_command_result(self, command_name: str, result: tuple[int, str]):
        result_value, message = result
        result_code = ResultCode(result_value)
        if result_code != ResultCode.OK:
            raise RuntimeError(
                f"{self}: Long-running command '{command_name}' finished with result {result_code.name} and message: {message}"
            )

    @property
    def command_timeout(self) -> int:
        """Returns the ``commandTimeout`` attribute value."""
        return self.read_attribute("commandTimeout", int)

    @command_timeout.setter
    def command_timeout(self, command_timeout_s: int):
        self.write_attribute("commandTimeout", int, command_timeout_s)

    @property
    def is_communicating(self) -> bool:
        """Indicates whether the controller device has a connection to the underlying controller devices."""
        return self.read_attribute("isCommunicating", bool)

    @property
    def _default_lrc_timeout_s(self) -> float:
        return self.command_timeout + LRC_TIMEOUT_MARGIN_S


class LowCspSubarrayDevice(SKASubarrayDevice):
    """TANGO device that controls a single LOW-CSP subarray."""

    def __init__(
        self,
        fqdn: str,
        device: tango.DeviceProxy,
        command_timeout_s: float = 30,
        logger: logging.Logger | None = None,
    ) -> None:
        super().__init__(fqdn, device, command_timeout_s, logger)
        self._obs_state_changes = queue.Queue()
        self._obs_state_subscription = AttributeChangeEventSubscription(
            self._device, "obsState", lambda s: self._obs_state_changes.put(ObsState(s)), self._logger
        )

    def __enter__(self) -> "LowCspSubarrayDevice":  # type: ignore[override]
        super().__enter__()
        self._obs_state_subscription.__enter__()
        return self

    def __exit__(
        self, exc_type: type[BaseException] | None, exc_value: BaseException | None, exc_tb: TracebackType | None
    ) -> None:
        super().__exit__(exc_type, exc_value, exc_tb)
        self._obs_state_subscription.__exit__(exc_type, exc_value, exc_tb)

    def _handle_long_running_command_result(self, command_name: str, result: tuple[int, str]):
        result_value, message = result
        result_code = ResultCode(result_value)
        if result_code != ResultCode.OK:
            raise RuntimeError(
                f"{self}: Long-running command '{command_name}' finished with result {result_code.name} and message: {message}"
            )

    def abort(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        return super().abort(
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def assign_resources(  # type: ignore[override]
        self,
        schema: LowCspAssignResourcesSchema,
        timeout_s: float | None = None,
        wait_for_completion=True,
    ) -> None:
        return super().assign_resources(
            schema,
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def configure(  # type: ignore[override]
        self,
        schema: LowCspConfigureSchema,
        timeout_s: float | None = None,
        wait_for_completion=True,
    ) -> None:
        return super().configure(
            schema,
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def end(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        return super().end(
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def end_scan(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        return super().end_scan(
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def go_to_idle(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call GoToIdle command on the subarray device."""
        self.long_running_command(
            "GoToIdle",
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def release_all_resources(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        return super().release_all_resources(
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def release_resources(  # type: ignore[override]
        self,
        schema: LowCspReleaseResourcesSchema,
        timeout_s: float | None = None,
        wait_for_completion=True,
    ) -> None:
        return super().release_resources(
            schema,
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def restart(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        return super().restart(
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def scan(  # type: ignore[override]
        self,
        schema: LowCspScanSchema,
        timeout_s: float | None = None,
        wait_for_completion=True,
    ) -> None:
        return super().scan(
            schema,
            timeout_s=timeout_s or self._default_lrc_timeout_s,
            wait_for_completion=wait_for_completion,
        )

    @property
    def command_timeout(self) -> int:
        """Returns the ``commandTimeout`` attribute value."""
        return self.read_attribute("commandTimeout", int)

    @command_timeout.setter
    def command_timeout(self, command_timeout_s: int):
        self.write_attribute("commandTimeout", int, command_timeout_s)

    @property
    def is_communicating(self) -> bool:
        """Indicates whether the subarray device has a connection to the underlying devices."""
        return self.read_attribute("isCommunicating", bool)

    @property
    def obs_mode(self) -> tuple[ObsMode, ...]:  # type: ignore[override] - LMC returns a tuple instead of a single value
        return tuple(ObsMode(value) for value in self.read_attribute("obsMode", tuple))

    @property
    def _default_lrc_timeout_s(self) -> float:
        return self.read_attribute("commandTimeout", int) + LRC_TIMEOUT_MARGIN_S

    def get_obs_state_changes(self) -> list[ObsState]:
        """Retrieve the ``obsState`` change events that were received from the subarray device.

        :return: The received state changes since this method was last called.
        """
        state_changes = []
        while True:
            try:
                state_changes.append(self._obs_state_changes.get_nowait())
            except queue.Empty:
                return state_changes


class LowCspCapabilityPst(SKABaseDevice):
    """TANGO device that controls the LOW-CSP capability-pst."""

    def _devices_json(self) -> dict:
        """Returns the ``devicesJson`` attribute value as dict."""
        return json.loads(self.read_attribute("devicesJson", str))

    def _get_beam_json_by_id(self, beam_id: int):
        for beam in self._devices_json()["pst"]:
            if beam["dev_id"] == beam_id:
                return beam
        raise ValueError(f"Beam ID {beam_id} not found in the devices JSON")

    def get_pst_beam_state(self, beam_id: int) -> str:
        """Returns the state of PST beam by its id."""
        return self._get_beam_json_by_id(beam_id)["state"]

    def get_pst_beam_obs_state(self, beam_id: int) -> ObsState:
        """Returns the observation state of PST beam by its id."""
        obs_state_str = self._get_beam_json_by_id(beam_id)["obs_state"]
        return next(obs_state for obs_state in ObsState if obs_state.name == obs_state_str)

    def get_pst_processing_mode(self, beam_id: int) -> PstProcessingMode:
        """Returns the processing mode of PST beam by its id."""
        proc_mode_str = self._get_beam_json_by_id(beam_id)["processing_mode"]
        return next(mode for mode in PstProcessingMode if mode.name == proc_mode_str)


class LowCspDevices:
    """LOW-CSP TANGO devices."""

    def __init__(self, tango_context: TangoContext) -> None:
        self._tango_context = tango_context
        self._controller_fqdns = tango_context.get_device_fqdns("LowCspController")
        self._subarray_fqdns = tango_context.get_device_fqdns("LowCspSubarray")
        self._capability_pst_fqdns = tango_context.get_device_fqdns("LowCspCapabilityPst")

    def controller(self) -> LowCspControllerDevice:
        """Retrieve the LOW-CSP controller TANGO device."""
        assert len(self._controller_fqdns) > 0, "No controller devices found for LOW-CSP"
        return self._tango_context.get_device(self._controller_fqdns[0], LowCspControllerDevice)

    def all_subarrays(self) -> list[LowCspSubarrayDevice]:
        """Retrieve all LOW-CSP subarray TANGO devices."""
        return [self._tango_context.get_device(fqdn, LowCspSubarrayDevice) for fqdn in self._subarray_fqdns]

    def subarray_by_id(self, subarray_id: int) -> LowCspSubarrayDevice:
        """Retrieve a LOW-CSP subarray TANGO device by its identifier.

        :param subarray_id: The identifier of the subarray to retrieve. Subarray identifiers start at 1.
        """
        matches = [fqdn for fqdn in self._subarray_fqdns if fqdn.endswith(f"{subarray_id:02d}")]
        assert len(matches) > 0, f"No subarray devices found for LOW-CSP with id {subarray_id}"
        return self._tango_context.get_device(matches[0], LowCspSubarrayDevice)

    def capability_pst(self) -> LowCspCapabilityPst:
        """Retrieve the LOW-CSP capability-pst TANGO device."""
        assert len(self._capability_pst_fqdns) > 0, "No capability-pst devices found for LOW-CSP"
        return self._tango_context.get_device(self._capability_pst_fqdns[0], LowCspCapabilityPst)
