"""Module that provides SystemUnderTest configuration."""

import json
import os
from dataclasses import dataclass


@dataclass
class SystemUnderTestConfiguration:
    """Configuration of system under test."""

    _config_file_path = os.environ["TEST_CONFIG_PATH"]

    with open(_config_file_path, "r", encoding="utf-8") as f:
        _config = json.loads(f.read())

    p4_ptp_master_port: str = _config["p4_ptp_master_port"]
    """The name of the P4 switch port to which the PTP master signal is connected."""
    test_data_location_cnic: str = _config.get("test_data_location_cnic")
    """The location of the test data relative to the CNIC pods."""
    test_data_location_ci: str = _config.get("test_data_location_ci")
    """The location of the test data relative to the CI job."""
    pcap_output_file_location: str = _config.get("pcap_output_file_location")
    """The location of PCAP output files folder"""
    sdp_data_location: str = _config["sdp_data_location"]
    """The location of the SDP data products mount in the CI runner."""
    sdp_default_ip: str = _config["sdp_default_ip"]
    """The default IP address used as SDP receive host."""
