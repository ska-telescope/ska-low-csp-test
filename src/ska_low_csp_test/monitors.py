"""Module that provides monitoring utilities."""

import contextlib
import logging

from ska_control_model import ObsState

from ska_low_csp_test.sut import SystemUnderTest
from ska_low_csp_test.tango.events import AttributeChangeEventSubscription


@contextlib.contextmanager
def monitor_p4_routing_tables_during_scans(
    system_under_test: SystemUnderTest,
    logger: logging.Logger | None = None,
):
    """
    Monitor function that watches all CSP subarray devices for state changes,
    and logs the P4 switch routing tables every time one of the subarrays starts a new scan.
    """
    logger = logger or logging.getLogger(__name__)
    connector = system_under_test.low_cbf_devices.connector()

    def log_routing_tables_when_scanning(state: ObsState):
        """
        Log the P4 switch routing tables whenever the ``obsState`` changed to ``ObsState.SCANNING``.
        """
        if state != ObsState.SCANNING:
            return

        connector.spead_unicast_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        connector.sdp_ip_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        connector.basic_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        connector.psr_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect

    subscriptions = [
        AttributeChangeEventSubscription(
            subarray.device,
            "obsState",
            lambda s: log_routing_tables_when_scanning(ObsState(s)),
            logger,
        )
        for subarray in system_under_test.low_csp_devices.all_subarrays()
    ]

    with contextlib.ExitStack() as stack:
        for subscription in subscriptions:
            stack.enter_context(subscription)

        yield
