"""Module that provides classes to interact with LOW-CBF CNIC TANGO devices."""

import json
from typing import Literal

import backoff
import tango
from typing_extensions import NotRequired, TypedDict

from ska_low_csp_test.tango.tango import TangoContext, TangoDevice

__all__ = [
    "CnicDevice",
    "CnicDevices",
    "BeginTransmitSchema",
    "CnicVirtualDigitiserConfigItemSchema",
    "CnicVirtualDigitiserConfigSourceItemSchema",
    "CnicVirtualDigitiserConfigSourcesSchema",
    "PrepareTransmitArguments",
    "PrepareTransmitSchema",
    "ReceivePcapArguments",
    "ReceivePcapSchema",
    "ResetSchema",
    "SelectPersonalitySchema",
    "StopReceiveSchema",
]


class ReceivePcapArguments(TypedDict):
    """Arguments for the ``receive_pcap`` method."""

    out_filename: str
    packet_size: int
    n_packets: int


class ReceivePcapSchema(TypedDict):
    """Schema for the ``receive_pcap`` method."""

    method: Literal["receive_pcap"]
    arguments: ReceivePcapArguments


class PrepareTransmitArguments(TypedDict):
    """Arguments for the ``prepare_transmit`` method."""

    in_filename: str
    rate: float


class PrepareTransmitSchema(TypedDict):
    """Schema for the ``prepare_transmit`` method."""

    method: Literal["prepare_transmit"]
    arguments: PrepareTransmitArguments


class BeginTransmitSchema(TypedDict):
    """Schema for the ``begin_transmit`` method."""

    method: Literal["begin_transmit"]


class ResetSchema(TypedDict):
    """Schema for the ``reset`` method."""

    method: Literal["reset"]


class StopReceiveSchema(TypedDict):
    """Schema for the ``stop_receive`` method."""

    method: Literal["stop_receive"]


CallMethodSchema = ReceivePcapSchema | PrepareTransmitSchema | BeginTransmitSchema | ResetSchema | StopReceiveSchema


class SelectPersonalitySchema(TypedDict):
    """Schema for the parameter of the SelectPersonality command."""

    version: str
    source: NotRequired[str]
    memory: NotRequired[str]


class CnicVirtualDigitiserConfigSourceItemSchema(TypedDict):
    """Schema for the x and y sources parameter of the ConfigureVirtualDigitiser command."""

    tone: bool
    scale: int
    seed: NotRequired[int]
    fine_frequency: NotRequired[int]
    delay_polynomial: NotRequired[list[float]]


class CnicVirtualDigitiserConfigSourcesSchema(TypedDict):
    """Schema for the ``sources`` property of a single config of the ConfigureVirtualDigitiser command."""

    x: list[CnicVirtualDigitiserConfigSourceItemSchema]
    y: list[CnicVirtualDigitiserConfigSourceItemSchema]


class CnicVirtualDigitiserConfigItemSchema(TypedDict):
    """Schema for a single station-substation-channel-beam configuration of the ConfigureVirtualDigitiser command."""

    scan: int
    """Scan ID."""
    subarray: int
    """Subarray ID."""
    station: int
    """Station ID."""
    substation: int
    """Substation ID."""
    frequency: int
    """Frequency ID."""
    beam: int
    """Beam ID."""
    sources: CnicVirtualDigitiserConfigSourcesSchema


class CnicConfigureVirtualDigitiserSchema(TypedDict):
    """Schema for the ConfigureVirtualDigitiser command."""

    sps_packet_version: Literal[2] | Literal[3]
    """SPS packet version to produce (2 or 3)"""
    stream_configs: list[CnicVirtualDigitiserConfigItemSchema]
    """List of stream configurations."""


class CnicPulseConfigSchema(TypedDict):
    """Schema for the ConfigurePulsarMode command."""

    enable: bool
    """Property to disable or enable Pulsar."""
    sample_count: NotRequired[list[int]]
    """List of 3 values for off/on/start cnic pulsar sample count."""


class CnicDevice(TangoDevice):
    """The :py:class:`CnicDevice` TANGO device."""

    DEFAULT_CNIC_PULSAR_SAMPLE_COUNT = [512, 1028, 271]
    """Default values for off/on/start cnic pulsar sample counts."""

    def call_method(self, schema: CallMethodSchema) -> None:
        """Call the CallMethod command on the CNIC device."""
        self.command("CallMethod", json.dumps(schema))

    def configure_virtual_digitiser(self, schema: CnicConfigureVirtualDigitiserSchema) -> None:
        """Call the ConfigureVirtualDigitiser command on the CNIC device."""
        self.command("ConfigureVirtualDigitiser", json.dumps(schema))

    def select_personality(self, schema: SelectPersonalitySchema, timeout_s: int = 60) -> None:
        """Call the SelectPersonality command on the CNIC device."""
        self._device.set_timeout_millis(timeout_s * 1000)
        self.command("SelectPersonality", json.dumps(schema))

    def start_source_delays(self) -> None:
        """Call the StartSourceDelays command on the CNIC device."""
        self.command("StartSourceDelays", "low-cbf/delaypoly/0")

    def stop_source_delays(self) -> None:
        """Call the StoptSourceDelays command on the CNIC device."""
        self.command("StopSourceDelays")

    def configure_pulsar_mode(self, schema: CnicPulseConfigSchema):
        """Call the ConfigurePulsarMode command on the CNIC device."""
        self.command("ConfigurePulsarMode", json.dumps(schema))

    @property
    def active_personality(self) -> str:
        """The FPGA personality that is currently active on the CNIC."""
        return self.read_attribute("activePersonality", str)

    @property
    def ready_to_transmit(self) -> bool:
        """Whether the CNIC is ready to start transmitting a PCAP file."""
        return self.read_attribute("ready_to_transmit", bool)

    @property
    def finished_transmit(self) -> bool:
        """Whether the CNIC has finished transmitting a PCAP file."""
        return self.read_attribute("finished_transmit", bool)

    @property
    def finished_receive(self) -> bool:
        """Whether the CNIC has finished receiving a PCAP file."""
        return self.read_attribute("finished_receive", bool)

    @property
    def enable_vd(self) -> bool:
        """Whether VD is enabled on the CNIC."""
        return self.read_attribute("enable_vd", bool)

    @enable_vd.setter
    def enable_vd(self, value: bool) -> None:
        self.write_attribute("enable_vd", bool, value)

    @property
    def cmac__cmac_stat_rx_unicast(self) -> int:
        """The number of unicast packets received."""
        return self.read_attribute("cmac__cmac_stat_rx_unicast", int)

    @property
    def hbm_pktcontroller__duplex(self) -> bool:
        """Is the CNIC is duplex mode?"""
        return self.read_attribute("hbm_pktcontroller_duplex", bool)

    @hbm_pktcontroller__duplex.setter
    def hbm_pktcontroller__duplex(self, value: bool) -> None:
        self.write_attribute("hbm_pktcontroller__duplex", bool, value)

    @property
    def hbm_pktcontroller__rx_complete(self) -> bool:
        """Whether Rx is complete on the CNIC."""
        return self.read_attribute("hbm_pktcontroller__rx_complete", bool)

    @property
    def hbm_pktcontroller__tx_enable(self) -> bool:
        """Whether Tx mode is enabled on the CNIC."""
        return self.read_attribute("hbm_pktcontroller__tx_enable", bool)

    @hbm_pktcontroller__tx_enable.setter
    def hbm_pktcontroller__tx_enable(self, value: bool) -> None:
        self.write_attribute("hbm_pktcontroller__tx_enable", bool, value)

    @property
    def hbm_pktcontroller__rx_enable_capture(self) -> bool:
        """Whether Rx capture mode is enabled on the CNIC."""
        return self.read_attribute("hbm_pktcontroller__rx_enable_capture", bool)

    @hbm_pktcontroller__rx_enable_capture.setter
    def hbm_pktcontroller__rx_enable_capture(self, value: bool) -> None:
        self.write_attribute("hbm_pktcontroller__rx_enable_capture", bool, value)

    @property
    @backoff.on_exception(
        backoff.constant,
        tango.DevFailed,
        max_tries=3,
    )
    def hbm_pktcontroller__rx_packet_count(self) -> int:
        """The number of packets received in Rx mode."""
        return self.read_attribute("hbm_pktcontroller__rx_packet_count", int)

    @property
    def hbm_pktcontroller__rx_packets_to_capture(self) -> int:
        """The number of packets to capture in Rx mode."""
        return self.read_attribute("hbm_pktcontroller__rx_packets_to_capture", int)

    @property
    def serial_number(self):
        """Property for getting Serial Number of cnic device."""
        return self._device.serialNumber


class CnicDevices:
    """CNIC TANGO devices."""

    def __init__(self, tango_context: TangoContext) -> None:
        self._tango_context = tango_context
        self._fqdns = tango_context.get_device_fqdns("CnicDevice")

    def all(self) -> list[CnicDevice]:
        """Retrieve all CNIC TANGO devices."""
        return list(map(self._create_device, self._fqdns))

    def rx(self) -> CnicDevice:
        """Retrieve the CNIC TANGO device used as receiver."""
        assert len(self._fqdns) > 0, "No CNIC devices found"
        return self._create_device(self._fqdns[0])

    def tx(self) -> CnicDevice:
        """Retrieve the CNIC TANGO device used as transmitter."""
        assert len(self._fqdns) > 0, "No CNIC devices found"
        return self._create_device(self._fqdns[-1])

    def by_id(self, cnic_id: int) -> CnicDevice:
        """Retrieve a CNIC TANGO device by its identifier.

        :param cnic_id: The identifier of the CNIC to retrieve. CNIC identifiers start at 1.
        """
        matches = [fqdn for fqdn in self._fqdns if fqdn.endswith(f"{cnic_id}")]
        assert len(matches) > 0, f"No CNIC devices found with id {cnic_id}"
        return self._create_device(matches[0])

    def _create_device(self, fqdn: str) -> CnicDevice:
        return self._tango_context.get_device(fqdn, CnicDevice)
