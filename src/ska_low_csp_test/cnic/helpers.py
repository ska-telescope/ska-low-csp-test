"""Module that provides helpers for LOW-CBF CNIC TANGO devices."""

import logging

from ska_low_csp_test.cnic.devices import CnicDevice, SelectPersonalitySchema
from ska_low_csp_test.synchronisation import wait_for_condition

__all__ = ["initialise_cnic"]


def initialise_cnic(
    cnic: CnicDevice,
    firmware_version: str,
    firmware_source: str | None = None,
    select_personality_command_timeout_s: int | None = None,
    logger: logging.Logger | None = None,
) -> None:
    """Initialise a CNIC TANGO device.

    :param firmware_version: The CNIC firmware version to select.
    :param firmware_source: The source for the CNIC firmware.
        Use ``None`` to instruct the CNIC to use its default source.
    :param select_personality_command_timeout_s: Timeout for the CNIC's SelectPersonality command, in seconds.
        Use ``None`` to use the default timeout.
    :param logger: Python logger.
    """
    logger = logger or logging.getLogger(__name__)
    if cnic.active_personality == "cnic":
        logger.debug("CNIC %s already initialised", cnic.fqdn)
    else:
        logger.info(
            "Initialising CNIC %s with firmware version %s and firmware source %s",
            cnic.fqdn,
            firmware_version,
            firmware_source,
        )
        personality: SelectPersonalitySchema = {"version": firmware_version}
        if firmware_source:
            personality = {**personality, "source": firmware_source}
        kwargs = {"timeout_s": select_personality_command_timeout_s} if select_personality_command_timeout_s else {}
        cnic.select_personality(personality, **kwargs)

        logger.debug("Waiting for CNIC %s to finish programming firmware", cnic.fqdn)
        wait_for_condition(
            lambda: cnic.active_personality == "cnic",
            timeout_s=30,
            message_on_timeout=lambda: f"CNIC {cnic.fqdn} active personality did not change",
        )

    # not only doing the following when the personality is left unchanged, on the off chance that a Tx CNIC has been
    # redeployed as an Rx CNIC, or vice-versa
    cnic.stop_source_delays()
    cnic.call_method({"method": "stop_receive"})
    cnic.call_method({"method": "reset"})
    cnic.configure_pulsar_mode({"enable": False, "sample_count": cnic.DEFAULT_CNIC_PULSAR_SAMPLE_COUNT})
    cnic.wait_for_attribute_value("hbm_pktcontroller__duplex", bool, False)
    cnic.hbm_pktcontroller__duplex = True
