"""SPS station data packet format.

Known bugs
----------

The SCAPY logic contained herein has not been tested in creating packets at
all. It has only been verified to be able to unpack packets!

This currently supports unpacking both Version 1 (LFAA SPEAD) and Version 2
(SPS SPEAD) packets. It also has preliminary support for Version 3. However, it
is biased towards Version 3: for instance Version 2 deprecates a specific field
in the antenna info section, and marks the space occupied by it as being
reserved for future use; version 3 defines this area to be used for the
Aperture ID. Also, Version 2 has two bytes for the Heap Counter field reserved
for the Substation Channel ID, whereas these bytes form part of the Packet
Counter in Version 3.
"""

from scapy.all import UDP  # type: ignore # pylint: disable=no-name-in-module
from scapy.all import Packet, Padding, bind_layers
from scapy.fields import (
    BitEnumField,
    BitField,
    ByteField,
    Field,
    IntField,
    PacketField,
    PacketListField,
    ShortField,
    XByteField,
    XShortField,
)

# Fields


class SixBytesField(Field):
    """6-bytes field."""

    def __init__(self, name, default):
        Field.__init__(self, name, default, "6s")

    def m2i(self, pkt, x):
        return int.from_bytes(x, "big")


class XFifteenBitIntField(BitField):
    """15-bits field with hex representation."""

    def __init__(self, name, default):
        BitField.__init__(self, name, default, 15, 4)

    def i2repr(self, pkt, x):
        return f"0x{x:04X}"


# SPEAD Items


class AntennaInfo(Packet):
    """SPS Antenna Info item contents."""

    fields_desc = [
        ByteField("substationId", 0),
        ByteField("subarrayId", 0),
        ShortField("stationId", 0),
        ShortField("apertureId", 0),
    ]


class CenterFreq(Packet):
    """SPS Center Frequency item contents."""

    fields_desc = [
        SixBytesField("frequency_hz", 0),
    ]


class ChannelInfo(Packet):
    """SPS Channel Information item contents."""

    fields_desc = [
        XShortField("logicalChannelId", 0),
        ShortField("beamId", 0),
        ShortField("frequencyId", 0),
    ]


class HeapCounter(Packet):
    """SPS Heap Counter item contents."""

    fields_desc = [
        ShortField("stationChannelId", 0),
        IntField("packetCounter", 0),
    ]


class PacketLength(Packet):
    """SPS Packet Length item contents."""

    # AKA PktLen
    fields_desc = [
        SixBytesField("packetPayloadLength", 0x2000),
    ]


class SampleOffset(Packet):
    """SPS Sample Offset item contents."""

    fields_desc = [
        SixBytesField("payloadOffset", 0),
    ]


class ScanId(Packet):
    """SPS Scan Identifier item contents."""

    fields_desc = [
        XShortField("scanIdReserved", 0x0),
        IntField("scanId", 0),
    ]


class SpeadHeader(Packet):
    """SPEAD Header item contents."""

    fields_desc = [
        XByteField("magic", 0x53),
        XByteField("version", 0x04),
        XByteField("itemPointerWidth", 0x02),
        XByteField("heapAddrWidth", 0x06),
        XShortField("reserved", 0x0000),
        XShortField("numberOfItems", 0x0008),
    ]


class SyncTime(Packet):
    """LFFA Synchronisation Time item contents."""

    fields_desc = [
        XShortField("syncTimeReserved", 0x0),
        IntField("unixEpochTime_s", 0),
    ]


class Timestamp(Packet):
    """LFFA Timestamp item contents."""

    fields_desc = [
        SixBytesField("timestamp_ns", 0),
    ]


class Value(Packet):
    """LFFA Sample Value item contents."""

    name = "Value"
    fields_desc = [
        ByteField("vPolReal", 0),
        ByteField("vPolImag", 0),
        ByteField("hPolReal", 0),
        ByteField("hPolImag", 0),
    ]


class ItemPointer(Packet):
    """SPEAD Item definition."""

    fields_desc = [
        BitEnumField("itemAddressMode", 0, 1, {0: "Absolute", 1: "Immediate"}),
        XFifteenBitIntField("itemIdentifier", 0),
    ]


# Packet


class SpsSpead(Packet):
    """SPS SPEAD packet."""

    name = "SpeadPacket"
    fields_desc = [
        PacketField("header", SpeadHeader(), SpeadHeader),
        PacketListField("items", [], ItemPointer, count_from=lambda pkt: pkt.header.numberOfItems),
        PacketListField(
            "values", [], Value, count_from=lambda _: 0x0800  # earlier firmware versions do not have the size field populated
        ),
    ]


bind_layers(UDP, SpsSpead)

bind_layers(SpeadHeader, Padding)

bind_layers(ItemPointer, HeapCounter, {"itemAddressMode": 1, "itemIdentifier": 0x0001})
bind_layers(ItemPointer, PacketLength, {"itemAddressMode": 1, "itemIdentifier": 0x0004})
bind_layers(ItemPointer, CenterFreq, {"itemAddressMode": 1, "itemIdentifier": 0x1011})
bind_layers(ItemPointer, SyncTime, {"itemAddressMode": 1, "itemIdentifier": 0x1027})
bind_layers(ItemPointer, Timestamp, {"itemAddressMode": 1, "itemIdentifier": 0x1600})
bind_layers(ItemPointer, ChannelInfo, {"itemAddressMode": 1, "itemIdentifier": 0x3000})
bind_layers(ItemPointer, AntennaInfo, {"itemAddressMode": 1, "itemIdentifier": 0x3001})
bind_layers(ItemPointer, ScanId, {"itemAddressMode": 1, "itemIdentifier": 0x3010})
bind_layers(ItemPointer, SampleOffset, {"itemIdentifier": 0x3300})

bind_layers(AntennaInfo, Padding)
bind_layers(CenterFreq, Padding)
bind_layers(ChannelInfo, Padding)
bind_layers(HeapCounter, Padding)
bind_layers(PacketLength, Padding)
bind_layers(SampleOffset, Padding)
bind_layers(ScanId, Padding)
bind_layers(SyncTime, Padding)
bind_layers(Timestamp, Padding)
bind_layers(Value, Padding)
