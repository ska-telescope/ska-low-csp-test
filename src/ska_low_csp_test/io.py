"""Module that provides functionality to deal with test input and output."""

import logging
import math
import os
import time
import warnings
from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from typing import Callable, Iterable, Optional

import allure
from astropy import units as u
from astropy.coordinates import ICRS, AltAz, SkyCoord

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.cbf.helpers import get_processors_for_fsp_ids
from ska_low_csp_test.cnic.devices import (
    CnicConfigureVirtualDigitiserSchema,
    CnicDevice,
    CnicPulseConfigSchema,
    CnicVirtualDigitiserConfigSourceItemSchema,
)
from ska_low_csp_test.delaypoly.devices import DelaypolyDevice, DelaypolyRaDecSchema
from ska_low_csp_test.domain import sky_coordinates_from_altaz
from ska_low_csp_test.domain.channels import SPECTRAL_MODE_FINE_CHANNELS_PER_COARSE_CHANNEL
from ska_low_csp_test.domain.model import LowCspConfigureSchema, LowCspScanSchema
from ska_low_csp_test.plugins import SubarrayLifecyclePlugin
from ska_low_csp_test.synchronisation import wait_for_condition

__all__ = [
    "OutputCapture",
    "OutputCaptureSettings",
    "SpsInputSimulator",
    "SpsInputSimulatorSettings",
    "TestIO",
]

warnings.warn(
    "The ska_low_csp_test.io module is deprecated, use ska_low_csp_test.testware instead",
    DeprecationWarning,
    stacklevel=2,
)


DEFAULT_SCAN_INTEGRATION_PERIODS = 1
"""Default number of integration periods a scan lasts."""


@dataclass
class OutputCaptureSettings:
    """Settings for the output capture."""

    output_dir_cnic: str
    """Path to the directory where captured output files should be stored, relative to the CNIC Kubernetes Pod."""

    output_dir_test: str
    """Path to the directory where captured output files should be stored, relative to the CI Kubernetes Pod."""

    min_packet_size: int = 200
    """The minimum packet size to use when capturing output, in bytes.

    Note::

        Using a packet size of less than ~200 bytes prevents capturing multiple scans to the same PCAP.
    """


@dataclass
class ActiveCaptureDetails:  # pylint: disable=too-many-instance-attributes
    """Details related to the output capture in progress."""

    beam_idx: int | None = None
    """Index of beam of interest."""

    pre_scan_packet_count = 0
    """Number of packets seen on processor network interface before SCAN command issued."""

    post_scan_packet_count = 0
    """Number of packets seen on processor network interface after SCAN command issued."""

    channel_count = 0
    """Correlator input channel count."""

    channel_multiplier = SPECTRAL_MODE_FINE_CHANNELS_PER_COARSE_CHANNEL
    """Factor between Correlator input channel count and output channel count."""

    integration_duration_s = 0.0
    """Duration of scan in seconds."""

    output_filepath = ""
    """Full path to output capture file."""

    failed = False
    """Failure detected during capture."""

    is_explicit = False
    """Is explicit capture control active."""

    has_pending_flush = False
    """Is a flush pending after an explicit capture."""

    enable = True
    """Allow user to disable capture."""

    flush_pps = 2000
    """Expected packet-per-second to disk write rate."""

    scan_end_time = datetime.min
    """Scan start time."""


class OutputCapture(SubarrayLifecyclePlugin):  # pylint: disable=too-many-instance-attributes
    """Capture the output of a LOW-CSP observation into a file."""

    def __init__(  # pylint: disable=too-many-arguments
        self,
        low_cbf_devices: LowCbfDevices,
        cnic_device: CnicDevice,
        settings: OutputCaptureSettings,
        logger: logging.Logger | None = None,
        do_check_for_file: bool = True,
    ) -> None:
        self._low_cbf_devices = low_cbf_devices
        self._allocator = low_cbf_devices.allocator()
        self._connector = low_cbf_devices.connector()
        self._correlators = []
        self._cnic = cnic_device
        self._settings = settings
        self._logger = logger or logging.getLogger(__name__)

        self._default_integration_periods = DEFAULT_SCAN_INTEGRATION_PERIODS
        self._current_integration_periods: int | None = None
        self._current_scan_config: LowCspConfigureSchema | None = None
        self._current_scan_duration_s: float | None = None
        self._capture = ActiveCaptureDetails()

        self._files: dict[tuple[str, int], str] = {}
        self._logger.info("OutputCapture is using CNIC %s", self._cnic.fqdn)

        # It is a temporary solution for CNIC writing speed issue.Remove when SKB-360 will be resolved.
        self._do_check_for_file = do_check_for_file

    def begin(self):
        """Explicitly start output capture."""
        if not self._current_scan_config:
            raise RuntimeError("Capture start commanded, but no current scan configuration is set")
        self._logger.debug("Explicitly start capture.")

        self._capture.is_explicit = True

        config_id = self._current_scan_config["common"]["config_id"]
        file_name = f"config-{config_id}_scan.pcap"
        self._capture.output_filepath = os.path.join(self._settings.output_dir_test, file_name)

        if not self._capture.enable:
            return

        self._logger.debug("Start capturing to PCAP file %s", file_name)
        self._cnic.call_method(
            {
                "method": "receive_pcap",
                "arguments": {
                    "n_packets": 999_999_999,
                    "packet_size": self._settings.min_packet_size,
                    "out_filename": os.path.join(self._settings.output_dir_cnic, file_name),
                },
            }
        )
        self._files[config_id, -1] = file_name

    def end(self):
        """Explicitly end output capture."""
        self._logger.debug("Explicitly end active capture.")
        self._cnic.call_method({"method": "stop_receive"})
        self._capture.has_pending_flush = True
        self._capture.is_explicit = False

    def flush(self):
        """Wait for PCAP to flush to disk."""
        if not self._capture.has_pending_flush:
            self._logger.warning("No flush pending.")
            return

        self._logger.debug("Explicitly wait for disk-flush.")
        try:
            wait_for_condition(
                lambda: self._cnic.finished_receive,
                timeout_s=math.ceil(self._cnic.hbm_pktcontroller__rx_packet_count / self._capture.flush_pps),
                message_on_timeout=lambda: f"Received {self._cnic.hbm_pktcontroller__rx_packet_count} packets.",
            )
        except TimeoutError:
            if not os.path.exists(self._capture.output_filepath):
                self._logger.debug("File does not exist: %s", self._capture.output_filepath)
                raise

        self._wait_for_sync_completion()
        self._capture.has_pending_flush = False

    def post_configure(self, schema: LowCspConfigureSchema) -> None:
        """Configure the output capture."""
        self._current_scan_config = schema
        self._configure_routing()

        self._capture.beam_idx = 0  # assume first beam is beam of interest
        self._capture.channel_count = len(schema["lowcbf"]["stations"]["stn_beams"][self._capture.beam_idx]["freq_ids"])

    def pre_scan(self, schema: LowCspScanSchema) -> None:
        """Start capturing output."""
        if not self._current_scan_config:
            self._logger.warning("Pre-scan hook called but no current scan configuration is set")
            return

        self._log_routing_tables()

        capture, config = self._capture, self._current_scan_config
        if (
            capture.beam_idx is not None
            and "lowcbf" in config
            and "vis" in config["lowcbf"]
            and "stn_beams" in config["lowcbf"]["vis"]
            and capture.beam_idx < len(config["lowcbf"]["vis"]["stn_beams"])
        ):
            integration_ms = config["lowcbf"]["vis"]["stn_beams"][capture.beam_idx]["integration_ms"]
        else:
            self._logger.warning("Could not retrieve 'integration_ms' from scan configuration.")
            return

        fsp_ids = list(self._fsp_ids)
        self._correlators = list(get_processors_for_fsp_ids(self._low_cbf_devices, fsp_ids=fsp_ids)) if fsp_ids else []

        if self._current_scan_duration_s:
            integration_periods = math.ceil(1000 * self._current_scan_duration_s / integration_ms)
        else:
            integration_periods = self._current_integration_periods or self._default_integration_periods
        capture.integration_duration_s = integration_periods * integration_ms / 1000

        capture.pre_scan_packet_count = self._cnic.cmac__cmac_stat_rx_unicast

        config_id, scan_id = config["common"]["config_id"], schema["lowcbf"]["scan_id"]
        if capture.is_explicit:
            *_, self._files[config_id, scan_id] = os.path.split(capture.output_filepath)
            return
        file_name = f"config-{config_id}_scan-{scan_id}.pcap"
        capture.output_filepath = os.path.join(self._settings.output_dir_test, file_name)

        if not capture.enable:
            return

        # We add 2 integration periods to account for INIT and END packets sent by the correlator
        n_packets_to_capture = capture.channel_count * capture.channel_multiplier * (integration_periods + 2)
        self._logger.debug("Start capturing %d packets to PCAP file %s", n_packets_to_capture, file_name)
        self._cnic.call_method(
            {
                "method": "receive_pcap",
                "arguments": {
                    "n_packets": n_packets_to_capture,
                    "packet_size": self._settings.min_packet_size,
                    "out_filename": os.path.join(self._settings.output_dir_cnic, file_name),
                },
            }
        )
        self._files[config_id, scan_id] = file_name

    def post_scan(self, schema: LowCspScanSchema) -> None:
        self._capture.scan_end_time = datetime.now(tz=timezone.utc) + timedelta(seconds=self._capture.integration_duration_s)

        def get_received_packet_count(capture: ActiveCaptureDetails) -> int:
            """Fetch current packet count, and update the capture details."""
            capture.post_scan_packet_count = self._cnic.cmac__cmac_stat_rx_unicast
            return capture.post_scan_packet_count

        wait_for_condition(
            lambda: get_received_packet_count(self._capture) > self._capture.pre_scan_packet_count,
            timeout_s=15,
            message_on_timeout=lambda: "No INIT packets seen.",
        )

    def pre_end_scan(self) -> None:
        """Delay END_SCAN.

        Note: a capture failure can only be detected if the retries are exhausted before the scan duration is reached.
        This means that to detect a capture failure, the scan duration needs to exceed MAX_RETRIES seconds.
        """
        self._logger.info("Waiting for CNIC to receive approximately %d seconds of data", self._capture.integration_duration_s)

        self._logger.debug("Packet count before SCAN command: %d", self._capture.pre_scan_packet_count)
        previous_packet_count = self._capture.post_scan_packet_count
        retries = MAX_RETRIES = 10  # pylint: disable=invalid-name
        stats_delay_read_time = datetime.now(timezone.utc)
        while retries:
            time.sleep(1)
            if stats_delay_read_time <= datetime.now(timezone.utc):
                stats_delay_read_time += timedelta(seconds=5)
                for correlator in self._correlators:
                    correlator.stats_delay  # pylint: disable=pointless-statement  # we're after the side-effect
            if self._cnic.hbm_pktcontroller__rx_complete:
                self._logger.debug("RX complete")
                break
            current_packet_count = self._cnic.cmac__cmac_stat_rx_unicast
            delta = current_packet_count - previous_packet_count
            self._logger.debug("Packet count delta: %d", delta)
            if delta < 0:
                self._logger.debug("Packet count decrease: %d now vs %d earlier", current_packet_count, previous_packet_count)
                # the counter was reset, or it wrapped !? -- can't continue
                self._capture.failed = True
                break
            if (self._capture.is_explicit or not self._capture.enable) and self._capture.scan_end_time < datetime.now(
                timezone.utc
            ):
                self._logger.debug("Scan duration (%ds) has elapsed.", self._capture.integration_duration_s)
                # we're done!
                break
            if delta == 0:
                self._logger.debug("No new packets: %d now vs %d previously", current_packet_count, previous_packet_count)
                # we're not receiving _any_ packets
                retries -= 1
            else:
                retries = MAX_RETRIES
            previous_packet_count = current_packet_count
        else:
            self._logger.info("Haven't seen any new packets after %d retries. Giving up!", MAX_RETRIES)
            self._capture.failed = self._cnic.cmac__cmac_stat_rx_unicast == self._capture.post_scan_packet_count

    def post_end_scan(self) -> None:
        """Finish capturing output."""
        self._log_routing_tables()

        if self._capture.failed:
            self._logger.warning(
                "Capture failure detected: no correlator output seen. Not waiting on CNIC to finish receiving."
            )
            self._cnic.call_method({"method": "stop_receive"})
            raise RuntimeError("Capture failure detected: no correlator output seen.")

        post_end_packet_count = self._cnic.cmac__cmac_stat_rx_unicast
        delta = post_end_packet_count - self._capture.pre_scan_packet_count
        self._logger.debug("Packet count after END SCAN command: %d (delta: %d)", post_end_packet_count, delta)

        if self._capture.is_explicit:
            return

        timeout_s = math.ceil(self._cnic.hbm_pktcontroller__rx_packets_to_capture / self._capture.flush_pps)
        self._logger.info(
            "Waiting for CNIC to finish writing received data to file: %s for %d seconds",
            self._capture.output_filepath,
            timeout_s,
        )

        try:
            wait_for_condition(
                lambda: self._cnic.finished_receive,
                timeout_s=timeout_s,
                message_on_timeout=lambda: f"Received {self._cnic.hbm_pktcontroller__rx_packet_count} packets.",
            )
        except TimeoutError:
            if not self._do_check_for_file:
                raise
            self._logger.warning("TimeoutError was raised during waiting for CNIC output. Workaround will be applied.")
            if not os.path.exists(self._capture.output_filepath):
                self._logger.debug("File does not exist: %s", self._capture.output_filepath)
                raise

    def _wait_for_sync_completion(self):
        """Wait for file to finish syncing."""

        def _stat_size() -> Optional[int]:
            try:
                return os.stat(self._capture.output_filepath).st_size
            except FileNotFoundError:
                return None

        self._logger.debug("Wait for file (%s) to settle...", self._capture.output_filepath)
        curr_size = _stat_size()
        retries = MAX_RETRIES = 30  # pylint: disable=invalid-name
        while retries and curr_size is not None:
            time.sleep(1)
            prev_size, curr_size = curr_size, _stat_size()
            if prev_size == curr_size:
                retries -= 1
            else:
                retries = MAX_RETRIES
        if curr_size is not None:
            self._logger.debug("File size (%d bytes) unchanged after %d retries.", curr_size, MAX_RETRIES)
        else:
            self._logger.warning("Capture file (%s) does not exist.", self._capture.output_filepath)

    def post_release_all_resources(self) -> None:
        # TODO: also add `post_release_resources()` with mutual exclusion on `_wait_for_sync_completion()` such that the wait
        # is only performed once
        if self._capture.has_pending_flush:
            # explicit capture sync handled by `flush()`
            return

        self._wait_for_sync_completion()

    def post_end(self) -> None:
        """Clear the scan configuration."""
        self._current_scan_config = None
        self._deconfigure_routing()

    def pre_abort(self) -> None:
        self._log_routing_tables()

    def post_abort(self) -> None:
        self._deconfigure_routing()

    def get_file(self, config_id: str, scan_id: int) -> str:
        """Get the location of the captured output.

        :param config_id: The configuration ID for which the scan output was captured.
        :param scan_id: The scan ID that was captured.
        :return: Path to the output file.
        """
        file_name = self._files[config_id, scan_id]
        return os.path.join(self._settings.output_dir_test, file_name)

    def clean(self):
        """Clear output files variable."""
        self._files = {}

    def set_integration_periods(self, periods: int) -> None:
        """Configure the OutputCapture to expect the given number of integration periods."""
        self._current_integration_periods = periods

    def set_scan_duration(self, scan_duration_s: float) -> None:
        """Configure the OutputCapture to remain scanning for the scan_duration_s duration."""
        self._current_scan_duration_s = scan_duration_s

    def set_capture_enable(self, enable: bool) -> None:
        """Enable/disable capture of data."""
        self._capture.enable = enable

    def set_minimum_capture_packet_size(self, size: int) -> None:
        """Adjust the minimum capture packet size."""
        self._settings.min_packet_size = size

    def _get_fpga_port(self, serial_number: str) -> str:
        for connection in self._allocator.connections:
            if "alveo" in connection and connection["alveo"] == serial_number:
                port_name = connection["port"]
                self._logger.debug("FPGA %s is connected to port %s", serial_number, port_name)
                return port_name

        raise RuntimeError(f"No port configured for serial number {serial_number}")

    @property
    def _fsp_ids(self) -> Iterable[int]:
        schema = self._current_scan_config
        if not schema or "vis" not in schema["lowcbf"]:
            return

        yield from schema["lowcbf"]["vis"]["fsp"]["fsp_ids"]

    @property
    def _fsp_ports(self) -> Iterable[str]:
        for fsp_id in self._fsp_ids:
            for serial_number in self._allocator.fsps.get(f"fsp_{fsp_id:02}", []):
                yield self._get_fpga_port(serial_number)

    def _configure_routing(self) -> None:
        self._logger.info("Configure port-to-port P4 routing")

        cnic_rx_port = self._get_fpga_port(self._cnic.serial_number)

        routes_to_add: list[tuple[str, str]] = []
        for port_name in self._fsp_ports:
            routes_to_add.append((port_name, cnic_rx_port))

        self._connector.add_basic_routes(routes_to_add)

    def _deconfigure_routing(self) -> None:
        self._logger.info("Deconfigure port-to-port P4 routing")

        routes = self._connector.basic_routing_table
        matching_routes = filter(lambda route: route["ingress port"] in self._fsp_ports, routes)
        routes_to_remove = [route["ingress port"] for route in matching_routes]
        if routes_to_remove:
            self._connector.remove_basic_routes(routes_to_remove)
            self._connector.basic_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect

    def _log_routing_tables(self):
        self._connector.spead_unicast_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        self._connector.sdp_ip_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        self._connector.basic_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        self._connector.psr_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect


@dataclass
class SpsInputSimulatorSettings:
    """Settings for the SPS input simulator."""

    get_sources_x: Callable[
        [int, int, int, int],
        list[CnicVirtualDigitiserConfigSourceItemSchema],
    ] = lambda station_id, substation_id, channel_id, beam_id: []
    """Function to configure a list of X sources for a single (station,substation,channel,beam) combination."""

    get_sources_y: Callable[
        [int, int, int, int],
        list[CnicVirtualDigitiserConfigSourceItemSchema],
    ] = lambda station_id, substation_id, channel_id, beam_id: []
    """Function to configure a list of Y sources for a single (station,substation,channel,beam) combination."""

    get_beam_direction: Callable[[int], Optional[AltAz]] = lambda scan_id: None
    """Function to configure a beam direction for a given scan_id."""

    get_source_directions: Callable[[int], Optional[list[AltAz]]] = lambda scan_id: None
    """Function to configure a source direction for a given scan_id."""

    config_pulse: Callable[[str], Optional[CnicPulseConfigSchema]] = lambda config_id: None
    """Function to configure CNIC into pulsar mode for a given configuration id."""


def _virtual_digitiser_config(
    settings: SpsInputSimulatorSettings,
    configure: LowCspConfigureSchema,
) -> CnicConfigureVirtualDigitiserSchema:
    """Generate Virtual Digitiser configuration.

    Note: the ``scan_id`` is hardcoded to zero as this information isn't used by the correlator. This does have an effect if
    the SPS data is captured, because the resultant PCAP file will not contain a valid ''scan_id''.
    """

    station_beams = configure["lowcbf"]["stations"]["stn_beams"]
    all_beams = set(station_beam["beam_id"] for station_beam in station_beams)
    all_channels = set(channel for station_beam in station_beams for channel in station_beam["freq_ids"])
    subarray_id = configure["common"]["subarray_id"]
    return {
        "sps_packet_version": 3,
        "stream_configs": [
            {
                "scan": 0,
                "subarray": subarray_id,
                "station": station_id,
                "substation": substation_id,
                "frequency": channel_id,
                "beam": beam_id,
                "sources": {
                    "x": settings.get_sources_x(station_id, substation_id, channel_id, beam_id),
                    "y": settings.get_sources_y(station_id, substation_id, channel_id, beam_id),
                },
            }
            for station_id, substation_id in configure["lowcbf"]["stations"]["stns"]
            for channel_id in all_channels
            for beam_id in all_beams
        ],
    }


def _virtual_digitiser_pulse_config(
    settings: SpsInputSimulatorSettings, config_schema: LowCspConfigureSchema, cnic_device: CnicDevice
) -> CnicPulseConfigSchema:
    """Generate Pulse Configuration for CNIC."""
    pulse_config = settings.config_pulse(config_schema["common"]["config_id"])
    if pulse_config is None:
        return {"enable": False, "sample_count": cnic_device.DEFAULT_CNIC_PULSAR_SAMPLE_COUNT}
    return pulse_config


class SpsInputSimulator(SubarrayLifecyclePlugin):
    """Generate SPS input data."""

    def __init__(  # pylint: disable=too-many-arguments
        self,
        cnic_device: CnicDevice,
        delaypoly_device: DelaypolyDevice,
        settings: SpsInputSimulatorSettings,
        reference_datetime: datetime | None = None,
        logger: logging.Logger | None = None,
    ) -> None:
        self._cnic = cnic_device
        self._delaypoly_emulator = delaypoly_device
        self._current_scan_config: LowCspConfigureSchema | None = None
        self._settings = settings
        self._ref_datetime = reference_datetime or datetime.now(timezone.utc)

        self._logger = logger or logging.getLogger(__name__)
        self._logger.info("InputSimulation is using CNIC %s", self._cnic.fqdn)

    def post_configure(self, schema: LowCspConfigureSchema) -> None:
        """Configure the input simulator."""
        self._current_scan_config = schema
        vd_config = _virtual_digitiser_config(self._settings, schema)
        self._cnic.configure_virtual_digitiser(vd_config)
        self._cnic.configure_pulsar_mode(_virtual_digitiser_pulse_config(self._settings, schema, self._cnic))
        self._cnic.start_source_delays()

    def pre_scan(self, schema: LowCspScanSchema) -> None:
        """Start the input simulator."""
        assert self._current_scan_config is not None, "Pre-scan hook called but no scan configuration is set"

        settings, configure, scan = self._settings, self._current_scan_config, schema
        self._configure_delaypoly_emulator(settings, configure, scan)
        self._wait_for_valid_delays(configure)

    def post_end(self) -> None:
        """Clear the scan configuration."""
        self._cnic.stop_source_delays()
        self._cnic.configure_pulsar_mode({"enable": False, "sample_count": self._cnic.DEFAULT_CNIC_PULSAR_SAMPLE_COUNT})
        self._current_scan_config = None

    def post_abort(self) -> None:
        """Reset the input simulator."""
        self._cnic.stop_source_delays()
        self._current_scan_config = None

    def sky_coordinates_from_altaz(self, *altaz_coords: AltAz) -> tuple[SkyCoord, ...]:
        """Wrapper for ''sky_coordinates_from_altaz()'' that supplies the reference ``time`` value."""
        _, *coordinates = sky_coordinates_from_altaz(*altaz_coords, time=self._ref_datetime)
        return tuple(coordinates)

    def _configure_delaypoly_emulator(
        self, settings: SpsInputSimulatorSettings, configure: LowCspConfigureSchema, scan: LowCspScanSchema
    ):
        scan_id = scan["lowcbf"]["scan_id"]
        subarray_id = configure["common"]["subarray_id"]
        station_beams = configure["lowcbf"]["stations"]["stn_beams"]
        all_beams = set(station_beam["beam_id"] for station_beam in station_beams)

        beam_alt_az = settings.get_beam_direction(scan_id)
        sources_alt_az = settings.get_source_directions(scan_id)

        for beam_id in all_beams:
            if beam_alt_az is None:
                self._delaypoly_emulator.beam_delay(
                    {"subarray_id": subarray_id, "beam_id": beam_id, "delay": [{"stn": 0, "nsec": 0.0}]}
                )
            else:
                (direction,) = self.sky_coordinates_from_altaz(beam_alt_az)
                direction_alt_az = _sky_coord_to_ra_dec(direction)
                self._delaypoly_emulator.beam_ra_dec(
                    {
                        "subarray_id": subarray_id,
                        "beam_id": beam_id,
                        "direction": direction_alt_az,
                    }
                )

            if sources_alt_az is None:
                self._delaypoly_emulator.source_delay(
                    {"subarray_id": subarray_id, "beam_id": beam_id, "delay": [[{"stn": 0, "nsec": 0.0}]] * 4}
                )
            else:
                directions = self.sky_coordinates_from_altaz(*sources_alt_az)
                directions_ra_dec = list(map(_sky_coord_to_ra_dec, directions))
                while len(directions_ra_dec) < 4:  # CNIC-VD expects at least 4 directions or it'll crash
                    directions_ra_dec.append(directions_ra_dec[0])
                self._delaypoly_emulator.source_ra_dec(
                    {
                        "subarray_id": subarray_id,
                        "beam_id": beam_id,
                        "direction": directions_ra_dec,
                    }
                )

    def _wait_for_valid_delays(self, config: LowCspConfigureSchema):
        subarray_id = config["common"]["subarray_id"]
        beam_ids = list(beam["beam_id"] for beam in config["lowcbf"]["stations"]["stn_beams"])
        self._logger.info("Waiting for delays for subarray %d and beams %s to become valid", subarray_id, beam_ids)
        cur_poly_time = {
            beam_id: self._delaypoly_emulator.read_source_attribute(subarray_id, beam_id)["start_validity_sec"]
            for beam_id in beam_ids
        }
        wait_for_condition(
            lambda: all(
                self._delaypoly_emulator.read_source_attribute(subarray_id, beam_id)["start_validity_sec"]
                != cur_poly_time[beam_id]
                for beam_id in beam_ids
            ),
            timeout_s=15,
            interval_s=0.25,
        )


def _sky_coord_to_ra_dec(sky_coord: SkyCoord) -> DelaypolyRaDecSchema:
    radec = sky_coord.transform_to(ICRS)
    return {
        "ra": radec.ra.to_string(u.hour),  # type: ignore
        "dec": radec.dec.to_string(u.deg),  # type: ignore
    }


class TestIO:
    """Encapsulate test input & output."""

    __test__ = False  # Whoah there, pytest. This isn't a test.

    def __init__(
        self,
        input_simulator: SpsInputSimulator,
        output_capture: OutputCapture,
        delaypoly_emulator: DelaypolyDevice,
        logger: logging.Logger | None = None,
    ) -> None:
        self._input_simulator = input_simulator
        self._output_capture = output_capture
        self._delaypoly_emulator = delaypoly_emulator
        self._logger = logger or logging.getLogger(__name__)

    def output_capture_end(self):
        """End output capture"""
        self._output_capture.end()

    def output_capture_begin(self):
        """Begin output capture."""
        self._output_capture.begin()

    def output_capture_flush(self):
        """Wait for flush-to-disk to complete."""
        self._output_capture.flush()

    @allure.step("Set scan duration to {periods} integration periods")
    def set_integration_periods(self, periods: int) -> None:
        """Set scan integration periods.

        :param periods: Number of scan integration periods.
        """
        self._output_capture.set_integration_periods(periods)

    @allure.step("Set scan duration to {scan_duration_s} seconds")
    def set_scan_duration(self, scan_duration_s: float):
        """Set scan duration.

        :param scan_duration_s: The scan time in seconds.
        """
        self._output_capture.set_scan_duration(scan_duration_s)

    def get_output(self, config_id: str, scan_id: int) -> str:
        """Retrieve test output."""
        return self._output_capture.get_file(config_id, scan_id)

    def set_capture_enable(self, enable: bool) -> None:
        """Enable/disable capturing of data."""
        self._output_capture.set_capture_enable(enable)

    def set_minimum_capture_packet_size(self, size: int) -> None:
        """Adjust the minimum capture packet size."""
        self._output_capture.set_minimum_capture_packet_size(size)
