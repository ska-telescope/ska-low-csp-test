"""Utility functions to deal with synchronisation to remote state."""

import logging
from typing import Any, Callable, TypedDict

import backoff

__all__ = ["wait_for_condition"]


class _BackoffDetails(TypedDict):
    target: Callable[..., Any]
    args: tuple[Any, ...]
    kwargs: dict[str, Any]
    tries: int
    elapsed: float


def wait_for_condition(
    condition: Callable[[], bool],
    timeout_s: float,
    message_on_timeout: Callable[[], str] | None = None,
    interval_s: float = 1,
    logger: logging.Logger | None = None,
) -> None:
    """Wait for a given condition to evaluate to ``True`` or until it times out.

    :param condition: Callable that is periodically invoked until it returns ``True``.
    :param timeout_s: The maximum time to wait for the condition to pass, in seconds.
    :param message_on_timeout: Callable to produce a message for the ``TimeoutError`` when a timeout occurs.
        This callable is invoked at the time of the timeout.
    :param interval_s: The time in between each condition invocation, in seconds.
    :param logger: Python logger.
    """
    logger = logger or logging.getLogger(__name__)

    def _handle_timeout(details: _BackoffDetails) -> None:
        description = f"Timeout after waiting {int(details['elapsed'])} seconds for condition."
        if message_on_timeout:
            description = f"{description} Message: {message_on_timeout()}"

        raise TimeoutError(description)

    @backoff.on_predicate(
        backoff.constant,
        max_time=timeout_s,
        backoff_log_level=logging.DEBUG,
        on_giveup=_handle_timeout,
        jitter=None,
        interval=interval_s,
    )
    def _is_finished() -> bool:
        try:
            return condition()
        except Exception:
            logger.debug("Exception while waiting for condition.", exc_info=True)
            return False

    _is_finished()
