"""Utilities to check PST output."""

import json
import logging
import pathlib
import time
from typing import Any

import allure
import backoff
import yaml

from ska_low_csp_test.pst.devices import PstDevices

__all__ = [
    "PstBeamMonitor",
    "PstOutput",
    "PstVoltageRecorderDataProduct",
]


class PstVoltageRecorderDataProduct:
    """
    Represents a single PST Voltage Recorder data product as described in
    https://confluence.skatelescope.org/display/SWSI/Voltage+Recorder+Data+Products.
    """

    def __init__(self, eb_id: str, scan_id: int, path: pathlib.Path) -> None:
        self._eb_id = eb_id
        self._scan_id = scan_id
        self._path = path

    @property
    def eb_id(self):
        """The execution block id for which this data product was generated."""
        return self._eb_id

    @property
    def scan_id(self):
        """The scan id for which this data product was generated."""
        return self._scan_id

    def read_ska_data_product_file(self) -> Any:
        """
        Attempt to read the ``ska-data-product.yaml`` file at the root of the data product directory.

        :raise AssertionError: if the file does not exist or contains invalid YAML
        :return: the contents of the file
        """
        file_path = self._check_file_exists("ska-data-product.yaml")
        with open(file_path, encoding="utf-8") as stream:
            try:
                return yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                raise AssertionError(f"{str(file_path)} contains invalid YAML") from exc

    def read_scan_config_file(self) -> Any:
        """
        Attempt to read the ``scan_configuration.json`` file at the root of the data product directory.

        :raise AssertionError: if the file does not exist or contains invalid JSON
        :return: the contents of the file
        """
        file_path = self._check_file_exists("scan_configuration.json")
        with open(file_path, encoding="utf-8") as stream:
            try:
                return json.load(stream)
            except json.JSONDecodeError as exc:
                raise AssertionError(f"{str(file_path)} contains invalid JSON") from exc

    def get_data_files(self):
        """
        Retrieve the paths to all DADA files in the ``data`` directory.

        :raise AssertionError: if the directory does not exist
        :return: list of :py:class:`pathlib.Path`
        """
        dir_path = self._check_directory_exists("data")
        return list(dir_path.glob("*.dada"))

    def get_weights_files(self):
        """
        Retrieve the paths to all DADA files in the ``weights`` directory.

        :raise AssertionError: if the directory does not exist
        :return: list of :py:class:`pathlib.Path`
        """
        dir_path = self._check_directory_exists("weights")
        return list(dir_path.glob("*.dada"))

    def get_stat_files(self):
        """
        Retrieve the paths to all HDF5 files in the ``stat`` directory.

        :raise AssertionError: if the directory does not exist
        :return: list of :py:class:`pathlib.Path`
        """
        dir_path = self._check_directory_exists("stat")
        return list(dir_path.glob("*.h5"))

    def _check_file_exists(self, relative_path: str) -> pathlib.Path:
        path = self._path / relative_path

        with allure.step(f"Verify that file {relative_path} exist"):

            @backoff.on_exception(backoff.expo, AssertionError, max_time=5 * 60)
            def _assert_path_exists():
                assert path.exists(), f"{path} does not exist"

            _assert_path_exists()
            assert path.is_file(), f"{path} is not a file"

        return path

    def _check_directory_exists(self, relative_path: str) -> pathlib.Path:
        path = self._path / relative_path

        with allure.step(f"Verify that directory {relative_path} exist"):

            @backoff.on_exception(backoff.expo, AssertionError, max_time=5 * 60)
            def _assert_path_exists():
                assert path.exists(), f"{path} does not exist"

            _assert_path_exists()
            assert path.is_dir(), f"{path} is not a directory"

        return path


class PstOutput:
    """Wrapper around PST output data."""

    def __init__(
        self,
        root_dir: str,
        logger: logging.Logger | None = None,
    ):
        self._root_dir = pathlib.Path(root_dir)
        self._logger = logger or logging.getLogger(__name__)

        if not self._root_dir.exists():
            self._logger.warning("PST output root directory %s does not exist", self._root_dir)

    @backoff.on_exception(backoff.expo, AssertionError, max_time=5 * 60)
    def get_voltage_recorder_data_product(self, eb_id: str, scan_id: int) -> PstVoltageRecorderDataProduct:
        """
        Get the voltage recorder data product for a scan.

        :param eb_id: The execution block id that the scan belongs to
        :param scan_id: The scan id
        :raises AssertionError: if the directory for the data product does not exist
        :return: :py:class:`PstVoltageRecorderDataProduct`
        """
        path = self._root_dir / "product" / eb_id / "pst-low" / str(scan_id)
        assert path.exists(), f"{path} does not exist"
        return PstVoltageRecorderDataProduct(eb_id=eb_id, scan_id=scan_id, path=path)


class PstBeamMonitor:
    """Utility to help monitor a PST beam."""

    def __init__(
        self,
        pst_devices: PstDevices,
        logger: logging.Logger | None = None,
    ):
        self._pst_devices = pst_devices
        self._logger = logger or logging.getLogger(__name__)

    def monitor_voltage_recorder(
        self,
        beam_id: int,
        duration_s: float,
        interval_s: float = 1.0,
    ):
        """
        Periodically log information about the PST beam while it's recording.

        Calling this method starts a monitoring loop that blocks the calling thread until ``duration_s`` has elapsed.

        :param beam_id: The id of the PST beam to monitor.
        :param duration_s: Time in seconds to monitor the beam.
        :param interval_s: Interval in seconds to check the progress.
        """
        beam = self._pst_devices.beam_by_id(beam_id)
        deadline = time.time() + duration_s
        if interval_s is None:
            interval_s = max(duration_s / 10, 1.0)
        while time.time() < deadline:
            self._logger.info(
                "PST beam data: receive rate=%f, record rate=%f, drop rate=%f, malformed=%d, misdirected=%d",
                beam.data_receive_rate,
                beam.data_record_rate,
                beam.data_drop_rate,
                beam.malformed_packets,
                beam.misdirected_packets,
            )
            sleep_time_s = min(interval_s, deadline - time.time())
            time.sleep(sleep_time_s)
