"""Module that provides classes to interact with PST TANGO devices."""

import json
from typing import TypedDict

from ska_low_csp_test.tango.ska_devices import SKASubarrayDevice
from ska_low_csp_test.tango.tango import TangoContext

__all__ = ["PstBeam", "PstDevices"]


class PstBeam(SKASubarrayDevice):
    """TANGO device that controls a single PST beam."""

    ChannelBlock = TypedDict(
        "ChannelBlock",
        destination_host=str,
        destination_port=int,
        destination_mac=str,
        start_pst_channel=int,
        start_pst_frequency=int,
        num_pst_channels=int,
    )

    ChannelBlockConfiguration = TypedDict(
        "ChannelBlockConfiguration",
        num_channel_blocks=int,
        channel_blocks=list[ChannelBlock],
    )

    @property
    def channel_block_configuration(self) -> ChannelBlockConfiguration:
        """Read the value of the ``channelBlockConfiguration`` attribute."""
        return json.loads(self.read_attribute("channelBlockConfiguration", str))

    @property
    def data_dropped(self):
        """Read the value of the ``dataDropped`` attribute."""
        return self.read_attribute("dataDropped", int)

    @property
    def data_drop_rate(self):
        """Read the value of the ``dataDropRate`` attribute."""
        return self.read_attribute("dataDropRate", float)

    @property
    def data_received(self):
        """Read the value of the ``dataReceived`` attribute."""
        return self.read_attribute("dataReceived", int)

    @property
    def data_receive_rate(self):
        """Read the value of the ``dataReceiveRate`` attribute."""
        return self.read_attribute("dataReceiveRate", float)

    @property
    def data_recorded(self):
        """Read the value of the ``dataRecorded`` attribute."""
        return self.read_attribute("dataRecorded", int)

    @property
    def data_record_rate(self):
        """Read the value of the ``dataRecordRate`` attribute."""
        return self.read_attribute("dataRecordRate", float)

    @property
    def malformed_packets(self):
        """Read the value of the ``malformedPackets`` attribute."""
        return self.read_attribute("malformedPackets", int)

    @property
    def malformed_packet_rate(self):
        """Read the value of the ``malformedPacketRate`` attribute."""
        return self.read_attribute("malformedPacketRate", float)

    @property
    def misdirected_packets(self):
        """Read the value of the ``misdirectedPackets`` attribute."""
        return self.read_attribute("misdirectedPackets", int)

    @property
    def misdirected_packet_rate(self):
        """Read the value of the ``misdirectedPacketRate`` attribute."""
        return self.read_attribute("misdirectedPacketRate", float)

    @property
    def misordered_packets(self):
        """Read the value of the ``misorderedPackets`` attribute."""
        return self.read_attribute("misorderedPackets", int)

    @property
    def misordered_packet_rate(self):
        """Read the value of the ``misorderedPacketRate`` attribute."""
        return self.read_attribute("misorderedPacketRate", float)

    def _handle_long_running_command_result(self, command_name, result):
        if result != "Completed":
            self._logger.warning("Long-running command '%s' returned unexpected result %s", command_name, result)


class PstDevices:
    """PST TANGO devices."""

    def __init__(self, tango_context: TangoContext) -> None:
        self._tango_context = tango_context
        self._beam_fqdns = tango_context.get_device_fqdns("PstBeam")

    def all_beams(self) -> list[PstBeam]:
        """Retrieve all PST beam TANGO devices."""
        return [self._tango_context.get_device(fqdn, PstBeam) for fqdn in self._beam_fqdns]

    def beam_by_id(self, beam_id: int) -> PstBeam:
        """Retrieve a PST beam TANGO device by its identifier.

        :param beam_id: The identifier of the beam to retrieve.
        """
        matches = [fqdn for fqdn in self._beam_fqdns if fqdn.endswith(f"{beam_id:02d}")]
        assert len(matches) > 0, f"No PST beam devices found with id {beam_id}"
        return self._tango_context.get_device(matches[0], PstBeam)
