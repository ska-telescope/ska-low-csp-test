"""Module to work with SPEAD data."""

import abc
import logging

import spead2
import spead2.recv

module_logger = logging.getLogger(__name__)


class SpeadDataVisitor(abc.ABC):
    """Abstract base class to visit SPEAD data heaps."""

    def visit_start_of_stream_heap(self, heap: spead2.recv.Heap, items: dict[str, spead2.Item]) -> None:
        """
        Visit a start-of-stream heap.

        :param heap: The start-of-stream heap to visit
        :param items: The items in the heap
        """

    def visit_data_heap(self, heap: spead2.recv.Heap, items: dict[str, spead2.Item]) -> None:
        """
        Visit a data heap.

        :param heap: The data heap to visit
        :param items: The items in the heap
        """

    def visit_end_of_stream_heap(self, heap: spead2.recv.Heap, items: dict[str, spead2.Item]) -> None:
        """
        Visit an end-of-stream heap.

        :param heap: The end-of-stream heap to visit
        :param items: The items in the heap
        """

    def on_finish(self) -> None:
        """
        Handler called after all SPEAD data is processed.
        """


class SpeadDataReader:  # pylint: disable=too-few-public-methods
    """Utility to read and process SPEAD data."""

    def __init__(self, *visitors: SpeadDataVisitor) -> None:
        self._visitors = visitors

    def read_pcap_file(self, pcap_file_path: str, logger: logging.Logger | None = None) -> None:
        """
        Read SPEAD data from the given PCAP file.

        :param pcap_file_path: Path to a PCAP file containing SPEAD data.
        """
        logger = logger or module_logger
        logger.info("Start reading SPEAD data from file: %s", pcap_file_path)
        stream = spead2.recv.Stream(
            spead2.ThreadPool(),
            config=spead2.recv.StreamConfig(stop_on_stop_item=False),
        )
        stream.add_udp_pcap_file_reader(pcap_file_path, filter="")
        item_group = spead2.ItemGroup()
        heap_number = 0

        for heap_number, heap in enumerate(stream):
            items = item_group.update(heap)

            if heap_number == 0:
                logger.debug(
                    "SPEAD flavour: SPEAD-%d-%d v%s",
                    heap.flavour.item_pointer_bits,
                    heap.flavour.heap_address_bits,
                    heap.flavour.version,
                )

            if heap.is_start_of_stream():
                for visitor in self._visitors:
                    visitor.visit_start_of_stream_heap(heap, items)
                continue

            if heap.is_end_of_stream():
                for visitor in self._visitors:
                    visitor.visit_end_of_stream_heap(heap, items)
                continue

            for visitor in self._visitors:
                visitor.visit_data_heap(heap, items)

        stream.stop()
        logger.debug("Read %d heaps", heap_number + 1)
        logger.info("Finished reading SPEAD data from file: %s", pcap_file_path)

        for visitor in self._visitors:
            visitor.on_finish()
