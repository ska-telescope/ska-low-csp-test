"""Module that provides classes for CBF Lifecycle plugins."""

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.domain.model import LowCspScanSchema
from ska_low_csp_test.plugins import SubarrayLifecyclePlugin


class RoutingTableLogger(SubarrayLifecyclePlugin):
    """Log P4 Routing Tables"""

    def __init__(self, low_cbf_devices: LowCbfDevices):
        self._connector = low_cbf_devices.connector()

    def post_scan(self, _schema: LowCspScanSchema) -> None:
        self._log_routing_tables()

    def pre_end(self) -> None:
        self._log_routing_tables()

    def _log_routing_tables(self):
        self._connector.spead_unicast_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        self._connector.sdp_ip_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        self._connector.basic_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
        self._connector.psr_routing_table  # pylint: disable=pointless-statement  # we're after the side-effect
