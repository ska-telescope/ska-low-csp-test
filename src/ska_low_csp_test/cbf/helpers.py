"""Module containing helper functions for interacting with LOW-CBF."""

import logging

from ska_low_csp_test.cbf.devices import LowCbfDevices

__all__ = [
    "configure_ptp",
    "get_processors_for_fsp_ids",
    "cbf_station_beam_delay_poly_uri",
    "cbf_timing_beam_delay_poly_uri",
    "cbf_timing_beam_jones_uri",
]


def get_processors_for_fsp_ids(
    low_cbf_devices: LowCbfDevices,
    fsp_ids: list[int],
    logger: logging.Logger | None = None,
):
    """
    Retrieve all LOW-CBF processors belonging to the given FSP IDs.
    """
    logger = logger or logging.getLogger(__name__)
    allocator = low_cbf_devices.allocator()

    for fsp_id in fsp_ids:
        for serial_number in allocator.fsps.get(f"fsp_{fsp_id:02}", []):
            processor_fqdn = allocator.processor_device_fqdns.get(serial_number)
            if processor_fqdn is None:
                logger.warning("FPGA serial number %s did not match any LowCbfProcessor device", serial_number)
                continue

            for processor in low_cbf_devices.all_processors():
                if processor.fqdn == processor_fqdn:
                    yield processor


def configure_ptp(
    low_cbf_devices: LowCbfDevices,
    ptp_clock_port: str,
    logger: logging.Logger | None = None,
):
    """
    Configure PTP on the ``LowCbfConnector`` TANGO device.
    """

    logger = logger or logging.getLogger(__name__)
    allocator = low_cbf_devices.allocator()
    connector = low_cbf_devices.connector()
    alveo_ports = [connection["port"] for connection in allocator.connections if "alveo" in connection]

    connector.add_ptp_clock_port({"PTPClock": [{"port": ptp_clock_port}]})
    connector.add_ports_to_ptp({"PTPMulti": [{"port": port} for port in alveo_ports]})
    connector.add_ptp_entry(
        {
            "ptp": [
                {
                    "src": {"port": port},
                    "dst": {"port": ptp_clock_port},
                }
                for port in alveo_ports
            ],
        }
    )


def cbf_station_beam_delay_poly_uri(subarray_id: int, station_beam_id: int) -> str:
    """LOW-CBF station delaypoly emulator URI."""
    return f"low-cbf/delaypoly/0/delay_s{subarray_id:02}_b{station_beam_id:02}"


def cbf_timing_beam_delay_poly_uri(subarray_id: int, station_beam_id, pst_beam_id: int) -> str:
    """LOW-CBF timing beam delaypoly emulator URI."""
    return f"low-cbf/delaypoly/0/pst_s{subarray_id:02}_b{station_beam_id:02}_{pst_beam_id}"


def cbf_timing_beam_jones_uri() -> str:
    """LOW-CBF timing beam Jones matrix emulator URI."""
    return ""
