"""Module that provides classes to interact with LOW-CBF TANGO devices."""

import contextlib
import enum
import json
from typing import Mapping

import tango
from typing_extensions import NotRequired, TypedDict

from ska_low_csp_test.tango.ska_devices import SKABaseDevice, SKASubarrayDevice
from ska_low_csp_test.tango.tango import TangoContext

__all__ = [
    "LowCbfAllocatorDevice",
    "LowCbfConnectorBasicRouteSchema",
    "LowCbfConnectorDevice",
    "LowCbfConnectorSdpIpRouteSchema",
    "LowCbfConnectorSpeadRouteSchema",
    "LowCbfControllerDevice",
    "LowCbfDevices",
    "LowCbfHardwareConnectionAlveo",
    "LowCbfHardwareConnectionLink",
    "LowCbfProcessorDebugRegWriteCommandSchema",
    "LowCbfProcessorDevice",
    "LowCbfProcessorStatsDelayAttributeBeamsSchema",
    "LowCbfProcessorStatsDelayAttributeBeamsStnDelayNsSchema",
    "LowCbfProcessorStatsDelayAttributeSchema",
    "LowCbfProcessorStatsModeAttributeSchema",
    "LowCbfSubarrayDevice",
]


class LowCbfHardwareConnectionAlveo(TypedDict):
    """Hardware connection information for a connected Alveo."""

    switch: str
    port: str
    speed: int
    alveo: str


class LowCbfHardwareConnectionLink(TypedDict):
    """Hardware connection information for a link."""

    switch: str
    port: str
    speed: int
    link: str


def parse_hardware_connection(line: str) -> LowCbfHardwareConnectionAlveo | LowCbfHardwareConnectionLink:
    """
    Parse a hardware connection string of the following format:

    switch=p4_01  port=9/0  speed=100  alveo=XFL1XXQM0FKW
    switch=p4_01  port=2/0  speed=100  link=stn_001
    """
    items = {}
    for key, value in [item.split("=") for item in line.split()]:
        if key == "speed":
            items[key] = int(value)
        else:
            items[key] = value

    if "alveo" in items:
        return LowCbfHardwareConnectionAlveo(**items)

    return LowCbfHardwareConnectionLink(**items)


class LowCbfAllocatorDevice(SKABaseDevice):
    """The :py:class:`LowCbfAllocator` TANGO device."""

    # TODO: remove when all users have been reworked.
    @property
    def fsps(self) -> dict[str, list[str]]:
        """Wrapper for Allocator.fsps Tango attribute.

        :return: A dictionary of FSP identifiers with their associated Alveo serial numbers.
        """
        return json.loads(self.read_attribute("fsps", str))

    @property
    def processor_device_fqdns(self) -> dict[str, str]:
        """Wrapper for the procDevFqdn TANGO attribute.

        :return: A dictionary containing Alveo serial numbers mapped to processor TANGO device FQDNs.
        """
        return json.loads(self.read_attribute("procDevFqdn", str))

    @property
    def connections(self) -> list[LowCbfHardwareConnectionAlveo | LowCbfHardwareConnectionLink]:
        """Property for getting connection of Allocator Tango Device."""
        connections_prop_name = "hardware_connections"
        connections_prop = self._device.get_property(connections_prop_name)
        return [parse_hardware_connection(line) for line in connections_prop[connections_prop_name]]


LowCbfConnectorBasicRouteSchema = TypedDict(
    "LowCbfConnectorBasicRouteSchema",
    {
        "ingress port": str,
        "port": str,
    },
)
"""Basic Route information."""

LowCbfConnectorPsrRouteSchema = TypedDict(
    "LowCbfConnectorPsrRouteSchema",
    {
        "Beam": str,
        "port": str,
        "UDP_port": int,
    },
)


class LowCbfConnectorSdpIpRouteSchema(TypedDict):
    """SDP IP Route information."""

    IP_Address: str
    port: str


class LowCbfConnectorSpeadRouteSchema(TypedDict):
    """SPEAD Route information."""

    Frequency: int
    Beam: int
    Sub_array: int
    port: str


@contextlib.contextmanager
def _suppress_already_exists_error():
    try:
        yield
    except tango.DevFailed as err:
        if not any("ALREADY_EXISTS" in each.desc for each in err.args):
            raise err


class LowCbfConnectorDevice(SKABaseDevice):  # pylint: disable=too-many-public-methods
    """The :py:class:`LowCbfConnector` TANGO device."""

    @property
    def basic_routing_table(self) -> list[LowCbfConnectorBasicRouteSchema]:
        """Property for getting the basic routing table."""
        return json.loads(self.read_attribute("BasicRoutingTable", str))["Basic"]

    def add_basic_routes(self, routes: list[tuple[str, str]]) -> None:
        """
        Add routes to the basic routing table.

        :param routes: list of (source port name, destination port name) tuples
        """
        update_basic_entry_arg = {
            "basic": [{"src": {"port": src_port}, "dst": {"port": dst_port}} for src_port, dst_port in routes]
        }

        self.command("UpdateBasicEntry", json.dumps(update_basic_entry_arg))

    def remove_basic_routes(self, src_ports: list[str]) -> None:
        """
        Remove routes from the basic routing table.

        :param src_ports: list of source port names
        """
        remove_basic_entry_arg = {"basic": [{"src": {"port": src_port}} for src_port in src_ports]}

        self.command("RemoveBasicEntry", json.dumps(remove_basic_entry_arg))

    def clear_basic_routing_table(self) -> None:
        """Clear the basic routing table."""
        self.command("ClearBasicTable", "all")

    @property
    def psr_routing_table(self) -> list[LowCbfConnectorPsrRouteSchema]:
        """Property for getting the PSR routing table."""
        return json.loads(self.read_attribute("PSRRoutingTable", str))["PSR"]

    def add_psr_routes(self, routes: list[tuple[int, str, int]]):
        """
        Add routes to the PSR routing table.

        :param routes: list of (beam id, PST P4 port, PST UDP port) tuples
        """
        update_psr_entry_arg = {
            "psr": [
                {"src": {"beam": beam_id}, "dst": {"port": pst_p4_port, "udp_port": pst_udp_port}}
                for beam_id, pst_p4_port, pst_udp_port in routes
            ]
        }

        self.command("UpdatePSREntry", json.dumps(update_psr_entry_arg))

    def remove_psr_routes(self, routes: list[int]):
        """
        Remove routes from the PSR routing table.

        :param routes: list of beam ids.
        """
        remove_psr_entry_arg = {"psr": [{"src": {"beam": beam_id}} for beam_id in routes]}

        self.command("RemovePSREntry", json.dumps(remove_psr_entry_arg))

    def clear_psr_routing_table(self) -> None:
        """Clear the PSR routing table."""
        self.command("ClearPSRTable", "all")

    @property
    def sdp_ip_routing_table(self) -> list[LowCbfConnectorSdpIpRouteSchema]:
        """Property for getting the SDP IP routing table."""
        return json.loads(self.read_attribute("SdpIpRoutingTable", str))["SDP_IP"]

    def add_sdp_ip_routes(self, routes: list[tuple[str, str]]) -> None:
        """
        Add a route to the SDP IP routing table.
        :param routes: list of (source IP address, destination port name) tuples
        """
        update_sdp_ip_entry_arg = {
            "sdp_ip": [{"src": {"ip": src_ip}, "dst": {"port": dst_port}} for src_ip, dst_port in routes]
        }
        self.command("UpdateSdpIpEntry", json.dumps(update_sdp_ip_entry_arg))

    def clear_sdp_ip_routing_table(self) -> None:
        """Clear the SDP IP routing table."""
        self.command("ClearSdpIpTable", "all")

    def remove_sdp_ip_routes(self, src_ips: list[str]) -> None:
        """
        Remove route from the SDP IP routing table.

        :param src_ips: list of source IP addresses
        """
        remove_sdp_ip_entry_arg = {"sdp_ip": [{"src": {"ip": src_ip}} for src_ip in src_ips]}
        self.command("RemoveSdpIpEntry", json.dumps(remove_sdp_ip_entry_arg))

    @property
    def spead_unicast_routing_table(self) -> list[LowCbfConnectorSpeadRouteSchema]:
        """Property for getting the SPEAD Unicast Routing Table."""
        return json.loads(self.read_attribute("SpeadUnicastRoutingTable", str))["Spead"]

    def add_spead_unicast_routes(self, routes: list[tuple[int, int, int, str]]) -> None:
        """
        Add a route to the SPEAD unicast routing table.

        :param routes: list of (frequency channel identifier,
                                beam identifier,
                                subarray identifier,
                                destination port name) tuples
        """
        update_spead_unicast_entry_arg = {
            "spead": [
                {
                    "src": {"frequency": frequency_id, "beam": beam_id, "sub_array": subarray_id},
                    "dst": {"port": dst_port},
                }
                for frequency_id, beam_id, subarray_id, dst_port in routes
            ]
        }
        self.command("UpdateSpeadUnicastEntry", json.dumps(update_spead_unicast_entry_arg))

    def clear_spead_unicast_routing_table(self) -> None:
        """Clear the SPEAD unicast routing table."""
        self.command("ClearSpeadUnicastTable", "all")

    def remove_spead_unicast_routes(self, routes: list[tuple[int, int, int]]) -> None:
        """
        Remove route from the SPEAD unicast routing table.

        :param routes: list of (frequency channel identifier, beam identifier, subarray identifier) tuples
        """
        remove_spead_unicast_entry_arg = {
            "spead": [
                {
                    "src": {"frequency": frequency_id, "beam": beam_id, "sub_array": subarray_id},
                }
                for frequency_id, beam_id, subarray_id, in routes
            ]
        }
        self.command("RemoveSpeadUnicastEntry", json.dumps(remove_spead_unicast_entry_arg))

    def reset_port_statistics(self) -> None:
        """Wrapper for Connector.ResetPortStatistics()."""
        self.command("ResetPortStatistics")

    PortDescriptor = TypedDict(
        "PortDescriptor",
        {
            "port": str,
        },
    )

    SrcDstRouteDescriptor = TypedDict(
        "SrcDstRouteDescriptor",
        {
            "src": PortDescriptor,
            "dst": PortDescriptor,
        },
    )

    AddPtpClockPortCommandArg = TypedDict(
        "AddPtpClockPortCommandArg",
        {
            "PTPClock": list[PortDescriptor],
        },
    )

    def add_ptp_clock_port(self, arg: AddPtpClockPortCommandArg) -> None:
        """Wrapper for the ``AddPTPClockPort`` command."""
        with _suppress_already_exists_error():
            self.command("AddPTPClockPort", json.dumps(arg))

    AddPortsToPtpCommandArg = TypedDict(
        "AddPortsToPtpCommandArg",
        {
            "PTPMulti": list[PortDescriptor],
        },
    )

    def add_ports_to_ptp(self, arg: AddPortsToPtpCommandArg) -> None:
        """Wrapper for the ``AddPortsToPTP`` command."""
        self.command("AddPortsToPTP", json.dumps(arg))

    AddPtpEntryCommandArg = TypedDict(
        "AddPtpEntryCommandArg",
        {
            "ptp": list[SrcDstRouteDescriptor],
        },
    )

    def add_ptp_entry(self, arg: AddPtpEntryCommandArg) -> None:
        """Wrapper for the ``AddPTPEntry`` command."""
        with _suppress_already_exists_error():
            self.command("AddPTPEntry", json.dumps(arg))

    UpdatePtpEntryCommandArg = TypedDict(
        "UpdatePtpEntryCommandArg",
        {
            "ptp": list[SrcDstRouteDescriptor],
        },
    )

    def update_ptp_entry(self, arg: UpdatePtpEntryCommandArg) -> None:
        """Wrapper for the ``UpdatePTPEntry`` command."""
        with _suppress_already_exists_error():
            self.command("UpdatePTPEntry", json.dumps(arg))

    def clear_ptp_table(self) -> None:
        """Wrapper for the ``ClearPTPTable`` command."""
        self.command("ClearPTPTable", "")

    HealthStatusPort = TypedDict(
        "HealthStatusPort",
        {
            "Enable": bool,
            "Up": bool,
            "Speed": str,
            "Rx": int,
            "Tx": int,
        },
    )

    HealthStatusGeneral = TypedDict(
        "HealthStatusGeneral",
        {
            "IP_address": str,
            "Program": str,
            "TCP_port": str,
        },
    )

    HealthStatus = TypedDict(
        "HealthStatus",
        {
            "ports": Mapping[str, HealthStatusPort],
            "general": HealthStatusGeneral,
        },
    )

    @property
    def health_status(self) -> HealthStatus:
        """Property for the ``health_status`` TANGO attribute."""
        return json.loads(self.read_attribute("health_status", str))


class LowCbfControllerDevice(SKABaseDevice):
    """The :py:class:`LowCbfController` TANGO device."""


class LowCbfProcessorStatsDelayAttributeBeamsStnDelayNsSchema(TypedDict):
    """JSON schema for the ``stats_delay.beams.stn_delay_ns`` attribute of the LowCbfProcessor TANGO device."""

    stn: int
    ns: float


class LowCbfProcessorStatsDelayAttributeBeamsSchema(TypedDict):
    """JSON schema for the ``stats_delay.beams`` attribute of the LowCbfProcessor TANGO device."""

    beam_id: int
    valid_delay: bool
    subscription_valid: bool
    delay_start_secs: list[float]
    stn_delay_ns: list[LowCbfProcessorStatsDelayAttributeBeamsStnDelayNsSchema]


class LowCbfProcessorStatsDelayAttributeSchema(TypedDict):
    """JSON schema for the ``stats_delay`` attribute of the LowCbfProcessor TANGO device."""

    subarray_id: int
    beams: list[LowCbfProcessorStatsDelayAttributeBeamsSchema]
    firmware: str | None
    fw_version: NotRequired[str]


class LowCbfProcessorStatsModeAttributeSchema(TypedDict):
    """JSON schema for the ``stats_mode`` attribute of the LowCbfProcessor TANGO device."""

    ready: bool
    firmware: str | None
    fw_version: NotRequired[str]


class LowCbfProcessorDebugRegWriteCommandSchema(TypedDict):
    """JSON schema for the ``DebugRegWrite`` command of the LowCbfProcessor TANGO device."""

    name: str
    offset: int
    value: int


class LowCbfProcessorDevice(SKABaseDevice):
    """The :py:class:`LowCbfProcessor` TANGO device."""

    @property
    def fw_personality(self) -> str:
        """Property to retrieve the ``fw_personality`` attribute."""
        return self.read_attribute("fw_personality", str)

    @property
    def serial_number(self):
        """Property to retrieve the ``serialNumber`` attribute."""
        return self.read_attribute("serialNumber", str)

    @property
    def stats_delay(self) -> LowCbfProcessorStatsDelayAttributeSchema:
        """Property to retrieve the ``stats_delay`` attribute."""
        return json.loads(self.read_attribute("stats_delay", str))

    @property
    def stats_mode(self) -> LowCbfProcessorStatsModeAttributeSchema:
        """Property to retrieve the ``stats_mode`` attribute."""
        return json.loads(self.read_attribute("stats_mode", str))

    def debug_reg_write(self, schema: LowCbfProcessorDebugRegWriteCommandSchema) -> None:
        """Wrapper for the ``DebugRegWrite`` command."""
        self.command("DebugRegWrite", json.dumps(schema))


class LowCbfSubarrayDevice(SKASubarrayDevice):
    """The :py:class:`LowCbfSubarray` TANGO device."""

    class DelaysValidity(enum.IntEnum):
        """The legal values of the ``delaysValid`` attribute.

        As defined in the `CBF Subarray Device`_.

        .. _CBF Subarray Device: https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf/-/blob/4aaed8e06eec2de2735341dc48c6cd1fe5143b6f/src/ska_low_cbf/subarray/subarray_device.py#L101
        """  # noqa: E501 pylint: disable=line-too-long

        INVALID = 0
        VALID = 1
        UNUSED = 2

    @property
    def assigned_processors(self) -> list[str]:
        """Property to retrieve the processors currently assigned to the subarray."""
        return json.loads(self.read_attribute("assigned_processors", str))

    @property
    def delays_valid(self) -> DelaysValidity:
        """Property to retrieve the ``delaysValid`` attribute."""
        return self.read_attribute("delaysValid", LowCbfSubarrayDevice.DelaysValidity)


class LowCbfDevices:
    """LOW-CBF TANGO devices."""

    def __init__(self, tango_context: TangoContext) -> None:
        self._tango_context = tango_context
        self._allocator_fqdns = tango_context.get_device_fqdns("LowCbfAllocator")
        self._connector_fqdns = tango_context.get_device_fqdns("LowCbfConnector")
        self._controller_fqdns = tango_context.get_device_fqdns("LowCbfController")
        self._processor_fqdns = tango_context.get_device_fqdns("LowCbfProcessor")
        self._subarray_fqdns = tango_context.get_device_fqdns("LowCbfSubarray")

    def allocator(self) -> LowCbfAllocatorDevice:
        """Retrieve the LOW-CBF allocator TANGO device."""
        assert len(self._allocator_fqdns) > 0, "No allocator devices found for LOW-CBF"
        return self._tango_context.get_device(self._allocator_fqdns[0], LowCbfAllocatorDevice)

    def connector(self) -> LowCbfConnectorDevice:
        """Retrieve the LOW-CBF connector TANGO device."""
        assert len(self._connector_fqdns) > 0, "No connector devices found for LOW-CBF"
        return self._tango_context.get_device(self._connector_fqdns[0], LowCbfConnectorDevice)

    def controller(self) -> LowCbfControllerDevice:
        """Retrieve the LOW-CBF controller TANGO device."""
        assert len(self._controller_fqdns) > 0, "No controller devices found for LOW-CBF"
        return self._tango_context.get_device(self._controller_fqdns[0], LowCbfControllerDevice)

    def all_processors(self) -> list[LowCbfProcessorDevice]:
        """Retrieve all LOW-CBF processor TANGO devices."""
        return [self._tango_context.get_device(fqdn, LowCbfProcessorDevice) for fqdn in self._processor_fqdns]

    def processor_by_id(self, processor_id: str) -> LowCbfProcessorDevice:
        """Retrieve a LOW-CBF processor TANGO device by its identifier.

        :param processor_id: The identifier of the processor to retrieve. Processor identifiers are of the form "0.0.1".
        """
        matches = [fqdn for fqdn in self._processor_fqdns if fqdn.endswith(processor_id)]
        assert len(matches) > 0, f"No processor devices found for LOW-CBF with id {processor_id}"
        return self._tango_context.get_device(matches[0], LowCbfProcessorDevice)

    def all_subarrays(self) -> list[LowCbfSubarrayDevice]:
        """Retrieve all LOW-CBF subarray TANGO devices."""
        return [self._tango_context.get_device(fqdn, LowCbfSubarrayDevice) for fqdn in self._subarray_fqdns]

    def subarray_by_id(self, subarray_id: int) -> LowCbfSubarrayDevice:
        """Retrieve a LOW-CBF subarray TANGO device by its identifier.

        :param subarray_id: The identifier of the subarray to retrieve. Subarray identifiers start at 1.
        """
        matches = [fqdn for fqdn in self._subarray_fqdns if fqdn.endswith(f"{subarray_id:02d}")]
        assert len(matches) > 0, f"No subarray devices found for LOW-CBF with id {subarray_id}"
        return self._tango_context.get_device(matches[0], LowCbfSubarrayDevice)
