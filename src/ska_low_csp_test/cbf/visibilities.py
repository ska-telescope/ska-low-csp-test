"""Helpers to work with LOW-CBF visibility data."""

import logging
from collections import defaultdict

import allure
import numpy as np
import pandas as pd
import spead2
import spead2.recv
import xarray as xr

from ska_low_csp_test.domain.spead import LowCbfSdpSpeadHeapCounter
from ska_low_csp_test.domain.spead import LowCbfSdpSpeadPolarizationOrderingIndex as Polarization
from ska_low_csp_test.domain.spead import LowCbfSdpSpeadStationBaselineMapping, LowCbfVisibilitySpeadItem
from ska_low_csp_test.spead import SpeadDataReader, SpeadDataVisitor

__all__ = [
    "Polarization",
    "baselines_amplitude",
    "baselines_phase",
    "channel_averaged_visibilities",
    "time_averaged_scan_amplitude",
    "time_averaged_scan_phase",
    "time_averaged_visibilities",
    "unpack_pcap_file",
    "get_visibility_data",
    "get_visibility_metadata",
]

module_logger = logging.getLogger(__name__)


def baselines_amplitude(
    time_averaged_vis: xr.DataArray,
    baseline_idx: int | list[int],
    polarization: Polarization,
) -> xr.DataArray:
    """Calculate baseline amplitude values from time averaged visibilities array.

    :param time_averaged_vis: xarray DataArray with time averaged visibilities
    :param baseline_idx: baseline index / indexes
    :param polarization: Polarisation selector

    :returns: xr DataArray with amplitude values
    """
    amplitude = xr.apply_ufunc(np.abs, time_averaged_vis.isel(baseline=baseline_idx, polarization=polarization.value))
    amplitude.name = f"{polarization.name} Amplitude"
    amplitude.attrs["units"] = "arb"
    return amplitude


def baselines_phase(
    time_averaged_vis: xr.DataArray,
    baseline_idx: int | list[int],
    polarization: Polarization,
) -> xr.DataArray:
    """Calculate baseline phase values from time averaged visibilities array.

    :param time_averaged_vis: xarray DataArray with time averaged visibilities
    :param baseline_idx: baseline index / indexes
    :param polarization: Polarisation selector

    :returns: xr DataArray with phase values in radians
    """
    phase = xr.apply_ufunc(np.angle, time_averaged_vis.isel(baseline=baseline_idx, polarization=polarization.value))
    phase.name = f"{polarization.name} Phase"
    phase.attrs["units"] = "rad"
    return phase


def channel_averaged_visibilities(data: xr.DataArray) -> xr.DataArray:
    """Average all visibilities over all channels.

    :param data: :py:class:`~xarray.DataArray` containing visibilities

    :returns: :py:class:`~xarray.DataArray` containing visibilities averaged over all channels
    """
    return data.mean(dim="channel_id")


def scan_visibilities(data: xr.DataArray, trim_first_s: int = 0) -> xr.DataArray:
    """Retrieve a subset of a scan starting after the first n seconds.

    :param data: :py:class:`~xarray.DataArray` containing visibilities
    :param trim_first_s: number of seconds to discard at beginning of scan

    :returns: the scan visibility samples, excluding the first n seconds.
    """
    t0 = data.coords["epoch_offset"].data[0]
    return data.where(data.epoch_offset >= t0 + trim_first_s * 1e9)


def time_averaged_scan_amplitude(
    data: xr.DataArray,
    baseline_idx: int | list[int],
    polarization: Polarization,
    trim_first_s: int = 0,
) -> xr.DataArray:
    """Average scan visibilities over a subset of time samples.

    :param data: :py:class:`~xarray.DataArray` containing visibilities.
    :param baseline_idx: baseline index / indexes
    :param polarization: Polarisation selector
    :param trim_first_s: number of seconds to discard at start of scan

    :returns: phase of time averaged scan visibilities
    """
    visibilities = scan_visibilities(data, trim_first_s)
    time_averaged_vis = visibilities.mean(dim="epoch_offset")
    return baselines_amplitude(time_averaged_vis, baseline_idx=baseline_idx, polarization=polarization)


def time_averaged_scan_phase(
    data: xr.DataArray,
    baseline_idx: int | list[int],
    polarization: Polarization,
    trim_first_s: int = 0,
) -> xr.DataArray:
    """Average scan visibilities over a subset of time samples.

    :param data: :py:class:`~xarray.DataArray` containing visibilities.
    :param baseline_idx: baseline index / indexes
    :param polarization: Polarisation selector
    :param trim_first_s: number of seconds to discard at start of scan

    :returns: phase of time averaged scan visibilities
    """
    visibilities = scan_visibilities(data, trim_first_s)
    time_averaged_vis = visibilities.mean(dim="epoch_offset")
    return baselines_phase(time_averaged_vis, baseline_idx=baseline_idx, polarization=polarization)


def time_averaged_visibilities(data: xr.DataArray) -> xr.DataArray:
    """Average all visibilities over time samples.

    :param data: :py:class:`~xarray.DataArray` containing visibilities.

    :returns: :py:class:`~xarray.DataArray` containing visibilities averaged over time
    """
    return data.mean(dim="epoch_offset")


@allure.step("Unpack PCAP file")
def unpack_pcap_file(
    pcap_file_path: str,
    logger: logging.Logger | None = None,
) -> xr.DataTree:
    """Unpack a PCAP file containing visibility SPEAD data.

    This function extracts the visibility data and metadata of each scan into a :py:class:`xarray.DataSet`,
    and combines all datasets into a single :py:class:`xarray.DataTree` representing the full contents of the file.
    Scans are grouped based on their subarray and beam IDs and their paths in the tree are represented as::

        /subarray_<subarray_id>/beam_<beam_id>/scan_<scan_id>/[data|metadata]

    For example::

        /
        ├─ subarray_01/
        │  ├─ beam_01/
        │  │  ├─ scan_1/
        │  │  │  ├─ data
        │  │  │  ├─ metadata
        │  │  ├─ scan_2/
        │  │  │  ├─ data
        │  │  │  ├─ metadata
        ├─ subarray_02/
        │  ├─ beam_01/
        │  │  ├─ scan_1/
        │  │  │  ├─ data
        │  │  │  ├─ metadata

    For more information, refer to :doc:`xarray:user-guide/data-structures`.

    The DataTree also has a "content" attribute with a list of dictionaries with
    ``subarray_id``, ``beam_id`` and ``scan_ids`` values which describe the
    contents present inside the DataTree.

    :param pcap_file_path: Path to the PCAP file containing visibility data.
    :param logger: Python logger.

    :returns: A :py:class:`~xarray.DataTree` representing the PCAP file contents.
    """
    extractor = _VisibilityDataExtractor(logger=logger)
    reader = SpeadDataReader(extractor)
    reader.read_pcap_file(pcap_file_path, logger=logger)

    dt = xr.DataTree.from_dict(
        {
            f"subarray_{subarray_id:02}/beam_{beam_id:02}": collector.scans
            for (subarray_id, beam_id), collector in extractor.collectors.items()
        }
    )

    dt.attrs["content"] = [
        {
            "subarray_id": subarray_id,
            "beam_id": beam_id,
            "scan_ids": list(map(lambda scan: int(scan.removeprefix("scan_")), collector.scans.keys())),
        }
        for (subarray_id, beam_id), collector in extractor.collectors.items()
    ]

    return dt


def get_visibility_data(
    tree: xr.DataTree,
    subarray_id: int,
    beam_id: int,
    scan_id: int,
    station_baseline_mapping: LowCbfSdpSpeadStationBaselineMapping | None = None,
) -> xr.DataArray:
    """Retrieve visibility data from a :py:class:`xarray.DataTree` for a specific subarray, beam and scan.

    :param tree: a :py:class:`~xarray.DataTree` containing captured visibilities from one or more scans.
    :param subarray_id: The subarray that performed the scan.
    :param beam_id: The beam for which output was captured.
    :param scan_id: The scan identifier.
    :param station_baseline_mapping: A mapping containing the stations used for the scan that, when provided,
        is used to assign new coordinates to the ``baseline`` dimension of the resulting array.

    :returns: The :py:class:`~xarray.DataArray` containing the visibility data.
    :raises ValueError: when no visibility data exists for the given subarray, beam and scan.
    """
    try:
        data = tree[f"/subarray_{subarray_id:02}/beam_{beam_id:02}/scan_{scan_id}/data/visibilities"]

        if station_baseline_mapping is None:
            n_baselines = data.shape[2]
            n_stations = int((np.sqrt(8 * n_baselines + 1) - 1) / 2)
            module_logger.debug("n_baselines (%d) -> n_stations: %d", n_baselines, n_stations)
            station_baseline_mapping = LowCbfSdpSpeadStationBaselineMapping(range(n_stations))

        return data.assign_coords({"baseline": list(map(str, station_baseline_mapping.baselines))})
    except KeyError as ex:
        module_logger.error(
            "Visibility data for subarray_id=%d, beam_id=%d and scan_id=%d does not exist",
            subarray_id,
            beam_id,
            scan_id,
            exc_info=True,
        )
        raise ValueError("No visibility data available") from ex


def get_visibility_metadata(
    tree: xr.DataTree,
    subarray_id: int,
    beam_id: int,
    scan_id: int,
) -> pd.DataFrame:
    """Retrieve visibility metadata from a :py:class:`xarray.DataTree` for a specific subarray, beam and scan.

    :param tree: a :py:class:`~xarray.DataTree` containing captured visibilities from one or more scans.
    :param subarray_id: The subarray that performed the scan.
    :param beam_id: The beam for which output was captured.
    :param scan_id: The scan identifier.

    :returns: The :py:class:`~pandas.DataFrame` containing the visibility metadata.
    :raises ValueError: when no visibility metadata exists for the given subarray, beam and scan.
    """
    try:
        metadata = tree[f"/subarray_{subarray_id:02}/beam_{beam_id:02}/scan_{scan_id}/metadata"]
        return metadata.dataset.to_dataframe()
    except KeyError as ex:
        module_logger.error(
            "Scan metadata for subarray_id=%d, beam_id=%d and scan_id=%d does not exist",
            subarray_id,
            beam_id,
            scan_id,
            exc_info=True,
        )
        raise ValueError("No visibility metadata available") from ex


class _VisibilityDataExtractor(SpeadDataVisitor):
    def __init__(
        self,
        logger: logging.Logger | None = None,
    ):
        self._logger = logger or module_logger
        self.collectors: dict[tuple[int, int], _VisibilityDataCollector] = defaultdict(
            lambda: _VisibilityDataCollector(logger=logger)
        )

    def visit_start_of_stream_heap(self, heap: spead2.recv.Heap, items: dict[str, spead2.Item]) -> None:
        self.collectors[self._key(heap)].visit_start_of_stream_heap(heap, items)

    def visit_data_heap(self, heap: spead2.recv.Heap, items: dict[str, spead2.Item]) -> None:
        self.collectors[self._key(heap)].visit_data_heap(heap, items)

    def visit_end_of_stream_heap(self, heap: spead2.recv.Heap, items: dict[str, spead2.Item]) -> None:
        self.collectors[self._key(heap)].visit_end_of_stream_heap(heap, items)

    def on_finish(self) -> None:
        for collector in self.collectors.values():
            collector.on_finish()

    def _key(self, heap: spead2.recv.Heap) -> tuple[int, int]:
        heap_counter = LowCbfSdpSpeadHeapCounter.unpack(heap.cnt)
        return heap_counter.subarray_id, heap_counter.beam_id


class _VisibilityDataCollector(SpeadDataVisitor):  # pylint: disable=too-many-instance-attributes
    def __init__(
        self,
        logger: logging.Logger | None = None,
    ) -> None:
        self._logger = logger or module_logger

        self._current_metadata = []
        self._current_integration_period = {}
        self._current_scan = {}
        self._epoch_offset = -1
        self._integration_id = -1
        self._scan_id = -1

        self.scans = xr.DataTree()

    def visit_start_of_stream_heap(self, heap: spead2.recv.Heap, items: dict[str, spead2.Item]) -> None:
        self._flush_scan()

        row = {}
        for key, item in items.items():
            row[key] = item.value

            if key == LowCbfVisibilitySpeadItem.SCAN_ID:
                self._scan_id = item.value

        self._current_metadata.append(row)

    def visit_data_heap(self, heap: spead2.recv.Heap, items: dict[str, spead2.Item]) -> None:
        heap_counter = LowCbfSdpSpeadHeapCounter.unpack(heap.cnt)

        if self._integration_id != heap_counter.integration_id:
            epoch_offset = items[LowCbfVisibilitySpeadItem.VISIBILITY_EPOCH_OFFSET].value
            if self._epoch_offset == epoch_offset:
                self._logger.warning(
                    "Integration changed (%d -> %d), but epoch offset unchanged (%d)",
                    self._integration_id,
                    heap_counter.integration_id,
                    epoch_offset,
                )
            else:
                self._logger.debug(
                    "Moving to new time offset %d (integration_id = %d)",
                    epoch_offset,
                    heap_counter.integration_id,
                )
                self._flush_integration_period()
            self._integration_id = heap_counter.integration_id
            self._epoch_offset = epoch_offset

        self._current_integration_period[heap_counter.channel_id] = items[
            LowCbfVisibilitySpeadItem.VISIBILITY_BASELINE_DATA
        ].value["VIS"]

    def on_finish(self) -> None:
        self._flush_scan()

    def _flush_integration_period(self):
        if not self._current_integration_period:
            return

        self._logger.debug("Appending integration period for integration_id: %d", self._integration_id)

        channel_ids, visibilities = [], []
        for channel_id, vis in self._current_integration_period.items():
            channel_ids.append(channel_id)
            visibilities.append(vis)

        self._current_scan[self._epoch_offset] = xr.DataArray(
            np.array(visibilities),
            dims=["channel_id", "baseline", "polarization"],
            coords={
                "channel_id": channel_ids,
                "polarization": [p.name for p in Polarization],
            },
        )

        self._current_integration_period = {}

    def _flush_scan(self):
        self._flush_integration_period()

        if not self._current_scan:
            return

        self._logger.debug("Appending scan for scan_id: %d", self._scan_id)
        metadata = pd.DataFrame(self._current_metadata)
        data = xr.Dataset(self._current_scan).to_dataarray(dim="epoch_offset")
        data["center_frequency"] = (
            "channel_id",
            list(
                metadata.loc[channel_id][LowCbfVisibilitySpeadItem.VISIBILITY_CENTRE_FREQUENCY]
                for channel_id in data["channel_id"].data
            ),
        )
        self.scans[f"scan_{self._scan_id}"] = xr.DataTree.from_dict(
            {
                "/data": data.to_dataset(name="visibilities"),
                "/metadata": xr.Dataset.from_dataframe(metadata),
            }
        )

        self._current_scan = {}
        self._current_metadata = []
