"""Workarounds related to LOW-CBF."""

import contextlib
import logging
from collections import defaultdict

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.pst.devices import PstDevices

__all__ = [
    "Skb769Workaround",
]


class Skb769Workaround:
    """
    Workaround for :jira:`SKB-769` to automatically create P4 routes between LOW-CBF beamformers and PST.
    """

    def __init__(
        self,
        low_cbf_devices: LowCbfDevices,
        pst_devices: PstDevices,
        logger: logging.Logger | None = None,
    ):
        self._connector = low_cbf_devices.connector()
        self._pst_devices = pst_devices
        self._logger = logger or logging.getLogger(__name__)

        self._beam_id_to_ports: dict[int, list[str]] = defaultdict(lambda: [])
        for connection in low_cbf_devices.allocator().connections:
            if "link" in connection and connection["link"].startswith("pst_"):
                beam_id = int(connection["link"].strip("pst_"))
                self._beam_id_to_ports[beam_id].append(connection["port"])

        self._current_timing_beam_ids = set()

    @contextlib.contextmanager
    def route_timing_beams(self, timing_beam_ids: list[int]):
        """
        Manage routes in the P4 PSR routing table to route LOW-CBF beamformer output to PST.

        This returns a context manager that creates the routes upon entering the context,
        and removes the routes again when the context exits.

        :param timing_beam_ids: Timing beam ids for which to manage the routes.
        """
        routes_to_add: list[tuple[int, str, int]] = []
        for beam_id in timing_beam_ids:
            pst_beam = self._pst_devices.beam_by_id(beam_id)
            channel_block_config = pst_beam.channel_block_configuration
            if not channel_block_config:
                raise ValueError("PST beam has no channel block configuration, unable to determine destination port")

            if channel_block_config["num_channel_blocks"] > 1:
                self._logger.warning(
                    "%s has more than one channel blocks configured,"
                    " but this workaround can only route to one PST destination port",
                    pst_beam.fqdn,
                )

            pst_udp_port = channel_block_config["channel_blocks"][0]["destination_port"]

            p4_ports = self._beam_id_to_ports[beam_id]
            if not p4_ports:
                raise ValueError(f"No P4 ports configured for timing beam ID {beam_id}")

            if len(p4_ports) > 1:
                self._logger.warning(
                    "Multiple P4 ports configured for timing beam ID %d, but this workaround can only route to one P4 port",
                    beam_id,
                )
            p4_port = p4_ports[0]

            self._logger.info(
                "Routing timing beam ID %d to port %s with UDP destination port %d",
                beam_id,
                p4_port,
                pst_udp_port,
            )
            routes_to_add.append((beam_id, p4_port, pst_udp_port))

        self._connector.add_psr_routes(routes_to_add)

        try:
            yield
        finally:
            active_beam_ids = set(int(route["Beam"]) for route in self._connector.psr_routing_table)
            beam_ids_to_remove = active_beam_ids.intersection(timing_beam_ids)

            for beam_id in beam_ids_to_remove:
                self._logger.info("Removing PSR routes for timing beam ID %d", beam_id)

            self._connector.remove_psr_routes(list(beam_ids_to_remove))
