"""Module containing helper functions to work with delay polynomials emulated by LOW-CBF."""

from astropy import units as u
from astropy.coordinates import ICRS, SkyCoord

from ska_low_csp_test.delaypoly.devices import DelaypolyDevice, DelaypolyRaDecSchema, DelaypolyStationDelaySchema
from ska_low_csp_test.synchronisation import wait_for_condition

__all__ = [
    "wait_for_valid_delays",
    "sky_coord_to_ra_dec",
    "NO_DELAY",
]

NO_DELAY: DelaypolyStationDelaySchema = {"stn": 0, "nsec": 0.0}


def wait_for_valid_delays(
    delaypoly_device: DelaypolyDevice,
    subarray_and_beam_ids: list[tuple[int, int]],
    timeout_s: float = 305.0,
    interval_s: float = 1.0,
):
    """
    Wait for the delay polynomials for the given subarray and station beam pairs to become valid.

    This monitors the ``start_validity_sec`` value of each source delay attribute and blocks the calling thread
    until all values have updated.

    :param delaypoly_device: The delaypoly device to watch for changes.
    :param subarray_and_beam_ids: (subarray_id, station_beam_id) pairs for which the delays should be monitored.
    :param timeout_s: Maximum number of seconds to wait for the delays to update.
    :param interval_s: Interval in seconds in between polling the delays for changes.
    :raises TimeoutError: When the ``timeout_s`` has been reached before all delays become valid.
    """

    def _read_delay_poly_time():
        for subarray_id, station_beam_id in subarray_and_beam_ids:
            yield delaypoly_device.read_source_attribute(subarray_id, station_beam_id)["start_validity_sec"]

    cur_poly_time = list(_read_delay_poly_time())
    wait_for_condition(
        lambda: all(t[0] != t[1] for t in zip(cur_poly_time, _read_delay_poly_time())),
        timeout_s=timeout_s,
        interval_s=interval_s,
    )


def sky_coord_to_ra_dec(sky_coord: SkyCoord) -> DelaypolyRaDecSchema:
    """
    Create a dictionary with right-ascension (RA) and declanation (Dec) for the given sky coordinate.

    :param sky_coord: The :py:class:`~astropy.coordinates.SkyCoord` to convert.
    :returns: :py:class:`~ska_low_csp_test.delaypoly.devices.DelaypolyRaDecSchema`.
    """
    radec = sky_coord.transform_to(ICRS)
    return {
        "ra": radec.ra.to_string(u.hour),  # type: ignore
        "dec": radec.dec.to_string(u.deg),  # type: ignore
    }
