"""Module that provides classes to interact with LOW TMC TANGO devices."""

import json
from typing import TypedDict

from ska_low_csp_test.domain.model import LowCspDelayModelSchema
from ska_low_csp_test.tango.ska_devices import SKABaseDevice
from ska_low_csp_test.tango.tango import TangoContext

__all__ = [
    "DelaypolyBeamDelaySchema",
    "DelaypolyBeamRaDecSchema",
    "DelaypolyDevice",
    "DelaypolyDevices",
    "DelaypolyRaDecSchema",
    "DelaypolySourceDelaySchema",
    "DelaypolySourceRaDecSchema",
    "DelaypolyStationDelaySchema",
]


class DelaypolyRaDecSchema(TypedDict):
    """JSON schema for the RaDec structure."""

    ra: str
    dec: str


class DelaypolyBeamRaDecSchema(TypedDict):
    """JSON schema for the ``BeamRaDec`` command of the Delaypoly TANGO device."""

    subarray_id: int
    beam_id: int
    direction: DelaypolyRaDecSchema


class DelaypolySourceRaDecSchema(TypedDict):
    """JSON schema for the ``SourceRaDec`` command of the Delaypoly TANGO device"""

    subarray_id: int
    beam_id: int
    direction: list[DelaypolyRaDecSchema]


class DelaypolyStationDelaySchema(TypedDict):
    """JSON schema for station delay values."""

    stn: int
    nsec: float


class DelaypolyBeamDelaySchema(TypedDict):
    """JSON schema for the ``BeamDelay`` command of the Delaypoly TANGO device."""

    subarray_id: int
    beam_id: int
    delay: list[DelaypolyStationDelaySchema]


class DelaypolySourceDelaySchema(TypedDict):
    """JSON schema for the ``SourceDelay`` command of the Delaypoly TANGO device."""

    subarray_id: int
    beam_id: int
    delay: list[list[DelaypolyStationDelaySchema]]


class DelaypolyDevice(SKABaseDevice):
    """The :py:class:`Delaypoly` TANGO device."""

    def beam_delay(self, schema: DelaypolyBeamDelaySchema):
        """Wrapper for the ``BeamDelay`` command."""
        self.command("BeamDelay", json.dumps(schema))

    def source_delay(self, schema: DelaypolySourceDelaySchema):
        """Wrapper for the ``SourceDelay`` command."""
        self.command("SourceDelay", json.dumps(schema))

    def pst_delay(self, schema: DelaypolySourceDelaySchema):
        """Wrapper for the ``PstDelay`` command."""
        self.command("PstDelay", json.dumps(schema))

    def beam_ra_dec(self, schema: DelaypolyBeamRaDecSchema):
        """Wrapper for the ``BeamRaDec`` command."""
        self.command("BeamRaDec", json.dumps(schema))

    def source_ra_dec(self, schema: DelaypolySourceRaDecSchema):
        """Wrapper for the ``SourceRaDec`` command."""
        self.command("SourceRaDec", json.dumps(schema))

    def pst_ra_dec(self, schema: DelaypolySourceRaDecSchema):
        """Wrapper for the ``PstRaDec`` command."""
        self.command("PstRaDec", json.dumps(schema))

    def set_seconds_after_epoch(self, seconds: int):
        """Wrapper for the ``SetSecondsAfterEpoch`` command."""
        self.command("SetSecondsAfterEpoch", seconds)

    def read_delay_attribute(self, subarray_id: int, beam_id: int) -> LowCspDelayModelSchema:
        """Helper to read the ``delay_sXX_bYY`` attributes."""
        return json.loads(self.read_attribute(f"delay_s{subarray_id:02}_b{beam_id:02}", str))

    def read_source_attribute(self, subarray_id: int, beam_id: int) -> LowCspDelayModelSchema:
        """Helper to read the ``source_sXX_bYY_1`` attributes."""
        return json.loads(self.read_attribute(f"source_s{subarray_id:02}_b{beam_id:02}_1", str))


class DelaypolyDevices:
    """Delaypoly TANGO devices."""

    def __init__(self, tango_context: TangoContext) -> None:
        self._tango_context = tango_context
        self._delaypoly_fqdns = tango_context.get_device_fqdns("DelayDevice")

    def delaypoly(self) -> DelaypolyDevice:
        """Retrieve the LOW-CBF delaypoly TANGO device."""
        assert len(self._delaypoly_fqdns) > 0, "No delay poly devices found for LOW-CBF"
        return self._tango_context.get_device(self._delaypoly_fqdns[0], DelaypolyDevice)
