"""Module to handle LFAA PCAP files."""

import os
import struct
import sys
from dataclasses import dataclass
from itertools import product
from typing import Optional, Set, Tuple

import numpy as np
import numpy.typing as npt
from scapy.all import Packet, ls, rdpcap  # noqa pylint: disable=unused-import; imported for side-effects
from scapy.utils import PcapReader

from ska_low_csp_test.domain.channels import coarse_channel_center_frequency
from ska_low_csp_test.sps import AntennaInfo, ChannelInfo, PacketLength, ScanId, SpsSpead, SyncTime, Timestamp


@dataclass
class LFAAPcapFileInfo:  # pylint: disable=too-many-instance-attributes
    """Information extracted from an LFAA PCAP file."""

    SINGLE_CHANNEL_RATE = 0.18  # 0.09ns/packet * 2 polarisations
    NOF_COMPLEX = 2  # real and imaginary values of complex number
    NOF_POLS = 2  # X an Y polarisation
    NOF_SAMPLES_PER_POL = 2048  # samples/LFAA Packet
    INTEGRATION_PERIOD_LENGTH_S = 1080 * 2048 * 408 / 1e9  # (1080ns/sample) * (2048 samples/LFAA Packet) * (408 LFAA packets)
    """
    The length in seconds of an integration period.

    Source: https://confluence.skatelescope.org/display/SE/Model+Configuration
    """

    file_name: str
    packet_count: int
    packet_size: int
    station_count: int
    channel_count: int
    unix_epoch_time_start: int
    unix_epoch_time_end: int
    station_channel_frequency: npt.NDArray[np.float64]
    timesamples: list[Tuple[int]]
    frequency_ids: set[int]

    @property
    def unix_epoch_time_delta(self) -> int:
        """
        The difference in seconds between the first and the last timestamp in the PCAP file.
        """
        return self.unix_epoch_time_end - self.unix_epoch_time_start

    @property
    def rate(self) -> float:
        """
        Calculate the correct rate for the PCAP file.
        """
        return LFAAPcapFileInfo.SINGLE_CHANNEL_RATE * self.channel_count

    @property
    def integration_period_count(self) -> float:
        """
        The number of integration periods in the PCAP file.
        """
        return self.unix_epoch_time_delta / LFAAPcapFileInfo.INTEGRATION_PERIOD_LENGTH_S

    @property
    def channelized_data(self) -> npt.NDArray:
        """Channelized data based on timesamples list from provided PCAP file."""
        nof_time_samples = (
            np.array(self.timesamples).size
            // self.station_count
            // self.channel_count
            // LFAAPcapFileInfo.NOF_SAMPLES_PER_POL
            // (LFAAPcapFileInfo.NOF_POLS * LFAAPcapFileInfo.NOF_COMPLEX)
        )
        channelized_data = np.array(self.timesamples).reshape(  # pylint: disable=too-many-function-args
            nof_time_samples,
            self.station_count,
            self.channel_count,
            LFAAPcapFileInfo.NOF_POLS * LFAAPcapFileInfo.NOF_COMPLEX,
            LFAAPcapFileInfo.NOF_SAMPLES_PER_POL,
        )
        return channelized_data


DEFAULT_SPS_PACKET_PAYLOAD_LEN = 8192


@dataclass
class SpsSpeadHeader:  # pylint: disable=too-many-instance-attributes
    """SPEAD header contents of a version 2 SPS station data packet."""

    unix_epoch_time_s: int
    timestamp_ns: int
    scan_id: int
    beam_id: int
    frequency_id: int
    substation_id: int
    subarray_id: int
    station_id: int
    timesamples_struct: Tuple[int]

    @staticmethod
    def parse(packet: Packet) -> "SpsSpeadHeader":
        """
        Parse the SPEAD header contents from an SPS station data SPEAD packet.
        """

        def calc_offset(packet, layer, field):
            """Calculate the offset of a field, in a packet.

            Ref: https://stackoverflow.com/a/75835475"""
            offset = 0
            while packet:  # for each payload
                for fld in packet.fields_desc:  # for each field
                    if fld.name == field and isinstance(packet, layer):
                        return int(offset)
                    offset += fld.i2len(packet, packet.getfieldval(fld.name))  # add length
                packet = packet.payload
            return -1

        pkt = packet[SpsSpead]
        header_len = calc_offset(pkt, SpsSpead, "values")
        payload_len = (
            pkt[PacketLength].packetPayloadLength
            or DEFAULT_SPS_PACKET_PAYLOAD_LEN  # the packetPayloadLength field hasn't always been populated
        )
        timesamples = struct.unpack(f">{payload_len}b", pkt.raw_packet_cache[header_len:])
        try:
            scan_id = pkt[ScanId].scanId
        except IndexError:
            scan_id = 0  # not available in V1 of packet format
        return SpsSpeadHeader(
            unix_epoch_time_s=pkt[SyncTime].unixEpochTime_s,
            timestamp_ns=pkt[Timestamp].timestamp_ns,
            scan_id=scan_id,
            beam_id=pkt[ChannelInfo].beamId,
            frequency_id=pkt[ChannelInfo].frequencyId,
            substation_id=pkt[AntennaInfo].substationId,
            subarray_id=pkt[AntennaInfo].subarrayId,
            station_id=pkt[AntennaInfo].stationId,
            timesamples_struct=timesamples,
        )

    def __str__(self):
        return (
            f"Packet - spead_unix_epoch_time         {hex(self.unix_epoch_time_s)}\n"
            f"Packet - spead_timestamp               {hex(self.timestamp_ns)}\n"
            f"Packet - spead_scan_id                 {hex(self.scan_id)}\n"
            f"Packet - spead_beam_id                 {hex(self.beam_id)}\n"
            f"Packet - spead_frequency_id            {hex(self.frequency_id)}\n"
            f"Packet - spead_substation_id           {hex(self.substation_id)}\n"
            f"Packet - spead_subarray_id             {hex(self.subarray_id)}\n"
            f"Packet - spead_station_id              {hex(self.station_id)}\n"
        )


def extract_lfaa_pcap_info(pcap_file_path: str) -> LFAAPcapFileInfo:  # pylint: disable=too-many-locals
    """
    Extract information from a PCAP file containing LFAA SPEAD packets.

    :param pcap_file_path: Path to the PCAP file
    :returns: `LFAAPcapFileInfo`
    """
    packet_size = 0
    packet_number: Optional[int] = None
    unix_epoch_time_start: Optional[int] = None
    unix_epoch_time_end: Optional[int] = None
    station_ids: Set[int] = set()
    frequency_ids: Set[int] = set()
    timesamples: list[Tuple[int]] = []

    unix_epoch_time_start, unix_epoch_time_end = sys.maxsize, 0
    with PcapReader(pcap_file_path) as packets:
        for packet_number, packet in enumerate(packets):
            if not packet_size:
                packet_size = len(packet)

            header = SpsSpeadHeader.parse(packet)
            unix_epoch_time_start = min(unix_epoch_time_start, header.unix_epoch_time_s)
            unix_epoch_time_end = max(unix_epoch_time_end, header.unix_epoch_time_s)
            station_id = header.station_id
            station_ids.add(station_id)
            frequency_id = header.frequency_id
            frequency_ids.add(frequency_id)
            timesamples.append(header.timesamples_struct)

    station_channel_frequency = np.zeros([len(station_ids), len(frequency_ids)])
    station_map = {station_id: index for index, station_id in enumerate(sorted(station_ids))}
    frequency_map = {frequency_id: index for index, frequency_id in enumerate(sorted(frequency_ids))}
    channel_frequency_map = {
        frequency_id: coarse_channel_center_frequency(frequency_id) for frequency_id in sorted(frequency_ids)
    }
    for station_id, (frequency_id, frequency_hz) in product(station_map, channel_frequency_map.items()):
        station_channel_frequency[station_map[station_id], frequency_map[frequency_id]] = frequency_hz

    return LFAAPcapFileInfo(
        file_name=os.path.basename(pcap_file_path),
        packet_count=packet_number + 1 if packet_number is not None else 0,
        packet_size=packet_size,
        station_count=len(station_ids),
        channel_count=len(frequency_ids),
        unix_epoch_time_start=unix_epoch_time_start,
        unix_epoch_time_end=unix_epoch_time_end,
        station_channel_frequency=station_channel_frequency,
        timesamples=timesamples,
        frequency_ids=frequency_ids,
    )
