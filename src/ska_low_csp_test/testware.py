"""Helper classes to control the LOW-CSP testware, such as input signal generation and output capture."""

import contextlib
import logging
import os
import time

from ska_control_model import ResultCode

from ska_low_csp_test.cbf.devices import LowCbfAllocatorDevice, LowCbfConnectorDevice
from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema, CnicDevice, CnicPulseConfigSchema
from ska_low_csp_test.delaypoly.devices import DelaypolyDevice
from ska_low_csp_test.delaypoly.helpers import wait_for_valid_delays
from ska_low_csp_test.synchronisation import wait_for_condition
from ska_low_csp_test.tango.ska_devices import SKABaseDevice
from ska_low_csp_test.tango.tango import TangoContext

__all__ = [
    "PsrOutputCapture",
    "PsrOutputCaptureSession",
    "VisibilityOutputCapture",
    "VisibilityOutputCaptureSession",
    "PacketCaptureDevice",
    "StationSignalGenerator",
    "TestwareDevices",
]


class PacketCaptureDevice(SKABaseDevice):
    """Wrapper for the ``PacketCapture`` TANGO device."""

    @property
    def ip_address(self) -> str:
        """The IP address of the capture device."""
        return self.read_attribute("ip_address", str)

    @property
    def capture_stats__file_size(self) -> int:
        """The current size of the capture file."""
        return self.read_attribute("capture_stats__file_size", int)

    @property
    def capture_stats__packets_captured(self) -> int:
        """The current number of packets captured."""
        return self.read_attribute("capture_stats__packets_captured", int)

    @property
    def capture_stats__packets_dropped(self) -> int:
        """The current number of packets dropped."""
        return self.read_attribute("capture_stats__packets_dropped", int)

    @property
    def capture_stats__packets_received(self) -> int:
        """The current number of packets received."""
        return self.read_attribute("capture_stats__packets_received", int)

    def begin_capture(self, capture_file_name: str):
        """Begin capturing packets to a file."""
        return self.long_running_command("BeginCapture", capture_file_name)

    def end_capture(self):
        """End capturing packets to a file."""
        return self.long_running_command("EndCapture")

    def _handle_long_running_command_result(self, command_name: str, result: tuple[int, str]):
        result_value, message = result
        result_code = ResultCode(result_value)
        if result_code != ResultCode.OK:
            raise RuntimeError(
                f"{self}: Long-running command '{command_name}' finished with result {result_code.name} and message: {message}"
            )


class TestwareDevices:
    """Testware devices."""

    def __init__(self, tango_context: TangoContext) -> None:
        self._tango_context = tango_context
        self._packet_capture_device_fqdns = tango_context.get_device_fqdns("PacketCapture")

    def all_packet_capture_devices(self):
        """Retrieve all ``PacketCapture`` TANGO devices."""
        return [self._tango_context.get_device(fqdn, PacketCaptureDevice) for fqdn in self._packet_capture_device_fqdns]


class VisibilityOutputCaptureSession:
    """A single session that is capturing visibility output."""

    def __init__(
        self,
        destination_file_path: str,
        packet_capture_device: PacketCaptureDevice,
        logger: logging.Logger | None = None,
    ) -> None:
        self._destination_file_path = destination_file_path
        self._packet_capture_device = packet_capture_device
        self._logger = logger or logging.getLogger(__name__)
        self._start_time = time.time()

    @property
    def destination_file_path(self):
        """Retrieve the destination file path."""
        return self._destination_file_path

    @property
    def start_time(self):
        """Retrieve the session start time."""
        return self._start_time

    def monitor(self, duration_s: float, interval_s: float | None = None) -> None:
        """
        Periodically log information about the active capture.

        Calling this method starts a monitoring loop that blocks the calling thread until ``duration_s`` has elapsed.

        :param duration_s: Time in seconds to monitor the capture.
        :param interval_s: Interval in seconds to check the progress.
        """
        deadline = time.time() + duration_s
        if interval_s is None:
            interval_s = max(duration_s / 10, 1.0)
        while time.time() < deadline:
            self._logger.info(
                "Capture statistics: file size=%d, packets received=%d, packets captured=%d, packets dropped=%d",
                self._packet_capture_device.capture_stats__file_size,
                self._packet_capture_device.capture_stats__packets_received,
                self._packet_capture_device.capture_stats__packets_captured,
                self._packet_capture_device.capture_stats__packets_dropped,
            )
            sleep_time_s = min(interval_s, deadline - time.time())
            time.sleep(sleep_time_s)


class VisibilityOutputCapture:
    """Captures visibility output from LOW-CBF using the PacketCapture TANGO device."""

    def __init__(
        self,
        packet_capture_device: PacketCaptureDevice,
        local_capture_dir: str,
        logger: logging.Logger | None = None,
    ) -> None:
        self._packet_capture_device = packet_capture_device
        self._local_capture_dir = local_capture_dir
        self._logger = logger or logging.getLogger(__name__)

    @property
    def ip_address(self) -> str:
        """
        The IP address of the capture device.

        Use this as destination IP address for LOW-CBF visibilities in the JSON configuration.
        """
        return self._packet_capture_device.ip_address

    @contextlib.contextmanager
    def capture(self, destination_file: str):
        """
        Capture the output to a given destination file.

        This method returns a context manager that automatically completes the capture when the context completes.
        The :py:class:`OutputCaptureSession` that is yielded by this context manager can be used to monitor the active
        capture.

        Example usage:

        .. code-block:: python

            with output_capture.capture(destination_file="capture.pcap") as session:
                # Perform a 10-second scan on the subarray
                subarray_under_test.scan(scan_1_json)
                session.monitor(duration_s=10.0)
                subarray_under_test.end_scan()

                # Perform another scan on the subarray
                subarray_under_test.scan(scan_2_json)
                session.monitor(duration_s=20.0)
                subarray_under_test.end_scan()

                # Store the path of the captured file for use outside the context
                captured_output_path = session.destination_file_path

            # Do something with the captured output
            output = visibilities.unpack_pcap_file(captured_output_path)

        :param destination_file: name of capture destination file.
        :returns: Context manager.
        """
        session = VisibilityOutputCaptureSession(
            destination_file_path=os.path.join(self._local_capture_dir, destination_file),
            packet_capture_device=self._packet_capture_device,
        )

        self._logger.info("Start capturing output to %s", destination_file)
        self._packet_capture_device.begin_capture(destination_file)

        try:
            yield session
        finally:
            self._logger.info("Stop capturing to %s", destination_file)
            self._packet_capture_device.end_capture()


class PsrOutputCaptureSession:
    """A single session that is capturing PSR output."""

    def __init__(  # pylint: disable=too-many-arguments
        self,
        cnic: CnicDevice,
        beam_ids: list[int],
        destination_port: str,
        destination_file_path: str,
        logger: logging.Logger | None = None,
    ) -> None:
        self._cnic = cnic
        self._beam_ids = beam_ids
        self._destination_port = destination_port
        self._destination_file_path = destination_file_path
        self._logger = logger or logging.getLogger(__name__)
        self._start_time = time.time()

    @property
    def beam_ids(self):
        """Retrieve the PSR beam ids."""
        return self._beam_ids

    @property
    def destination_port(self):
        """Retrieve the destination port number."""
        return self._destination_port

    @property
    def destination_file_path(self):
        """Retrieve the destination file path."""
        return self._destination_file_path

    @property
    def start_time(self):
        """Retrieve the session start time."""
        return self._start_time

    def monitor(self, duration_s: float, interval_s: float | None = None) -> None:
        """
        Periodically log information about the active capture.

        Calling this method starts a monitoring loop that blocks the calling thread until ``duration_s`` has elapsed.

        :param duration_s: Time in seconds to monitor the capture.
        :param interval_s: Interval in seconds to check the progress.
        """
        deadline = time.time() + duration_s
        if interval_s is None:
            interval_s = max(duration_s / 10, 1.0)
        while time.time() < deadline:
            self._logger.info("Total packets received: %d", self._cnic.hbm_pktcontroller__rx_packet_count)
            sleep_time_s = min(interval_s, deadline - time.time())
            time.sleep(sleep_time_s)


class PsrOutputCapture:
    """Captures beamformer output from LOW-CBF using CNIC TANGO device."""

    def __init__(  # pylint: disable=too-many-arguments
        self,
        cnic: CnicDevice,
        allocator: LowCbfAllocatorDevice,
        connector: LowCbfConnectorDevice,
        remote_capture_dir: str,
        local_capture_dir: str,
        logger: logging.Logger | None = None,
    ) -> None:
        self._cnic = cnic
        self._allocator = allocator
        self._connector = connector
        self._remote_capture_dir = remote_capture_dir
        self._local_capture_dir = local_capture_dir
        self._logger = logger or logging.getLogger(__name__)

    @contextlib.contextmanager
    def capture(self, beam_ids: list[int], destination_file: str):
        """
        Capture the output of the given PSR beams to a given destination file.

        This method returns a context manager that automatically completes the capture when the context completes.
        The :py:class:`OutputCaptureSession` that is yielded by this context manager can be used to monitor the active
        capture.

        Example usage:

        .. code-block:: python

            with output_capture.capture(beam_ids=[1], destination_file="capture.pcap") as session:
                # Perform a 10-second scan on the subarray
                subarray_under_test.scan(scan_1_json)
                session.monitor(duration_s=10.0)
                subarray_under_test.end_scan()

                # Perform another scan on the subarray
                subarray_under_test.scan(scan_2_json)
                session.monitor(duration_s=20.0)
                subarray_under_test.end_scan()

                # Store the path of the captured file for use outside the context
                captured_output_path = session.destination_file_path

            # Do something with the captured output
            output = visibilities.unpack_pcap_file(captured_output_path)

        :param beam_ids: list of PSR beam ids whose output to capture.
        :param destination_file: name of capture destination file.
        :returns: Context manager.
        """
        session = PsrOutputCaptureSession(
            cnic=self._cnic,
            beam_ids=beam_ids,
            destination_port=self._get_fpga_p4_port(serial_number=self._cnic.serial_number),
            destination_file_path=os.path.join(self._local_capture_dir, destination_file),
        )

        self._add_p4_routes(session)
        self._logger.info("Start capturing output from PSR beams %s to %s", beam_ids, destination_file)
        self._cnic.call_method(
            {
                "method": "receive_pcap",
                "arguments": {
                    "n_packets": 999_999_999,
                    "packet_size": 79,
                    "out_filename": os.path.join(self._remote_capture_dir, destination_file),
                },
            }
        )
        try:
            yield session
        finally:
            self._logger.info("Stop capturing from PSR beams %s to %s", beam_ids, destination_file)
            self._cnic.call_method({"method": "stop_receive"})

            self._remove_p4_routes(session)
            self._wait_for_cnic_to_finish(session)
            self._wait_for_file_to_sync(session)

    def _add_p4_routes(self, session: PsrOutputCaptureSession):
        self._logger.info("Configure PSR P4 routing")

        routes_to_add: list[tuple[int, str, int]] = []
        for beam_id in session.beam_ids:
            routes_to_add.append((beam_id, session.destination_port, 2000))  # CNIC does not care about the UDP port

        self._connector.add_psr_routes(routes_to_add)

    def _remove_p4_routes(self, session: PsrOutputCaptureSession):
        self._logger.info("Deconfigure PSR P4 routing")

        configured_beams = [route["Beam"] for route in self._connector.psr_routing_table]
        routes_to_remove = [beam_id for beam_id in session.beam_ids if str(beam_id) in configured_beams]
        if routes_to_remove:
            self._connector.remove_psr_routes(routes_to_remove)

    def _get_fpga_p4_port(self, serial_number: str):
        for connection in self._allocator.connections:
            if "alveo" in connection and connection["alveo"] == serial_number:
                port_name = connection["port"]
                self._logger.debug("FPGA %s is connected to port %s", serial_number, port_name)
                return port_name

        raise RuntimeError(f"No port configured for serial number {serial_number}")

    def _wait_for_cnic_to_finish(self, session: PsrOutputCaptureSession):
        flush_timeout_s = (time.time() - session.start_time) * 3
        self._logger.info("Waiting %d seconds for capture to finish", flush_timeout_s)
        try:
            wait_for_condition(
                lambda: self._cnic.finished_receive,
                timeout_s=flush_timeout_s,
                message_on_timeout=lambda: f"Received {self._cnic.hbm_pktcontroller__rx_packet_count} packets.",
            )
        except TimeoutError:
            self._logger.warning("TimeoutError was raised during waiting for CNIC output. Workaround will be applied.")
            if not os.path.exists(session.destination_file_path):
                self._logger.debug("File does not exist: %s", session.destination_file_path)
                raise

    def _wait_for_file_to_sync(self, session: PsrOutputCaptureSession):
        file = session.destination_file_path

        def _stat_size() -> int | None:
            try:
                return os.stat(file).st_size
            except FileNotFoundError:
                return None

        self._logger.info("Wait for file (%s) to settle", file)
        curr_size = _stat_size()
        retries = MAX_RETRIES = 30  # pylint: disable=invalid-name
        while retries and curr_size is not None:
            time.sleep(1)
            prev_size, curr_size = curr_size, _stat_size()
            if prev_size == curr_size:
                retries -= 1
            else:
                retries = MAX_RETRIES
        if curr_size is not None:
            self._logger.debug("File size (%d bytes) unchanged after %d retries.", curr_size, MAX_RETRIES)
        else:
            self._logger.warning("Capture file (%s) does not exist.", file)


class StationSignalGenerator:
    """Generates station signals using the CNIC-VD and the LOW-CBF delaypoly emulator."""

    def __init__(
        self,
        cnic: CnicDevice,
        delaypoly: DelaypolyDevice,
        logger: logging.Logger | None = None,
    ) -> None:
        self._cnic = cnic
        self._delaypoly = delaypoly
        self._logger = logger or logging.getLogger(__name__)

    @contextlib.contextmanager
    def generate(
        self,
        vd_config: CnicConfigureVirtualDigitiserSchema,
        pulse_config: CnicPulseConfigSchema | None = None,
    ):
        """
        Generate simulated station signals as input for correlator/beamformer.

        This method returns a context manager that automatically stops the signal generation when the context completes.

        Usage:

        .. code-block:: python

            with station_signal_generator.generate(
                vd_config=vd_config,
                pulse_config=pulse_config,
            ):
                # Process generated station signals in this block.
                # For example, start a scan on a subarray
                subarray.Scan(scan_json)

        :param vd_config: JSON configuration used to configure the CNIC-VD.
        :param pulse_config: JSON configuration used to control the pulsar settings of the CNIC-VD.
        :returns: Context manager.
        """
        self._logger.info("Configuring CNIC virtual digitiser")
        self._cnic.configure_virtual_digitiser(vd_config)
        if pulse_config is not None:
            self._logger.info("Configuring CNIC pulsar mode")
            self._cnic.configure_pulsar_mode(pulse_config)

        self._logger.info("Waiting for delays to become valid")
        subarray_and_station_beams = set((config["subarray"], config["beam"]) for config in vd_config["stream_configs"])
        wait_for_valid_delays(self._delaypoly, list(subarray_and_station_beams))

        self._logger.info("Starting signal generation on CNIC-VD")
        self._cnic.start_source_delays()
        try:
            yield
        finally:
            self._logger.info("Stopping signal generation on CNIC-VD")
            self._cnic.stop_source_delays()
            if pulse_config is not None:
                self._logger.info("Resetting CNIC pulsar mode")
                self._cnic.configure_pulsar_mode(
                    {
                        "enable": False,
                        "sample_count": self._cnic.DEFAULT_CNIC_PULSAR_SAMPLE_COUNT,
                    }
                )
