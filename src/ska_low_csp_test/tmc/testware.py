"""Helper classes to control the LOW-TMC testware, such as input signal generation."""

import contextlib
import json
import logging

from ska_low_csp_test.cnic.devices import CnicConfigureVirtualDigitiserSchema, CnicDevice, CnicPulseConfigSchema


class TmcStationSignalGenerator:
    """Generates station signals for TMC using the CNIC-VD using the delay polynomials from TMC"""

    def __init__(
        self,
        cnic: CnicDevice,
        logger: logging.Logger | None = None,
    ) -> None:
        self._cnic = cnic
        self._logger = logger or logging.getLogger(__name__)

    @contextlib.contextmanager
    def generate(
        self,
        vd_config: CnicConfigureVirtualDigitiserSchema,
        pulse_config: CnicPulseConfigSchema | None = None,
    ):
        """
        Generate simulated station signals as input for correlator/beamformer using the delay polynomials from TMC.

        This method returns a context manager that automatically stops the signal generation when the context completes.

        Usage:

        .. code-block:: python

            with station_signal_generator.generate(
                vd_config=vd_config,
                pulse_config=pulse_config,
            ):
                # Process generated station signals in this block.
                # For example, start a scan on a subarray
                subarray.Scan(scan_json)

        :param vd_config: JSON configuration used to configure the CNIC-VD.
        :param pulse_config: JSON configuration used to control the pulsar settings of the CNIC-VD.
        :returns: Context manager.
        """
        self._logger.info("Configuring CNIC virtual digitiser")
        self._cnic.configure_virtual_digitiser(vd_config)
        if pulse_config is not None:
            self._logger.info("Configuring CNIC pulsar mode")
            self._cnic.configure_pulsar_mode(pulse_config)

        subarray_and_station_beams = set((config["subarray"], config["beam"]) for config in vd_config["stream_configs"])
        tm_source_delays_json = {
            subarray_id: {
                station_beam_id: [
                    f"ska_low/tm_leaf_node/csp_subarray{subarray_id:02}",
                    "DelayModel",
                ]
            }
            for subarray_id, station_beam_id in subarray_and_station_beams
        }

        self._logger.info("Starting signal generation on CNIC-VD")
        self._cnic.command("StartTmSourceDelays", json.dumps(tm_source_delays_json))
        try:
            yield
        finally:
            self._logger.info("Stopping signal generation on CNIC-VD")
            self._cnic.stop_source_delays()
            if pulse_config is not None:
                self._logger.info("Resetting CNIC pulsar mode")
                cnic_configure_pulsar_mode_json = {
                    "enable": False,
                    "sample_count": self._cnic.DEFAULT_CNIC_PULSAR_SAMPLE_COUNT,
                }
                self._cnic.configure_pulsar_mode(cnic_configure_pulsar_mode_json)
