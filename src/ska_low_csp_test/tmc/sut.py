"""Module that provides classes to model the TMC system under test."""

import logging
from types import TracebackType
from typing import Any, Mapping

import allure
import tango
from pytest_check.check_functions import check_func
from ska_control_model import ObsMode, ObsState

from ska_low_csp_test.synchronisation import wait_for_condition
from ska_low_csp_test.tmc.devices import LowTmcSubarrayDevice

__all__ = ["TmcSubarrayUnderTest"]


class TmcSubarrayUnderTest:
    """The main entrypoint to interact with a single TMC subarray."""

    def __init__(
        self,
        subarray_device: LowTmcSubarrayDevice,
        logger: logging.Logger | None = None,
    ):
        self._subarray = subarray_device
        self._logger = logger or logging.getLogger(__name__)

    def __enter__(self) -> "TmcSubarrayUnderTest":
        self._subarray.__enter__()
        return self

    def __exit__(
        self, exc_type: type[BaseException] | None, exc_value: BaseException | None, exc_tb: TracebackType | None
    ) -> None:
        self._subarray.__exit__(exc_type, exc_value, exc_tb)

    @allure.step("Configure the TMC subarray under test")
    def configure(
        self,
        schema: Mapping[str, Any],
        wait_for_obs_state: ObsState | None = ObsState.READY,
    ) -> None:
        """Call ``Configure`` command on the TMC subarray device.

        :param schema: See :doc:`ska-telmodel:schemas/tmc/ska-low-tmc-configure`
        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        """
        self._subarray.configure(schema)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)

    @allure.step("Start a scan on the TMC subarray under test")
    def scan(
        self,
        schema: Mapping[str, Any],
        wait_for_obs_state: ObsState | None = ObsState.SCANNING,
    ) -> None:
        """
        Call the ``Scan`` command on the TMC subarray device.

        :param schema: See :doc:`ska-telmodel:schemas/tmc/ska-low-tmc-scan`
        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        """
        self._subarray.scan(schema)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)

    @allure.step("Wait for scan to end on TMC subarray under test")
    def wait_for_scan_end(self, timeout_s: float) -> None:
        """Wait for scan to finish and the state changes afterwards.

        :param timeout_s: The time to wait for the scan to complete.
        """
        wait_for_condition(lambda: self._subarray.obs_state != ObsState.SCANNING, timeout_s=timeout_s)

    @allure.step("Deconfigure the TMC subarray under test")
    def end(
        self,
        wait_for_obs_state: ObsState | None = ObsState.IDLE,
    ) -> None:
        """Call ``End`` command on the TMC subarray device.

        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        """
        self._subarray.end()
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)

    @allure.step("Reset the TMC subarray under test")
    def reset(self) -> None:
        """
        This resets the TMC subarray to ``ObsState.EMPTY``, according to the SKA observing state machine defined in `ADR-8`_.

        .. _ADR-8: https://confluence.skatelescope.org/pages/viewpage.action?pageId=105416556
        """
        self._logger.info("Resetting TMC subarray")

        if self._subarray.obs_state not in {ObsState.EMPTY, ObsState.ABORTING, ObsState.ABORTED, ObsState.FAULT}:
            self._subarray.abort()
            self._subarray.wait_for_attribute_value("obsState", ObsState, ObsState.ABORTED)

        if self._subarray.obs_state in {ObsState.ABORTED, ObsState.FAULT}:
            self._subarray.restart()
            self._subarray.wait_for_attribute_value("obsState", ObsState, ObsState.EMPTY)

    @check_func
    @allure.step("Verify that the TMC subarray obsState is {expected}")
    def verify_obs_state(self, expected: ObsState):
        """Verify that the ``obsState`` of the TMC subarray matches the expected state."""
        assert self._subarray.obs_state == expected

    @check_func
    @allure.step("Verify that the TMC subarray obsMode is {expected}")
    def verify_obs_mode(self, expected: ObsMode):
        """Verify that the ``obsMode`` of the TMC subarray matches the expected mode."""
        assert self._subarray.obs_mode == expected

    @check_func
    @allure.step("Verify that the TMC subarray state is {expected}")
    def verify_state(self, expected: tango.DevState):
        """Verify that the ``obsMode`` of the TMC subarray matches the expected mode."""
        assert self._subarray.state == expected

    @property
    def obs_state(self):
        """The observation state of the subarray under test."""
        return self._subarray.obs_state
