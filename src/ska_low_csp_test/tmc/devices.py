"""Module that provides classes to interact with LOW-TMC TANGO devices."""

import contextlib
import enum
import json
import queue
from typing import Any, Mapping

import allure
from ska_control_model import ResultCode

from ska_low_csp_test.tango.events import AttributeChangeEventSubscription
from ska_low_csp_test.tango.ska_devices import SKABaseDevice, SKASubarrayDevice
from ska_low_csp_test.tango.tango import TangoContext


class TmcLeafNodeType(enum.Enum):
    """The different types of leaf nodes for TMC"""

    CSP = enum.auto()
    MCCS = enum.auto()
    SDP = enum.auto()


class LowTmcCentralNodeDevice(SKABaseDevice):
    """The :py:class:`CentralNodeLow` TANGO device."""

    @allure.step("Turn on the telescope from TMC central node")
    def telescope_on(self, timeout_s: float | None = None) -> None:
        """Call TelescopeOn command on remote device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("TelescopeOn", timeout_s=timeout_s)

    @allure.step("Turn off the telescope from TMC central node")
    def telescope_off(self, timeout_s: float | None = None) -> None:
        """Call TelescopeOff command on remote device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("TelescopeOff", timeout_s=timeout_s)

    @allure.step("Put the telescope in standby from TMC central node")
    def telescope_standby(self, timeout_s: float | None = None) -> None:
        """Call TelescopeStandby command on remote device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("TelescopeStandby", timeout_s=timeout_s)

    @allure.step("Assign resources from the TMC central node")
    def assign_resources(self, schema: Mapping[str, Any], timeout_s: float | None = None) -> None:
        """Call AssignResources command on the TMC central node device.

        :schema: Dictionary containing the resources to assign.
        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("AssignResources", json.dumps(schema), timeout_s=timeout_s)

    @allure.step("Release resources from the TMC central node")
    def release_resources(self, schema: Mapping[str, Any], timeout_s: float | None = None) -> None:
        """Call ReleaseResources command on the TMC central node device.

        :param schema: Dictionary containing the resources to release.
        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("ReleaseResources", json.dumps(schema), timeout_s=timeout_s)

    def _handle_long_running_command_result(self, command_name: str, result: tuple[int, str]):
        result_value, message = result
        result_code = ResultCode(result_value)
        if result_code != ResultCode.OK:
            raise RuntimeError(
                f"{self}: Long-running command '{command_name}' finished with result {result_code.name} and message: {message}"
            )


class LowTmcSubarrayDevice(SKASubarrayDevice):
    """The :py:class:`SubarrayNodeLow` TANGO device."""

    def _handle_long_running_command_result(self, command_name: str, result: tuple[int, str]):
        result_value, message = result
        result_code = ResultCode(result_value)

        # FIXME SKB-745: Workaround, abort command does not receive updated resultcode after finishing
        if command_name.lower() == "abort" and result_code == ResultCode.STARTED:
            return

        if result_code != ResultCode.OK:
            raise RuntimeError(
                f"{self}: Long-running command '{command_name}' finished with result {result_code.name} and message: {message}"
            )


class LowTmcSubarrayLeafNodeDevice(SKABaseDevice):
    """The TMC subarray leaf node TANGO device."""

    @property
    def delay_model(self) -> dict[str, Any] | None:
        """Receive delay model of the device.
        Returns ``None`` when the delay model is empty."""
        delay_model_str = self.read_attribute("delayModel", str)
        return json.loads(delay_model_str) if delay_model_str else None

    @contextlib.contextmanager
    def collect_delay_model_change_events(self):
        """Adds all ``DelayModel`` change events to a queue which is yielded as the context session."""
        delay_model_change_events = queue.SimpleQueue[str]()
        with AttributeChangeEventSubscription(self._device, "DelayModel", delay_model_change_events.put):
            yield delay_model_change_events

    def _handle_long_running_command_result(self, command_name: str, result: tuple[int, str]):
        result_value, message = result
        result_code = ResultCode(result_value)

        if result_code != ResultCode.OK:
            raise RuntimeError(
                f"{self}: Long-running command '{command_name}' finished with result {result_code.name} and message: {message}"
            )


class LowSdpSubarrayMockDevice(SKABaseDevice):
    """The :py:class:`HelperSdpSubarray` TANGO device."""

    def set_direct_receive_addresses(self, receive_addresses: dict[str, Any]):
        """Command to set receive addresses on the TMC subarray"""
        self.command("SetDirectreceiveAddresses", json.dumps(receive_addresses))

    @property
    def receive_addresses(self) -> dict[str, Any]:
        """Receive addresses of the device."""
        return json.loads(self.read_attribute("receiveAddresses", str))


class LowTmcDevices:
    """LOW-TMC TANGO devices."""

    def __init__(self, tango_context: TangoContext) -> None:
        self._tango_context = tango_context
        self._central_node_fqdns = tango_context.get_device_fqdns("CentralNodeLow")
        self._subarray_fqdns = tango_context.get_device_fqdns("SubarrayNodeLow")
        self._sdp_subarray_mock_fqdns = tango_context.get_device_fqdns("HelperSdpSubarray")

        self._subarray_leaf_node_fqdns = {
            TmcLeafNodeType.CSP: tango_context.get_device_fqdns("CspSubarrayLeafNodeLow"),
            TmcLeafNodeType.MCCS: tango_context.get_device_fqdns("MccsSubarrayLeafNodeLow"),
            TmcLeafNodeType.SDP: tango_context.get_device_fqdns("SdpSubarrayLeafNodeLow"),
        }

    def central_node(self) -> LowTmcCentralNodeDevice:
        """Retrieve the LOW-TMC central node TANGO device."""
        assert len(self._central_node_fqdns) > 0, "No central node found for LOW-TMC"
        return self._tango_context.get_device(self._central_node_fqdns[0], LowTmcCentralNodeDevice)

    def all_subarrays(self) -> list[LowTmcSubarrayDevice]:
        """Retrieve all LOW-TMC subarray TANGO devices."""
        return [self._tango_context.get_device(fqdn, LowTmcSubarrayDevice) for fqdn in self._subarray_fqdns]

    def subarray_by_id(self, subarray_id: int) -> LowTmcSubarrayDevice:
        """Retrieve a LOW-TMC subarray TANGO device by its identifier.

        :param subarray_id: The identifier of the subarray to retrieve.
        """
        matches = [fqdn for fqdn in self._subarray_fqdns if fqdn.endswith(f"{subarray_id}")]
        assert len(matches) > 0, f"No subarray devices found for LOW-TMC with id {subarray_id}"
        return self._tango_context.get_device(matches[0], LowTmcSubarrayDevice)

    def all_sdp_subarrays(self) -> list[LowSdpSubarrayMockDevice]:
        """Retrieve all SDP subarray TANGO devices."""
        return [self._tango_context.get_device(fqdn, LowSdpSubarrayMockDevice) for fqdn in self._sdp_subarray_mock_fqdns]

    def sdp_subarray_by_id(self, subarray_id: int) -> LowSdpSubarrayMockDevice:
        """Retrieve a SDP subarray TANGO device by its identifier.

        :param subarray_id: The identifier of the subarray to retrieve.
        """
        matches = [fqdn for fqdn in self._sdp_subarray_mock_fqdns if fqdn.endswith(f"{subarray_id:02d}")]
        assert len(matches) > 0, f"No SDP subarray devices found for LOW-TMC with id {subarray_id}"
        return self._tango_context.get_device(matches[0], LowSdpSubarrayMockDevice)

    def subarray_leaf_node(self, leaf_node_type: TmcLeafNodeType, leaf_node_id: int) -> LowTmcSubarrayLeafNodeDevice:
        """Retrieve a subarray leaf node by its identifier and type

        :param leaf_node_type: The type of leaf node to retrieve.
        :param leaf_node_id: The identifier of the subarray leaf node to retrieve.
        """
        fqdns = self._subarray_leaf_node_fqdns[leaf_node_type]
        matches = [fqdn for fqdn in fqdns if fqdn.endswith(f"{leaf_node_id:02d}")]
        assert len(matches) > 0, f"No subarray leaf node devices found for LOW-TMC with id {leaf_node_id}"
        return self._tango_context.get_device(matches[0], LowTmcSubarrayLeafNodeDevice)
