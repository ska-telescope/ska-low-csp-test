"""
This module contains a pytest plugin that records session-level environment properties to the Allure environment file.
"""

import os
from typing import Any

import pytest

from ska_low_csp_test.reporting import RecordEnvironmentProperty

ALLURE_ENVIRONMENT_PLUGIN_KEY = pytest.StashKey["AllureEnvironmentPlugin"]()


class AllureEnvironmentPlugin:
    """
    Pytest plugin to write the recorded environment properties to the Allure environment file.
    """

    def __init__(self, output_dir: str) -> None:
        self._output_dir = output_dir
        self._props: dict[str, Any] = {}

    def record_environment_property(self, key: str, value: Any) -> None:
        """
        Record an environment property.
        """
        self._props[key] = value

    @pytest.hookimpl
    def pytest_sessionfinish(self):
        """
        Hook to write the collected data to file.
        """
        dirname = os.path.abspath(self._output_dir)
        os.makedirs(dirname, exist_ok=True)
        filename = os.path.join(dirname, "environment.properties")
        with open(filename, "w", encoding="utf8") as file:
            for key, value in sorted(self._props.items()):
                file.write(f"{key}={value}\n")


def pytest_configure(config: pytest.Config) -> None:
    """
    Hook to add extra configuration to the pytest config.
    """

    if allure_report_dir := config.getoption("allure_report_dir"):
        config.stash[ALLURE_ENVIRONMENT_PLUGIN_KEY] = AllureEnvironmentPlugin(allure_report_dir)  # type: ignore
        config.pluginmanager.register(config.stash[ALLURE_ENVIRONMENT_PLUGIN_KEY])


def pytest_unconfigure(config: pytest.Config) -> None:
    """
    Hook to remove extra configuration from the pytest config.
    """
    if ALLURE_ENVIRONMENT_PLUGIN_KEY in config.stash:
        config.pluginmanager.unregister(config.stash[ALLURE_ENVIRONMENT_PLUGIN_KEY])


@pytest.fixture(scope="session")
def record_environment_property(request: pytest.FixtureRequest) -> RecordEnvironmentProperty:
    """
    Fixture to record any environment properties in the session,
    so that they can be stored in a report at the end of the session.
    """

    def noop(key: str, value: Any):  # pylint:disable=unused-argument
        """
        No-op function in case Allure is not used.
        """

    if plugin := request.config.stash.get(ALLURE_ENVIRONMENT_PLUGIN_KEY, None):
        return plugin.record_environment_property

    return noop
