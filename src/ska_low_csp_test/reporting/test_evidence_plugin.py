"""
This module provides a pytest plugin to record evidence from tests, which is automatically attached to reports.
"""

from collections import defaultdict
from typing import Mapping

import allure
import pytest
from pytest_xray import evidence
from pytest_xray.constant import JIRA_XRAY_FLAG

from ska_low_csp_test.reporting import RecordTestEvidence

__test__ = False  # Whoah there, pytest. This isn't a test.


XRAY_TEST_EVIDENCE_PLUGIN_KEY = pytest.StashKey["XrayTestEvidencePlugin"]()


class XrayTestEvidencePlugin:
    """
    Pytest plugin to add attachments as test evidence to Xray reports.
    """

    def __init__(self) -> None:
        self._evidence: Mapping[str, list[tuple[str, str, str | bytes]]] = defaultdict(list)

    def store(self, nodeid, file_name: str, content_type: str, content: str | bytes):
        """
        Store an attachment for a pytest node.
        """
        self._evidence[nodeid].append((file_name, content_type, content))

    @pytest.hookimpl(wrapper=True)
    def pytest_runtest_makereport(self):
        """
        Hook that attaches any collected attachments as evidence to the report.
        Used by the pytest-jira-xray plugin.
        """
        report: pytest.TestReport = yield
        evidences = getattr(report, "evidences", [])
        if report.when == "call":
            for file_name, content_type, content in self._evidence[report.nodeid]:
                evidences.append(
                    evidence.evidence(
                        data=content,  # type: ignore
                        filename=file_name,
                        content_type=content_type,
                    )
                )

        if report.caplog:
            evidences.append(evidence.text(report.caplog, "pytest.log"))

        setattr(report, "evidences", evidences)
        return report


def pytest_configure(config: pytest.Config) -> None:
    """
    Hook to add extra configuration to the pytest config.
    """
    if config.getoption(JIRA_XRAY_FLAG):
        config.stash[XRAY_TEST_EVIDENCE_PLUGIN_KEY] = XrayTestEvidencePlugin()
        config.pluginmanager.register(config.stash[XRAY_TEST_EVIDENCE_PLUGIN_KEY])


def pytest_unconfigure(config: pytest.Config) -> None:
    """
    Hook to remove extra configuration from the pytest config.
    """
    if XRAY_TEST_EVIDENCE_PLUGIN_KEY in config.stash:
        config.pluginmanager.unregister(config.stash[XRAY_TEST_EVIDENCE_PLUGIN_KEY])


def _content_type_to_attachment_type(content_type: str) -> allure.attachment_type:
    for t in allure.attachment_type:
        if t.value[0] == content_type:
            return t

    raise ValueError(f"Content type {content_type} is not supported")


@pytest.fixture
def record_test_evidence(request: pytest.FixtureRequest) -> RecordTestEvidence:
    """
    Fixture to record test evidence and attach it to the reports.
    """

    def _func(file_name: str, content_type: str, content: str | bytes):
        allure.attach(
            content,
            name=file_name,
            attachment_type=_content_type_to_attachment_type(content_type),
        )

        if plugin := request.config.stash.get(XRAY_TEST_EVIDENCE_PLUGIN_KEY, None):
            plugin.store(request.node.nodeid, file_name, content_type, content)

    return _func
