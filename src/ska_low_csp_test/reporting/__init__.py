"""
Module containing classes and helpers for reporting functionality.
"""

import enum
from typing import Any, Protocol

import allure
import pytest

__all__ = [
    "RecordEnvironmentProperty",
    "RecordTestEvidence",
    "TestSuite",
    "test_case",
]


class RecordEnvironmentProperty(Protocol):
    """
    Function that records an environment property used in reporting.
    """

    def __call__(self, key: str, value: Any):
        """
        Record an environment property.

        :param key: The key of the environment property.
        :param value: The value of the environment property.
        """


class RecordTestEvidence(Protocol):
    """
    Function that records test evidence used in reporting.
    """

    def __call__(self, file_name: str, content_type: str, content: str | bytes):
        """
        Record a file as test evidence.

        :param file_name: The name of the file.
        :param content_type: The content type of the file.
        :param content: The file content.
        """


class TestSuite(str, enum.Enum):
    """
    Describes which group a test belongs to.

    A test suite corresponds to a single test plan in JAMA and Xray.
    """

    __test__ = False  # Whoah there, pytest. This isn't a test.

    TELESCOPE_MONITORING_AND_CONTROL = "Telescope Monitoring and Control"
    PULSAR_TIMING = "Pulsar Timing"
    CORRELATOR_BEAMFORMER = "Correlator Beamformer"
    PERFORMANCE = "Performance"
    PULSAR_SEARCH = "Pulsar Search"
    MULTIPLE_SUBARRAY = "Multiple Subarrays"
    ENGINEERING_MODE = "Engineering Mode"
    COMMAND_AND_CONTROL = "Command and Control"
    ABORT_RESTART = "Abort-Restart"


def test_case(  # pylint: disable=too-many-arguments
    title: str,
    suite: TestSuite,
    jama_url: str | None,
    xray_id: str | None,
    known_defects: list[str] | None = None,
):
    """
    Decorator that adds metadata to a test function for reporting purposes.

    :param title: The title of the test case.
    :param suite: The suite the test belongs to.
    :param category: The category the test belongs to.
    :param jama_url: The URL to the test case description in JAMA.
    :param xray_id: The issue key of the corresponding Xray test.
    :param known_defects: List of JIRA issue keys describing known defects for this test, such as SKBs.
    """
    defects = known_defects or []
    decorators = [
        allure.title(title),
        allure.parent_suite(suite.value),
        *(allure.issue(defect, name=defect) for defect in defects),
    ]

    if jama_url:
        decorators.append(allure.link(jama_url, name="JAMA Test Case"))

    if xray_id:
        decorators.append(allure.testcase(xray_id, name=xray_id))
        decorators.append(pytest.mark.xray(xray_id, defects=defects))

    def decorator(func):
        for f in decorators:
            func = f(func)
        return func

    return decorator


test_case.__test__ = False  # type: ignore # pylint: disable=unused-variable
