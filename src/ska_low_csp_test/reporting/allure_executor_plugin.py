"""
This module contains a pytest plugin that creates an Allure executor file based on environment variables.
"""

import json
import os

import pytest

ALLURE_GITLAB_EXECUTOR_PLUGIN_KEY = pytest.StashKey["AllureGitLabExecutorPlugin"]()


class AllureGitLabExecutorPlugin:
    """
    Pytest plugin to write GitLab environment variables to the Allure executor file.
    """

    def __init__(self, output_dir: str) -> None:
        self._output_dir = output_dir
        self._props = {}

    @pytest.hookimpl
    def pytest_sessionstart(self):
        """
        Hook to collect GitLab environment information.
        """
        self._props = {
            "type": "gitlab",
            "name": os.environ["CI_PROJECT_PATH"],
            "url": os.environ["CI_PROJECT_URL"],
            "buildOrder": os.environ["CI_PIPELINE_IID"],
            "buildName": os.environ["CI_JOB_NAME"],
            "buildUrl": os.environ["CI_JOB_URL"],
        }

    @pytest.hookimpl
    def pytest_sessionfinish(self):
        """
        Hook to write the collected data to file.
        """
        dirname = os.path.abspath(self._output_dir)
        os.makedirs(dirname, exist_ok=True)
        filename = os.path.join(dirname, "executor.json")
        with open(filename, "w", encoding="utf8") as file:
            json.dump(self._props, file)


def pytest_configure(config: pytest.Config) -> None:
    """
    Hook to add extra configuration to the pytest config.
    """

    if allure_report_dir := config.getoption("allure_report_dir"):
        if os.environ.get("GITLAB_CI", None):
            config.stash[ALLURE_GITLAB_EXECUTOR_PLUGIN_KEY] = AllureGitLabExecutorPlugin(allure_report_dir)  # type: ignore
            config.pluginmanager.register(config.stash[ALLURE_GITLAB_EXECUTOR_PLUGIN_KEY])


def pytest_unconfigure(config: pytest.Config) -> None:
    """
    Hook to remove extra configuration from the pytest config.
    """
    if ALLURE_GITLAB_EXECUTOR_PLUGIN_KEY in config.stash:
        config.pluginmanager.unregister(config.stash[ALLURE_GITLAB_EXECUTOR_PLUGIN_KEY])
