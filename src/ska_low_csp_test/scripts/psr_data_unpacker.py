"""
Command-line interface to unpack LOW-CBF PSR data.

Knwown Issues:
--------------

This unpacker has, as of yet, only been tested against LOW CBF PST-TAB output.
As such it does not have logic to unpack Stokes Parameter data format, and makes
no guarantees as to whether it is able to handle any of the other PSR products.
"""

import argparse
import logging
import os
from pathlib import Path

import xarray as xr

from ska_low_csp_test.psr import PsrDataReader, PsrDataVisitor, PsrPacket, unpack_pcap_file

module_logger = logging.getLogger(__name__)


class PsrPrettyPrinter(PsrDataVisitor):
    """PSR packet pretty-printer"""

    def visit_packet(self, _index: int, packet: PsrPacket) -> None:
        """Visit a packet."""
        packet.pprint()

    def on_finish(self) -> None:
        """Handler called after all PSR data is processed."""
        print("-eod-")


def main(
    pcap_file_path: str,
    output_base: str | None,
    write_zarr_: bool,
) -> None:
    """Main script entry point."""

    if output_base or write_zarr_:
        basename = os.path.basename(pcap_file_path)
        if output_base and output_base.endswith("/"):
            output_base = output_base + basename
        psr_samples = unpack_pcap_file(pcap_file_path)
        write_zarr(psr_samples, output_base or basename)
    else:
        visitor = PsrPrettyPrinter()
        reader = PsrDataReader(visitor)
        reader.read_pcap_file(pcap_file_path)


def cli() -> None:
    """Command-line parsing."""

    parser = argparse.ArgumentParser(description="Unpacks a PCAP file containing PSR TAB data.")
    parser.add_argument("file", type=Path, help="Path to the PCAP file", metavar="pcap_file_path")
    parser.add_argument(
        "--output-base",
        "-o",
        help="Filename and / or directory for output zarr store; specifying only this implies `-z` too",
        metavar="path/to/unpacked/psr-data",
        type=str,
    )
    parser.add_argument(
        "--verbose",
        "-v",
        action="store_true",
        help="Increase verbosity level",
    )
    parser.add_argument(
        "--write-zarr",
        "-z",
        action="store_true",
        help="Write PSR packet contents as an Xarray to a zarr key-value store",
    )

    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(format="%(message)s", level=logging.INFO)
    main(
        args.file,
        args.output_base,
        args.write_zarr,
    )


def write_zarr(samples: xr.Dataset, output_base: str) -> None:
    """Write zarr file.

    :param samples: XArray Dataset with PSR samples
    :param output_base: name and / or destination directory of the output file
    """
    module_logger.info("Start writing PSR data to file: %s", output_base)
    samples.to_zarr(f"{output_base}.zarr")
    module_logger.info("Finished writing PSR data to file: %s", output_base)


if __name__ == "__main__":
    cli()
