"""Unpack a SPS v3 station data PCAP file into a human-readable format."""

import argparse
import typing

import dpkt

from ska_low_csp_test import sps3


def main(
    file_pcap: typing.BinaryIO,
) -> None:
    """Main script entry point."""
    pcap_reader = dpkt.pcap.UniversalReader(file_pcap)
    sps_packets = _get_packets(pcap_reader)
    for packet in sps_packets:
        packet.pprint()
    print("-eod-")


def cli() -> None:
    """Command-line parsing."""
    parser = argparse.ArgumentParser(description="Unpacks a PCAP file containing a SPS station data SPEAD stream.")
    parser.add_argument("file", type=argparse.FileType("rb"), help="Path to the PCAP file")

    args = parser.parse_args()
    main(
        args.file,
    )


def _get_packets(reader: dpkt.pcap.Reader | dpkt.pcapng.Reader) -> list[sps3.SpsV3Packet]:
    """Read samples from PCAP file.

    :param pcap: PCAP reader instance

    :returns: list of SpsPackets
    """
    sps_packets = []
    for _, packet in reader:
        eth = dpkt.ethernet.Ethernet(packet)
        if eth.type != dpkt.ethernet.ETH_TYPE_IP:
            continue

        ip = eth.data
        if ip.p != dpkt.ip.IP_PROTO_UDP:
            continue

        udp = ip.data
        sps_packets.append(sps3.SpsV3Packet(udp.data))
    return sps_packets


if __name__ == "__main__":
    cli()
