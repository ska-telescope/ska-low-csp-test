r"""Unpack a SPS station data PCAP file into a human-readable format.

Usage hints
-----------

By default the format is rather verbose. One is not generally interested in
seeing all the detail, so a reduced format can be obtained by filtering it
through ``grep``, with exclusions, as shown below:

::

    sps-data-unpacker data.cap | grep -v "Item\|item\(A\|I\)" | less

Using the ``--output-base`` switch in isolation implies the ``--write-png`` flag. In other words, saying

::

    sps-data-unpacker -o foo data.pcap

is the same as saying

::

    sps-data-unpacker -o foo -w data.pcap
"""

import argparse
import itertools
import os.path
from functools import partial
from operator import attrgetter

import matplotlib.pyplot as plt
import numpy as np
from scapy.all import PcapReader

from ska_low_csp_test.sps import SpsSpead

NUM_SAMPLES_PER_CHANNEL = 2048


def main(
    filename_pcap: str,
    output_base: str,
    write_png_: bool,
    write_npy_: bool,
) -> None:
    """Main script entry point."""
    pcap = PcapReader(filename_pcap)

    if write_npy_:
        write_npy(pcap, output_base or os.path.basename(filename_pcap))
    elif output_base or write_png_:
        write_png(pcap, output_base or os.path.basename(filename_pcap))
    else:
        for packet in pcap:
            lfaa = packet[SpsSpead]
            lfaa.show()


def cli() -> None:
    """Command-line parsing."""
    parser = argparse.ArgumentParser(description="Unpacks a PCAP file containing a SPS station data SPEAD stream.")
    parser.add_argument("file", type=argparse.FileType("r"), help="Path to the PCAP file")
    parser.add_argument(
        "--output-base",
        "-o",
        type=str,
        help="Base filename for output PNG or NPY files; specifying only this implies `-w` too",
    )
    parser.add_argument(
        "--write-png",
        "-w",
        action="store_true",
        help="Write H-pol & V-pol real and imaginary sample components as PNG files",
    )
    parser.add_argument(
        "--write-npy",
        "-W",
        action="store_true",
        help="Write H-pol & V-pol real and imaginary sample components as matrix to a NPY file",
    )

    args = parser.parse_args()
    main(
        args.file.name,
        args.output_base,
        args.write_png,
        args.write_npy,
    )


def _get_samples(pcap: PcapReader) -> np.ndarray:
    """Read samples from PCAP file.

    :param pcap: PCAP reader instance

    :returns: numpy matrix of the sample values
    """
    values = map(lambda packet: packet[SpsSpead].values, pcap)
    pols = map(partial(map, attrgetter("vPolReal", "vPolImag", "hPolReal", "hPolImag")), values)
    samples = map(partial(map, lambda t: np.array(t, dtype=((np.ubyte, 4)))), pols)
    flattened = itertools.chain.from_iterable(samples)
    array = np.fromiter(flattened, dtype=(((np.byte, 4))))
    matrix = np.reshape(array, (-1, NUM_SAMPLES_PER_CHANNEL, 4))
    return matrix


def write_npy(pcap: PcapReader, output_base: str) -> None:
    """Write NPY file.

    :param pcap: PCAP reader instance
    :param output_base: base name of the output file
    """
    samples = _get_samples(pcap)
    with open(f"{output_base}.npy", "wb") as file_:
        np.save(file_, samples)


def write_png(pcap: PcapReader, output_base: str) -> None:
    """Write PNG file.

    The pixel values for the "_re" image is: ``["hPolReal", 0, "vPolReal"]``,
    the pixel values for the "_im" image is: ``[0, "hPolImag", "vPolImag"]``.

    :param pcap: PCAP reader instance
    :param output_base: base name of the output file
    """
    pixvals = _get_samples(pcap) / 255 + 0.5
    zero = np.zeros((len(pixvals), NUM_SAMPLES_PER_CHANNEL))
    re, im = np.dstack((pixvals[:, :, 2], zero, pixvals[:, :, 0])), np.dstack((zero, pixvals[:, :, 3], pixvals[:, :, 1]))
    plt.imsave(output_base + "_re.png", re)
    plt.imsave(output_base + "_im.png", im)


if __name__ == "__main__":
    cli()
