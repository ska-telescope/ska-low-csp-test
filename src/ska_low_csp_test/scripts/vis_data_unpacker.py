"""
Command-line interface to unpack LOW-CBF visibility data.
"""

import argparse
import logging
import os
from pathlib import Path

from ska_low_csp_test.cbf.visibilities import unpack_pcap_file

logger = logging.getLogger(__name__)


def unpack(
    file_path: Path,
    zarr_output_path: Path | None,
):
    """
    Unpack a PCAP file containing LOW-CBF visibility data.

    :param file_path: Path of the PCAP file to unpack.
    :param zarr_output_path: File path to write unpacked metadata to.
    """
    logger.debug("File size: %f MB", os.stat(file_path).st_size / 1e6)
    data = unpack_pcap_file(file_path.as_posix(), logger=logger)

    if zarr_output_path is not None:
        logger.info("Writing unpacked data to zarr archive at %s", zarr_output_path)
        data.to_zarr(zarr_output_path)


def cli():
    """
    Entrypoint for the command-line interface.
    """
    parser = argparse.ArgumentParser(
        epilog=(
            "Documentation for this command can be found at "
            "https://developer.skao.int/projects/ska-low-csp-test/en/latest/tools/vis-data-unpacker.html"
        )
    )
    parser.add_argument(
        "pcap_file_path",
        type=Path,
        help="Path to a PCAP file containing visibility data",
    )

    parser.add_argument(
        "--write-zarr",
        "-z",
        type=Path,
        metavar="path/to/output.zarr",
        help="Write the unpacked data to a .zarr archive at the provided path",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
    )

    args = parser.parse_args()

    logging_level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=logging_level)

    unpack(
        file_path=args.pcap_file_path,
        zarr_output_path=args.write_zarr,
    )


if __name__ == "__main__":
    cli()
