"""Analyse SPS station data from a PCAP file."""

import argparse

import matplotlib.pyplot as plt
import numpy as np

from ska_low_csp_test.pcap import extract_lfaa_pcap_info


def main() -> None:
    """Main script entrypoint."""
    parser = argparse.ArgumentParser(
        description="Extracts and prints information from a PCAP file containing a SPS station data SPEAD stream."
    )
    parser.add_argument("file", type=argparse.FileType("r"), help="Path to the PCAP file")
    parser.add_argument(
        "--png-output-file",
        "-o",
        type=argparse.FileType("w"),
        dest="png_output_file",
        help="When provided, creates .png graph, based on channelized data and save it to the provided path",
    )

    args = parser.parse_args()
    pcap_info = extract_lfaa_pcap_info(args.file.name)

    print(f"Data file {args.file.name}")
    print(f"  has {pcap_info.packet_count} packets of size {pcap_info.packet_size} bytes")
    print(f"  simulating data from {pcap_info.station_count} stations")
    frequencies = pcap_info.station_channel_frequency[0]
    print(f"  for {pcap_info.channel_count} channels: {pcap_info.frequency_ids}")
    print(f"  with frequencies {list(frequencies / 1e6)} MHz")
    delta_t = pcap_info.unix_epoch_time_delta
    integration_periods = pcap_info.integration_period_count
    print(f"  approximately {delta_t} seconds representing {integration_periods:.2f} correlator dumps")
    if args.png_output_file:
        channel_data = np.mean(pcap_info.channelized_data, axis=-1)
        # Convert real,imag dimensions into complex array
        #     c_arr = arr[...,0] + arr[...,1] * 1j
        v_channels = channel_data[:, 0, :, 0] + 1j * channel_data[:, 0, :, 1]
        plt.figure()
        # 1e6 is to convert Hz to MHz
        values_x = pcap_info.station_channel_frequency[0] / 1e6
        values_y = np.abs(np.mean(v_channels, axis=0))
        plt.semilogy(values_x, values_y, "o-")
        plt.xticks(values_x)
        plt.grid(True, which="minor", ls=":")
        plt.grid(True, which="major", ls="--")
        plt.xlabel("freq [MHz]")
        plt.ylabel("Amp [arb dB]")

        file_path = args.png_output_file.name
        # save the result as png image
        plt.savefig(file_path)
        print(f"Wrote image to {file_path}")


if __name__ == "__main__":
    main()
