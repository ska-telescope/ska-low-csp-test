"""Module that provides classes to model the system under test."""

import logging
from types import TracebackType
from typing import Any

import allure
import tango
from pytest_check.check_functions import check_func
from ska_control_model import AdminMode, ObsMode, ObsState

from ska_low_csp_test.cbf.devices import LowCbfDevices
from ska_low_csp_test.cbf.helpers import configure_ptp
from ska_low_csp_test.cbf.plugins import RoutingTableLogger
from ska_low_csp_test.domain.model import (
    LowCspAssignResourcesSchema,
    LowCspConfigureSchema,
    LowCspReleaseResourcesSchema,
    LowCspScanSchema,
)
from ska_low_csp_test.lmc.devices import LowCspDevices, LowCspSubarrayDevice
from ska_low_csp_test.plugins import SubarrayLifecyclePlugin
from ska_low_csp_test.pst.devices import PstDevices

__all__ = ["SubarrayUnderTest", "SystemUnderTest"]


class SubarrayUnderTest:
    """The main entrypoint to interact with a single subarray."""

    def __init__(
        self,
        subarray_id: int,
        subarray_device: LowCspSubarrayDevice,
        logger: logging.Logger | None = None,
    ):
        self.id = subarray_id
        self._subarray = subarray_device
        self._logger = logger or logging.getLogger(__name__)
        self._lifecycle_plugins: set[SubarrayLifecyclePlugin] = set()

    def __enter__(self) -> "SubarrayUnderTest":
        self._subarray.__enter__()
        return self

    def __exit__(
        self, exc_type: type[BaseException] | None, exc_value: BaseException | None, exc_tb: TracebackType | None
    ) -> None:
        self._subarray.__exit__(exc_type, exc_value, exc_tb)

    def register_lifecycle_plugins(self, *plugins: SubarrayLifecyclePlugin) -> None:
        """Register one or more plugins to hook into this subarray's lifecycle."""
        for plugin in plugins:
            self._logger.debug("Registering lifecycle plugin %s", plugin)
            self._lifecycle_plugins.add(plugin)

    def remove_lifecycle_plugins(self, *plugins: SubarrayLifecyclePlugin) -> None:
        """Remove one or more registered lifecycle plugins."""
        for plugin in plugins:
            if plugin in self._lifecycle_plugins:
                self._logger.debug("Removing lifecycle plugin %s", plugin)
                self._lifecycle_plugins.remove(plugin)

    def reset(self) -> None:
        """Reset the subarray under test.

        This resets the subarray to ``ObsState.EMPTY``, according to the SKA observing state machine defined in `ADR-8`_.

        .. _ADR-8: https://confluence.skatelescope.org/pages/viewpage.action?pageId=105416556
        """
        if self._subarray.obs_state == ObsState.EMPTY:
            self._subarray.get_obs_state_changes()  # Clear any leftover state changes
            return

        self._logger.info("Resetting subarray under test")

        if self._subarray.obs_state not in {ObsState.ABORTING, ObsState.ABORTED, ObsState.FAULT}:
            self.abort()

        if self._subarray.obs_state in {ObsState.ABORTED, ObsState.FAULT}:
            self.restart()
        self._subarray.get_obs_state_changes()  # Clear any leftover state changes

    @allure.step("Assign resources on the subarray under test")
    def assign_resources(
        self,
        schema: LowCspAssignResourcesSchema,
        wait_for_obs_state: ObsState | None = ObsState.IDLE,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``AssignResources`` command on the subarray.

        :param schema: See :doc:`ska-telmodel:schemas/csp/low/assignresources/index`
        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_assign_resources", schema)
        self._subarray.assign_resources(schema, wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        self._call_lifecycle_method("post_assign_resources", schema)

    @allure.step("Configure the subarray under test")
    def configure(
        self,
        schema: LowCspConfigureSchema,
        wait_for_obs_state: ObsState | None = ObsState.READY,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``Configure`` command on the subarray.

        :param schema: See :doc:`ska-telmodel:schemas/csp/low/configure/index`
        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_configure", schema)
        self._subarray.configure(schema, wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        with allure.step(f"Stations used for configuration: {schema['lowcbf']['stations']['stns']}"):
            pass
        self._call_lifecycle_method("post_configure", schema)

    @allure.step("Start a scan on the subarray under test")
    def scan(
        self,
        schema: LowCspScanSchema,
        wait_for_obs_state: ObsState | None = ObsState.SCANNING,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``Scan`` command on the subarray.

        :param schema: See :doc:`ska-telmodel:schemas/csp/low/scan/index`
        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_scan", schema)
        self._subarray.scan(schema, wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        self._call_lifecycle_method("post_scan", schema)

    @allure.step("End a scan on the subarray under test")
    def end_scan(
        self,
        wait_for_obs_state: ObsState | None = ObsState.READY,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``EndScan`` command on the subarray.

        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_end_scan")
        self._subarray.end_scan(wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        self._call_lifecycle_method("post_end_scan")

    @allure.step("Deconfigure the subarray under test")
    def end(
        self,
        wait_for_obs_state: ObsState | None = ObsState.IDLE,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``End`` command on the subarray.

        .. note:: The implementation actually uses the ``GoToIdle`` command defined by Low CSP.LMC.

        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_end")
        self._subarray.go_to_idle(wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        self._call_lifecycle_method("post_end")

    @allure.step("Release resources from the subarray under test")
    def release_resources(
        self,
        schema: LowCspReleaseResourcesSchema,
        wait_for_obs_state: ObsState | None = None,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``ReleaseResources`` command on the subarray.

        :param schema: See :doc:`ska-telmodel:schemas/csp/low/releaseresources/index`
        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_release_resources", schema)
        self._subarray.release_resources(schema, wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        self._call_lifecycle_method("post_release_resources", schema)

    @allure.step("Release all resources from the subarray under test")
    def release_all_resources(
        self,
        wait_for_obs_state: ObsState | None = ObsState.EMPTY,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``ReleaseAllResources`` command on the subarray.

        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_release_all_resources")
        self._subarray.release_all_resources(wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        self._call_lifecycle_method("post_release_all_resources")

    @allure.step("Abort the subarray under test")
    def abort(
        self,
        wait_for_obs_state: ObsState | None = ObsState.ABORTED,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``Abort`` command on the subarray.

        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_abort")
        self._subarray.abort(wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        self._call_lifecycle_method("post_abort")

    @allure.step("Restart the subarray under test")
    def restart(
        self,
        wait_for_obs_state: ObsState | None = ObsState.EMPTY,
        wait_for_completion=True,
    ) -> None:
        """
        Call the ``Restart`` command on the subarray.

        :param wait_for_obs_state: When provided, wait for the ``obsState`` to change to the expected value after the
            command completes.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        """
        self._call_lifecycle_method("pre_restart")
        self._subarray.restart(wait_for_completion=wait_for_completion)
        if wait_for_obs_state is not None:
            self._subarray.wait_for_attribute_value("obsState", ObsState, wait_for_obs_state)
        self._call_lifecycle_method("post_restart")

    def _call_lifecycle_method(self, method_name: str, *args: Any) -> None:
        for plugin in self._lifecycle_plugins:
            self._logger.debug("Calling subarray lifecycle method '%s' on plugin %s", method_name, plugin)
            getattr(plugin, method_name)(*args)

    @check_func
    @allure.step("Verify that the subarray obsState is {expected}")
    def verify_obs_state(self, expected: ObsState):
        """Verify that the ``obsState`` of the subarray matches the expected state."""
        assert self._subarray.obs_state == expected

    @check_func
    @allure.step("Verify that the subarray obsState transitions are {expected}")
    def verify_obs_state_transitions(self, *expected: ObsState):
        """Verify that the ``obsState`` transitions of the subarray match the expected state transitions."""
        assert self._subarray.get_obs_state_changes() == list(expected)

    @check_func
    @allure.step("Verify that the subarray obsMode is {expected}")
    def verify_obs_mode(self, *expected: ObsMode):
        """Verify that the ``obsMode`` of the subarray matches the expected mode(s)."""
        assert self._subarray.obs_mode == expected

    @property
    def command_timeout(self) -> int:
        """Wrapper for the subarray command timeout property."""
        return self._subarray.command_timeout

    @command_timeout.setter
    def command_timeout(self, command_timeout_s: int) -> None:
        self._subarray.command_timeout = command_timeout_s


class SystemUnderTest:
    """The main entrypoint to interact with the whole system."""

    def __init__(
        self,
        low_csp_devices: LowCspDevices,
        low_cbf_devices: LowCbfDevices,
        pst_devices: PstDevices,
        logger: logging.Logger | None = None,
    ) -> None:
        self.low_csp_devices = low_csp_devices
        self.low_cbf_devices = low_cbf_devices
        self.pst_devices = pst_devices
        self._logger = logger or logging.getLogger(__name__)

    def reset(self) -> None:
        """Reset the system under test to an initial state."""
        self._logger.info("Resetting system under test")

        low_csp_controller = self.low_csp_devices.controller()
        low_csp_subarrays = self.low_csp_devices.all_subarrays()

        for subarray in low_csp_subarrays:
            match subarray.state:
                case tango.DevState.FAULT:
                    subarray.restart()
                case tango.DevState.ON:
                    if subarray.obs_state == ObsState.EMPTY:
                        continue
                    if subarray.obs_state not in {ObsState.ABORTING, ObsState.ABORTED, ObsState.FAULT}:
                        subarray.abort()
                    if subarray.obs_state in {ObsState.ABORTED, ObsState.FAULT}:
                        subarray.restart()
                case _ as state:
                    self._logger.debug("Subarray %s in state %s.", subarray, state)
                    continue
            subarray.get_obs_state_changes()  # Clear any leftover state changes

        self._logger.info("Turning off LOW-CSP controller")
        low_csp_controller.admin_mode = AdminMode.OFFLINE
        low_csp_controller.wait_for_attribute_value("isCommunicating", bool, False)

        self._logger.info("Waiting for LOW-CSP subarrays to turn off")
        for subarray in low_csp_subarrays:
            subarray.wait_for_attribute_value(
                "state",
                tango.DevState,
                tango.DevState.DISABLE,
                timeout_s=120,
            )

        self._logger.info("Turning on LOW-CSP controller")
        low_csp_controller.admin_mode = AdminMode.ONLINE
        low_csp_controller.wait_for_attribute_value("isCommunicating", bool, True)
        low_csp_controller.on()

        self._logger.info("Waiting for LOW-CSP subarrays to turn on")
        for subarray in low_csp_subarrays:
            subarray.wait_for_attribute_value("state", tango.DevState, tango.DevState.ON)

        self._logger.info("Finished resetting system under test")

    def subarray(self, subarray_id: int) -> SubarrayUnderTest:
        """Retrieve a subarray under test."""
        subarray = SubarrayUnderTest(
            subarray_id=subarray_id,
            subarray_device=self.low_csp_devices.subarray_by_id(subarray_id),
            logger=self._logger,
        )
        subarray.register_lifecycle_plugins(RoutingTableLogger(self.low_cbf_devices))

        return subarray

    @allure.step("Configure PTP")
    def configure_ptp(self, ptp_clock_port: str) -> None:
        """Configure PTP."""
        configure_ptp(
            low_cbf_devices=self.low_cbf_devices,
            ptp_clock_port=ptp_clock_port,
            logger=self._logger,
        )
