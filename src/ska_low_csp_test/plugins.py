"""Module that provides base classes for plugins."""

import abc

from ska_low_csp_test.domain.model import (
    LowCspAssignResourcesSchema,
    LowCspConfigureSchema,
    LowCspReleaseResourcesSchema,
    LowCspScanSchema,
)

__all__ = ["SubarrayLifecyclePlugin"]


class SubarrayLifecyclePlugin(abc.ABC):
    """Base for classes that want to plug into the subarray lifecycle."""

    def pre_assign_resources(self, schema: LowCspAssignResourcesSchema) -> None:
        """Called before the subarray is instructed to assign resources."""

    def post_assign_resources(self, schema: LowCspAssignResourcesSchema) -> None:
        """Called after the subarray is instructed to assign resources."""

    def pre_configure(self, schema: LowCspConfigureSchema) -> None:
        """Called before the subarray is instructed to configure."""

    def post_configure(self, schema: LowCspConfigureSchema) -> None:
        """Called after the subarray is instructed to configure."""

    def pre_scan(self, schema: LowCspScanSchema) -> None:
        """Called before the subarray is instructed to start scanning."""

    def post_scan(self, schema: LowCspScanSchema) -> None:
        """Called after the subarray is instructed to start scanning."""

    def pre_end_scan(self) -> None:
        """Called before the subarray is instructed to stop scanning."""

    def post_end_scan(self) -> None:
        """Called after the subarray is instructed to stop scanning."""

    def pre_end(self) -> None:
        """Called before the subarray is instructed to deconfigure."""

    def post_end(self) -> None:
        """Called after the subarray is instructed to deconfigure."""

    def pre_release_resources(self, schema: LowCspReleaseResourcesSchema) -> None:
        """Called before the subarray is instructed to release resources."""

    def post_release_resources(self, schema: LowCspReleaseResourcesSchema) -> None:
        """Called after the subarray is instructed to release resources."""

    def pre_release_all_resources(self) -> None:
        """Called before the subarray is instructed to release all resources."""

    def post_release_all_resources(self) -> None:
        """Called after the subarray is instructed to release all resources."""

    def pre_abort(self) -> None:
        """Called before the subarray is instructed to abort."""

    def post_abort(self) -> None:
        """Called after the subarray is instructed to abort."""

    def pre_restart(self) -> None:
        """Called before the subarray is instructed to restart."""

    def post_restart(self) -> None:
        """Called after the subarray is instructed to restart."""
