"""Module that provides utilities to work with TANGO events."""

import logging
from collections.abc import Iterable
from types import TracebackType
from typing import Any, Callable

import tango
from ska_control_model import AdminMode, HealthState, ObsMode, ObsState

__all__ = ["AttributeChangeEventSubscription"]


ENUM_HINT = {
    "adminmode": AdminMode,
    "healthstate": HealthState,
    "obsmode": ObsMode,
    "obsstate": ObsState,
}


def enum_hint(name: str) -> Callable[[int], str]:
    """Convert enum name to enum type."""
    enum_ = ENUM_HINT.get(name.lower(), lambda _: None)

    def wrapped(value: int) -> str:
        """Convert enum value to string representation."""
        if enum_:
            return f" <{enum_.__name__}.{enum_(value).name}>"
        return " <?>"

    return wrapped


def get_enum_hint(value: tango.DeviceAttribute) -> Callable[[Any], str]:
    """Dynamically convert enum value to string representation."""

    if value.type is tango._tango.CmdArgType.DevEnum:  # pylint: disable=protected-access,c-extension-no-member
        return enum_hint(value.name)

    return lambda _: ""


class AttributeChangeEventSubscription:
    """Helper class to subscribe to attribute change events on a TANGO device.

    This class subscribes to change events for an attribute on a given TANGO device.
    It unpacks the new attribute value from the event and passes it on to the given value handler.
    """

    def __init__(
        self,
        device: tango.DeviceProxy,
        attribute: str,
        value_handler: Callable[[Any], None],
        logger: logging.Logger | None = None,
    ) -> None:
        self._device = device
        self._attribute = attribute
        self._handler = value_handler
        self._logger = logger or logging.getLogger(__name__)
        self._subscription_id: int | None = None

    def __enter__(self) -> None:
        if self._subscription_id:
            return

        self._logger.debug("%s: Subscribing to change events", self)
        self._subscription_id = self._device.subscribe_event(self._attribute, tango.EventType.CHANGE_EVENT, self._handle_event)

    def __exit__(
        self, exc_type: type[BaseException] | None, exc_value: BaseException | None, exc_tb: TracebackType | None
    ) -> None:
        if not self._subscription_id:
            return

        self._logger.debug("%s: Unsubscribing from change events", self)

        try:
            self._device.unsubscribe_event(self._subscription_id)
        except Exception:
            self._logger.warning(
                "%s: Exception occurred when trying to unsubscribe from change events",
                self,
                exc_info=True,
            )

    def _handle_event(self, event: tango.EventData) -> None:
        try:
            self._logger.debug("%s: Received change event for attribute", self)
            if event.err:
                self._logger.warning("%s: Received failed change event, error stack: %s", self, event.errors)
                return

            value = event.attr_value
            if isinstance(value, tango.DeviceAttribute):
                hint, value = get_enum_hint(value), value.value

            if isinstance(value, Iterable) and not isinstance(value, str):
                hint_text = [hint(x) for x in value]
            else:
                hint_text = hint(value)

            self._logger.debug("%s: Value changed to `%s`%s", self, value, hint_text)

            self._handler(value)
        except Exception:
            self._logger.exception("%s: Error while handling change event", self, exc_info=True)

    def __repr__(self) -> str:
        return f"TangoAttribute({self._device.name()}/{self._attribute})"
