"""Module that provides utilities to work with long-running commands in TANGO devices."""

import json
import logging
import queue
from types import TracebackType
from typing import Any

import tango
from ska_control_model import ResultCode, TaskStatus

from ska_low_csp_test.tango.events import AttributeChangeEventSubscription

__all__ = ["LongRunningCommandTracker"]


class LongRunningCommandTracker:
    """Helper class to track the execution of long-running commands in TANGO devices and wait for them to complete.

    This class monitors the ``longRunningCommandStatus`` and ``longRunningCommandResult`` attributes of a
    :py:class:`tango.DeviceProxy` to allow blocking calls to long-running TANGO commands.

    For details on long-running commands in SKA TANGO devices,
    refer to :doc:`ska-tango-base:guide/long_running_command`.
    """

    def __init__(self, device: tango.DeviceProxy, command_id: str, logger: logging.Logger | None = None) -> None:
        self._logger = logger or logging.getLogger(__name__)
        self._device = device
        self._command_id = command_id
        self._status_events = queue.LifoQueue[TaskStatus]()
        self._result_events = queue.LifoQueue[Any]()

        self._subscriptions = [
            AttributeChangeEventSubscription(device, "longRunningCommandStatus", self._handle_status_event, self._logger),
            AttributeChangeEventSubscription(device, "longRunningCommandResult", self._handle_result_event, self._logger),
        ]

    def __enter__(self) -> "LongRunningCommandTracker":
        self._logger.debug("%s: Start tracking long-running command", self)
        for subscription in self._subscriptions:
            subscription.__enter__()
        return self

    def __exit__(
        self, exc_type: type[BaseException] | None, exc_value: BaseException | None, exc_tb: TracebackType | None
    ) -> None:
        self._logger.debug("%s: Stop tracking long-running command", self)
        for subscription in self._subscriptions:
            subscription.__exit__(exc_type, exc_value, exc_tb)

    def _handle_status_event(self, value: tuple[str, ...]) -> None:
        # Long-running command status is a tuple in the form of (command1_id, status_1, command2_id, status_2, ...)
        # Example:
        # (
        #     '1704375684.0971081_48889183214313_AssignResources',
        #     'COMPLETED',
        #     '1704375685.0774016_178074327124287_ReleaseAllResources',
        #     'QUEUED'
        # )
        for command_id, status_name in zip(value[::2], value[1::2]):
            if command_id == self._command_id:
                status = TaskStatus[status_name]
                self._logger.debug("%s: Status changed to %s", self, status)
                self._status_events.put(status)

    def _handle_result_event(self, value: tuple[str, str]) -> None:
        # Long-running command result is a tuple in the form of (command_id, result_json)
        # Example: ('1704374623.109921_248250154929430_AssignResources', '[0, "assign completed  1/1"]')
        command_id, result_json = value
        if command_id != self._command_id:
            return

        result = json.loads(result_json)
        self._logger.debug("%s: Result changed to %s", self, result)
        self._result_events.put(result)

    def wait_for_command(self, timeout_s: float) -> ResultCode:
        """Wait for the long-running command to finish.

        Upon invocation, the calling thread is blocked until the long-running command status reaches one
        of its final states ``TaskStatus.COMPLETED``, ``TaskStatus.ABORTED``, ``TaskStatus.FAILED`` or
        ``TaskStatus.REJECTED``, or a timeout occurs.

        After the command finishes, the corresponding :py:class:`ResultCode` is returned.

        :param timeout_s: The time to wait for the command to finish before giving up, in seconds.
        :return: The :py:class:`ResultCode` of the command
        :raises RuntimeError: An exception is raised when the timeout is reached.
        """
        self._logger.debug("%s: Start waiting for long-running command to finish", self)
        try:
            while True:
                status = self._status_events.get(timeout=timeout_s)
                self._logger.debug("%s: Current status: %s", self, status)

                if status in {TaskStatus.COMPLETED, TaskStatus.ABORTED, TaskStatus.FAILED, TaskStatus.REJECTED}:
                    break

        except queue.Empty:
            # pylint: disable-next=raise-missing-from
            raise TimeoutError(f"Timed out waiting for long-running command to complete: {self._command_id}")

        result = self._result_events.get(timeout=timeout_s)

        if status != TaskStatus.COMPLETED:
            self._logger.warning("%s: Command did not complete. Status: %s, result: %s", self, status.name, result)

        self._logger.debug("%s: Done waiting for long-running command to finish", self)
        return result

    def __repr__(self) -> str:
        return f"LongRunningCommandTracker(commandId={self._command_id})"
