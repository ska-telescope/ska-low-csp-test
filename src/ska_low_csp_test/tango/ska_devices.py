"""Module that provides base classes for SKA TANGO devices."""

import json
import time
from typing import Any, Callable, Mapping

from ska_control_model import (
    AdminMode,
    ControlMode,
    HealthState,
    LoggingLevel,
    ObsMode,
    ObsState,
    ResultCode,
    SimulationMode,
    TestMode,
)

from ska_low_csp_test.tango.long_running_commands import LongRunningCommandTracker
from ska_low_csp_test.tango.tango import TangoDevice

__all__ = ["SKABaseDevice", "SKASubarrayDevice"]


class SKABaseDevice(TangoDevice):
    """An SKA TANGO device.

    This class is modeled after the ``SKABaseDevice`` in ``ska-tango-base``,
    see :doc:`ska-tango-base:api/base/base_device`.
    """

    def _attributes_to_monitor(self) -> dict[str, Callable[[Any], Any]]:
        return {
            **super()._attributes_to_monitor(),
            "adminMode": AdminMode,
            "healthState": HealthState,
        }

    def long_running_command(
        self,
        command: str,
        param: Any | None = None,
        timeout_s: float | None = None,
        wait_for_completion=True,
    ) -> None:
        """Invoke a long-running command on the TANGO device and wait for it to complete.

        :param command: The name of the long-running command to invoke.
        :param param: The parameter for the long-running command, if any.
        :param timeout_s: The time to wait for the long-running command to complete.
            If no timeout is given, it uses a default value.
        :param wait_for_completion: Whether to wait for the long-running command to complete.
            If set to ``False``, this method will return as soon as the long-running command is started/queued by the device.
        :raises RuntimeError: If the :py:class:`ResultCode` of the long-running command indicates that the command failed.
        """
        self._log_message_truncated("%s: Calling long-running command '%s' with parameter: %s", self, command, param)

        [[result], [msg_or_command_id]] = self._device.command_inout(command, param)
        result = ResultCode(result)

        if result not in [ResultCode.STARTED, ResultCode.QUEUED]:
            raise RuntimeError(
                f"{self}: Long-running command '{command}' failed with result {result.name}. Message = {msg_or_command_id}"
            )

        if not wait_for_completion:
            self._logger.debug("Not waiting for long-running-command '%s' to complete", command)
            return

        with LongRunningCommandTracker(self._device, msg_or_command_id, self._logger) as tracker:
            result = tracker.wait_for_command(timeout_s=timeout_s or self._command_timeout_s)

        # TODO: The current implementation of the LowCspSubarray device notifies us that the LRC completes before
        # updating its internal state, instead of the other way around. This is because the state change is implemented
        # as an internal callback that runs on completion of the LRC.
        #
        # I'm not sure whether _all_ TANGO devices in SKAO are built that way, so just to be safe we wait a little
        # longer after the LRC completes so that the device has time to react to it before we do.
        time.sleep(0.1)
        self._handle_long_running_command_result(command, result)

    def _handle_long_running_command_result(self, command_name: str, result: Any):  # pylint:disable=unused-argument
        self._logger.warning(
            "Not validating result of long-running-command '%s': method not overridden by subclass", command_name
        )

    def init(self) -> None:
        """Call Init command on remote device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.command("Init")

    def on(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call On command on remote device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("On", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def off(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call Off command on remote device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("Off", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def reset(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call Reset command on remote device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("Reset", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def standby(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call Standby command on remote device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("Standby", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    @property
    def health_state(self) -> HealthState:
        """Health state of the device."""
        return self.read_attribute("healthState", HealthState)

    @property
    def version_id(self) -> str:
        """Software version of the device."""
        return self.read_attribute("versionId", str)

    @property
    def admin_mode(self) -> AdminMode:
        """Admin mode of the device."""
        return self.read_attribute("adminMode", AdminMode)

    @admin_mode.setter
    def admin_mode(self, value: AdminMode) -> None:
        self.write_attribute("adminMode", AdminMode, value)

    @property
    def control_mode(self) -> ControlMode:
        """Control mode of the device."""
        return self.read_attribute("controlMode", ControlMode)

    @control_mode.setter
    def control_mode(self, value: ControlMode) -> None:
        self.write_attribute("controlMode", ControlMode, value)

    @property
    def simulation_mode(self) -> SimulationMode:
        """Simulation mode of the device."""
        return self.read_attribute("simulationMode", SimulationMode)

    @simulation_mode.setter
    def simulation_mode(self, value: SimulationMode) -> None:
        self.write_attribute("simulationMode", SimulationMode, value)

    @property
    def test_mode(self) -> TestMode:
        """Test mode of the device."""
        return self.read_attribute("testMode", TestMode)

    @test_mode.setter
    def test_mode(self, value: TestMode) -> None:
        self.write_attribute("testMode", TestMode, value)

    @property
    def logging_level(self) -> LoggingLevel:
        """Logging level of the device."""
        return self.read_attribute("loggingLevel", LoggingLevel)

    @logging_level.setter
    def logging_level(self, value: LoggingLevel) -> None:
        self.write_attribute("loggingLevel", LoggingLevel, value)


class SKASubarrayDevice(SKABaseDevice):
    """An SKA subarray TANGO device.

    This class is modeled after the ``SKASubarray`` in ``ska-tango-base``,
    see :doc:`ska-tango-base:api/subarray/subarray_device`.
    """

    def _attributes_to_monitor(self) -> dict[str, Callable[[Any], Any]]:
        return {
            **super()._attributes_to_monitor(),
            "obsMode": ObsMode,
            "obsState": ObsState,
        }

    def abort(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call Abort command on the subarray device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("Abort", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def assign_resources(self, schema: Mapping[str, Any], timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call AssignResources command on the subarray device.

        :schema: Dictionary containing the resources to assign.
        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command(
            "AssignResources",
            json.dumps(schema),
            timeout_s=timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def configure(self, schema: Mapping[str, Any], timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call Configure command on the subarray device.

        :param schema: Dictionary containing the configuration.
        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command(
            "Configure",
            json.dumps(schema),
            timeout_s=timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def end(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call End command on the subarray device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("End", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def end_scan(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call EndScan command on the subarray device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("EndScan", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def obs_reset(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call ObsReset command on the subarray device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("ObsReset", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def release_all_resources(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call ReleaseAllResources command on the subarray device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("ReleaseAllResources", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def release_resources(self, schema: Mapping[str, Any], timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call ReleaseResources command on the subarray device.

        :param schema: Dictionary containing the resources to release.
        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command(
            "ReleaseResources",
            json.dumps(schema),
            timeout_s=timeout_s,
            wait_for_completion=wait_for_completion,
        )

    def restart(self, timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call Restart command on the subarray device.

        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("Restart", timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    def scan(self, schema: Mapping[str, Any], timeout_s: float | None = None, wait_for_completion=True) -> None:
        """Call Scan command on the subarray device.

        :param schema: Dictionary containing the subarray scan configuration
        :param timeout_s: The time to wait for the long-running command to complete, or ``None`` to use the default.
        """
        self.long_running_command("Scan", json.dumps(schema), timeout_s=timeout_s, wait_for_completion=wait_for_completion)

    @property
    def obs_mode(self) -> ObsMode:
        """The observation mode of the subarray."""
        return self.read_attribute("obsMode", ObsMode)

    @property
    def obs_state(self) -> ObsState:
        """The observation state of the subarray."""
        return self.read_attribute("obsState", ObsState)
