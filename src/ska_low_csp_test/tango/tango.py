"""Module that provides helper utilities to work with TANGO devices."""

import functools
import logging
from collections.abc import Iterable
from types import TracebackType
from typing import Any, Callable, Type, TypedDict, TypeVar

import backoff
import tango

from ska_low_csp_test.synchronisation import wait_for_condition
from ska_low_csp_test.tango.events import AttributeChangeEventSubscription

__all__ = ["TangoDevice", "TangoContext"]

AttributeT = TypeVar("AttributeT")


class TangoDevice:
    """Testing wrapper class around a TANGO device proxy."""

    def __init__(
        self,
        fqdn: str,
        device: tango.DeviceProxy,
        command_timeout_s: float = 30,
        logger: logging.Logger | None = None,
    ) -> None:
        self._fqdn = fqdn
        self._command_timeout_s = command_timeout_s
        self._logger = logger or logging.getLogger(__name__)
        self._device = device
        self._device.set_timeout_millis(int(command_timeout_s * 1000))

        def _log_attribute_change(attr_name: str, attr_formatter: Callable[[Any], Any], attr_value: Any) -> None:
            if isinstance(attr_value, Iterable) and not isinstance(attr_value, str):
                formatted_attr = [attr_formatter(x) for x in attr_value]
            else:
                formatted_attr = attr_formatter(attr_value)
            self._log_message_truncated("%s: Attribute %s changed to %s", self, attr_name, formatted_attr)

        self._attr_monitors = [
            AttributeChangeEventSubscription(
                device, attr_name, functools.partial(_log_attribute_change, attr_name, attr_formatter)
            )
            for attr_name, attr_formatter in self._attributes_to_monitor().items()
        ]

    def _attributes_to_monitor(self) -> dict[str, Callable[[Any], Any]]:
        return {
            "state": lambda s: s,
        }

    def read_attribute(self, attr_name: str, attr_type: Type[AttributeT]) -> AttributeT:
        """Read the value of the given attribute.

        :param attribute: The name of the attribute to read.
        :return: The attribute value.
        """
        value = attr_type(self._device.read_attribute(attr_name).value)
        self._logger.debug("%s: Attribute %s = %s", self, attr_name, value)
        return value

    def wait_for_attribute_value(
        self,
        attr_name: str,
        attr_type: Type[AttributeT],
        desired_value: AttributeT,
        timeout_s: float = 60,
    ) -> None:
        """Wait for an attribute to have the desired value.

        This function blocks the calling thread until the given attribute reaches the desired value
        or until a timeout occurs.

        :param attribute: The name of the attribute to read.
        :param desired_value: The desired value of the attribute.
        :param timeout_s: Number of seconds to wait before giving up.
        """
        self._logger.debug("%s: Waiting for %s to become %s", self, attr_name, desired_value)
        wait_for_condition(
            lambda: self.read_attribute(attr_name, attr_type) == desired_value,
            timeout_s=timeout_s,
        )

    def write_attribute(self, attr_name: str, attr_type: Type[AttributeT], value: AttributeT) -> None:
        """Update the value of the given attribute.

        :param attribute: The name of the attribute to write.
        :param value: The new value of the attribute.
        """
        self._log_message_truncated("%s: Setting attribute %s = %s", self, attr_name, value)
        self._device.write_attribute(attr_name, value)
        self.wait_for_attribute_value(attr_name, attr_type, value)

    def command(self, command: str, param: Any | None = None) -> None:
        """Call the given command on the device."""
        self._log_message_truncated("%s: Calling command '%s' with parameter: %s", self, command, param)
        result = self._device.command_inout(command, param)
        self._logger.debug("%s: Command '%s' finished. Result = %s", self, command, result)

    def ping(self) -> None:
        """Ping the TANGO device."""
        self._device.ping()

    def _log_message_truncated(self, message: str, *args, max_length: int = 400) -> None:
        """Log message % args, truncate when exceeding given max length

        This function will log with severity INFO, when the message is truncated
        it will also log the original message with severity DEBUG

        :param message: The message to be trunctated and logged.
        :param args: Argument(s) to be placed in the message.
        :param max_length: Maximum number of characters allowed before the message is truncated.
        """
        formatted_message = message % args
        if len(formatted_message) > max_length:
            self._logger.info(formatted_message[: max_length - 3] + "...")
            self._logger.debug(message, *args)
        else:
            self._logger.info(message, *args)

    @property
    def state(self) -> tango.DevState:
        """Read the state of the TANGO device."""
        return self._device.state()

    @property
    def device(self) -> tango.DeviceProxy:
        """Get the TANGO device proxy."""
        return self._device

    @property
    def fqdn(self) -> str:
        """Get the FQDN of the TANGO device."""
        return self._fqdn

    def __enter__(self) -> "TangoDevice":
        self._logger.debug("%s: Start monitoring attributes: %s", self, self._attributes_to_monitor().keys())
        for monitor in self._attr_monitors:
            monitor.__enter__()
        return self

    def __exit__(
        self, exc_type: type[BaseException] | None, exc_value: BaseException | None, exc_tb: TracebackType | None
    ) -> None:
        self._logger.debug("%s: Stop monitoring attributes", self)
        for monitor in self._attr_monitors:
            monitor.__exit__(exc_type, exc_value, exc_tb)

    def __repr__(self) -> str:
        return f"TangoDevice({self.fqdn})"


DeviceTypeT = TypeVar("DeviceTypeT", bound=TangoDevice)
BackoffDetailsType = TypedDict("BackoffDetailsType", {"args": list, "elapsed": float})


class TangoContext:
    """Helper class to interact with a running TANGO system."""

    def __init__(
        self,
        tango_db_host: tuple[str, int] | None = None,
        logger: logging.Logger | None = None,
        custom_proxy_supplier: Callable[..., tango.DeviceProxy] | None = None,
    ) -> None:
        self._device_fqdn_prefix = ""
        self._logger = logger or logging.getLogger(__name__)
        self._proxy_supplier = custom_proxy_supplier or tango.DeviceProxy
        self._raw_proxies: dict[str, tango.DeviceProxy] = {}

        if tango_db_host:
            host, port = tango_db_host
            self._logger.debug("Using custom TANGO host %s:%d", host, port)
            self._device_fqdn_prefix = f"tango://{host}:{port}/"
            self._tango_db = tango.Database(host, port)
        else:
            self._logger.debug("Using TANGO_HOST environment variable")
            self._tango_db = tango.Database()

    def get_device_fqdns(self, device_class: str) -> list[str]:
        """Get the FQDNs of all TANGO devices with the given device class.

        :param device_class: The name of the device class for the TANGO device.
        :return: A list of FQDNs.
        """
        fqdns = self._tango_db.get_device_exported_for_class(device_class).value_string
        self._logger.debug("Found %d TANGO devices with class %s: %s", len(fqdns), device_class, fqdns)
        return fqdns

    def get_device(
        self,
        fqdn: str,
        device_type: Type[DeviceTypeT],
        green_mode: tango.GreenMode = tango.GreenMode.Synchronous,
    ) -> DeviceTypeT:
        """Get a device proxy to the TANGO device with the given fqdn.

        This will return an existing proxy if already created, else it will create a :py:class:`tango.DeviceProxy`
        instance and then wrap it in the given device type.

        :param fqdn: The FQDN of the TANGO device that the proxy is for.
        :param device_type: The type of the device to get.
        :param green_mode: The TANGO green mode, the default is ``GreenMode.Synchronous``.
        """

        def _on_giveup_connect(details: BackoffDetailsType) -> None:
            fqdn = details["args"][1]
            elapsed = details["elapsed"]
            self._logger.warning("Gave up trying to connect to device %s after %d seconds.", fqdn, elapsed)

        @backoff.on_exception(
            backoff.expo,
            tango.DevFailed,
            on_giveup=_on_giveup_connect,  # type: ignore
            factor=0.1,
            max_time=120.0,
        )
        def _get_proxy() -> tango.DeviceProxy:
            return self._proxy_supplier(self._device_fqdn_prefix + fqdn, green_mode=green_mode)

        if fqdn not in self._raw_proxies:
            self._logger.debug("Creating new device proxy for %s", fqdn)
            try:
                self._raw_proxies[fqdn] = _get_proxy()
            except Exception:
                self._raw_proxies[fqdn] = self._proxy_supplier(fqdn)

        return device_type(fqdn=fqdn, logger=self._logger, device=self._raw_proxies[fqdn])
