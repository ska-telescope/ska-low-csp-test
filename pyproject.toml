[tool.poetry]
name = "ska-low-csp-test"
version = "0.1.0"
description = "L2 tests for LOW-CSP"
authors = [
    "Sander Ploegsma <sander.ploegsma@topic.nl>",
    "Deneys Maartens <deneys.maartens@topic.nl>",
    "Bohdan Dovbysh <bohdan.dovbysh@topic.nl>",
]
license = "BSD-3-Clause"
readme = "README.md"

[tool.poetry.scripts]
psr-data-unpacker = "ska_low_csp_test.scripts.psr_data_unpacker:cli"
sps-data-analyser = "ska_low_csp_test.scripts.sps_data_analyser:main"
sps-data-unpacker = "ska_low_csp_test.scripts.sps_data_unpacker:cli"
sps3-data-unpacker = "ska_low_csp_test.scripts.sps3_data_unpacker:cli"
vis-data-unpacker = "ska_low_csp_test.scripts.vis_data_unpacker:cli"

[[tool.poetry.source]]
name = "skao"
url = "https://artefact.skao.int/repository/pypi-internal/simple"
priority = "primary"

[[tool.poetry.source]]
name = "PyPI"
priority = "supplemental"

[tool.poetry.dependencies]
allure-python-commons = "^2.13.2"
astropy = "^6.0.1"
backoff = "^2.2.1"
bitstring = "^4.1.4"
matplotlib = "^3.8.2"
numpy = "^1.26.3"
pandas = "^2.1.4"
pytango = "^9.5.0"
pytest = "^7.4.3"
pytest-check = "^2.3.1"
pytest-jira-xray = "^0.9.0"
pytest-repeat = "^0.9.3"
python = "^3.10"
scapy = "^2.5.0"
ska-control-model = "^1.2.0"
spead2 = "^4.1.1"
typing-extensions = "^4.10.0"
xarray = "^2024.3.0"
zarr = "^2.17.2"
dpkt = "^1.9.8"
pyyaml = "^6.0.2"

[tool.poetry.group.docs.dependencies]
ska-ser-sphinx-theme = "^0.1.2"
sphinx-autoapi = "^3.0.0"

[tool.poetry.group.test.dependencies]
allure-pytest = "^2.13.2"
coverage = "^7.3.2"
hypothesis = "^6.92.1"
nbmake = "^1.4.6"
pytest-cov = "^4.1.0"
pytest-dependency = "^0.6.0"
pytest-exit-code = "^0.1.0"
pytest-order = "^1.2.0"
pytest-randomly = "^3.15.0"

[tool.poetry.group.dev.dependencies]
black = "^23.12.0"
flake8 = "^6.1.0"
isort = "^5.13.2"
jupyter = "^1.0.0"
nbqa = "^1.7.1"
pylint = "^3.0.3"
pylint-junit = "^0.3.4"

[build-system]
requires = ["poetry-core>=1.8"]
build-backend = "poetry.core.masonry.api"

[tool.black]
line-length = 127

[tool.flake8]
max-line-length = 127

[tool.isort]
profile = "black"
line_length = 127

[tool.pylint.format]
max-line-length = 127

[tool.pylint.messages_control]
disable = [
    "fixme",
    "too-few-public-methods",
    "broad-exception-caught",
    "duplicate-code",
    "too-many-ancestors",     # https://github.com/pylint-dev/pylint/issues/9101, will be fixed in 3.1.0
]

[tool.pylint.typecheck]
ignored-modules = ["astropy.units"]

[tool.pytest.ini_options]
log_date_format = "%Y-%m-%d %H:%M:%S"
log_format = "%(asctime)s.%(msecs)03d :: %(levelname)-8s :: %(message)s"
log_file_format = "%(asctime)s.%(msecs)03d :: %(levelname)-8s :: %(message)s"
