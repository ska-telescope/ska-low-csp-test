# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) command to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

DOCS_SPHINXOPTS ?= $(SPHINXOPTS)

-include .make/base.mk
-include .make/python.mk
-include PrivateRules.mak

# Override base vars
PROJECT = ska-low-csp-test

# Override vars for python targets
PYTHON_LINE_LENGTH = 127
PYTHON_TEST_FILE = src
PYTHON_SWITCHES_FOR_PYLINT = --disable=too-many-positional-arguments
PYTHON_VARS_AFTER_PYTEST = --randomly-dont-reset-seed

# Override vars for notebook targets
NOTEBOOK_LINT_TARGET = notebooks
NOTEBOOK_SWITCHES_FOR_PYLINT = --disable=missing-module-docstring,import-error,too-many-positional-arguments

# Custom vars
GIT_REVISION=$(shell git rev-parse HEAD)

JIRA_URL ?= https://jira.skatelescope.org
JIRA_USERNAME ?=
JIRA_PASSWORD ?=

PYTEST_LOG_LEVEL ?= INFO
PYTEST_LOG_CLI_LEVEL ?= INFO
PYTEST_OPTIONS ?=

XRAY_PUBLISH ?= false
XRAY_TEST_EXECUTION ?=
XRAY_TEST_PLAN ?=
PYTEST_XRAY_OPTIONS = --jira-xray

TANGO_HOST ?= ska-low-csp-databaseds.ska-low-csp-dev.svc.cluster.local:10000
TEST_CONFIG_PATH ?= config/ds-psi-dev.json

ifeq ($(REGRESSION_TEST_RUNNER), ska-digital-signal-psi-ci)
	TANGO_HOST = ska-low-csp-databaseds.ska-low-csp-ci.svc.cluster.local:10000
	TEST_CONFIG_PATH = config/ds-psi-ci.json
endif

TESTS_TO_RUN ?=

TEST_ENV = TANGO_HOST=$(TANGO_HOST) \
	PYTHONPATH=./src:/app/src:/app/src/ska_low_csp_test/ \
	XRAY_API_BASE_URL=$(JIRA_URL) \
	XRAY_API_USER=$(JIRA_USERNAME) \
	XRAY_API_PASSWORD=$(JIRA_PASSWORD) \
	XRAY_EXECUTION_REVISION=$(GIT_REVISION) \
	TEST_CONFIG_PATH=$(TEST_CONFIG_PATH)

ifneq ($(strip $(XRAY_PUBLISH)),true)
	PYTEST_XRAY_OPTIONS += --xraypath build/tests/xray.json
endif

ifneq ($(strip $(XRAY_TEST_EXECUTION)),)
	PYTEST_XRAY_OPTIONS += --execution $(XRAY_TEST_EXECUTION)
endif

ifneq ($(strip $(XRAY_TEST_PLAN)),)
	PYTEST_XRAY_OPTIONS += --testplan $(XRAY_TEST_PLAN)
endif

test:  ## Run tests with pytest
	$(TEST_ENV) pytest \
	--junitxml=build/tests/pytest-junit.xml \
	--alluredir=build/tests/allure \
	--allure-link-pattern="tms:https://jira.skatelescope.org/browse/{}" \
	--allure-link-pattern="issue:https://jira.skatelescope.org/browse/{}" \
	--log-level=$(PYTEST_LOG_LEVEL) \
	--log-cli-level=$(PYTEST_LOG_CLI_LEVEL) \
	--log-file-level=DEBUG \
	--log-file=build/tests/debug.log \
	--show-capture=no \
	--randomly-dont-reset-seed \
	$(PYTEST_XRAY_OPTIONS) \
	$(PYTEST_OPTIONS) \
	$(TEST_DIR) \
	$(if $(TESTS_TO_RUN),-k $(TESTS_TO_RUN)); \
	echo $$? > build/tests/status
	@echo "Pytest exited with code: $$(cat build/tests/status)"
