# ska-low-csp-test

## Configuration

- Use a configuration.json file from /config directory or use existing one in your system.
- Run tests: make test TEST_CONFIG_PATH=<path_to_your_config_file>

## Support

For questions or remarks related to this project, contact the TOPIC Team:

- Slack: [#team-topic](https://skao.slack.com/archives/C05CZKCM22U)
- [Confluence](https://confluence.skatelescope.org/display/SE/TOPIC+Team)
