"""Global pytest configuration."""

import logging

import pytest

pytest.register_assert_rewrite(
    "ska_low_csp_test.sut",
    "ska_low_csp_test.tmc.sut",
    "ska_low_csp_test.pst.output",
)

pytest_plugins = [
    "ska_low_csp_test.reporting.allure_environment_plugin",
    "ska_low_csp_test.reporting.allure_executor_plugin",
    "ska_low_csp_test.reporting.test_evidence_plugin",
]


def pytest_exception_interact(call: pytest.CallInfo, report: pytest.TestReport) -> None:
    """
    Hook to log errors in the debug logs.

    Omits stacktrace for ``AssertionError`` instances.
    """
    if call.excinfo.type is AssertionError:
        logging.error("Assertion failed:\n%s", report.longreprtext)
    else:
        logging.error(
            "An unexpected error occurred:\n%s",
            report.longreprtext,
            exc_info=call.excinfo.value,
        )
