{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ITC.L.AA0.5.NB Recovery from FSP failure during scan\n",
    "\n",
    "This notebook implements the following JAMA test case: [ITC.L.AA0.5.NB Recovery from FSP failure during scan](https://skaoffice.jamacloud.com/perspective.req#/testCases/1137303?projectId=335).\n",
    "\n",
    "In order to run this notebook, make sure you meet the following requirements:\n",
    "\n",
    "- LOW-CSP is deployed to a Kubernetes cluster, integrated with a P4 switch and at least two Alveos used as LOW-CBF processors\n",
    "- You are able to SSH into the Supermicro and have `sudo` rights\n",
    "- The system running the notebook is able to connect to the Kubernetes cluster (either by running via BinderHub or on a system connected to Digital Signal PSI VPN)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notebook Setup\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install jupyter-ui-poll tabulate"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "import os\n",
    "import sys\n",
    "import time\n",
    "from datetime import datetime\n",
    "\n",
    "import ipywidgets as widgets\n",
    "import tabulate\n",
    "from IPython.display import Markdown, display\n",
    "from jupyter_ui_poll import ui_events\n",
    "from ska_control_model import ObsState\n",
    "\n",
    "from ska_low_csp_test.cbf.devices import LowCbfDevices\n",
    "from ska_low_csp_test.cbf.helpers import get_processors_for_fsp_ids\n",
    "from ska_low_csp_test.lmc.devices import LowCspDevices\n",
    "from ska_low_csp_test.pst.devices import PstDevices\n",
    "from ska_low_csp_test.sut import SystemUnderTest\n",
    "from ska_low_csp_test.tango.tango import TangoContext"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### TANGO / Kubernetes settings\n",
    "\n",
    "Change the settings below to point to the correct Kubernetes namespace and TANGO database.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "KUBE_CLUSTER_DOMAIN = \"cluster.local\"\n",
    "KUBE_NAMESPACE = \"ska-low-csp-dev\"\n",
    "TANGO_DATABASEDS_NAME = \"ska-low-csp-databaseds\"\n",
    "TANGO_DATABASEDS_PORT = 10000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notebook settings\n",
    "\n",
    "The following constants change the behavior of the notebook, customize at will.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "FSP_ID_a = 1  # FSP ID used for the first configuration  # pylint: disable=invalid-name\n",
    "FSP_ID_b = 2  # FSP ID used for the second configuration  # pylint: disable=invalid-name\n",
    "SUBARRAY_ID = 1\n",
    "VIS_FW_VERSION = \"vis:0.1.2\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Logging setup\n",
    "\n",
    "This will ensure everything will log to cell outputs.\n",
    "IPython defaults to logging to `stderr` but the cells need to print to `stdout`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logging.basicConfig(\n",
    "    format=\"%(asctime)s | %(levelname)s : %(message)s\",\n",
    "    level=logging.INFO,\n",
    "    stream=sys.stdout,\n",
    ")\n",
    "\n",
    "logger = logging.getLogger()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialize System Under Test\n",
    "\n",
    "This section brings the system under test into a default state.\n",
    "The selected subarray is brought to an `EMPTY` state if needed.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tango_context = TangoContext(\n",
    "    tango_db_host=(f\"{TANGO_DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.{KUBE_CLUSTER_DOMAIN}\", TANGO_DATABASEDS_PORT),\n",
    ")\n",
    "low_csp_devices = LowCspDevices(tango_context)\n",
    "low_cbf_devices = LowCbfDevices(tango_context)\n",
    "pst_devices = PstDevices(tango_context)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sut = SystemUnderTest(\n",
    "    low_csp_devices=low_csp_devices,\n",
    "    low_cbf_devices=low_cbf_devices,\n",
    "    pst_devices=pst_devices,\n",
    ")\n",
    "sut.reset()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray = sut.subarray(SUBARRAY_ID)\n",
    "subarray.reset()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Start Scan on Subarray\n",
    "\n",
    "This section assigns resources to the selected subarray and applies the configuration as described in the JAMA test case.\n",
    "\n",
    "After the last cell finishes running the system should be in a scanning state.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.assign_resources(\n",
    "    {\n",
    "        \"interface\": \"https://schema.skao.int/ska-low-csp-assignresources/3.2\",\n",
    "        \"common\": {\n",
    "            \"subarray_id\": SUBARRAY_ID,\n",
    "        },\n",
    "        \"lowcbf\": {},\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.configure(\n",
    "    {\n",
    "        \"interface\": \"https://schema.skao.int/ska-low-csp-configure/3.2\",\n",
    "        \"subarray\": {\n",
    "            \"subarray_name\": \"science period 23\",\n",
    "        },\n",
    "        \"common\": {\n",
    "            \"config_id\": str(datetime.now()),\n",
    "            \"subarray_id\": SUBARRAY_ID,\n",
    "        },\n",
    "        \"lowcbf\": {\n",
    "            \"stations\": {\n",
    "                \"stns\": [(345, 1), (350, 1), (352, 1), (431, 1)],\n",
    "                \"stn_beams\": [\n",
    "                    {\n",
    "                        \"beam_id\": 1,\n",
    "                        \"freq_ids\": list(range(100, 108)),\n",
    "                        \"delay_poly\": f\"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b01\",\n",
    "                    },\n",
    "                ],\n",
    "            },\n",
    "            \"vis\": {\n",
    "                \"fsp\": {\n",
    "                    \"firmware\": VIS_FW_VERSION,\n",
    "                    \"fsp_ids\": [FSP_ID_a],\n",
    "                },\n",
    "                \"stn_beams\": [\n",
    "                    {\n",
    "                        \"stn_beam_id\": 1,\n",
    "                        \"host\": [(0, \"192.168.2.1\")],\n",
    "                        \"mac\": [(0, \"0c-42-a1-9c-a2-1b\")],\n",
    "                        \"port\": [(0, 20000, 1)],\n",
    "                        \"integration_ms\": 849,\n",
    "                    },\n",
    "                ],\n",
    "            },\n",
    "        },\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.scan(\n",
    "    {\n",
    "        \"interface\": \"https://schema.skao.int/ska-low-csp-scan/3.2\",\n",
    "        \"common\": {\n",
    "            \"subarray_id\": SUBARRAY_ID,\n",
    "        },\n",
    "        \"lowcbf\": {\n",
    "            \"scan_id\": 1,\n",
    "        },\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulate FSP Failure\n",
    "\n",
    "In this section we simulate an FSP failure by manually resetting the underlying Alveo FPGAs using the Xilinx command-line utility.\n",
    "After the reset is performed, the LOW-CBF Processors are expected to report a `DEGRADED` health state, which should flow upwards to the subarray.\n",
    "\n",
    "### Instructions\n",
    "\n",
    "Open an SSH connection to the Supermicro, and use the command below to identify the Alveo FPGAs.\n",
    "It will show the PCI address and corresponding serial number for each FPGA.\n",
    "\n",
    "```sh\n",
    "ubuntu@clp-k8s-fpga-worker-1:~$ sudo ${XILINX_XRT}/bin/xbmgmt examine --batch -r all 2>&1 \\\n",
    "                                | sed -e '/xilinx/s/^  .\\(.\\{12\\}\\).*/\\1/p;d' \\\n",
    "                                | xargs -n 1 sudo ${XILINX_XRT}/bin/xbmgmt examine -d \\\n",
    "                                | grep '^\\[\\|Serial' \\\n",
    "                                | paste - - \\\n",
    "                                | sed -e 's/ :.*://'\n",
    "[0000:d6:00.0] XFL1IYUNES2E\n",
    "[0000:d1:00.0] XFL1EG4H5YXY\n",
    "[0000:ce:00.0] XFL1XXQM0FKW\n",
    "[0000:57:00.0] XFL1SZ2IUU2I\n",
    "[0000:56:00.0] XFL1BB1SEWXK\n",
    "[0000:52:00.0] XFL121Y1KXMA\n",
    "```\n",
    "\n",
    "To perform a reset on an FPGA, note its PCI address and use the following command:\n",
    "\n",
    "```sh\n",
    "ubuntu@clp-k8s-fpga-worker-1:~$ xbutil reset -d 0000:d6:00.0\n",
    "Performing 'HOT Reset' on\n",
    "  -[0000:d6:00.0]\n",
    "Are you sure you wish to proceed? [Y/n]:\n",
    "```\n",
    "\n",
    "The cells below will provide more detailed information of the exact steps to take.\n",
    "Please follow the instructions carefully.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identify Alveo FPGAs to reset\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fsp_a_processors = list(get_processors_for_fsp_ids(low_cbf_devices, fsp_ids=[FSP_ID_a]))\n",
    "for processor in fsp_a_processors:\n",
    "    logger.info(\"%s is part of FSP %d, serial number: %s\", processor.fqdn, FSP_ID_a, processor.serial_number)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reset Alveo FPGAs while monitoring SUT status\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ui_done = False  # pylint: disable=invalid-name\n",
    "out = widgets.Output()\n",
    "\n",
    "\n",
    "def _continue(target):\n",
    "    global ui_done  # pylint: disable=global-statement\n",
    "    ui_done = True\n",
    "    target.description = \"👍\"\n",
    "    target.disabled = True\n",
    "\n",
    "\n",
    "@out.capture(clear_output=True, wait=True)\n",
    "def _print_status():\n",
    "    def _table_row(device):\n",
    "        return [\n",
    "            device.fqdn,\n",
    "            str(device.state),\n",
    "            device.health_state.name,\n",
    "            None if \"subarray\" not in device.fqdn else device.obs_state.name,\n",
    "        ]\n",
    "\n",
    "    print(\n",
    "        tabulate.tabulate(\n",
    "            [\n",
    "                *[_table_row(device) for device in [low_csp_devices.controller(), *low_csp_devices.all_subarrays()]],\n",
    "                tabulate.SEPARATING_LINE,\n",
    "                *[\n",
    "                    _table_row(device)\n",
    "                    for device in [\n",
    "                        low_cbf_devices.controller(),\n",
    "                        *low_cbf_devices.all_subarrays(),\n",
    "                        low_cbf_devices.allocator(),\n",
    "                        low_cbf_devices.connector(),\n",
    "                        *low_cbf_devices.all_processors(),\n",
    "                    ]\n",
    "                ],\n",
    "            ],\n",
    "            headers=[\"Device\", \"State\", \"HealthState\", \"ObsState\"],\n",
    "        )\n",
    "    )\n",
    "\n",
    "\n",
    "btn = widgets.Button(description=\"Continue to next cell\", button_style=\"primary\")\n",
    "btn.on_click(_continue)\n",
    "\n",
    "display(\n",
    "    Markdown(\n",
    "        f\"\"\"\n",
    "The table below shows the current state of the system under test, and automatically refreshes every second.\n",
    "\n",
    "Take note of the state before we induce the failure,\n",
    "then perform a reset of the Alveo FPGAs that correspond to the following serial numbers:\n",
    "\n",
    "{os.linesep.join(\"- \" + p.serial_number for p in fsp_a_processors)}\n",
    "\n",
    "After performing the reset, keep an eye on the health states in the SUT.\n",
    "The following TANGO devices should react to the port going down:\n",
    "\n",
    "{os.linesep.join(\"- \" + p.fqdn for p in fsp_a_processors)}\n",
    "- {low_cbf_devices.subarray_by_id(SUBARRAY_ID).fqdn}\n",
    "- {low_csp_devices.subarray_by_id(SUBARRAY_ID).fqdn}\n",
    "\n",
    "Once you're satisfied, continue running the rest of this notebook by clicking the Continue button below.\n",
    "\"\"\"\n",
    "    )\n",
    ")\n",
    "display(out)\n",
    "display(btn)\n",
    "\n",
    "# Wait for user to press the button\n",
    "with ui_events() as poll:  # pylint: disable=not-context-manager\n",
    "    while ui_done is False:\n",
    "        poll(10)  # React to UI events (up to 10 at a time)\n",
    "        _print_status()\n",
    "        time.sleep(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Abort scan on Subarray\n",
    "\n",
    "This section returns the subarray into an `EMPTY` state, by invoking the `Abort` command, followed by the `Restart` command.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.abort()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.restart()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.verify_obs_state(ObsState.EMPTY)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reconfigure subarray with different FSP and perform another scan\n",
    "\n",
    "This section again assigns resources to the subarray and applies the same configuration as the previous attempt.\n",
    "The only difference in the configuration is the FSP identifier, which is now `FSP_ID_B`.\n",
    "\n",
    "It starts a scan on the subarray, and waits a predetermined amount of time before ending the scan.\n",
    "Finally, the subarray is deconfigured and released.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.assign_resources(\n",
    "    {\n",
    "        \"interface\": \"https://schema.skao.int/ska-low-csp-assignresources/3.2\",\n",
    "        \"common\": {\n",
    "            \"subarray_id\": SUBARRAY_ID,\n",
    "        },\n",
    "        \"lowcbf\": {},\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.configure(\n",
    "    {\n",
    "        \"interface\": \"https://schema.skao.int/ska-low-csp-configure/3.2\",\n",
    "        \"subarray\": {\n",
    "            \"subarray_name\": \"science period 23\",\n",
    "        },\n",
    "        \"common\": {\n",
    "            \"config_id\": str(datetime.now()),\n",
    "            \"subarray_id\": SUBARRAY_ID,\n",
    "        },\n",
    "        \"lowcbf\": {\n",
    "            \"stations\": {\n",
    "                \"stns\": [(345, 1), (350, 1), (352, 1), (431, 1)],\n",
    "                \"stn_beams\": [\n",
    "                    {\n",
    "                        \"beam_id\": 1,\n",
    "                        \"freq_ids\": list(range(100, 108)),\n",
    "                        \"delay_poly\": f\"low-cbf/delaypoly/0/delay_s{SUBARRAY_ID:02}_b01\",\n",
    "                    },\n",
    "                ],\n",
    "            },\n",
    "            \"vis\": {\n",
    "                \"fsp\": {\n",
    "                    \"firmware\": VIS_FW_VERSION,\n",
    "                    \"fsp_ids\": [FSP_ID_b],\n",
    "                },\n",
    "                \"stn_beams\": [\n",
    "                    {\n",
    "                        \"stn_beam_id\": 1,\n",
    "                        \"host\": [(0, \"192.168.2.1\")],\n",
    "                        \"mac\": [(0, \"0c-42-a1-9c-a2-1b\")],\n",
    "                        \"port\": [(0, 20000, 1)],\n",
    "                        \"integration_ms\": 849,\n",
    "                    },\n",
    "                ],\n",
    "            },\n",
    "        },\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.scan(\n",
    "    {\n",
    "        \"interface\": \"https://schema.skao.int/ska-low-csp-scan/3.2\",\n",
    "        \"common\": {\n",
    "            \"subarray_id\": SUBARRAY_ID,\n",
    "        },\n",
    "        \"lowcbf\": {\n",
    "            \"scan_id\": 1,\n",
    "        },\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SCAN_TIME_S = 60\n",
    "\n",
    "for _ in range(SCAN_TIME_S):\n",
    "    print(\".\", end=\"\")\n",
    "    time.sleep(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.end_scan()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.end()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray.release_all_resources()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recovering the system\n",
    "\n",
    "Resetting an FPGA while it is in use will render it unusable until the host is rebooted completely.\n",
    "To recover the system after this notebook is finished, SSH into the Supermicro and run:\n",
    "\n",
    "```sh\n",
    "ubuntu@clp-k8s-fpga-worker-1:~$ sudo reboot\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "Although an FPGA being reset via the command-line tooling is not a likely failure scenario for a production system,\n",
    "this test does highlight how the system reacts when an FPGA stops responding.\n",
    "\n",
    "Most importantly, it shows that an observation can be retried by switching to a different FSP.\n",
    "Since this is the ultimate goal of this test, we can consider it to have passed.\n",
    "\n",
    "However, when it comes to system observability, this test illustrates the following pain points.\n",
    "\n",
    "### Incorrect health states\n",
    "\n",
    "As soon as the FPGA is reset, the health state of _all_ subarrays switches to `DEGRADED`.\n",
    "This is likely caused by the fact that the P4 port connected to the FPGA is considered \"down\",\n",
    "causing the `LowCbfConnector` to report a `DEGRADED` health state.\n",
    "\n",
    "This then causes the subarrays to also report `DEGRADED`, as described in the\n",
    "[LOW CBF Health Monitoring documentation](https://developer.skao.int/projects/ska-low-cbf/en/0.9.0/health.html).\n",
    "To us, this seems like an overreaction: other subarrays not currently using the failing FSP should not be affected.\n",
    "\n",
    "### Processor TANGO device crashes\n",
    "\n",
    "As soon as the FPGA is reset, the corresponding `LowCbfProcessor` TANGO device crashes, and the Kubernetes Pod restarts.\n",
    "This causes the TANGO device to reset to its initial state, and since it is unable to reprogram the FPGA, it loses all state information.\n",
    "\n",
    "There is no official specification for what a TANGO device should do in this case.\n",
    "However, following the often used \"remote control\" analogy to describe TANGO devices,\n",
    "we would expect the `LowCbfProcessor` TANGO device to react to the FPGA going down by changing its state and/or firing off alarms, not by crashing.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ska-low-csp-test-fMbL-zyK-py3.10",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
