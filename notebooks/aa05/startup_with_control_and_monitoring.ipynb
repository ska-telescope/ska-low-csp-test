{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Startup with control and monitoring\n",
    "\n",
    "This notebook test is implemented based upon [Startup with control and monitoring](https://skaoffice.jamacloud.com/perspective.req?docId=1121575&projectId=335).\n",
    "\n",
    "References:\n",
    "- [LOW.CSP LMC Documentation](https://developer.skatelescope.org/projects/ska-csp-lmc-low/en/latest/lmc/low_csp_lmc.html)\n",
    "- [LOW.CSP LMC Tango Clients Examples](https://developer.skatelescope.org/projects/ska-csp-lmc-low/en/latest/example/example.html)\n",
    "- [CSP LMC commands for AA05](https://confluence.skatelescope.org/display/SE/CSP+LMC+commands+for+AA05)\n",
    "\n",
    "The LOW CSP release is deployed onto the Kubernetes (k8s) cluster as a software release with the underlying assumption that the CBF hardware is installed and available on the network.\n",
    "After deployment the LOW CSP and associated subsystems are DISABLED and the TMC has to set the LOW CSP to adminMode=ONLINE to establish communication and enable command and control of the system.\n",
    "\n",
    "Note: There is no direct ON command to LOW CSP. On the TMC command for adminMode=ONLINE controllers and subarrays for LOW CSP.LMC and LOW CBF will be in the ON state.\n",
    "The PST and PSS beams (when available) will remain in OFF state.\n",
    "\n",
    "The notebook will interrogate device states and report back attribute values as part of the verification output.    \n",
    "For visual inspection of device attributes the Taranta API interface is used.    \n",
    "You can access the interface via a web browser by pointing the URL to the appropriate namespace on your k8s cluster:\n",
    "\n",
    "    http://<k8s_CLUSTER>/<KUBE_NAMESPACE>/taranta/devices/low-csp/\n",
    "\n",
    "For example, to view the TANGO attributes for a deployment on the CLP, go to http://k8s.clp.skao.int/ska-low-csp-baseline/taranta/devices/low-csp/.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prerequisites\n",
    "\n",
    "- All necessary equipment are installed and verified\n",
    "- Assume a network is available and all equipment/systems are powered\n",
    "- P4 switch is configured in order to control CBF\n",
    "- Fresh deployment of LOW CSP on the k8s cluster"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notebook settings\n",
    "\n",
    "This section controls the settings used in this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Imports\n",
    "\n",
    "This section downloads and imports packages used in this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Looking in indexes: https://pypi.org/simple, https://artefact.skao.int/repository/pypi-internal/simple\n",
      "Requirement already satisfied: pytango in /srv/conda/envs/notebook/lib/python3.10/site-packages (9.5.0)\n",
      "Requirement already satisfied: ska-control-model in /srv/conda/envs/notebook/lib/python3.10/site-packages (0.3.4)\n",
      "Collecting colorama\n",
      "  Downloading colorama-0.4.6-py2.py3-none-any.whl (25 kB)\n",
      "Requirement already satisfied: numpy>=1.13.3 in /srv/conda/envs/notebook/lib/python3.10/site-packages (from pytango) (1.26.4)\n",
      "Requirement already satisfied: packaging in /srv/conda/envs/notebook/lib/python3.10/site-packages (from pytango) (23.2)\n",
      "Requirement already satisfied: psutil in /srv/conda/envs/notebook/lib/python3.10/site-packages (from pytango) (5.9.8)\n",
      "Requirement already satisfied: transitions<0.10.0,>=0.9.0 in /srv/conda/envs/notebook/lib/python3.10/site-packages (from ska-control-model) (0.9.0)\n",
      "Requirement already satisfied: six in /srv/conda/envs/notebook/lib/python3.10/site-packages (from transitions<0.10.0,>=0.9.0->ska-control-model) (1.16.0)\n",
      "Installing collected packages: colorama\n",
      "Successfully installed colorama-0.4.6\n",
      "Note: you may need to restart the kernel to use updated packages.\n"
     ]
    }
   ],
   "source": [
    "%pip install --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple pytango ska-control-model colorama"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tango\n",
    "from colorama import Fore\n",
    "from ska_control_model import AdminMode, HealthState, ObsState\n",
    "\n",
    "from ska_low_csp_test.synchronisation import wait_for_condition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Test environment\n",
    "\n",
    "These settings configure the notebook to point to the correct environment where the system under test is deployed.\n",
    "Make sure to update these settings to point to the correct TANGO database and Kubernetes cluster/namespace."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "KUBE_CLUSTER_DOMAIN = \"cluster.local\"\n",
    "KUBE_NAMESPACE = \"ska-low-csp-dev\"\n",
    "TANGO_DATABASEDS_NAME = \"ska-low-csp-databaseds\"\n",
    "TANGO_DATABASEDS_PORT = 10000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Test settings\n",
    "\n",
    "These settings configure the logic in the notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Choose the `AdminMode` that is used to start up the system under test. Possible values:\n",
    "- `AdminMode.ONLINE`\n",
    "- `AdminMode.ENGINEERING`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "ADMIN_MODE = AdminMode.ONLINE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notebook setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define helper functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def retrieve_device_proxies(device_class_name: str):\n",
    "    \"\"\"\n",
    "    Query the TANGO database for devices exported for the given device class name.\n",
    "    Returns TANGO device proxies for each exported device.\n",
    "    \"\"\"\n",
    "    tango_db = tango.Database(f\"{TANGO_DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.{KUBE_CLUSTER_DOMAIN}\", TANGO_DATABASEDS_PORT)\n",
    "    tango_host = f\"tango://{TANGO_DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.{KUBE_CLUSTER_DOMAIN}:{TANGO_DATABASEDS_PORT}\"\n",
    "    device_fqdns = tango_db.get_device_exported_for_class(device_class_name).value_string\n",
    "\n",
    "    print(device_class_name + \":\")\n",
    "    for fqdn in device_fqdns:\n",
    "        print(\"  -\", fqdn)\n",
    "        yield tango.DeviceProxy(f\"{tango_host}/{fqdn}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Coloured printing functions for strings that use universal ANSI escape sequences.\n",
    "# fail: bold red, pass: bold green\n",
    "\n",
    "\n",
    "def print_fail(message, start=\"\", end=\"\\n\"):\n",
    "    \"\"\"Print coloured fail message.\"\"\"\n",
    "    print(f\"{start}{Fore.RED}{message}{Fore.RESET}\", end=end)\n",
    "\n",
    "\n",
    "def print_pass(message, start=\"\", end=\"\\n\"):\n",
    "    \"\"\"Print coloured pass message.\"\"\"\n",
    "    print(f\"{start}{Fore.GREEN}{message}{Fore.RESET}\", end=end)\n",
    "\n",
    "\n",
    "def print_status(message: str, is_pass: bool, start=\"\", end=\"\\n\"):\n",
    "    \"\"\"Print coloured status message.\"\"\"\n",
    "    print_func = print_pass if is_pass else print_fail\n",
    "    print_func(message, start=start, end=end)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set up TANGO device proxies\n",
    "\n",
    "Sets up a connection to the TANGO database and queries it for the TANGO devices that are exported.\n",
    "\n",
    "The following device classes are expected to be present:\n",
    "- `LowCspController`\n",
    "- `LowCspSubarray`\n",
    "- `LowCbfController`\n",
    "- `LowCbfSubarray`\n",
    "- `LowCbfProcessor`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "LowCspController:\n",
      "  - low-csp/control/0\n",
      "LowCspSubarray:\n",
      "  - low-csp/subarray/01\n",
      "  - low-csp/subarray/02\n",
      "  - low-csp/subarray/03\n",
      "  - low-csp/subarray/04\n",
      "LowCbfController:\n",
      "  - low-cbf/control/0\n",
      "LowCbfProcessor:\n",
      "  - low-cbf/processor/0.0.0\n",
      "  - low-cbf/processor/0.0.1\n",
      "  - low-cbf/processor/0.0.2\n",
      "  - low-cbf/processor/0.0.3\n",
      "LowCbfSubarray:\n",
      "  - low-cbf/subarray/01\n",
      "  - low-cbf/subarray/02\n",
      "  - low-cbf/subarray/03\n",
      "  - low-cbf/subarray/04\n",
      "LowCbfAllocator:\n",
      "  - low-cbf/allocator/0\n"
     ]
    }
   ],
   "source": [
    "low_csp_controller = next(retrieve_device_proxies(\"LowCspController\"))\n",
    "low_csp_subarrays = list(retrieve_device_proxies(\"LowCspSubarray\"))\n",
    "\n",
    "low_cbf_controller = next(retrieve_device_proxies(\"LowCbfController\"))\n",
    "low_cbf_processors = list(retrieve_device_proxies(\"LowCbfProcessor\"))\n",
    "low_cbf_subarrays = list(retrieve_device_proxies(\"LowCbfSubarray\"))\n",
    "low_cbf_allocator = next(retrieve_device_proxies(\"LowCbfAllocator\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Verify current state of LOW-CSP TANGO devices\n",
    "\n",
    "After initial deployment, all LOW-CSP TANGO devices are expected to:\n",
    "\n",
    "- Have their `adminMode` set to `AdminMode.OFFLINE`\n",
    "- Be in the `DISABLE` state\n",
    "- Report their `healthState` attribute as `UNKNOWN`.\n",
    "\n",
    "**Note**:\n",
    "Should any of the `AdminMode.OFFLINE` assertions in the following cell result in \"FAILED\", please redeploy your system an run this notebook again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[32mInitial admin modes as expected\u001b[39m\n",
      "\u001b[32mInitial device states as expected\u001b[39m\n",
      "\u001b[32mInitial health states as expected\u001b[39m\n",
      "\u001b[32mInitial subarrays obsState is as expected\u001b[39m\n"
     ]
    }
   ],
   "source": [
    "for device in [low_csp_controller, low_cbf_controller, *low_csp_subarrays, *low_cbf_subarrays]:\n",
    "    assert (\n",
    "        device.adminMode == AdminMode.OFFLINE\n",
    "    ), f\"{device.name()}/adminMode is {device.adminMode.name}/{device.adminMode.value}\"\n",
    "\n",
    "for device in [*low_cbf_processors]:\n",
    "    assert (\n",
    "        device.adminMode == AdminMode.ONLINE\n",
    "    ), f\"{device.name()}/adminMode is {device.adminMode.name}/{device.adminMode.value}\"\n",
    "\n",
    "print_pass(\"Initial admin modes as expected\")\n",
    "\n",
    "for device in [low_csp_controller, low_cbf_controller, *low_csp_subarrays, *low_cbf_subarrays]:\n",
    "    assert device.state() == tango.DevState.DISABLE, f\"{device.name()}/state is {device.state()}\"\n",
    "\n",
    "for device in [*low_cbf_processors]:\n",
    "    assert device.state() == tango.DevState.ON, f\"{device.name()}/state is {device.state()}\"\n",
    "\n",
    "print_pass(\"Initial device states as expected\")\n",
    "\n",
    "for device in [low_csp_controller]:\n",
    "    assert (\n",
    "        device.healthState == HealthState.UNKNOWN\n",
    "    ), f\"{device.name()}/healthState is {device.healthState.name}/{device.healthState.value}\"\n",
    "\n",
    "print_pass(\"Initial health states as expected\")\n",
    "\n",
    "for device in [*low_csp_subarrays, *low_cbf_subarrays]:\n",
    "    assert device.obsState == ObsState.EMPTY, f\"{device.name()}/obsState is {device.obsState.name}/{device.obsState.value}\"\n",
    "\n",
    "print_pass(\"Initial subarrays obsState is as expected\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set `adminMode` on LOW-CSP controller\n",
    "\n",
    "Setting the `adminMode` on the LOW CSP.LMC controller should initialize the LOW-CSP system.\n",
    "The `adminMode` should automatically propagate to the LOW CSP.LMC subarrays, the LOW CBF controller and subarrays.\n",
    "\n",
    "The following devices are expected to automatically switch their state from `DISABLED` to `ON`:\n",
    "\n",
    "- LOW CSP.LMC controller\n",
    "- LOW CSP.LMC subarrays\n",
    "- LOW CBF controller\n",
    "- LOW CBF subarrays\n",
    "\n",
    "The device `healthState` attributes are expected to switch from `UNKNOWN` to `OK`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "low_csp_controller.adminMode = ADMIN_MODE\n",
    "wait_for_condition(lambda: low_csp_controller.isCommunicating, timeout_s=60)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Admin mode:\n",
      "\u001b[32m- low-csp/control/0/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-cbf/control/0/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/01/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/02/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/03/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/04/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/01/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/02/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/03/adminMode is ONLINE/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/04/adminMode is ONLINE/0\u001b[39m\n",
      "State:\n",
      "\u001b[32m- low-csp/control/0/state is ON\u001b[39m\n",
      "\u001b[32m- low-cbf/control/0/state is ON\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/01/state is ON\u001b[39m\n",
      "\u001b[31m- low-csp/subarray/02/state is DISABLE\u001b[39m\n",
      "\u001b[31m- low-csp/subarray/03/state is DISABLE\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/04/state is ON\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/01/state is ON\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/02/state is ON\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/03/state is ON\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/04/state is ON\u001b[39m\n",
      "Health state:\n",
      "\u001b[31m- low-csp/control/0/healthState is DEGRADED/1\u001b[39m\n",
      "\u001b[32m- low-cbf/control/0/healthState is OK/0\u001b[39m\n",
      "\u001b[31m- low-cbf/allocator/0/healthState is UNKNOWN/3\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/01/healthState is OK/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/02/healthState is OK/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/03/healthState is OK/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/04/healthState is OK/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/01/healthState is OK/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/02/healthState is OK/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/03/healthState is OK/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/04/healthState is OK/0\u001b[39m\n",
      "Subarrays obsState:\n",
      "\u001b[32m- low-csp/subarray/01/obsState is EMPTY/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/02/obsState is EMPTY/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/03/obsState is EMPTY/0\u001b[39m\n",
      "\u001b[32m- low-csp/subarray/04/obsState is EMPTY/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/01/obsState is EMPTY/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/02/obsState is EMPTY/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/03/obsState is EMPTY/0\u001b[39m\n",
      "\u001b[32m- low-cbf/subarray/04/obsState is EMPTY/0\u001b[39m\n"
     ]
    }
   ],
   "source": [
    "print(\"Admin mode:\")\n",
    "for device in [low_csp_controller, low_cbf_controller, *low_csp_subarrays, *low_cbf_subarrays]:\n",
    "    print_status(\n",
    "        f\"- {device.name()}/adminMode is {device.adminMode.name}/{device.adminMode.value}\",\n",
    "        is_pass=device.adminMode == ADMIN_MODE,\n",
    "    )\n",
    "\n",
    "print(\"State:\")\n",
    "for device in [low_csp_controller, low_cbf_controller, *low_csp_subarrays, *low_cbf_subarrays]:\n",
    "    print_status(f\"- {device.name()}/state is {device.state()}\", is_pass=device.state() == tango.DevState.ON)\n",
    "\n",
    "\n",
    "print(\"Health state:\")\n",
    "for device in [low_csp_controller, low_cbf_controller, low_cbf_allocator, *low_csp_subarrays, *low_cbf_subarrays]:\n",
    "    print_status(\n",
    "        f\"- {device.name()}/healthState is {device.healthState.name}/{device.healthState.value}\",\n",
    "        is_pass=device.healthState == HealthState.OK,\n",
    "    )\n",
    "\n",
    "print(\"Subarrays obsState:\")\n",
    "for device in [*low_csp_subarrays, *low_cbf_subarrays]:\n",
    "    print_status(\n",
    "        f\"- {device.name()}/obsState is {device.obsState.name}/{device.obsState.value}\",\n",
    "        is_pass=device.obsState == ObsState.EMPTY,\n",
    "    )"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
